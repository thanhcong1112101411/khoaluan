// ------------------ socket io ---------------------------
import io from 'socket.io-client';

export const baseUrl = "http://localhost:8200";
export const ImageProductLink = baseUrl + "/public/assets/products/";
export const ImageBannerLink = baseUrl + "/public/assets/banner/";
export const ImageBannerCompanyLink = baseUrl + "/images/bannerCompany/";
export const ImageNewsCompanyLink = baseUrl + "/images/newsImage/";
export const ImageLogoLink = baseUrl + "/public/assets/logo/";
export const ImagesLink = baseUrl + "/images/images/";
export const ImageSliderLink = baseUrl + "/public/assets/slider/";

export const socket = io(baseUrl);

