
export const updateObject = (oldObject, updatedProperties) =>{
	return{
		...oldObject,
		...updatedProperties
	};
}
export const checkValidity = (value, rules) =>{
     let isValid = true;
     if (!rules) {
        return true;
     }
        
    if (rules.required) {
        if (typeof(value) == 'object'){
            isValid = ( Object.size(value) ||value.length > 0 ) && isValid;
        }else{
            isValid = value.trim() !== '' && isValid;
        }
    }

    if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
        const pattern = /^\d+$/;
        isValid = pattern.test(value) && isValid
    }

    return isValid;
}
export const formatName = (string) =>{
    return string.substr(0,34) + "...";
}
export const formatCurrency = (number) =>{
    var n = number;
    if(isNaN(n) == false){
        n = n.toString();
    }
    n = n.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");    
    return  n2.split('').reverse().join('');
}
export const formatDate = (date) => {
    const event = new Date(date);
    const vi = event.toLocaleString("en-US", {timeZone: "Asia/Bangkok"});

    let Parts = vi.split(",");
    let datePart = Parts[0].split("/");
    return Parts[1] + " - " + datePart[1] + "/" + datePart[0] + "/" + datePart[2];

    // let dateParts = date.split("-");
    // return dateParts[2].substr(3,8) + " " + dateParts[2].substr(0,2)+"/"+ dateParts[1] +"/"+dateParts[0];
}
export const formatDateYYMMDD = (date) => {
    let dateParts = date.split("-");
    return dateParts[0] + "-" + dateParts[1] +"-"+ dateParts[2].substr(0,2);
}
export const formatDateDDMMYY = (date) => {
    // let dateParts = date.split("-");
    // return dateParts[2].substr(0,2) + "-" + dateParts[1] + "-" + dateParts[0];

    const event = new Date(date);
    const vi = event.toLocaleString("en-US", {timeZone: "Asia/Bangkok"});

    let Parts = vi.split(",");
    let datePart = Parts[0].split("/");
    return datePart[1] + "-" + datePart[0] + "-" + datePart[2];
}
export const formatPhone = (phoneString) =>{
    if (!phoneString){
        return "";
    }
    phoneString = "0" + phoneString;
    var n = phoneString.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$& ");    
    return  n2.split('').reverse().join('');
}
export const getDateNow = () =>{
    let date = new Date(Date.now());
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    if(month.toString().length == 1){
        month = "0"+month;
    }
    if(day.toString().length == 1){
        day = "0"+day;
    }
    return (year+"-"+month+"-"+day);
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        size++;
    }
    return size;
};

