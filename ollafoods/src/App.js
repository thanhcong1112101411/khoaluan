import React, {Component} from 'react';
import { Route, Switch, withRouter, Redirect, Router } from 'react-router-dom';
import { connect } from 'react-redux';
import history from './history';

//--------------- suppost package --------------------------------
import 'bootstrap/dist/css/bootstrap.min.css';

//--------------- components -------------------------------------
import RouteItem from 'RouteItem/routeItem';
import Layout from 'Hoc/Layout/layout';

//------------------------------- USER -------------------------------
import Home from 'Containers/User/Home/home';
import Products from './Containers/User/Products/Products';
import ProductDetail from 'Containers/User/ProductDetail/productDetail';
import Cart from 'Containers/User/Cart/cart';
import News from 'Containers/User/News/news';
import NewDetail from 'Containers/User/NewDetail/newDetail';
import signIn from 'Containers/User/SignIn/signIn';
import SignUp from 'Containers/User/SignUp/signUp';
import ForgetAccount from 'Containers/User/ForgetAccount/forgetAccount';
import FeedBack from 'Containers/User/FeedBack/feedBack';
import ResetPassword from 'Containers/User/ResetPassword/resetPassword';
import Envoice from 'Containers/User/Envoice/envoice';
import EnvoiceDetail from 'Containers/User/EnvoiceDetail/envoiceDetail';
import Profile from 'Containers/User/Profile/profile';

//------------------------------- ADMIN -------------------------------
import Login from 'Containers/Admin/Login/login';
import Dashboard from 'Containers/Admin/Dashboard/dashboard';

//------------------------------- DEFAULT -----------------------------
import defaultPage from 'Containers/DefaultPage/defaultPage';
import Payment from 'Containers/User/Payment/payment';

class App extends Component{
    render(){

        const routesComponent = this.props.screens.map(ig => {
            return(
                <RouteItem key={ig.id} component = {ig.url} />
            )
        });
        return(
            <Layout>
                
                <Switch>
                    {/* ------------------------ DEFAULT ------------------------------------- */}
                    <Route path="/default-page" component = {defaultPage} />
                    
                    {/* ------------------------ USER ------------------------------------- */}
                    <Route path="/" exact component={Home} />
                    <Route path="/product-detail/:id" exact component = {ProductDetail} />
                    <Route path="/products" exact component = {Products} />
                    <Route path="/cart" exact component = {Cart} />
                    <Route path="/news" exact component = {News} />
                    <Route path="/news/:id" exact component = {NewDetail} />

                    {this.props.isUser ? <Route path="/feedbacks" exact component = {FeedBack} /> : ""}
                    {this.props.isUser ? <Route path="/envoices" exact component = {Envoice} /> : ""}
                    {this.props.isUser ? <Route path="/envoices/:id" exact component = {EnvoiceDetail} /> : ""}
                    {this.props.isUser ? <Route path="/profile" exact component = {Profile} /> : ""}
                    {this.props.isUser ? <Route path="/payment" exact component = {Payment} /> : ""}

                    {!this.props.isUser ? <Route path="/sign-in" exact component = {signIn} /> : ""}
                    {!this.props.isUser ? <Route path="/sign-up" exact component = {SignUp} /> : ""}
                    {!this.props.isUser ? <Route path="/reset-password" exact component = {ResetPassword} /> : ""}
                    {!this.props.isUser ? <Route path="/forget-account" exact component = {ForgetAccount} /> : ""}
                    
                    {/* ------------------------ ADMIN ------------------------------------- */}
                    <Route>
                        <Route path = "/admin" exact component={Dashboard} />
                        <Route path = "/admin/login" component={ Login } />
                        {routesComponent}

                    </Route>
                    
                    <Redirect to="/default-page" />
                    
                </Switch>
                
            </Layout>
        )
    }
}

const mapStateToProps = state =>{
    return{
        isAdmin: state.auth.uuid_admin != "",
        isUser: state.auth.uuid_user != "",
        screens: state.auth.screens,
    };
  };
  
export default withRouter(connect(mapStateToProps)(App));
