import React from 'react';
import {Link } from 'react-router-dom';

import './buttonLink.css';

const buttonLink = (props) =>{
    return(
        <Link 
            to={props.to} 
            className = "buttonLinkStyle"
        >{props.title}</Link>
    )
}
export default buttonLink;