import React from 'react';

import './bannerTemplate1.css';
import style from '../../../../../Shared/style.css';

const bannerTemplate1 = (props) => {
    const contentClasses = ["container", style.container,"content"];
    // if pageFrom is service, add "padding-top": 10%; else add "display: flex, alignItem: center"
    if (props.pageFrom == "service") {
        contentClasses.push("servicePrivate");
    }else{
        contentClasses.push("restPrivate");
    }
    return (
        <div className="bannerTemplate">
            <img width="100%" src={props.imageBanner}/>
            <div 
                className={contentClasses.join(" ")}
            >
                <div>
                    <h1>{props.title}</h1>
                </div>
            </div>
        </div>
    );
}

export default bannerTemplate1;