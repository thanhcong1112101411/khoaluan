import React from 'react';

import './titleAdmin.css';
import { Link } from 'react-router-dom';

const TitleAdmin = (props) =>{
    
    let part = null;
    if (props.type == "add"){
        part = (
            <Link to={props.url}>Thêm Mới</Link>
        )
    }

    return(

        <div className="titleAdmin">
            <h1>{props.children}</h1>
            {part}
        </div>
    );
}

export default TitleAdmin;