import React, { Component } from 'react';
// ------------------------- CSS ------------------------
import './loading.css';

//-------------------------- COMPONENTS ----------------
import LoadingImage from 'public/images/loading.gif';
import BackDrop from '../BackDrop/backDrop';

const loading = (props) => {

        return (
            <div className = {props.show ? "": "visible"}>
                <BackDrop 
                    show = {props.show}
                />
                <div className = "loading">
                    <img width = "20%" src = {LoadingImage} />
                    <p>Đang phân tích dữ liệu</p>
                </div>
            </div>
        );
    
}

export default loading;