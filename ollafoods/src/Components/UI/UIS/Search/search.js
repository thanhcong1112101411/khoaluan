import React, { Component } from 'react';

import './search.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
// changeSearchValue, holder, value
class Search extends Component {
    render() {
        return (
            <div className = "Search">
                <div>
                    <input 
                        type="text" 
                        placeholder={this.props.holder} 
                        value = {this.props.value}
                        onChange = {this.props.changed} />
                    <span>
                        <FontAwesomeIcon icon={faSearch} />
                    </span>
                </div>
            </div>
        );
    }
}

export default Search;