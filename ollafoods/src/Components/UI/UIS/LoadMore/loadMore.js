import React from 'react';

// ----------------------------- CSS ----------------------//
import './loadMore.css';


const LoadMore = (props) => {
    return(
        <div className = "loadMore">
            <button
                disabled = {props.load_more}
                onClick = {props.clicked}
            >
                Xem Thêm
            </button>
        </div>
    );
}

export default LoadMore;