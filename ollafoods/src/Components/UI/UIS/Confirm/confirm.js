import React from 'react';

import './confirm.css';

import BackDrop from '../BackDrop/backDrop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
const confirm = (props) =>{
    return(
        <div className = {props.show ? "": "visible"}>
            <BackDrop 
                show = {props.show} 
                clicked = {props.onCancelClick}
            />
            <div className = "confirmStyle">
                <div className = "icon">
                    <span className = "iconStyle"><FontAwesomeIcon icon = {faExclamation} /></span>
                    <span className = "title">Xác Nhận!</span>
                </div>
                <p className = "content">{props.content}</p>
                <div>
                    <button 
                        className = "confirmBtn"
                        onClick = {props.onConfirmClick}
                    >Xác Nhận</button>
                    <button 
                        className = "cancelBtn"
                        onClick = {props.onCancelClick}
                    >Hủy Bỏ</button>
                </div>
            </div>
        </div>
    );
}

export default confirm;