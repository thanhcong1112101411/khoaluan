import React from 'react';

import './buttonFirst.css';
const buttonFirst = (props) =>{
    return(
        <div className = "buttonFirstStyle" >
            <button
                onClick = {props.clicked}
            >{props.title}</button>
        </div>
    );
}

export default buttonFirst;