import React from 'react';

import './heading.css';

const heading = (props) =>{
    return(
        <div className ="headingHome">
            <h2>{props.children}</h2>
        </div>
    );
}

export default heading;