import React, {Component} from 'react';

import './pagination.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleLeft, faAngleDoubleRight, faReply, faShare } from '@fortawesome/free-solid-svg-icons';

class Pagination extends Component {

    state = {
        showPage: 3,
    }

    onClickStepPageLeft = () => {
        if (this.props.currentPage != 1){
            this.props.nextPage(this.props.currentPage - 1);
        }
    }

    onClickStepPageRight = () => {
        if (this.props.currentPage != this.props.pages){
            this.props.nextPage(this.props.currentPage + 1);
        }
    }

    render() {
        const pageLinks = [];

        if (this.props.currentPage != 1 && this.props.pages > this.state.showPage){
            pageLinks.push(
                <li className = "paginationItem vv">
                    <button>
                        ...
                    </button>
                </li>
            );
        }

        if (this.props.pages <= this.state.showPage){
            for(let i = 1; i <= this.props.pages; i++){
                let stylePage = ["paginationItem"];
                if(this.props.currentPage == i){
                    stylePage.push("active");
                }
    
                pageLinks.push(
                    <li className = {stylePage.join(" ")} key={i} onClick = {()=>this.props.nextPage(i)}>
                        <button>{i}</button>
                    </li>
                );
            }
        }else{
            const lastPage = this.props.currentPage + this.state.showPage -1;
            if (lastPage >= this.props.pages){
                const start = this.props.pages - this.state.showPage + 1;
                for(let i = start; i <= this.props.pages; i++){
                    let stylePage = ["paginationItem"];
                    
                    if(this.props.currentPage == i){
                        stylePage.push("active");
                    }
        
                    pageLinks.push(
                        <li className = {stylePage.join(" ")} key={i} onClick = {()=>this.props.nextPage(i)}>
                            <button>{i}</button>
                        </li>
                    );
                }
            }else{
                for(let i = this.props.currentPage; i <= lastPage; i++){
                    let stylePage = ["paginationItem"];
                    if(this.props.currentPage == i){
                        stylePage.push("active");
                    }
        
                    pageLinks.push(
                        <li className = {stylePage.join(" ")} key={i} onClick = {()=>this.props.nextPage(i)}>
                            <button>{i}</button>
                        </li>
                    );
                }
            }
        }

        if (this.props.currentPage + this.state.showPage - 1 < this.props.pages){
            pageLinks.push(
                <li className = "paginationItem vv">
                    <button>
                        ...
                    </button>
                </li>
            );
        }

        return (
            <div className ="paginationStyle" >
                <ul>
                    {
                        this.props.pages > this.state.showPage ?
                        <li className = "paginationItem">
                            <button
                                onClick = {()=>this.props.nextPage(1)}
                            >
                                <FontAwesomeIcon icon = {faReply} />
                            </button>
                        </li>
                        : ""
                    }

                    {
                        this.props.pages > this.state.showPage ?
                        <li className = "paginationItem">
                            <button
                                onClick = {this.onClickStepPageLeft}
                            >
                                <FontAwesomeIcon icon = {faAngleDoubleLeft} />
                            </button>
                        </li>
                        : ""
                    }
                    
                    {pageLinks}

                    {
                        this.props.pages > this.state.showPage ?
                        <li className = "paginationItem">
                            <button
                                onClick = {this.onClickStepPageRight}
                            >
                                <FontAwesomeIcon icon = {faAngleDoubleRight} />
                            </button>
                        </li>
                        : ""
                    }
                    
                    {
                        this.props.pages > this.state.showPage ?
                        <li className = "paginationItem">
                            <button
                                onClick = {()=>this.props.nextPage(this.props.pages)}
                            >
                                <FontAwesomeIcon icon = {faShare} />
                            </button>
                        </li>
                        : ""
                    }

                </ul>
            </div>
        );
    }
}

export default Pagination;