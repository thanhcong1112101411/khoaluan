import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

// -------------------- CSS ------------------------
import 'Shared/style.css';
import './titleImage.css';

const titleImage = (props) => {
    return(
        <div className="titleImage">
            <div>
                <h2>{props.children}</h2>
                <button
                    onClick = {props.clicked}
                >
                    <FontAwesomeIcon icon={faPlus} />
                </button>
            </div>
            <hr/>
        </div>
    )
}

export default titleImage;