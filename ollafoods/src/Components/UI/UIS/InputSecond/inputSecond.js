import React from 'react';
import CKEditor from 'ckeditor4-react';
import './inputSecond.css';

const ReactTelInput = require('react-telephone-input');

const InputSecond = ( props ) => {
    let inputElement = null;
    const inputClasses = ["InputElementDesign"];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push("Invalid");
    }

    switch ( props.elementType ) {
        case ( 'input' ):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <input
                        disabled = {props.elementConfig.disabled}
                        type = {props.elementConfig.type}
                        className={inputClasses.join(' ')}
                        {...props.elementConfig}
                        value={props.value}
                        onChange={props.changed} />
                </div>
                );
            break;
        case ( 'inputFile' ):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <input
                        type = {props.elementConfig.type}
                        className={inputClasses.join(' ')}
                        {...props.elementConfig}
                        onChange={props.changed} />
                </div>
                );
            break;
        case ( 'textarea' ):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <textarea
                        className={inputClasses.join(' ')}
                        {...props.elementConfig}
                        value={props.value}
                        rows = {props.elementConfig.rows}
                        onChange={props.changed} />
                </div>
            );
            break;
        case ( 'select' ):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <select
                        className={inputClasses.join(' ')}
                        value={props.value}
                        onChange={props.changed}>
                        {props.elementConfig.options.map(option => (
                            <option 
                                key={option.value} 
                                value = {option.value}
                                >
                                    {option.displayValue}
                            </option>
                        ))}
                    </select>
                </div>
            );
            break;
        
        case ( 'selectDefault' ):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    
                    <select
                        className={inputClasses.join(' ')}
                        value={props.value}
                        onChange={props.changed}>
                        {props.elementConfig.options.map(option => (
                            <option key={option.value} value={option.value}>
                                {option.displayValue}
                            </option>
                        ))}
                    </select>
                </div>
            );
            break;
        case ('checkbox'):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <div className = "list">
                        {props.elementConfig.options.map(option => (
                            <div key = {option.id}>
                                <input
                                    type = {props.elementConfig.type}
                                    onChange={props.changed} 
                                    id = {option.id}
                                    value = {option.id}
                                    checked = {
                                        props.value.findIndex(item => {
                                            return item === option.id
                                        }) > -1
                                    }
                                    
                                />
                                <label htmlFor={option.id} >{option.name}</label>
                            </div>
                            
                        ))}
                    
                    </div>
                </div>
            );
            break;
        case ('radio'):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <div className = "list">
                        {props.elementConfig.options.map(option => (
                            <div key = {option.id}>
                                <input
                                    type = {props.elementConfig.type}
                                    onChange={props.changed} 
                                    id = {option.id}
                                    value = {option.id}
                                    checked = {
                                        props.value == option.id
                                    }
                                />
                                <label htmlFor={option.id} >{option.name}</label>
                            </div>
                        ))}
                    </div>
                </div>
            );
            break;
        case ('ckeditor'):
            inputElement = (
                <div className = "element">
                    <p>{props.elementConfig.placeholder}<span className="note">{props.elementConfig.note}</span></p>
                    <CKEditor
                        onBeforeLoad={ ( CKEDITOR ) => ( CKEDITOR.disableAutoInline = true ) }
                        data={props.value}
                        type={props.elementConfig.type}
                        onChange={props.changed}
                    />
                </div>
            );
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }
    let displayStyle = [];
    if(props.elementType === 'ckeditor'){
        displayStyle.push("ckeditor");
    }
    return (
        <div className = {displayStyle.join(" ")}>
            {inputElement}
        </div>
    );

};

export default InputSecond;