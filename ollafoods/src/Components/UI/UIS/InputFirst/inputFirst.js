import React from 'react';

import classes from './inputFirst.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faLock, faPhone, faEnvelope,faMapMarkedAlt, faClipboardCheck } from '@fortawesome/free-solid-svg-icons';

const input = ( props ) => {
    let inputElement = null;
    const inputClasses = ["InputElement"];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push("Invalid");
    }

    switch ( props.elementType ) {
        case ( 'input' ):
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case ( 'textarea' ):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case ( 'select' ):
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    value={props.value}
                    onChange={props.changed}>
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>
            );
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }
    let iconElement = null;
    switch(props.icon){
        case ('user'):
            iconElement = <FontAwesomeIcon icon = {faUser} />;
            break;
        case ('password'):
            iconElement = <FontAwesomeIcon icon = {faLock} />;
            break;
        case ('phone'):
            iconElement = <FontAwesomeIcon icon = {faPhone} />;
            break;
        case ('address'):
            iconElement = <FontAwesomeIcon icon = {faMapMarkedAlt} />;
            break;
        case ('email'):
            iconElement = <FontAwesomeIcon icon = {faEnvelope} />;
            break;
        case ('list'):
            iconElement = <FontAwesomeIcon icon = {faClipboardCheck} />;
            break;
        default:
            iconElement = <FontAwesomeIcon icon = {faUser} />;
            break;
    }
    
    return (
        <div className = "Input">
            <span>{iconElement}</span>
            {inputElement}
        </div>
    );

};

export default input;