import React from 'react';

import './alertFirst.css';

import BackDrop from '../BackDrop/backDrop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
const alert = (props) =>{

    let icon = null;
    if (props.data.varient == 'success'){
        icon = (
            <div className = "icon iconSuccess">
                <span className="iconStyle"><FontAwesomeIcon icon = {faCheckCircle} /></span>
                <span className="title">Thành Công!</span>
            </div>
        )
    } else if (props.data.varient == 'danger'){
        icon = (
            <div className = "icon iconDanger">
                <span className="iconStyle"><FontAwesomeIcon icon = {faExclamationTriangle} /></span>
                <span className="title">Lỗi!</span>
            </div>
        )
    }

    return(
        <div className = {props.show ? "": "visible"}>
            <BackDrop 
                show = {props.show} 
                clicked = {props.clicked}
            />
            <div className = "alertStyle">
                {icon}
                <p>{props.data.content}</p>
                <div>
                    <button 
                        className = "confirmBtn"
                        onClick = {props.clicked}
                    >Xác Nhận</button>
                </div>
            </div>
        </div>
    );
}

export default alert;