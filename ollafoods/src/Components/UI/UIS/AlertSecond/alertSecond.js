import React, {Component} from 'react';

import './alertSecond.css';

class alert extends Component{
    render(){
        const arrayStyle = ["alertSeacond"];
        if(!this.props.show){
            arrayStyle.push("visible");
            arrayStyle.push("moveOut");
            
        }else{
            arrayStyle.push("moveIn");
        }
        return(
            <div className={arrayStyle.join(" ")}>
                <p>{this.props.title}</p>
            </div>
        );
    }
}
export default alert;