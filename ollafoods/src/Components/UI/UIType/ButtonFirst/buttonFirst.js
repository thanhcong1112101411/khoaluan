import React, { Component } from 'react';
import './buttonFirst.css';
// type, clicked, disabled, align
class buttonFirst extends Component {
    state = {  }
    render() {
        let style = [buttonFirst];
        if(this.props.type == "main"){
            style.push("main");
        }else if(this.props.type == "success"){
            style.push("success");
        }else if(this.props.type == "danger"){
            style.push("danger");
        }else if(this.props.type == "primary"){
            style.push("primary");
        }
        return (
            <div style={{textAlign: this.props.align}}>
                <button
                    className = {style.join(" ")}
                    onClick = {this.props.clicked}
                    disabled = {this.props.disabled}
                >
                    {this.props.children}
                </button>
            </div>
        );
    }
}

export default buttonFirst;