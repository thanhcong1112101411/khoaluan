import React from 'react';

import './linkTable.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEye, faBan,faPenAlt,faEyeSlash, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

const linkTable = ( props ) => {
    let inputElement = null;

    switch ( props.type ) {
        case ( 'plus' ):
            inputElement = (
                <Link
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faPlus} />
                </Link>
                );
            break;
        case ( 'eye' ):
            inputElement = (
                <Link 
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faEye} />
                </Link>
                );
            break;
        case ( 'pen' ):
            inputElement = (
                <Link 
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faPenAlt} />
                </Link>
                );
            break;
        case ( 'ban' ):
            inputElement = (
                <Link 
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faBan} />
                </Link>
                );
            break;
        case ( 'eyeslash' ):
            inputElement = (
                <Link 
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faEyeSlash} />
                </Link>
                );
            break;
        case ( 'trash' ):
            inputElement = (
                <Link 
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faTrash} />
                </Link>
                );
            break;
        default:
            inputElement = (
                <Link 
                    className = "buttonTableStyle"
                    title = {props.title}
                    to = {props.url}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faPlus} />
                </Link>
                );
    }

    return inputElement;

};

export default linkTable;