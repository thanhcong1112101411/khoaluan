import React from 'react';

import './buttonTable.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEye, faBan,faPenAlt,faEyeSlash, faTrash, faLock, faLockOpen, faCheck } from '@fortawesome/free-solid-svg-icons';

const buttonTable = ( props ) => {
    let inputElement = null;

    switch ( props.type ) {
        case ( 'plus' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faPlus} />
                </button>
                );
            break;
        case ( 'eye' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faEye} />
                </button>
                );
            break;
        case ( 'pen' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faPenAlt} />
                </button>
                );
            break;
        case ( 'ban' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faBan} />
                </button>
                );
            break;
        case ( 'eyeslash' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faEyeSlash} />
                </button>
                );
            break;
        case ( 'trash' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faTrash} />
                </button>
                );
            break;
        case ( 'lock' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faLock} />
                </button>
                );
            break;
        case ( 'lockopen' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faLockOpen} />
                </button>
                );
            break;
        case ( 'check' ):
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faCheck} />
                </button>
                );
            break;

        default:
            inputElement = (
                <button 
                    className = "buttonTableStyle"
                    title = {props.title}
                    onClick = {props.clicked}
                    style={{color: props.color}}
                >
                    <FontAwesomeIcon icon = {faPlus} />
                </button>
                );
    }

    return inputElement;

};

export default buttonTable;