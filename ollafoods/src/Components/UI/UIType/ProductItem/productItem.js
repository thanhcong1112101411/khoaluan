import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './productItem.css';
import 'Shared/style.css';
import {formatCurrency, formatName} from 'Shared/utility';
import { ImageProductLink, baseUrl } from 'Shared/server';

//------------------------------ COMPONENTS --------------------------

class ProductItem extends Component {
    
    onAddProductToCartHandler = (event) =>{
        event.preventDefault();
        this.props.onAddProductToCartClicked(this.props.id);
    }
    render() {
        let exportPrice = this.props.exportPrice;
        if (this.props.discountAmount){
            exportPrice = exportPrice * (1 - this.props.discountAmount / 100);
        }
        return (
            <div className = "productItem">
                <Link to={"/product-detail/"+this.props.id}>
                    <div className="productImage">
                        <img width="100%" src={baseUrl + "/" + this.props.images.url} />
                    </div>
                    <div className="productContent">
                        <h1 className="h3">{this.props.name}</h1>
                        <div className = "priceStyle">
                            <p>{formatCurrency(exportPrice)} đ</p>
                            {
                                this.props.discountAmount ?
                                <span className = "discountPrice">{formatCurrency(this.props.exportPrice)} đ</span>
                                : ""
                            }
                        </div>
                    </div>
                    {
                        this.props.discountAmount ?
                        <div className = "discount">
                            <span>{"-" + this.props.discountAmount + "%"}</span>
                        </div>
                        : ""
                    }
                    
                </Link>
            </div>
            
        );
    }
}

export default ProductItem;