import React, { Component } from 'react';

//--------------------------------- COMPONENTS ----------------------------------
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import { AccountDefault } from 'Utils/enum.utils';

class decentralizationItem extends Component {
    onUpdateHandler = (event, id, uuid) => {
        event.preventDefault();
        this.props.toggleUpdateModel();
        this.props.setUsingId(id, uuid);
    }
    render() {
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.name}</td>
                <td>{this.props.email}</td>
                <td>{this.props.role}</td>
                {(this.props.id == AccountDefault)}
                <td>
                    <ButtonTable
                        type="pen"
                        title="Chọn nhóm quyền"
                        clicked = {(event) => this.onUpdateHandler(event, this.props.id, this.props.uuid)}
                    />
                </td>
            </tr>
        );
    }
}

export default decentralizationItem;