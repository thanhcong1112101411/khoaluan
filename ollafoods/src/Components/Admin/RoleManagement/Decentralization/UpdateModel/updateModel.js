import React, { Component } from 'react';

// --------------------------- CSS ---------------------------------
import './updateModel.css';
import 'Shared/style.css';

//---------------------------- COMPONENT ---------------------------
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import {updateObject, checkValidity} from 'Shared/utility';

class updateModel extends Component {
    state = {
        controls:{
            roleGroup: {
                elementType: 'radio',
                elementConfig: {
                    type: 'radio',
                    options: [],
                    placeholder: ""
                },
                value: 1,
                validation: {
                    required: true,
                },
                valid: true
            },

		},
        formIsValid: false,
    }

    componentWillReceiveProps(nextProps){
        let listRoleGroup = nextProps.listRoleGroup;

        if (listRoleGroup && listRoleGroup.length > 0){
            let controls = this.state.controls;
            controls.roleGroup.elementConfig.options = listRoleGroup;

            this.setState({controls: controls});
        }

        const using_id = nextProps.using_id;
        if (using_id){
            let controls = this.state.controls;
            controls.roleGroup.value = using_id;

            this.setState({controls: controls});
            this.setState({formIsValid: false});
        }

    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = event.target.value;

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) => {
        event.preventDefault();
        this.props.onChangeRoleGroup(this.state.controls.roleGroup.value, this.props.using_uuid, this.props.token);
        this.props.setStateIsLoad();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Cập nhật</Button>
            </form>
        );

        return (
            <div className = {this.props.show ? "": "visible"}>
                <BackDrop 
                    show = {this.props.show} 
                    clicked = {this.props.toggleUpdateModel}
                />
                <div className = "decentStyle" >
                    <h1>Lựa chọn nhóm quyền</h1>
                    <hr/>
                    {form}
                </div>
            </div>
        );
    }
}

export default updateModel;