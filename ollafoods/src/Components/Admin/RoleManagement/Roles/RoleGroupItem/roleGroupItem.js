import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';

class roleGroupItem extends Component {
    state = {  }

    onDeleteHandler = (event) => {
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }

    render() {

        let screenList = null;

        if(this.props.id == RoleDefault.ROOT){
            screenList = (
                <div>Toàn quền</div>
            )
        } else if (this.props.id == RoleDefault.NONE){
            screenList = (
                <div>Không có quyền quản trị</div>
            )
        }else{
            screenList = this.props.screens.map(ig => {
                return(
                    <p key = {ig.id} >{ig.name}</p>
                )
            })
        }
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.name}</td>
                
                <td className="privilege">
                    {screenList}
                </td>
                
                <td>{formatDate(this.props.created_at)}</td>
                    {
                        ( this.props.id != RoleDefault.ROOT && this.props.id != RoleDefault.NONE ) ? 
                        <td>
                            <ButtonTable
                                type="trash"
                                title="Xóa nhóm quyền"
                                clicked = {(event) => this.onDeleteHandler(event)}
                            />
                            <LinkTable
                                type="pen"
                                title="Chỉnh sửa nhóm quyền"
                                url = {this.props.url}
                            />
                        </td>
                        : <td><span style = {{color: "red"}}>Mặc định</span></td>
                    }
            </tr>
        );
    }
}

export default roleGroupItem;