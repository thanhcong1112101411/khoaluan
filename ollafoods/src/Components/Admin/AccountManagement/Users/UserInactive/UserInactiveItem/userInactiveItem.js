import React, { Component } from 'react';
import { formatDate, formatPhone } from 'Shared/utility';

//--------------------------------- COMPONENTS ----------------------------------
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';


class userInactiveItem extends Component {
    state = {  }

    onOpenHandler = (event) => {
        event.preventDefault();
        this.props.setUsingUuid(this.props.uuid);
        this.props.toggleConfirmSetStatusOpen();
    }

    render() {
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.name}</td>
                <td>{this.props.email}</td>
                <td>{formatPhone(this.props.phone)}</td>
                <td>{formatDate(this.props.created_at)}</td>
                <td>
                    <ButtonTable
                        type="lockopen"
                        title="mở khóa tài khoản"
                        clicked = {(event) => this.onOpenHandler(event)}
                    />
                    <LinkTable
                        type="pen"
                        title="Chỉnh sửa thông tin"
                        url = {this.props.update_url}
                    />
                    <LinkTable
                        type="eye"
                        title="Xem chi tiết"
                        url = {this.props.detail_url}
                    />
                </td>
            </tr>
        );
    }
}

export default userInactiveItem;