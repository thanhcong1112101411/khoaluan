import React, { Component } from 'react';

// ------------------------------- CSS -------------------------------------
import 'Shared/style.css';

// ------------------------------ COMPONENTS -------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Search from 'Components/UI/UIS/Search/search';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import Item from './AccountActiveItem/accountActiveItem';
import { socket } from 'Shared/server';


class accountActive extends Component {
    state = {
        searchValue : '',
        current_page : 1,
    }

    componentDidMount(){
        this.props.onFetchAccount(
            this.state.searchValue, 
            this.state.searchValue, 
            this.state.current_page,
            this.props.token
        );

        // socket io
        socket.on("server-send-fetch-account", () => {
            this.props.onFetchAccount(
                this.state.searchValue, 
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })

        socket.on("server-send-change-role-group", () => {
            this.props.onFetchAccount(
                this.state.searchValue, 
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchAccount(this.state.searchValue, this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchAccount(value, value, 1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    key = {ig.uuid}
                    uuid = {ig.uuid}
                    index = {index + 1}
                    name = {ig.first_name}
                    email = {ig.email}
                    phone = {ig.phone}
                    created_at = {ig.created_at}
                    role = {ig.role.name}

                    // url
                    detail_url = {this.props.pathName + "/detail/" + ig.uuid}
                    update_url = {this.props.pathName + "/update/" + ig.uuid}

                    //event
                    toggleConfirmSetStatusBlock = {this.props.toggleConfirmSetStatusBlock}
                    setUsingUuid = {this.props.setUsingUuid}
                />  
            )
        })

        return (
            <Auxx>
                <Search
                    holder = "Tên người dùng, email người dùng"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Họ và tên</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Ngày tạo</th>
                            <th>Nhóm quyền</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    nextPage = {this.nextPage}
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                />
                
            </Auxx>
        );
    }
}

export default accountActive;