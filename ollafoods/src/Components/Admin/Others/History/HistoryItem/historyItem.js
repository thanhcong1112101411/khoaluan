import React, { Component } from 'react';

import { formatDate } from 'Shared/utility';

class historyItem extends Component {

    render() {
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.name}</td>
                <td>{this.props.table}</td>
                <td>{this.props.action}</td>
                <td>{formatDate(this.props.created_at)}</td>
            </tr>
        );
    }
}
export default historyItem;