import React from 'react';

import { formatDate } from 'Shared/utility';
import { Link } from 'react-router-dom';

const feedbackItem = (props) => {

    return (
        <tr>
            <td>{props.index}</td>
            <td>{props.title}</td>
            <td>{props.content}</td>
            <td>
                <Link to = {"/admin/users/detail/" + props.uuid}>
                    {props.name + " - " + props.email}
                </Link>
            </td>
            <td>{formatDate(props.created_at)}</td>
        </tr>
    );
}
export default feedbackItem;