import React, {Component} from 'react';

// ----------------------------- CSS ------------------------------------
import './uploadImageModel.css';
import 'Shared/style.css';

//---------------------------- COMPONENT ------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';

import {CopyToClipboard} from 'react-copy-to-clipboard';
import { baseUrl } from 'Shared/server';

class UploadImageModel extends Component{
    state = {
        value: '',
        copied: false,
        controls:{
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: 'File Ảnh'
                },
                value: [],
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
		},
        formIsValid: false,
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.image_upload.length > 0){
            this.setState({value: baseUrl + "/" + nextProps.image_upload});
        }
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = '';
        let valid = '';
        if (controlName == "images"){
            value = event.target.files[0];
            valid = event.target.value;
        }else if(controlName == "content"){
            value = event.editor.getData();
            valid = value;
        }
        else{
            value = event.target.value;
            valid = event.target.value;
        }

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(valid, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = async(event) =>{
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
        }
        this.props.onUploadImagesHandler(form);
    }


    render(){

        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Thêm</Button>
            </form>
        );

        return(
            <Auxx>
                <BackDrop
                    show = {this.props.show} 
                    clicked = {this.props.toggle}
                />
                {
                    this.props.show ?
                        <div className = "uploadImageModel">
                            <h1>Upload ảnh</h1>
                            <hr/>
                            <div className = "">
                                {form}
                            </div>
                            {
                                (this.state.value != '') ?
                                    <div className = "clipBoard">
                                        <input value={this.state.value}
                                            onChange={({target: {value}}) => this.setState({value, copied: false})} 
                                        />
                                        <CopyToClipboard text={this.state.value}
                                            onCopy={() => this.setState({copied: true})}>
                                            <button>Copy</button>
                                        </CopyToClipboard>
                                        {this.state.copied ? <span style={{color: 'red'}}>Copied.</span> : null}
                                    </div>
                                : ''
                            }
                            
                        </div>
                    :''
                }
                
                
            </Auxx>
        )
    }
}

export default UploadImageModel;
