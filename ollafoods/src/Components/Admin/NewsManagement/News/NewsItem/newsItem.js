import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';

class newsItem extends Component {

    onDeleteHandler = (event) => {
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }

    render() {
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>
                    <img width="150px"
                        src = {baseUrl + "/" + JSON.parse(this.props.image).name}
                        alt = {JSON.parse(this.props.image).alt}
                        title = {JSON.parse(this.props.image).title}
                    />
                </td>
                <td>{this.props.title}</td>
                <td>{this.props.url}</td>
                <td>{this.props.seen}</td>
                <td>{formatDate(this.props.created_at)}</td>
                <td>{formatDate(this.props.updated_at)}</td>
                <td>
                    <ButtonTable
                        type="trash"
                        title="Xóa tin tức"
                        clicked = {(event) => this.onDeleteHandler(event)}
                    />
                    <LinkTable
                        type="pen"
                        title="Chỉnh sửa tin tức"
                        url = {this.props.update_url}
                    />
                </td>
                
            </tr>
        );
    }
}

export default newsItem;