import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';



class NewItem extends Component {

    state = {  }

    onDeleteHandler=(event)=>{
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }

    render() {
        return (
            <tr>
                <td>{this.props.index}</td>
                <td><img src={baseUrl +"/"+ this.props.image} width="100px" /></td>
                <td>{this.props.title}</td>
                <td>{this.props.click}</td>
                <td>{formatDate(this.props.created_at)}</td>
                <td>
                    <ButtonTable
                        type="trash"
                        title="Xóa tin"
                        clicked = {(event) => this.onDeleteHandler(event)}
                    />
                    <LinkTable
                        type="pen"
                        title="Chỉnh sửa tin tức"
                        url = {this.props.url}
                    />
                </td>
            </tr>
        );
    }
}

export default NewItem;