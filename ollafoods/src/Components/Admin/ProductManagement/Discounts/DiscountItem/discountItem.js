import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate, formatDateDDMMYY } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';


class priceItem extends Component {
    state = {  }

    onDeleteHandler=(event)=>{
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }
    render() {
        return (
            <tr>
            <td>{this.props.index}</td>
            <td>{this.props.name}</td>
            <td>{formatDateDDMMYY(this.props.date_from)}</td>
            <td>{formatDateDDMMYY(this.props.date_to)}</td>
            <td>{this.props.amount}%</td>
            <td>
            <ButtonTable
                    type="trash"
                    title="Xóa giảm giá"
                    clicked = {(event) => this.onDeleteHandler(event)}
                />
                <LinkTable
                    type="eye"
                    title="Xem chi tiết"
                    url = {this.props.detail_url}
                />
            </td>
        </tr>
        );
    }
}

export default priceItem;