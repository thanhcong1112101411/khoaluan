import React, { Component } from 'react';


//------------------------ COMPONENT -----------------------
import {ImageProductLink, formatCurrency, formatDateDDMMYY} from 'Shared/utility';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import { baseUrl } from 'Shared/server';

class ProductList extends Component {

    render() {
        const productList = this.props.list.map((ig, index)=>{
            return(
                <tr key={ig.id}>
                    <td>{index + 1}</td>
                    <td>{ig.code}</td>
                    <td>
                        <img width="100px" src={baseUrl + "/" + JSON.parse(ig.images)[0].url} />
                    </td>
                    <td>{ig.name}</td>
                    <td>{formatDateDDMMYY(ig.created_at)}</td>
                </tr>      
            );
        })
        return (
            <div className = "envoiceProductStyle">
                <table className="table table-bordered mt-3 text-center">
                    <thead>
                        <tr style = {{color: "#f97c19", fontSize: "18px"}}>
                            <th>STT</th>
                            <th>Mã</th>
                            <th colSpan = "2">Sản phẩm</th>
                            <th>Ngày tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {productList}
                    </tbody>
                </table>
                
            </div>
                   
        );
    }
}

export default ProductList;