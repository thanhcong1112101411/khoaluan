import React, { Component } from 'react';

// ------------------------------- CSS -------------------------------------
import 'Shared/style.css';

// ------------------------------ COMPONENTS -------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Search from 'Components/UI/UIS/Search/search';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import Item from '../DiscountItem/discountItem';
import { socket } from 'Shared/server';

class discountWaiting extends Component {
    state = {
        searchValue : '',
        current_page : 1,
    }

    componentDidMount(){
        this.props.onFetchDiscounts(
            this.state.searchValue, 
            this.state.current_page,
            this.props.token,
        );

        // socket io
        socket.on("server-send-fetch-discounts", () => {
            this.props.onFetchDiscounts(
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchDiscounts(this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchDiscounts(value, 1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    key = {ig.id}
                    uuid = {ig.id}
                    index = {index + 1}
                    name = {ig.name}
                    date_from = {ig.date_from}
                    date_to = {ig.date_to}
                    amount = {ig.amount}

                    // url
                    detail_url = {this.props.pathName + "/detail/" + ig.id}
                    update_url = {this.props.pathName + "/update/" + ig.id}
                    
                    //event
                    toggleConfirmSetStatusOpen = {this.props.toggleConfirmSetStatusOpen}
                    setUsingUuid = {this.props.setUsingUuid}
                />  
            )
        })

        return (
            <Auxx>
                <Search
                    holder = "Tên giảm giá"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                        <th>STT</th>
                            <th>Tên giảm gía</th>
                            <th>Từ ngày</th>
                            <th>Đến ngày</th>
                            <th>Phần trăm giảm giá</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    nextPage = {this.nextPage}
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                />
            </Auxx>
        );
    }
}

export default discountWaiting;