import React, { Component } from 'react';

//------------------------------ CSS ------------------------
import './discountInfo.css';
//------------------------- COMPONENTS -----------------------
import Input from 'Components/UI/UIS/InputSecond/inputSecond';

class DiscountInfo extends Component {
    render() {
        let form = (
            <form>
                {this.props.formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.props.inputChangedHandler(event, formElement.id)} />
                ))}
            </form>
        );
        return (
            <div className = "infostyle">
                {form}
            </div>
        );
    }
}

export default DiscountInfo;