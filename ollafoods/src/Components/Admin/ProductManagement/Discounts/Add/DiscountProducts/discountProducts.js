import React, { Component } from 'react';

//---------------------------CSS ----------------------------
import './discountProducts.css';
//------------------------ COMPONENT -----------------------
import {ImageProductLink, formatCurrency} from 'Shared/utility';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import { baseUrl } from 'Shared/server';

class DiscountProducts extends Component {
    state = {  }

    deleteIconClicked = (event, id) =>{
        event.preventDefault();
        this.props.deleteProductPicking(id);
    }

    onAddProductClick = (event) => {
        event.preventDefault();
        this.props.toggleProductModel();
        this.props.onFetchProductsDiscountsHandler("", 1);
    }

    render() {
        const productList = this.props.productsPicking.map(ig=>{
            return(
                <tr key={ig.id}>
                    <td>{ig.code}</td>
                    <td>
                        <img width="100px" src={baseUrl + "/" + JSON.parse(ig.images)[0].url} />
                    </td>
                    <td>{ig.name}</td>
                    <td>{ig.unitName}</td>
                    <td>{formatCurrency(ig.importPrice)} đ</td>
                    <td>{formatCurrency(ig.exportPrice)} đ</td>
                    <td>
                        <ButtonTable
                            type="trash"
                            color ="red"
                            title="Xóa Sản Phẩm"
                            clicked = {(event) => this.deleteIconClicked(event, ig.id)}
                        />
                    </td>
                </tr>      
            );
        })
        return (
            <div className = "envoiceProductStyle">
                <Button
                    type="main"
                    align="right"
                    clicked = {this.onAddProductClick}
                >Thêm sản phẩm
                </Button>
                <table className="table table-bordered mt-3 text-center">
                    <thead>
                        <tr style = {{color: "#f97c19", fontSize: "18px"}}>
                            <th>Mã</th>
                            <th colSpan = "2">Sản phẩm</th>
                            <th>Đơn Vị</th>
                            <th>Giá nhập</th>
                            <th>Giá bán</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {productList}
                    </tbody>
                </table>
                {this.props.quantityProducts == 0 ?
                    <div style={{fontSize: "20px", marginBottom: "20px"}}>
                        Không có sản phẩm nào được chọn
                    </div> 
                    : ""
                }
                
            </div>
                   
        );
    }
}

export default DiscountProducts;