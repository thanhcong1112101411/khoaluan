import React, {Component} from 'react';

import './addProductModel.css';

import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEye, faBan,faPenAlt } from '@fortawesome/free-solid-svg-icons';
import {ImageProductLink, formatCurrency} from 'Shared/utility';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import { baseUrl } from 'Shared/server';

class AddProductModel extends Component{
    state = {
        currentPage: 1,
        searchValue: ""
    }
    // pagination, search
    componentDidMount(){
        this.props.onFetchProductsDiscountsHandler(this.state.searchValue, this.state.currentPage);
    }
    nextPage = (pageNumber) =>{
        this.setState({currentPage: pageNumber});
        this.props.onFetchProductsDiscountsHandler(this.state.searchValue,pageNumber);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchProductsDiscountsHandler(value,this.state.currentPage);
    }
    addIconClicked = (event, id) =>{
        event.preventDefault();
        this.props.addProductPicking(id);
        // this.props.onFetchProductsPickingDiscountsHandler();
    }
    render(){
        const productList = this.props.productsAdd.map(ig=>{
            let index = this.props.productsPicking.indexOf(ig.id);
            if (index == -1){
                return(
                    <tr key={ig.id}>
                        <td>{ig.code}</td>
                        <td>
                            <img width="100px" src={baseUrl + "/" + JSON.parse(ig.images)[0].url} />
                        </td>
                        <td>{ig.name}</td>
                        <td>{ig.unitName}</td>
                        <td>{formatCurrency(ig.importPrice)} đ</td>
                        <td>{formatCurrency(ig.exportPrice)} đ</td>
                        <td>
                            <ButtonTable
                                type="plus"
                                color ="green"
                                title="Thêm Sản Phẩm"
                                clicked = {(event) => this.addIconClicked(event, ig.id)}
                            />
                        </td>
                    </tr>
                );
            }
           
        })
        return(
            <div className = {this.props.show ? "": "visible"}>
                <BackDrop 
                    show = {this.props.show} 
                    clicked = {this.props.toggleProductModel}
                />
                <div className = "addModel">
                    <TitleAdmin>Lựa Chọn sản phẩm</TitleAdmin>
                    <hr/>
                    <Search
                        holder = "Tên sản phẩm, mã sản phẩm, tên danh mục"
                        value = {this.state.searchValue}
                        changed = {(event) => this.onChangeSearchValue(event)}
                    />
                    <table className="table table-bordered mt-3 text-center">
                        <thead>
                            <tr style = {{color: "#f97c19", fontSize: "18px"}}>
                                <th>Mã</th>
                                <th colSpan = "2">Sản phẩm</th>
                                <th>Đơn Vị</th>
                                <th>Giá nhập</th>
                                <th>Giá bán</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {productList}
                        </tbody>
                    </table>
                    
                    <Pagination 
                        pages = {this.props.pages}
                        currentPage = {this.state.currentPage}
                        nextPage = {this.nextPage}
                    />
                    <Button
                        align="right"
                        type="primary"
                        clicked = {this.props.toggleProductModel}
                    >Đóng</Button>
                </div>
            </div>
        );
    }
}
export default AddProductModel;