import React, { Component } from 'react';

import { baseUrl } from 'Shared/server';
import { formatCurrency, formatDate } from 'Shared/utility';


class item extends Component {
    
    render() {
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{formatCurrency(this.props.price) + " đ"}</td>
                <td>{formatDate(this.props.date_from)}</td>
            </tr>
        );
    }
}

export default item;