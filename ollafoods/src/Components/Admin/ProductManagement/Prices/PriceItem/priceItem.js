import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate, formatCurrency } from 'Shared/utility';
import {RoleDefault, ProductStatus} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';


class priceItem extends Component {
    state = {  }

    onDeleteHandler=(event)=>{
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }
    
    render() {
        let profit = "Chưa xác định";
        if (this.props.importPrice && this.props.exportPrice){
            profit = formatCurrency(this.props.exportPrice - this.props.importPrice) + " đ";
        }
        return (
            <tr>
            <td>{this.props.index}</td>
            <td>{this.props.code}</td>
            <td>
                <img width="100px" src={baseUrl + "/" + this.props.image.url} />
            </td>
            <td>{this.props.name}</td>
            {
                this.props.importPrice ?
                <td>{formatCurrency(this.props.importPrice) + " đ"}</td>
                :
                <td style = {{color: "red"}}>Chưa cập nhật</td>
            }
            {
                this.props.exportPrice ?
                <td>{formatCurrency(this.props.exportPrice) + " đ"}</td>
                :
                <td style = {{color: "red"}}>Chưa cập nhật</td>
            }
            <td>{profit}</td>
            {
                this.props.status == ProductStatus.ACTIVE ?
                <td style = {{color: "green"}}>Đang hoạt động</td>
                :
                <td style = {{color: "red"}}>Đã khóa</td>
            }
            
            <td>
                <LinkTable
                    type="eye"
                    title="Xem chi tiết"
                    url = {this.props.detail_url}
                />
                <LinkTable
                    type="pen"
                    title="Cập nhật giá"
                    url = {this.props.update_url}
                />
            </td>
        </tr>
        );
    }
}

export default priceItem;