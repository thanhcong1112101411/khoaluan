import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

//----------------------------- CSs --------------------------------------------
import 'Shared/style.css';

// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Item from "./Item/item";

class exportPrice extends Component {

    render() {
        
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    price = {ig.price}
                    date_from = {ig.date_from}
                />
            );
        })
        return (
            <Auxx>
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Giá</th><th>Từ ngày (Áp dụng)</th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
            </Auxx>
        );
    }
}
export default exportPrice;