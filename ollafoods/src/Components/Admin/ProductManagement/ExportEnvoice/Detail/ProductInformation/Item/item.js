import React, { Component } from 'react';
import { formatDate, formatPhone, formatCurrency, formatDateYYMMDD, formatDateDDMMYY } from 'Shared/utility';

//--------------------------------- COMPONENTS ----------------------------------
import { baseUrl } from 'Shared/server';


class Item extends Component {

    render() {
        let money = this.props.exportPrice;
        if (this.props.discountAmount){
            money = money * (1 - this.props.discountAmount / 100);
        }

        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.code}</td>
                <td>{this.props.name}</td>
                <td>
                    <img width="120px" src = {baseUrl + "/" + this.props.image.url} />
                </td>
                
                <td>{formatCurrency(this.props.exportPrice) + " đ"}</td>
                <td>{this.props.amount}</td>
                {
                    this.props.discountAmount ?
                    <td>{this.props.discountAmount + " %"}</td>
                    :
                    <td>Không áp dụng</td>
                }
                
                <td>{formatCurrency(this.props.money) + " đ"}</td>
                <td>{formatCurrency(this.props.utility) + " đ"}</td>
            </tr>
        );
    }
}

export default Item;