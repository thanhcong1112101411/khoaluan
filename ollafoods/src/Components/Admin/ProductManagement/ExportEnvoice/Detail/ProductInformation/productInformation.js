import React, { Component } from 'react';
import { formatCurrency } from 'Shared/utility';

// ------------------------- CSS ---------------------------
import 'Shared/style.css';

// ------------------------ COMPONENTS ---------------------
import Item from './Item/item';

class productInformation extends Component {

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    code = {ig.product ? ig.product.code : ""}
                    name = {ig.product ? ig.product.name : ""}
                    image = {ig.product ? JSON.parse(ig.product.images)[0] :null}
                    amount = {ig.amount}
                    exportPrice = {ig.exportPrice}
                    discountAmount = {ig.discount}
                    money = {ig.money}
                    utility = {ig.utility}
                />  
            )
        })

        return (
            <div className = "productInformation">
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã SP</th>
                            <th colSpan="2">Sản phẩm</th>
                            <th>Giá</th>
                            <th>Số lượng</th>
                            <th>Giảm giá</th>
                            <th>Thành tiền</th>
                            <th>Lợi nhuận</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default productInformation;