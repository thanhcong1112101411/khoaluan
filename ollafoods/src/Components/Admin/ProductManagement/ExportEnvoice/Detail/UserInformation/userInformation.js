import React, { Component } from 'react';
import { formatCurrency } from 'Shared/utility';

class userInformation extends Component {

    render() {
        return (
            <div className = "userInformation">
                <p>Họ tên:  
                    <span>{this.props.detail ? " " + this.props.detail.first_name : ""}</span>
                </p>
                <p>SĐT:  
                    <span>{this.props.detail ? " " + this.props.detail.phone : ""}</span>
                </p>
                <p>Email:  
                    <span>{this.props.detail ? " " + this.props.detail.email : ""}</span>
                </p>
                <p>Địa chỉ:  
                    <span>{this.props.detail ? " " + this.props.detail.address : ""}</span>
                </p>
            </div>
        );
    }
}

export default userInformation;