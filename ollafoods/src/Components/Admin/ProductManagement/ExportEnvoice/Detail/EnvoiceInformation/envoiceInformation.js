import React, { Component } from 'react';
import { formatCurrency } from 'Shared/utility';

class envoiceInformation extends Component {

    render() {
        return (
            <div className = "envoiceInformation">
                <p>Mã hóa đơn:  
                    <span>{this.props.detail ? " " + this.props.detail.code : ""}</span>
                </p>
                <p>Tổng tiền:  
                    <span>{this.props.detail ? " " + formatCurrency(this.props.detail.money) + " đ" : ""}</span>
                </p>
                <p>Lợi nhuận:  
                    <span>{this.props.detail ? " " + formatCurrency(this.props.detail.utility) + " đ" : ""}</span>
                </p>
                <p>Chú thích:  
                    <span>{this.props.detail ? " " + this.props.detail.note : ""}</span>
                </p>
            </div>
        );
    }
}

export default envoiceInformation;