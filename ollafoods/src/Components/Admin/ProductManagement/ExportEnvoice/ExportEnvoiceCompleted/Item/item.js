import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate, formatCurrency } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';


class Item extends Component {
    
    onCanceledHandler = (event) => {
        event.preventDefault();
        this.props.toggleConfirmSetStatusCanceled();
        this.props.setUsingId(this.props.id);
    }
    onWatingHandler = (event) => {
        event.preventDefault();
        this.props.toggleConfirmSetStatusWating();
        this.props.setUsingId(this.props.id);
    }

    render() {
        return (
            <tr>
            <td>{this.props.index}</td>
            <td>{this.props.code}</td>
            <td>{this.props.name}</td>
            <td>{this.props.email}</td>
            <td>{this.props.phone}</td>
            <td>{formatCurrency(this.props.money) + " đ"}</td>
            <td>{formatCurrency(this.props.utility) + " đ"}</td>
            <td>
                <ButtonTable
                    type="ban"
                    title="Chưa xữ lý"
                    clicked = {(event) => this.onWatingHandler(event)}
                />
                <LinkTable
                    type="pen"
                    title="Chỉnh sửa hóa đơn"
                    url = {this.props.update_url}
                />
                <LinkTable
                    type="eye"
                    title="Xem chi tiết"
                    url = {this.props.detail_url}
                />
                <ButtonTable
                    type="trash"
                    title="Hủy hóa đơn"
                    clicked = {(event) => this.onCanceledHandler(event)}
                />
            </td>
        </tr>
        );
    }
}

export default Item;