import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate, formatCurrency } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';


class Item extends Component {
 
    onCanceledHandler = (event) => {
        event.preventDefault();
        this.props.toggleConfirmSetStatusCanceled();
        this.props.setUsingId(this.props.id);
    }
    onCompletedHandler = (event) => {
        event.preventDefault();
        this.props.toggleConfirmSetStatusCompleted();
        this.props.setUsingId(this.props.id);
    }

    render() {
        return (
            <tr>
            <td>{this.props.index}</td>
            <td>{this.props.code}</td>
            <td>{this.props.name}</td>
            <td>{this.props.email}</td>
            <td>{this.props.phone}</td>
            <td>{formatCurrency(this.props.money) + " đ"}</td>
            <td>{formatCurrency(this.props.utility) + " đ"}</td>
            <td>
                <ButtonTable
                    type="check"
                    title="Đã hoàn thành"
                    clicked = {(event) => this.onCompletedHandler(event)}
                />
                <LinkTable
                    type="pen"
                    title="Chỉnh sửa hóa đơn"
                    url = {this.props.update_url}
                />
                <ButtonTable
                    type="trash"
                    title="Hủy hóa đơn"
                    clicked = {(event) => this.onCanceledHandler(event)}
                />
                <LinkTable
                    type="eye"
                    title="Thông tin chi tiết"
                    url = {this.props.detail_url}
                />
            </td>
        </tr>
        );
    }
}

export default Item;