import React, { Component } from 'react';

// ------------------------------- CSS -------------------------------------
import 'Shared/style.css';

// ------------------------------ COMPONENTS -------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Search from 'Components/UI/UIS/Search/search';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import Item from './Item/item';
import { socket } from 'Shared/server';

class exportEnvoiceWaiting extends Component {
    state = {
        searchValue : '',
        current_page : 1,
    }

    componentDidMount(){
        this.props.onFetchExportEnvoice(
            this.state.searchValue, 
            this.state.current_page,
            this.props.token,
        );

        // socket io
        socket.on("server-send-fetch-export-envoices", () => {
            this.props.onFetchExportEnvoice(
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchExportEnvoice(this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchExportEnvoice(value, 1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    id = {ig.id}
                    key = {ig.index}
                    index = {index + 1}
                    code = {ig.code}
                    name = {ig.user.first_name}
                    email = {ig.user.email}
                    phone = {ig.user.phone}
                    money = {ig.money}
                    utility = {ig.utility}
                    // url
                    detail_url = {this.props.pathName + "/detail/" + ig.id}
                    update_url = {this.props.pathName + "/update/" + ig.id}
                    
                    //event
                    toggleConfirmSetStatusCanceled = {this.props.toggleConfirmSetStatusCanceled}
                    toggleConfirmSetStatusCompleted = {this.props.toggleConfirmSetStatusCompleted}
                    setUsingId = {this.props.setUsingId}
                />  
            )
        })

        return (
            <Auxx>
                <Search
                    holder = "Mã hóa đơn"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã HĐ</th>
                            <th>Tên KH</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Tổng tiền</th>
                            <th>Lợi nhuận</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    nextPage = {this.nextPage}
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                />
            </Auxx>
        );
    }
}

export default exportEnvoiceWaiting;