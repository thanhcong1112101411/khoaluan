import React, { Component } from 'react';

// ------------------------------- CSS -------------------------------------
import 'Shared/style.css';

// ------------------------------ COMPONENTS -------------------------------
import Auxx from '../ProductLock/node_modules/Hoc/Auxx/auxx';
import Search from '../ProductLock/node_modules/Components/UI/UIS/Search/search';
import Pagination from '../ProductLock/node_modules/Components/UI/UIS/Pagination/pagination';
import Item from './ProductSellingItem/productSellingItem'
import { socket } from '../ProductLock/node_modules/Shared/server';

class productSellingItem extends Component {
    state = {
        searchValue : '',
        current_page : 1,
    }

    componentDidMount(){
        this.props.onFetchProducts(
            this.state.searchValue, 
            this.state.current_page,
            this.props.token,
        );

        // socket io
        socket.on("server-send-fetch-products", () => {
            this.props.onFetchProducts(
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchProducts(this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchProducts( value, 1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    key = {ig.uuid}
                    uuid = {ig.uuid}
                    index = {index + 1}
                    name = {ig.name}
                    importPrice = {ig.importPrice}
                    exportPrice = {ig.exportPrice}
                    discountAmount = {ig.discountAmount}
                    unitName = {ig.unitName}
                    total={0}
                    created_at = {ig.created_at}

                    // url
                    detail_url = {this.props.pathName + "/detail/" + ig.uuid}
                    update_url = {this.props.pathName + "/update/" + ig.uuid}
                    
                    //event
                    toggleConfirmSetStatusBlock = {()=>{this.props.toggleConfirmSetStatusBlock(ig.id)}}
                    setUsingUuid = {this.props.setUsingUuid}
                />  
            )
        })

        return (
            <Auxx>
                <Search
                    holder = "Tên sản phẩm"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                        <th>STT</th>
                            <th>Tên sản phẩm</th>
                            <th>Giá nhập</th>
                            <th>Giá bán</th>
                            <th>Giảm giá</th>
                            <th>Đơn vị tính</th>
                            <th>Thành tiền</th>
                            <th>Ngày tạo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    nextPage = {this.nextPage}
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                />
            </Auxx>
        );
    }
}

export default productSellingItem;