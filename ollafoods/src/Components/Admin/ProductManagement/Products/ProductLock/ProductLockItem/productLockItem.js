import React, { Component } from 'react';
import { formatDate, formatPhone, formatCurrency, formatDateYYMMDD, formatDateDDMMYY } from 'Shared/utility';

//--------------------------------- COMPONENTS ----------------------------------
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { baseUrl } from 'Shared/server';


class productWaiting extends Component {
    state = {  }

    onOpenHandler = (event) => {
        event.preventDefault();
        this.props.setUsingId(this.props.id);
        this.props.toggleConfirmSetStatusOpen(this.props.id);
    }

    render() {
        let profit = "Chưa xác định";
        if (this.props.importPrice && this.props.exportPrice){
            profit = formatCurrency(this.props.exportPrice - this.props.importPrice) + " đ";
        }

        let money = "Chưa xác định";
        if (this.props.exportPrice && this.props.discountAmount){
            money = formatCurrency(this.props.exportPrice * (1 - this.props.discountAmount / 100)) + " đ";
        }else if (this.props.exportPrice){
            money = formatCurrency(this.props.exportPrice) + " đ";
        }

        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.code}</td>
                <td>
                    <img width="120px" src = {baseUrl + "/" + this.props.image.url} />
                </td>
                <td>{this.props.name}</td>
                {
                    this.props.importPrice ?
                    <td>{formatCurrency(this.props.importPrice) + " đ"}</td>
                    :
                    <td style = {{color: "red"}}>Chưa cập nhật</td>
                }
                {
                    this.props.exportPrice ?
                    <td>{formatCurrency(this.props.exportPrice) + " đ"}</td>
                    :
                    <td style = {{color: "red"}}>Chưa cập nhật</td>
                }
                {
                    this.props.discountAmount ?
                    <td>{this.props.discountAmount + " %"}</td>
                    :
                    <td>Không áp dụng</td>
                }
                
                <td>{this.props.unitName}</td>
                <td>{money}</td>
                <td>{profit}</td>
                <td>{formatDateDDMMYY(this.props.created_at)}</td>
                <td>{formatDateDDMMYY(this.props.updated_at)}</td>
                <td>
                    <ButtonTable
                        type="lockopen"
                        title="Mở khóa sản phẩm"
                        clicked = {(event) => this.onOpenHandler(event)}
                    />
                    <LinkTable
                        type="pen"
                        title="Cập nhật thông tin sản phẩm"
                        url = {this.props.update_url}
                    />
                </td>
            </tr>
        );
    }
}

export default productWaiting;