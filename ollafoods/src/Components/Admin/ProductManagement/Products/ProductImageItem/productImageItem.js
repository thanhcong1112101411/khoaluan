import React, { Component } from 'react';

// --------------------------- CSS -------------------------------
import './productImageItem.css';
import 'Shared/style.css';


class ProductImageItem extends Component {
    state = {  }

    onDeleteImageItem=(event)=>{
        event.preventDefault();
        this.props.deleteImageItem(this.props.index);
    }
  
    
    render() {
        return (
            <div className = "productImageItem">
                <button
                    onClick={this.onDeleteImageItem}
                >x</button>
                {/* <p>{this.props.name}</p> */}
                <img width="100%" src= {this.props.image_url} />
            </div>
        );
    }
}

export default ProductImageItem;