import React, { Component } from 'react';
import { formatDate, formatPhone, formatCurrency, formatDateYYMMDD, formatDateDDMMYY } from 'Shared/utility';

//--------------------------------- COMPONENTS ----------------------------------
import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { baseUrl } from 'Shared/server';


class productWaiting extends Component {
    
    onBlockHandler = (event) => {
        event.preventDefault();
        this.props.setUsingId(this.props.id);
        this.props.toggleConfirmSetStatusBlock();
    }

    render() {
        let money = this.props.exportPrice;
        if (this.props.discountAmount){
            money = money * (1 - this.props.discountAmount / 100);
        }

        let profit = money - this.props.importPrice;

        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.code}</td>
                <td>
                    <img width="120px" src = {baseUrl + "/" + this.props.image.url} />
                </td>
                <td>{this.props.name}</td>
                <td>{formatCurrency(this.props.importPrice) + " đ"}</td>
                <td>{formatCurrency(this.props.exportPrice) + " đ"}</td>
                {
                    this.props.discountAmount ?
                    <td>{this.props.discountAmount + " %"}</td>
                    :
                    <td>Không áp dụng</td>
                }
                
                <td>{this.props.unitName}</td>
                <td>{formatCurrency(money) + " đ"}</td>
                <td>{formatCurrency(profit) + " đ"}</td>
                <td>{formatDateDDMMYY(this.props.created_at)}</td>
                <td>{formatDateDDMMYY(this.props.updated_at)}</td>
                <td>
                    <ButtonTable
                        type="lock"
                        title="Khóa sản phẩm"
                        clicked = {(event) => this.onBlockHandler(event)}
                    />
                    <LinkTable
                        type="pen"
                        title="Cập nhật thông tin sản phẩm"
                        url = {this.props.update_url}
                    />
                </td>
            </tr>
        );
    }
}

export default productWaiting;