import React, { Component } from 'react';

// ------------------------------- CSS -------------------------------------
import 'Shared/style.css';

// ------------------------------ COMPONENTS -------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Search from 'Components/UI/UIS/Search/search';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import Item from './ProductWaitingItem/productWaitingItem';
import { socket } from 'Shared/server';

class productWaiting extends Component {
    state = {
        searchValue : '',
        current_page : 1,
    }

    componentDidMount(){
        this.props.onFetchProducts(
            this.state.searchValue, 
            this.state.current_page,
            this.props.token,
        );

        // socket io
        socket.on("server-send-fetch-products", () => {
            this.props.onFetchProducts(
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchProducts(this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchProducts(value, 1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    code = {ig.code}
                    name = {ig.name}
                    image = {JSON.parse(ig.images)[0]}
                    importPrice = {ig.importPrice}
                    exportPrice = {ig.exportPrice}
                    discountAmount = {ig.discountAmount}
                    unitName = {ig.unitName}
                    created_at = {ig.created_at}
                    updated_at = {ig.updated_at}

                    // url
                    detail_url = {this.props.pathName + "/detail/" + ig.id}
                    update_url = {this.props.pathName + "/update/" + ig.id}
                    
                    //event
                    toggleConfirmSetStatusBlock = {this.props.toggleConfirmSetStatusBlock}
                    setUsingId = {this.props.setUsingId}
                />  
            )
        })

        return (
            <Auxx>
                <Search
                    holder = "Tên sản phẩm"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã SP</th>
                            <th colSpan="2">Sản phẩm</th>
                            <th>ĐVT</th>
                            <th>Ngày tạo</th>
                            <th>Cập nhật</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    nextPage = {this.nextPage}
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                />
            </Auxx>
        );
    }
}

export default productWaiting;