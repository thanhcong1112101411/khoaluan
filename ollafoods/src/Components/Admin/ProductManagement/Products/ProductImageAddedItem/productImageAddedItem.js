import React, { Component } from 'react';

// --------------------------- CSS -------------------------------
import './productImageAddedItem.css';
import 'Shared/style.css';

// --------------------------- COMPONENTS -----------------------
import { baseUrl } from 'Shared/server';


class ProductImageAddedItem extends Component {
    state = {  }

    onDeleteImageItem=(event)=>{
        event.preventDefault();
        this.props.deleteImageAddedItem(this.props.index);
    }
  
    
    render() {
        return (
            <div className = "productImageAddedItem">
                <button
                    onClick={this.onDeleteImageItem}
                >x</button>
                <img width="100%" src= {baseUrl + "/" + this.props.image_url} />
            </div>
        );
    }
}

export default ProductImageAddedItem;