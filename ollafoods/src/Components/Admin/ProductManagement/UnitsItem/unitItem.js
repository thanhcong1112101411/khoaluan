import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate } from 'Shared/utility';
import {RoleDefault} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';


class unitItem extends Component {
    state = {  }

    onDeleteHandler=(event)=>{
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }
    render() {
        return (
            <tr>
            <td>{this.props.index}</td>
            <td>{this.props.name}</td>
            <td>{formatDate(this.props.created_at)}</td>
            <td>
                <ButtonTable
                    type="trash"
                    title="Xóa đơn vị tính"
                    clicked = {(event) => this.onDeleteHandler(event)}
                />
                <LinkTable
                    type="pen"
                    title="Chỉnh sửa đơn vị tính"
                    url = {this.props.url}
                />
            </td>
        </tr>
        );
    }
}

export default unitItem;