import React, { Component } from 'react';

import {baseUrl} from 'Shared/server';

class imageItem extends Component {
    
    onDeleteClick = (event) => {
        event.preventDefault();
        this.props.toggleDeleteConfirm();
        this.props.setIdUsing(this.props.id);
    }
    render() {
        return (
            <div className = {["item", this.props.index == 1 ? "mainItem" : ""].join(" ")}>
                <img width="100%" src={baseUrl + "/" + this.props.link} title = {this.props.title} />
                <button
                    onClick = {(event) => this.onDeleteClick(event)}
                >X</button>
            </div>
        );
    }
}

export default imageItem;