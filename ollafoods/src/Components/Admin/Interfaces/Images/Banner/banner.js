import React, { Component } from 'react';

// ---------------------- CSS ------------------------
import 'Shared/style.css';
import './banner.css';
//----------------------- COMPONENTS -----------------
import TitleImage from 'Components/UI/UIS/TitleImage/titleImage';
import Auxx from 'Hoc/Auxx/auxx';
import InsertModel from '../InsertModel/insertModel';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import BannerItem from '../ImageItem/imageItem';
import { Imagetype } from 'Utils/enum.utils';

class banner extends Component {
    state = {
        insertModel: false,
    }

    // toggle
    toggleInsertModel = () => {
        this.setState({insertModel: !this.state.insertModel});
    }

    setAlertAddImageHandler = () => {
        this.toggleInsertModel();
        this.props.onSetAlertAddImage();
    }
    render() {

        const bannerList = this.props.list.map((ig, index) => {
            return (
                <BannerItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    link = {ig.link}
                    title = {ig.title}

                    toggleDeleteConfirm = {this.props.toggleDeleteConfirm}
                    setIdUsing = {this.props.setIdUsing}
                />
            )
        })
        return (
            <Auxx>
                <TitleImage
                    clicked = {this.toggleInsertModel}
                >Banner</TitleImage>
                <div className="BannerImagesManagement">
                    <div className = "imageList">
                        {bannerList}
                    </div>
                        
                </div>
                <InsertModel 
                    show={this.state.insertModel} 
                    onAddImagesHandler = {this.props.onAddImagesHandler}
                    toggleInsertModel = {this.toggleInsertModel}
                    image_type = {Imagetype.BANNER}
                />
                {
                    !!this.props.alert_add ?
                    <Alert
                        show = {!!this.props.alert_add}
                        data = {this.props.alert_add}
                        clicked = {this.setAlertAddImageHandler}
                    />
                    : ""
                }

            </Auxx>
        );
    }
}

export default banner;