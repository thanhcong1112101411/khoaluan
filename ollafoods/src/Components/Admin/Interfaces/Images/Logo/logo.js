import React, { Component } from 'react';

// ---------------------- CSS ------------------------
import 'Shared/style.css';
import './logo.css';
//----------------------- COMPONENTS -----------------
import TitleImage from 'Components/UI/UIS/TitleImage/titleImage';
import Auxx from 'Hoc/Auxx/auxx';
import InsertModel from '../InsertModel/insertModel';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import LogoItem from '../ImageItem/imageItem';
import { Imagetype } from 'Utils/enum.utils';

class contentTop extends Component {
    state = {
        insertModel: false,
    }

    // toggle
    toggleInsertModel = () => {
        this.setState({insertModel: !this.state.insertModel});
    }

    setAlertAddImageHandler = () => {
        this.toggleInsertModel();
        this.props.onSetAlertAddImage();
    }
    render() {

        const bannerList = this.props.list.map((ig, index) => {
            return (
                <LogoItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    link = {ig.link}
                    title = {ig.title}

                    toggleDeleteConfirm = {this.props.toggleDeleteConfirm}
                    setIdUsing = {this.props.setIdUsing}
                />
            )
        })
        return (
            <Auxx>
                <TitleImage
                    clicked = {this.toggleInsertModel}
                >Logo</TitleImage>
                <div className="LogoImagesManagement">
                    <div className = "imageList">
                        {bannerList}
                    </div>
                        
                </div>
                <InsertModel 
                    show={this.state.insertModel} 
                    onAddImagesHandler = {this.props.onAddImagesHandler}
                    toggleInsertModel = {this.toggleInsertModel}
                    image_type = {Imagetype.LOGO}
                />
                {
                    !!this.props.alert_add ?
                    <Alert
                        show = {!!this.props.alert_add}
                        data = {this.props.alert_add}
                        clicked = {this.setAlertAddImageHandler}
                    />
                    : ""
                }

            </Auxx>
        );
    }
}

export default contentTop;