import React, { Component } from 'react';

// ----------------------- CSS -------------------------------------
import 'Shared/style.css';
import './insertModel.css';

// ----------------------- COMPONENNTS --------------------------------
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import {updateObject, checkValidity} from 'Shared/utility';
import { Imagetype } from 'Utils/enum.utils';

class insertModel extends Component {
    state = {
        controls:{
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tiêu đề',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: 'File Ảnh'
                },
                value: [],
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },

        },
        formIsValid: false,
    }
    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = '';
        if (controlName == "images"){
            value = event.target.files[0];
        }else{
            value = event.target.value;
        }

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }
    orderHandler = (event) => {
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
        }
        form.append("type", this.props.image_type);
        // for (let formElementIdentifier in this.state.controls) {
        //     console.log(formElementIdentifier);
        //     if (formElementIdentifier == "images"){
        //         this.state.controls[formElementIdentifier].value.forEach((item) => {
        //             console.log(item);
        //             form.append('images', item);
        //         })
        //     } else{
        //         form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
        //     }
        // }
        
        this.props.onAddImagesHandler(form);
    }
    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Thêm</Button>
            </form>
        );

        let title = null;
        if (this.props.image_type == Imagetype.LOGO){
            title = (<h1></h1>);
        }else if (this.props.image_type == Imagetype.BANNER){
            title = (<h1>Thêm banner</h1>);
        }else{
            title = (<h1>Thêm content top</h1>);
        }

        return (
            <div className = {this.props.show ? "": "visible"}>
                <BackDrop 
                    show = {this.props.show} 
                    clicked = {this.props.toggleInsertModel}
                />
                <div className = "insertModelBannerImage" >
                    {title}
                    <hr/>
                    {form}
                </div>

            </div>
        );
    }
}

export default insertModel;