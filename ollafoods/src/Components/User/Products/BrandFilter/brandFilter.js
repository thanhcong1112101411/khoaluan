import React, { Component } from 'react';

// ----------------------------- CSS------------------------------------
import './brandFilter.css';
import 'Shared/style.css';

// ---------------------------- COMPONENT ------------------------------
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faSortDown } from '@fortawesome/free-solid-svg-icons';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';

class brandFilter extends Component {
    state = {
        controls: {
            brands: {
                elementType: 'checkbox',
                elementConfig: {
                    type: 'checkbox',
                    options: [],
                },
                value: [],
                validation: {
                    required: true,
                },
                valid: false
            },
        },
        
        showModel: false,
    }

    componentWillReceiveProps(nextProps){
        let allBrands = nextProps.list;

        if (allBrands && allBrands.length > 0){
            let controls = this.state.controls;
            controls.brands.elementConfig.options = allBrands;
            this.setState({controls: controls});
        }
    }

    // toggle
    toggleModel = () => {
        this.setState({showModel: !this.state.showModel});
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = event.target.value;

        if (controlName == "brands"){
            let roleValue = this.state.controls[controlName].value;
            if(event.target.checked){
                roleValue.push(parseInt(value));
            }else{
                const index = roleValue.findIndex((item)=>{
                    return item == value;
                });
                roleValue.splice(index,1);
            }
            value = roleValue;
            console.log(value);

            this.props.setStateBrand(value);
        }

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
            </form>
        );

        return (
            <div className = "brandFilter">
                <div className = "content">
                    <div className = "item"
                        onClick = {this.toggleModel}
                    >
                        <p>Thương hiệu</p>
                        <FontAwesomeIcon icon = {faSortDown} />
                    </div>
                    <div>
                        <BackDrop
                            show = {this.state.showModel}
                            clicked = {this.toggleModel}
                        />
                        <div className = {["model", this.state.showModel ? "" : "visible"].join(" ")}>
                            {form}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default brandFilter;