import React, { Component } from 'react';

// ----------------------------- CSS------------------------------------
import './priceFilter.css';
import 'Shared/style.css';

// ---------------------------- COMPONENT ------------------------------
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faSortDown } from '@fortawesome/free-solid-svg-icons';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';

class PriceFilter extends Component {
    state = {
        controls: {
            price:{
                elementType: 'selectDefault',
                elementConfig: {
                    options:[
                        {value: 0, displayValue: 'Giá mặc định'}, 
                        {value: 2, displayValue: 'Giá giảm dần'}, 
                        {value: 1, displayValue: 'Giá tăng dần'}, 
                    ],
                },
                value: 0,
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
        },
        
        showModel: false,
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = event.target.value;
        this.props.setStatePrice(value);
        
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
            </form>
        );

        return (
            <div className = "priceFilter">
                <div className = "content">
                    {form}
                </div>
            </div>
        );
    }
}

export default PriceFilter;