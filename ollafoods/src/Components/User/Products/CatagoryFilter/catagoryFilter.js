import React, { Component } from 'react';

// ------------------------- CSS -------------------------------
import './catagoryFilter.css';
import 'Shared/style.css';

// ---------------------- COMPONENTS -----------------------------
import Item from './Item/item';

class CatagoryFilter extends Component {
    state = {
        catagoryId: -1,
    }

    onChangeCatagory = (id) => {
        this.setState({catagoryId: id});
        this.props.setStateCatagory(id);
    }

    render() {
        let listCatagory = '';
        if (this.props.list){
            listCatagory = this.props.list.map(ig => {
                return(
                    <Item
                        key = {ig.id}
                        id = {ig.id}
                        name = {ig.name}
                        onChangeCatagory = {this.onChangeCatagory}
                        catagoryId = {this.state.catagoryId}
                    />
                )
            })
        }

        return (
            <div className ="catagoryFilter">
                <ul>
                    <Item
                        id = {-1}
                        name = {"Tất cả"}
                        onChangeCatagory = {this.onChangeCatagory}
                        catagoryId = {this.state.catagoryId}
                    />  
                    {listCatagory}
                </ul>
            </div>
        );
    }
}

export default CatagoryFilter;