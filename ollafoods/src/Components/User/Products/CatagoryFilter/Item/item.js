import React, { Component } from 'react';

class CatagoryFilterItem extends Component {

    onChangeCatagory = () => {
        this.props.onChangeCatagory(this.props.id);
    }

    render() {
        return (
            <li>
                <button
                    className = {this.props.catagoryId == this.props.id ? "active" : ""}
                    onClick = {this.onChangeCatagory}
                >{this.props.name}</button>
            </li>
        );
    }
}

export default CatagoryFilterItem;