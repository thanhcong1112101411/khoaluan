import React from 'react';

import './contentTop.css';
import 'Shared/style.css';
import { ImageBannerLink } from 'Shared/server';

const contentTop = (props) =>{
    return(
        <div className = "ContentTop">
            <div className = "container">
                <div className = "row">
                    <div className = "col-md-6 item">
                        <a href="#">
                            <img width = "100%" src={ImageBannerLink + 'ct1.png'} />
                        </a>
                    </div>
                    <div className = "col-md-6 item">
                        <a href="#">
                            <img width = "100%" src={ImageBannerLink + 'ct2.png'} />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default contentTop;