import React, { Component } from 'react';
import { baseUrl } from 'Shared/server';
import { Link } from 'react-router-dom';
import { formatDateDDMMYY } from 'Shared/utility';

class item extends Component {

    render() {
        return (
            <div className = "item">
                <img width="100%" src= {baseUrl + "/" + this.props.image} />
                <div className="background">
                    <img width="100%" src= {baseUrl + "/" + this.props.image} />
                </div>
                <div className = "content">
                    <span>{formatDateDDMMYY(this.props.date)}</span>
                    <div>
                        <h4>
                            <Link to= {"product-detail/" + this.props.id}>{this.props.title.substr(0,45) + "..."}</Link>
                        </h4>
                        {/* <p>
                            <Link to= {"product-detail/" + this.props.id}>{this.props.content}</Link>
                        </p> */}
                    </div>
                </div>
            </div>
        );
    }
}

export default item;