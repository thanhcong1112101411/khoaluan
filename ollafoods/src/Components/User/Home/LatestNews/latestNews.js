import React, { Component } from 'react';

import './latestNews.css';
import 'Shared/style.css';

import HeadingHome from 'Components/UI/UIS/Heading/heading';
import Item from './Item/item';

class latestNews extends Component {

    render(){
        console.log(this.props.list);
        const listItem = this.props.list.map(ig => {
            return (
                <div key = {ig.id} className = "col-lg-4 col-md-4 col-sm-6">
                    <Item
                        id = {ig.id}
                        title = {ig.title}
                        image = {ig.image}
                        date = {ig.created_at}
                    />
                </div>
            )
            
        })

        return(
            <section className = "latestNews">
                <HeadingHome>Tin tức mới nhất</HeadingHome>
                <div className="container">
                    <div className = "row">
                        {listItem}
                    </div>
                </div>
            </section>
        );
    }
}

export default latestNews;