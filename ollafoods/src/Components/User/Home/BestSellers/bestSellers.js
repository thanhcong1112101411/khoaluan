import React from 'react';

import './bestSellers.css';
import 'Shared/style.css';

import ProductItem from 'Components/UI/UIType/ProductItem/productItem';
import HeadingHome from 'Components/UI/UIS/Heading/heading';

const bestSellers = (props) => {
    return(
        <section className ="bestsellers">
            <div className ="heading">
                <HeadingHome>Sản phẩm bán chạy</HeadingHome>
            </div>
            <div className = "products">
                <div className="container">
                    <div className = "row">
                        <div className = "col-md-3">
                            <ProductItem />
                        </div>
                        <div className = "col-md-3">
                            <ProductItem />
                        </div>
                        <div className = "col-md-3">
                            <ProductItem />
                        </div>
                        <div className = "col-md-3">
                            <ProductItem />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default bestSellers;