import React from 'react';

import './trendingProducts.css';
import 'Shared/style.css';

import ProductItem from 'Components/UI/UIType/ProductItem/productItem';
import HeadingHome from 'Components/UI/UIS/Heading/heading';

const trendingProducts = (props) => {

    const listItem = props.list.map((ig, index) => {
        return(
            <div key = {ig.id} className=" col-lg-4 col-md-4 col-sm-6">
                <ProductItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    name = {ig.name}
                    created_at = {ig.created_at}
                    images = {JSON.parse(ig.images)[0]}
                    exportPrice={ig.exportPrice}
                    discountAmount = {ig.discountAmount}
                    // event
                    
                />
            </div>
        );
    })

    return(
        <section className ="trendingProducts">
            <HeadingHome>Sản phẩm mới nhất</HeadingHome>
            <div className = "products">
                <div className="container">
                    <div className = "row">
                        {listItem}
                    </div>
                </div>
            </div>
        </section>
    );
}

export default trendingProducts;