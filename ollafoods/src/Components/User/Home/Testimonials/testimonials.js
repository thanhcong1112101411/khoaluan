import React from 'react';

import './testimonials.css';

const testimonials = () =>{
    return(
        <section>
            <div className="testimonial">
                <div className="container">
                    <h2>Về SeasonalFoods</h2>
                    <p>SeasonalFoods là Công ty hoạt động trên các lĩnh vực như: phân phối sỉ và lẻ ngành hàng thực phẩm công nghệ, dịch vụ vận chuyển, cho thuê văn phòng, kho bãi, hợp tác đầu tư...
                    Những mặt hàng phân phối chủ yếu như: đường, nước giải khát, bia, rượu, sữa, cà phê, dầu ăn, thực phẩm đóng hộp, thực phẩm chế biến, bánhkẹo, thuốc lá,… Các sản phẩm do Infoodco phân phối được chọn lựa rất kỹ về chất lượng của những nhà sản xuất uy tín trong nước và được nhập khẩu.

Năm 2007, INFOODCO lọt vào TOP 500 doanh nghiệp lớn nhất Việt Nam được VNR500 bình chọn. Năm 2009, INFOODCO cũng đã từng được trao tặng Cúp vàng Sản phẩm – dịch vụ xuất sắc vì sức khoẻ và sự phát triển của cộng đồng. Bên cạnh đó, sản phẩm chủ lực đường túi thương hiệu “Con Ong” do chính INFOODCO sản xuất được người tiêu dùng ưa chuộng và bình chọn là Hàng Việt Nam chất lượng cao năm 2012. 
                    </p>
                </div>
                
            </div>
        </section>
    );
}

export default testimonials;