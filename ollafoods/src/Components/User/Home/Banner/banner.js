import React, { Component } from 'react';

// ------------------ CSS --------------------------
import './banner.css';
import 'Shared/style.css';

import {ImageSliderLink, ImageBannerLink, ImageLogoLink} from 'Shared/server';

class Banner extends Component {
    render() {
        return (
            <div className="HomeBanner">
                <div className="images">
                    <div className = "W20"></div>
                    <div></div>
                    <div className="fadeInDown animated">
                        <img width="100%" src={ImageSliderLink + "1.png"}/>
                    </div>
                    <div className="fadeInDown animated">
                    </div>
                    <div className="fadeInDown animated">
                        {/* <img width="100%" src={ImageSliderLink + "10.png"}/> */}
                    </div>
                    <div className = "W30" className="fadeInDown animated">
                        <img width="100%" src={ImageSliderLink + "11.png"}/>
                    </div>
                    <div className="fadeInRight animated">
                        {/* <img width="100%" src={ImageSliderLink + "13.png"}/> */}
                    </div>
                    <div className="fadeInRight animated">
                        <img width="100%" src={ImageSliderLink + "13.png"}/>
                    </div>
                    <div className = "W20" className="fadeInLeft animated">
                        <img width="100%" src={ImageSliderLink + "1.png"}/>
                    </div>
                    <div className = "W20" className="fadeInDown animated">
                        <img width="100%" src={ImageSliderLink + "10.png"}/>
                    </div>
                    <div className="fadeInDown animated">
                        <img width="100%" src={ImageSliderLink + "11.png"}/>
                    </div>
                    <div className="fadeInRight animated">
                        <img width="100%" src={ImageSliderLink + "13.png"}/>
                    </div>
                    
                </div>
                <img width="100%" src={ImageBannerLink + "white.png"}/>
                <div className="moveIn bannerContent container">
                    <div className = "heading">
                        <img src={ImageLogoLink + "logo.png"}/>
                        <p>Công ty cung cấp thực phẩm sạch an toàn, chất lượng hàng đầu Việt Nam</p>
                    </div>
                </div>
                <div className="bannerBg2">
                    <img width="100%" src={ ImageBannerLink +"whiteBg.png"}/>
                </div>
                
            </div>
        );
    }
}

export default Banner;