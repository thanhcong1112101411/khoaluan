import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

// --------------------------- CSS -------------------------
import './information.css';
import 'Shared/style.css';
import { baseUrl } from 'Shared/server';
import { formatCurrency } from 'Shared/utility';


class Information extends Component {

    addToCartHandler = () =>{
        this.props.onAddProductToCart(this.props.detail.id);
    }

    render() {
        let listImage = '';
        if (this.props.images){
            listImage = this.props.images.map(ig => {
                return (
                    <div key = {ig.id}>
                        <img src = {baseUrl + "/" + ig.url} />
                    </div>
                )
            })
        }

        let exportPrice = this.props.detail.exportPrice;
        if (this.props.detail.discountAmount){
            exportPrice = exportPrice * (1 - this.props.detail.discountAmount / 100);
        }

        return (
            <div className = "productInf">
                <div className="container">
                    <div className = "row">
                        <div className = "col-md-5 images">
                            <Carousel>
                                {listImage}
                            </Carousel>
                        </div>
                        <div className = "col-md-7 information">
                            <div>
                                <h1>{this.props.detail.name}</h1>
                                <hr/>
                                <div dangerouslySetInnerHTML={{__html: this.props.detail.description_first}} />
                                
                                <div className = "unitStyle">
                                    <p>Đơn vị tính : <span>{this.props.detail.unitName}</span></p>
                                </div>
                                {
                                    (this.props.detail.discountAmount) ?
                                    <div className = "discountStyle">
                                        <p>Giảm giá : <span>{this.props.detail.discountAmount + "%"}</span></p>
                                    </div>
                                    : ""
                                }
                                <div className = "priceStyle">
                                    <span className = "price">
                                        {formatCurrency( exportPrice + "")} đ
                                    </span>
                                    {
                                        (this.props.detail.rrp !=0) ?
                                        <span className = "rrp">
                                            {formatCurrency(this.props.detail.rrp+"")} đ
                                        </span>
                                        : ""
                                    }
                                    {
                                        (this.props.detail.discountAmount) ?
                                        <span className = "discountPrice">
                                            {formatCurrency(this.props.detail.exportPrice+"")} đ
                                        </span>
                                        : ""
                                    }
                                </div>
                                <button onClick={this.addToCartHandler} >Thêm Vào Giỏ Hàng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Information;