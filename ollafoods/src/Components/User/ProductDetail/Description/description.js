import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

//--------------------- CSS ---------------------------
import 'Shared/style.css';
import './description.css';


    class description extends React.Component{
    render(){
        return(
            <div className = "productDesc">
                <div className = "container">
                    <div>
                        <Tabs id="controlled-tab-example" className= "tabStyle">
                                <Tab eventKey="home" title="Mô tả">
                                    <div dangerouslySetInnerHTML={{__html: this.props.desSecond}} />
                                </Tab>
                        </Tabs>
                    </div>
                </div>
            </div>
        )
    }
}

export default description;