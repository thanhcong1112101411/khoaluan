import React, {Component} from 'react';

import './trendingProducts.css';
import 'Shared/style.css';

import ProductItem from "Components/UI/UIType/ProductItem/productItem";
import HeadingHome from 'Components/UI/UIS/Heading/heading';
import AliceCarousel from 'react-alice-carousel'
import 'react-alice-carousel/lib/alice-carousel.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { baseUrl } from 'Shared/server';
import ProductItemTest from './ProductItem/productItem';

class trendingProducts extends Component {
    items = [1, 2, 3, 4, 5]

    state = {
        currentIndex: 0,
        galleryItems: this.items.map((i) => 
            <img width="100%" src = {"http://localhost:8200/public\assets/products/1593441889069.png"} />
        ),
      }
     
    responsive = {
        0: { items: 2 },
        1024: { items: 4 },
    }
     
    slideNext = () => this.setState({ currentIndex: this.state.currentIndex + 1 })
 
    slidePrev = () => this.setState({ currentIndex: this.state.currentIndex - 1 })

    render(){

        const listProduct = this.props.list.map((ig, index) => {
            return(
                <ProductItem
                        key = {ig.id}
                        id = {ig.id}
                        index = {index + 1}
                        name = {ig.name}
                        created_at = {ig.created_at}
                        images = {JSON.parse(ig.images)[0]}
                        exportPrice={ig.exportPrice}
                        discountAmount = {ig.discountAmount}
                        // event
                        
                    />
            )
        })

        const listItem = this.props.list.map((ig, index) => {
            return(
                <div key = {ig.id} className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <ProductItem
                        key = {ig.id}
                        id = {ig.id}
                        index = {index + 1}
                        name = {ig.name}
                        created_at = {ig.created_at}
                        images = {JSON.parse(ig.images)[0]}
                        exportPrice={ig.exportPrice}
                        discountAmount = {ig.discountAmount}
                        // event
                        
                    />
                </div>
            );
        })

        return(
            <section className = {["trendingProducts","container", this.props.list.length == 0 ? "visible" : ""].join(" ")}>
                <HeadingHome>Thường được mua cùng</HeadingHome>
                {/* <AliceCarousel
                    items={this.state.galleryItems}
                    responsive={this.responsive}
                    autoPlayInterval={10000}
                    autoPlayDirection="ltl"
                    autoPlay={true}
                    fadeOutAnimation={true}
                    mouseTrackingEnabled={true}
                    playButtonEnabled={false}
                    disableAutoPlayOnAction={true}
                    dotsDisabled = {true}
                    mouseTrackingEnabled = {true}
                    slideToIndex={this.state.currentIndex}
                    buttonsDisabled = {true}
                />
                <button className = "buttonPrev" onClick={() => this.slidePrev()}>
                    <FontAwesomeIcon icon = {faAngleLeft} />
                </button>
                <button className = "buttonNext" onClick={() => this.slideNext()}>
                    <FontAwesomeIcon icon = {faAngleRight} />
                </button> */}
                <div className="container">
                        <div className="row">
                            {listItem}
                        </div>
                </div>

            </section>
            
        )
    }
}

export default trendingProducts;