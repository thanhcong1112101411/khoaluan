import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './productItem.css';
import {formatCurrency, formatName} from 'Shared/utility';

//------------------------------ COMPONENTS --------------------------

class ProductItem extends Component {
    
    onAddProductToCartHandler = (event) =>{
        event.preventDefault();
        this.props.onAddProductToCartClicked(this.props.id);
    }
    render() {
        return (
            <Link to={"/product-detail"}>
                <div className="productImage">
                    <img width="100%" src="http://localhost:8200/public\assets/products/1593441889069.png"/>
                </div>
                <div className="productContent">
                    <h1 className="h3">Hummingbird printed t-shirt</h1>
                    <div className = "priceStyle">
                        <p>200.000 đ</p>
                    </div>
                </div>
            </Link>
        );
    }
}

export default ProductItem;