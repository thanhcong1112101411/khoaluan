import React, { Component } from 'react';

// -------------------------- CSS ------------------------
import './productList.css';
import 'Shared/style.css';

//------------------------- COMPONENTS -----------------
import CartItem from './Item/item';
import CartItemMobile from './Item/mobileItem';
import Auxx from 'Hoc/Auxx/auxx';

class ProductList extends Component {
    state = {  }
    render() {
        const cartItemList = this.props.cart.map(ig=>{
            return(
                <CartItem 
                    key = {ig.id}
                    id = {ig.id}
                    name = {ig.name}
                    image = {JSON.parse(ig.images)[0]}
                    price = {ig.exportPrice}
                    quantity = {ig.quantityProduct}
                    discount = {ig.discountAmount}
                    unitName = {ig.unitName}
                    min = {ig.min}

                    // event
                    changeQuantityProductClicked = {this.props.changeQuantityProductClicked}
                    onDeleteCartItemClicked = {this.props.onDeleteCartItemClicked}
                />
            );
        })
        const cartItemListMobile = this.props.cart.map(ig=>{
            return(
                <CartItemMobile
                    key = {ig.id}
                    id = {ig.id}
                    name = {ig.name}
                    image = {JSON.parse(ig.images)[0]}
                    price = {ig.exportPrice}
                    quantity = {ig.quantityProduct}
                    discount = {ig.discountAmount}
                    unitName = {ig.unitName}
                    min = {ig.min}

                    // event
                    changeQuantityProductClicked = {this.props.changeQuantityProductClicked}
                    onDeleteCartItemClicked = {this.props.onDeleteCartItemClicked}
                />
            );
        })

        return (
            <Auxx>
                <div className = "cartProduct">
                    <div className = "container">
                        <table className = "table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th colSpan="2">Sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn vị</th>
                                    <th>Giá</th>
                                    <th>Giảm giá</th>
                                    <th>Thành Tiền</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {cartItemList}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="cartMobile">
                    <div className="container">
                        <div className="cartMobileRow">
                            {cartItemListMobile}
                        </div>
                    </div>
                </div>
            </Auxx>
            
        );
    }
}

export default ProductList;