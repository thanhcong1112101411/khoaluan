import React, { Component } from 'react';

import {ImageProductLink, formatCurrency} from 'Shared/utility';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { baseUrl } from 'Shared/server';

class CartItem extends Component {

    onChangeQuantityProductHandler = (event,id) =>{
        event.preventDefault();
        const quantity = event.target.value;
        // let quantity = event.target.value.replace(/\+|-/ig, '');
        if( quantity < 1 || quantity.toString() === ""){
            this.props.changeQuantityProductClicked(id,1);
        }else{
            this.props.changeQuantityProductClicked(id,quantity);
        }
    }

    onDeleteCartItemClickedHandler = (event,id) =>{
        event.preventDefault();
        this.props.onDeleteCartItemClicked(id);
    }

    render() {
        let discount = this.props.discount;
        let money = this.props.price*this.props.quantity;
        if (!this.props.discount){
            discount = 0;
        }else{
            money = this.props.price * (1 - this.props.discount/100) * this.props.quantity;
        }

        return (
            <tr>
                <td>
                    <img width="120px" src={baseUrl + "/" + this.props.image.url}/>
                </td>
                <td>
                    <Link to = {"/product-detail/" + this.props.id} >
                        {this.props.name}
                    </Link>
                </td>
                <td>
                    <input 
                        className = "text-center"
                        type="number" 
                        min= {this.props.min}
                        defaultValue = {this.props.quantity}
                        onChange = {(event)=>this.onChangeQuantityProductHandler(event,this.props.id)}
                        
                    />
                </td>
                <td>{this.props.unitName}</td>
                <td>{formatCurrency(this.props.price)} đ</td>
                
                <td>{discount} %</td>
                <td>
                    {formatCurrency(money)} đ
                </td>
                <td>
                    <button
                        title="Xóa Sản Phẩm"
                        onClick = {(event)=>this.onDeleteCartItemClickedHandler(event, this.props.id)}
                    >
                        <FontAwesomeIcon icon = {faTrash}/>
                    </button>
                </td>
            </tr>
        );
    }
}

export default CartItem;