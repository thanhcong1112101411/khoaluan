import React, { Component } from 'react';

import {ImageProductLink, formatCurrency} from 'Shared/utility';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { baseUrl } from 'Shared/server';

class CartItemMobile extends Component {

    onChangeQuantityProductHandler = (event,id) =>{
        event.preventDefault();
        const quantity = event.target.value;
        // let quantity = event.target.value.replace(/\+|-/ig, '');
        if( quantity < 1 || quantity.toString() === ""){
            this.props.changeQuantityProductClicked(id,1);
        }else{
            this.props.changeQuantityProductClicked(id,quantity);
        }
    }

    onDeleteCartItemClickedHandler = (event,id) =>{
        event.preventDefault();
        this.props.onDeleteCartItemClicked(id);
    }
    render() {
        let discount = this.props.discount;
        let money = this.props.price*this.props.quantity;
        if (!this.props.discount){
            discount = 0;
        }else{
            money = this.props.price * (1 - this.props.discount/100) * this.props.quantity;
        }
        return (
            <div className="cartMobileItem">
                <div className="image">
                    <img width="100%" src={baseUrl + "/" + this.props.image.url}/>
                </div>
                <div className="content">
                    <h1>{this.props.name}</h1>
                    <p className = "money">{formatCurrency(money)} đ</p>
                    <p className = "discount">Giảm giá:  
                        <span className="discount">{discount + "%"}</span>
                    </p>
                    <div className="quantumBtn">
                        <input
                            type="number" 
                            min= {this.props.min}
                            defaultValue = {this.props.quantity}
                            onChange = {(event)=>this.onChangeQuantityProductHandler(event,this.props.id)}
                        />
                        <span>{this.props.unitName}</span>
                    </div>
                </div>
                <div className="deleteBtn">
                    <button
                        title = "Xóa sản phẩm"
                        onClick = {(event)=>this.onDeleteCartItemClickedHandler(event, this.props.id)}
                    >X</button>
                </div>
            </div>
        );
    }
}

export default CartItemMobile;