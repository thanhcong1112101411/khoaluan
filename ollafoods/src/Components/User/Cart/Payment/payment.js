import React, { Component } from 'react';

// ---------------------- CSS -------------------------------
import './payment.css';
import 'Shared/style.css';
import { formatCurrency } from 'Shared/utility';
import { Link } from 'react-router-dom';

class Payment extends Component {
    orderHandler = (event) => {
        event.preventDefault();
        this.props.onOrderServiceHandler();
    }

    render() {
        return (
            <div className="payment" >
                <div className="container">
                    <div className="row">
                        <div className= "col-md-6 offset-md-6">
                            <div className="content">
                                <h1>Thanh Toán</h1>
                                <p>Tổng Tiền:  
                                    <span> {this.props.totalMoney ? formatCurrency(this.props.totalMoney) : 0} đ</span>
                                </p>
                                <button onClick = {this.orderHandler}>Thanh toán</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Payment;