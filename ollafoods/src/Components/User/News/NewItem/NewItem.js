import React, { Component } from 'react';

// -------------------------- CSS -------------------------------
import "./NewItem.css";
import 'Shared/style.css';
import { baseUrl } from 'Shared/server';
import { formatDate, formatDateDDMMYY } from 'Shared/utility';
import { Link } from 'react-router-dom';


class NewItem extends Component {
    render() {
        return (
            <div className="newsItem">
                <Link class="row" to={"/news/"+this.props.id}>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 image">
                        <img src={baseUrl +"/"+ this.props.image} width="100px" />  
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <h2>{this.props.title}</h2>
                        <div dangerouslySetInnerHTML={{__html: this.props.content.substring(0,150) + "..."}} />
                        <p><i>Ngày đăng:{formatDateDDMMYY(this.props.created_at)}</i></p>
                    </div>
                </Link>
            </div>
        );
    }
}

export default NewItem;