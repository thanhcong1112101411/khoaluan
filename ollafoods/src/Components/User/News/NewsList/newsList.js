import React, { Component } from 'react';

// ------------------------ CSS --------------------------
import './newsList.css';
import 'Shared/style.css';

//-------------------------- COMPONENT -------------------------
import NewsItem from '../NewItem/NewItem';



class NewsList extends Component{
    render(){
        const listItem = this.props.list.map((ig, index) => {
            return(
                <NewsItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    title = {ig.title}
                    content = {ig.content}
                    click={ig.click}
                    created_at = {ig.created_at}
                    image = {ig.image}
                />
            );
        })

        return(
            <div className = "newsList">
                <div className = "container">
                    <div className = "col-lg-10 col-md-10 col-sm-12 offset-lg-1 offset-md-1">
                        {listItem}
                    </div>
                </div>
            </div>
        );
    }
    
}

export default NewsList;