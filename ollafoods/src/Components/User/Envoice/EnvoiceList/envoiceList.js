import React, { Component } from 'react';

// ------------------------------- CSS -------------------------------------
import 'Shared/style.css';

// ------------------------------ COMPONENTS -------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Item from './Item/item';
import { socket } from 'Shared/server';

class EnvoiceList extends Component {

    componentDidMount(){
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <Item
                    id = {ig.id}
                    key = {ig.id}
                    code = {ig.code}
                    money = {ig.money}
                    created_at = {ig.created_at}
                    status = {ig.status}
                    detail_url = {this.props.path + "/" + ig.id}
                />  
            )
        })

        return (
            <div className = "container">
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr>
                            <th>Mã đơn hàng</th>
                            <th>Ngày mua</th>
                            <th>Tổng tiền</th>
                            <th>Trạng thái đơn hàng</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default EnvoiceList;