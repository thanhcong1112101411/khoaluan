import React, { Component } from 'react';

import ButtonTable from 'Components/UI/UIType/ButtonTable/buttonTable';
import LinkTable from 'Components/UI/UIType/LinkTable/linkTable';
import { formatDate, formatCurrency, formatDateDDMMYY } from 'Shared/utility';
import {RoleDefault, ExportEnvoiceType} from 'Utils/enum.utils';
import { baseUrl } from 'Shared/server';
import { Link } from 'react-router-dom';


class Item extends Component {
    render() {
        let status = "";
        if (this.props.status == ExportEnvoiceType.WATING){
            status = "Đang chờ xữ lý"
        }else if (this.props.status == ExportEnvoiceType.COMPLETED){
            status = "Thành công"
        }else{
            status = "Đã hủy"
        }
        return (
            <tr>
                <td>
                    <Link to = {this.props.detail_url}>
                        {this.props.code}
                    </Link>
                </td>
                <td>{formatDateDDMMYY(this.props.created_at)}</td>
                <td>{formatCurrency(this.props.money) + " đ"}</td>
                <td>{status}</td>
            </tr>
        );
    }
}

export default Item;