export const UserStatus = {
    ACTIVE : 1,
    INACTIVE : 2,
}
/*----------UserType-----------*/
export const UserType = {
    ADMIN : 1,
    USER : 2,
}

/*----------COUNTRY CODE DEFAULT----------*/
export const CountryCode = 84;

/*---------- RoleDefault ----------*/
export const RoleDefault = {
    ROOT : 1,
    USER : 2,
    NONE: 3,
}
/*----------- social type -------*/
export const SocialType = {
    FB : 1,
    GG : 2,
}
/*------------Reset passowrd--------------*/
export const MailStatus = {
    FORGOT_PASSWORD : 1,
    RESEND_EMAIL : 2,
}

/*---------- Screen type ---------*/
export const ScreenType = {
    OTHER : 1,
    NEWSMANAGEMENT : 2,
    PRODUCTMANAGEMENT : 3,
    ENVOICEMANAGEMENT : 4,
    ACCOUNTMANAGEMENT : 5,
    ROLES : 6,
    REPORTS : 7,
    INTERFACES : 8
}
/*-------------- Address default ---------------*/
export const AddressDefault = {
    cityId: 4,
    districtId: 9,
    wardId: 5341,
}

/*----------- GenderType -------------*/
export const GenderType = {
    MALE : 1,
    FEMALE : 2,
    OTHER : 3,
}

/*------------ Images Type -----------*/
export const Imagetype = {
    BANNER : 1,
    CONTENT_TOP : 2,
    LOGO: 3,
}
/*---------- AccountDefault -------*/
export const AccountDefault = 1;

/*------------ Product Type -----------*/
export const ProductType = {
    ACTIVE : 1,
    WATING : 3,
    INACTIVE: 2,
}

/*----------- ProductStatus -------------*/
export const ProductStatus = {
    ACTIVE : 1,
    INACTIVE : 2,
}

/*------------ Discount Type -----------*/
export const DiscountType = {
    APPLING : 1,
    WATING : 3,
    APPLIED: 2,
    DELETED:4,
}

/*------------ Export Envoice Type -----------*/
export const ExportEnvoiceType = {
    COMPLETED  : 2,
    WATING : 1,
    CANCELED: 3,
}

/*----------- PriceType -------------*/
export const PriceType = {
    IMPORT : 1, // gia nhap
    EXPORT : 2, // gia ban
}