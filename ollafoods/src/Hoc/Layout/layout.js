import React, { Component} from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

import 'Shared/style.css';

import UserHeader from 'Hoc/UserHeader/userHeader';
import UserFooter from 'Hoc/UserFooter/userFooter';
import AdminHeader from 'Hoc/AdminHeader/adminHeader';
import Auxx from 'Hoc/Auxx/auxx';

class layout extends Component{
    componentDidMount(){
        this.props.onFetchLogoImages();
        this.props.onFetchCart();
    }

    render(){
        let page = this.props.path.split("/")[1];
        
        let showAdmin = page == "admin";
        let showUser = page != "admin" && page != "default-page";
        if(this.props.path.split("/")[2] == "login"){
            showAdmin = false;
            showUser = false;
        }
        return(
            <Auxx>
                <AdminHeader 
                    show = {showAdmin}
                    onAdminLogout = {this.props.onAdminLogout}
                    onAdminCheckTimeOut = {this.props.onAdminCheckTimeOut}
                    name = {this.props.admin_name}
                    screens = {this.props.screens}
                    logo = {this.props.logo}
                />
                <UserHeader 
                    show = {showUser}
                    page = {page}
                    isUser = {this.props.isUser}
                    onUserLogout = {this.props.onUserLogout}
                    onUserCheckTimeOut = {this.props.onUserCheckTimeOut}
                    logo = {this.props.logo}
                    quantityProductsCart= {this.props.quantityProductsCart}
                    onfetchProductsUser = {this.props.onfetchProductsUser}

                    list_search = {this.props.list_search}
                    onFetchProductsSearch = {this.props.onFetchProductsSearch}
                />
                <main className = {showAdmin ? "container containerAdmin" : ""}>
                    {this.props.children}
                </main>
                <UserFooter show= {showUser} />
            </Auxx>
        )
    }
}

const mapStateToProps = state =>{
    return{
        path: state.auth.currentPath,
        isUser: state.auth.uuid_user != "",
        admin_name: state.auth.info_admin.first_name,
        screens: state.auth.screens,
        logo: state.image.logo,
        quantityProductsCart: state.cart.quantityProducts,
        list_search: state.products.list_search,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onUserLogout: () => dispatch(actions.userLogout()),
        onUserCheckTimeOut: () => dispatch(actions.userCheckTimeOut()),
        onAdminLogout: () => dispatch(actions.adminLogout()),
        onAdminCheckTimeOut: () => dispatch(actions.adminCheckTimeOut()),
        onFetchLogoImages: () => dispatch(actions.fetchLogoImages()),
        onFetchCart: ()=>dispatch(actions.fetchCart()),
        onfetchProductsUser: (brand, price, catagory, name, current_page) => dispatch(actions.fetchProductsUser(brand, price, catagory, name, current_page)),
        onFetchProductsSearch: (name) => dispatch(actions.fetchProductsSearch(name)),
    };  
};
export default connect(mapStateToProps, mapDispatchToProps)(layout);