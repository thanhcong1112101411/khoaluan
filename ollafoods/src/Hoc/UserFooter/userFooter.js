import React from 'react';

import './userFooter.css';
import 'Shared/style.css';

const userFooter = (props) =>{
    return(
        <footer className = {props.show ? "": "visible"}>
            <div className = "userFooter">
                <div className="container">
                    <div className = "row">
                        <div className="col-md-6 item">
                            <h3>Về OllaFoods</h3>
                            <p>SeasonalFoods là Công ty hoạt động trên các lĩnh vực như: phân phối sỉ và lẻ ngành hàng thực phẩm công nghệ, dịch vụ vận chuyển, cho thuê văn phòng, kho bãi, hợp tác đầu tư</p>
                        </div>
                        <div className="col-md-6 item">
                            <h3>Thông Tin Liên Hệ</h3>
                            <ul>
                                <li>
                                    SĐT: 0999999999999
                                </li>
                                <li>
                                    Email: ollafoods@gmail.com
                                </li>
                                <li>
                                    Địa chỉ: Thành Phố Hồ Chí Minh
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default userFooter;