import React from 'react';
import { 
    faBars, faHome, faUserTie, faHistory, faComment, faUserCircle, faUsers, faDiceD6, 
    faCube, faDollarSign, faMoneyCheckAlt, faEnvelopeOpen, faBroom, faBookmark, 
    faMoneyBillWave, faFax, faCogs, faHouseDamage, faImages, faUserFriends, faEdit, 
    faPenAlt, faUserGraduate, faUserClock, faAngleDown, faNewspaper, faClipboardList, 
    faChartLine, faChartPie, faExternalLinkSquareAlt,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link, NavLink } from 'react-router-dom';

const listFunctionItem  = (props) => {

    let icon = null;
    switch(props.icon){
        case 'faBars':
            icon = (
                <FontAwesomeIcon icon={faBars} />
            );
            break;
        case 'faHome':
            icon = (
                <FontAwesomeIcon icon={faHome} />
            );
            break;
        case 'faUserTie':
            icon = (
                <FontAwesomeIcon icon={faUserTie} />
            );
            break;
        case 'faHistory':
            icon = (
                <FontAwesomeIcon icon={faHistory} />
            );
            break;
        case 'faComment':
            icon = (
                <FontAwesomeIcon icon={faComment} />
            );
            break;
        case 'faUserCircle':
            icon = (
                <FontAwesomeIcon icon={faUserCircle} />
            );
            break;
        case 'faUsers':
            icon = (
                <FontAwesomeIcon icon={faUsers} />
            );
            break;
        case 'faDiceD6':
            icon = (
                <FontAwesomeIcon icon={faDiceD6} />
            );
            break;
        case 'faCube':
            icon = (
                <FontAwesomeIcon icon={faCube} />
            );
            break;
        case 'faDollarSign':
            icon = (
                <FontAwesomeIcon icon={faDollarSign} />
            );
            break;
        case 'faMoneyCheckAlt':
            icon = (
                <FontAwesomeIcon icon={faMoneyCheckAlt} />
            );
            break;
        case 'faEnvelopeOpen':
            icon = (
                <FontAwesomeIcon icon={faEnvelopeOpen} />
            );
            break;
        case 'faBroom':
            icon = (
                <FontAwesomeIcon icon={faBroom} />
            );
            break;
        case 'faBookmark':
            icon = (
                <FontAwesomeIcon icon={faBookmark} />
            );
            break;
        case 'faMoneyBillWave':
            icon = (
                <FontAwesomeIcon icon={faMoneyBillWave} />
            );
            break;
        case 'faFax':
            icon = (
                <FontAwesomeIcon icon={faFax} />
            );
            break;
        case 'faCogs':
            icon = (
                <FontAwesomeIcon icon={faCogs} />
            );
            break;
        case 'faHouseDamage':
            icon = (
                <FontAwesomeIcon icon={faHouseDamage} />
            );
            break;
        case 'faImages':
            icon = (
                <FontAwesomeIcon icon={faImages} />
            );
            break;
        case 'faUserFriends':
            icon = (
                <FontAwesomeIcon icon={faUserFriends} />
            );
            break;
        case 'faEdit':
            icon = (
                <FontAwesomeIcon icon={faEdit} />
            );
            break;
        case 'faPenAlt':
            icon = (
                <FontAwesomeIcon icon={faPenAlt} />
            );
            break;
        case 'faUserGraduate':
            icon = (
                <FontAwesomeIcon icon={faUserGraduate} />
            );
            break;
        case 'faUserClock':
            icon = (
                <FontAwesomeIcon icon={faUserClock} />
            );
            break;
        case 'faAngleDown':
            icon = (
                <FontAwesomeIcon icon={faAngleDown} />
            );
            break;
        case 'faNewspaper':
            icon = (
                <FontAwesomeIcon icon={faNewspaper} />
            );
            break;
        case 'faClipboardList':
            icon = (
                <FontAwesomeIcon icon={faClipboardList} />
            );
            break;
        case 'faChartLine':
            icon = (
                <FontAwesomeIcon icon={faChartLine} />
            );
            break;
        case 'faChartPie':
            icon = (
                <FontAwesomeIcon icon={faChartPie} />
            );
            break;
        case 'faExternalLinkSquareAlt':
            icon = (
                <FontAwesomeIcon icon={faExternalLinkSquareAlt} />
            );
            break;
        default:
            icon = (
                <FontAwesomeIcon icon={faExternalLinkSquareAlt} />
            );
            break;
    }
    return(
        <li key = {props.id}>
            <NavLink 
                to={props.to}
                onClick = {props.toggleListFunctionClick}
                activeClassName="activeMenu"
            >
                <span className="icon">
                    {icon}
                </span>
                <span className = "title">{props.name}</span>
            </NavLink>
        </li>
    );
}

export default listFunctionItem;