import React, {Component} from 'react';

// ---------------------------- CSS ------------------------------
import './listFunction.css';
import 'Shared/style.css';
import { faHome, faUserTie, faDiceD6, faEnvelopeOpen, faCogs, faEdit, faAngleDown, faChartLine} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link, NavLink } from 'react-router-dom';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Auxx from 'Hoc/Auxx/auxx';
import { ScreenType } from 'Utils/enum.utils';
import ListFunctionItem from './ListFunctionItem/listFunctionItem';
import { ImageLogoLink } from 'Shared/server';

class listFunction extends Component{
    render(){
        let other = [];
        let productManagement = [];
        let newsManagement = [];
        let envoiceManagement = [];
        let accountManagement = [];
        let reports = [];
        let interfaces = [];
        let roles = [];

        this.props.screens.forEach(element => {
            if(element.type == ScreenType.OTHER){
                other.push(element);
            }else if (element.type == ScreenType.PRODUCTMANAGEMENT){
                productManagement.push(element);
            }else if (element.type == ScreenType.NEWSMANAGEMENT){
                newsManagement.push(element);
            }else if (element.type == ScreenType.ENVOICEMANAGEMENT){
                envoiceManagement.push(element);
            }else if (element.type == ScreenType.ACCOUNTMANAGEMENT){
                accountManagement.push(element);
            }else if (element.type == ScreenType.REPORTS){
                reports.push(element);
            }else if (element.type == ScreenType.INTERFACES){
                interfaces.push(element);
            }else if (element.type == ScreenType.ROLES){
                roles.push(element);
            }
        });

        const otherList = other.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const newsList = newsManagement.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const productList = productManagement.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const envoiceList = envoiceManagement.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const accountList = accountManagement.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const reportList = reports.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const interfacesList = interfaces.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })
        const roleList = roles.map(ig => {
            return (
                <ListFunctionItem
                    key = {ig.id}
                    id = {ig.id}
                    icon = {ig.icon}
                    to = {"/admin/" + ig.url}
                    name = {ig.name}
                    toggleListFunctionClick = {this.props.toggleListFunctionClick}
                />
            )
        })

        return(
            <Auxx>
            <BackDrop
                show = {this.props.show}
                clicked = {this.props.toggleListFunctionClick}
            />
            <div className = {["listfunction", this.props.show ? "" : "visible"].join(" ")}>
                <ul>
                    {/* -------------------- Dashboard ---------------------------------------- */}
                    <li>
                        <NavLink 
                            to="/admin"
                            exact
                            activeClassName="activeMenu"
                            onClick = {this.props.toggleListFunctionClick}
                        >
                            <span className="icon"><FontAwesomeIcon icon={faHome} /></span>
                            <span className = "title">Dashboard</span>
                        </NavLink>
                    </li>

                    {/* -------------------- OTHERS ---------------------------------------- */}
                    {otherList}

                    {/* -------------------- NEWS LIST ---------------------------------------- */}
                    {newsList}

                    {/* -------------------- PRODUCT LIST ---------------------------------------- */}
                    <li className = {productList.length == 0 ? "visible" : ""}>
                        <NavLink 
                            to= "#"
                        >
                            <span className="icon"><FontAwesomeIcon icon={faDiceD6} /></span>
                            <span className = "title">Sản phẩm</span>
                            <span className = "showArrow"><FontAwesomeIcon icon={faAngleDown} /></span>
                        </NavLink>
                        <ul className = "subMenu">
                            {productList}
                        </ul>
                    </li>

                    {/* -------------------- ENVOICE LIST ---------------------------------------- */}
                    <li className = {envoiceList.length == 0 ? "visible" : ""}>
                        <NavLink 
                            to="#"
                        >
                            <span className="icon"><FontAwesomeIcon icon={faEnvelopeOpen} /></span>
                            <span className = "title">Hóa đơn</span>
                            <span className = "showArrow"><FontAwesomeIcon icon={faAngleDown} /></span>
                        </NavLink>
                        <ul className = "subMenu">
                            {envoiceList}
                        </ul>
                    </li>

                    {/* -------------------- ACCOUNT LIST ---------------------------------------- */}
                    <li className = {accountList.length == 0 ? "visible" : ""}>
                        <NavLink 
                            to="#"
                        >
                            <span className="icon"><FontAwesomeIcon icon={faUserTie} /></span>
                            <span className = "title">Tài khoản</span>
                            <span className = "showArrow"><FontAwesomeIcon icon={faAngleDown} /></span>
                        </NavLink>
                                
                        <ul className = "subMenu">
                            {accountList}
                        </ul>
                    </li>

                    {/* -------------------- Dashboard ---------------------------------------- */}
                    <li className = {roleList.length == 0 ? "visible" : ""}>
                        <NavLink 
                            to="#"
                        >
                            <span className="icon"><FontAwesomeIcon icon={faEdit} /></span>
                            <span className = "title">Quyền quản trị</span>
                            <span className = "showArrow"><FontAwesomeIcon icon={faAngleDown} /></span>
                        </NavLink>
                            <ul className = "subMenu">
                                {roleList}
                            </ul>
                    </li>

                    {/* -------------------- REPORT LIST ---------------------------------------- */}
                    <li className = {reportList.length == 0 ? "visible" : ""}>
                        <NavLink 
                            to="#"
                        >
                            <span className="icon"><FontAwesomeIcon icon={faChartLine} /></span>
                            <span className = "title">Báo cáo</span>
                            <span className = "showArrow"><FontAwesomeIcon icon={faAngleDown} /></span>
                        </NavLink>
                        <ul className = "subMenu">
                            {reportList}
                        </ul>
                    </li>

                    {/* -------------------- INTERFACE LIST ---------------------------------------- */}
                    <li className = {interfacesList.length == 0 ? "visible" : ""}>
                        <NavLink 
                            to="#"
                        >
                            <span className="icon"><FontAwesomeIcon icon={faCogs} /></span>
                            <span className = "title">Giao diện</span>
                            <span className = "showArrow"><FontAwesomeIcon icon={faAngleDown} /></span>
                        </NavLink>
                        <ul className = "subMenu">
                            {interfacesList}
                        </ul>
                    </li>
                </ul>
            </div>
        </Auxx>
        );
    }
}

export default listFunction;