import React, { Component } from 'react';

//-------------------- CSS -----------------------------------
import './adminHeader.css';
import 'Shared/style.css';
//-------------------- COMPONENTS ------------------------------
import { faBars, faHome, faUserTie} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link, NavLink } from 'react-router-dom';
import ListFunction from './ListFunction/listFunction';
import { baseUrl, ImageLogoLink } from 'Shared/server';

class AdminHeader extends Component {
    state = {
        showListFunction : false
    }

    componentDidMount(){
        this.props.onAdminCheckTimeOut();
        
    }

    // set state
    toggleListFunctionClick = ()=>{
        this.setState({showListFunction: !this.state.showListFunction})
    }

    onAdminLogoutHandler = (event) => {
        event.preventDefault();
        this.props.onAdminLogout();
    }

    render() {
        let logo = null;
        if (this.props.logo && this.props.logo.length > 0){
            logo = (
                <Link to="/admin">
                    <img 
                        width="120px" 
                        src= {ImageLogoLink + "logo.png"}
                    />
                </Link>
            )
        }

        return (
            <header className = {this.props.show ? "": "visible"}>
                <div className = "adminHeader">
                    <div className = "container containerAdmin content">
                        <div className="logo">
                            {logo}
                            <button onClick={this.toggleListFunctionClick}><FontAwesomeIcon icon={faBars} /></button>
                        </div>
                        <div className= "menu">
                            <ul>
                                <li>
                                    <Link to=""><FontAwesomeIcon icon={faHome} />
                                        <span>Trang Chủ</span>
                                    </Link>
                                </li>
                                <li className = "avatar">
                                    <FontAwesomeIcon icon={faUserTie} />
                                    <span>{this.props.name}</span>
                                    <div className = "subMenu">
                                        <div></div>
                                        <ul>
                                            <li>
                                                <button
                                                    onClick = {(event) => this.onAdminLogoutHandler(event)}
                                                >Đăng xuất</button>
                                                <button
                                                >Đổi mật khẩu</button>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ListFunction
                    screens = {this.props.screens}
                    show = {this.state.showListFunction}
                    toggleListFunctionClick = {this.toggleListFunctionClick}
                />
            </header>
        );
    }
}

export default AdminHeader;