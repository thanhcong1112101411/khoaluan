import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';

import './navigationItem.css';

class NavigationItem extends Component{
    render(){
        return(
            <li className="NavigationItem"
                onClick = {this.props.toggleMenuHandler}
            >
                <NavLink
                    to={this.props.link}
                    exact={this.props.exact}
                    activeClassName="activeMenu">
                        {this.props.children}
                </NavLink>
            </li>
        );
    }
}

export default NavigationItem;