import React from 'react';

import NavigationItem from './NavigationItem/navigationItem';

const navigationItems = (props) => (
    <ul className="NavigationItems">
        <NavigationItem toggleMenuHandler = {props.toggleMenuHandler} exact link="/" >Trang Chủ</NavigationItem>
        <NavigationItem toggleMenuHandler = {props.toggleMenuHandler} link="/products" >Sản Phẩm</NavigationItem>
        <NavigationItem toggleMenuHandler = {props.toggleMenuHandler} link="/news" >Tin Tức</NavigationItem>
        { props.isUser ? 
            <NavigationItem toggleMenuHandler = {props.toggleMenuHandler} link="/feedbacks" >Phản Hồi</NavigationItem>
            : ""
        }
    </ul>
);

export default navigationItems;