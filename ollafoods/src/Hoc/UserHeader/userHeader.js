import React,{Component} from 'react';
import {Link, Redirect, Router} from 'react-router-dom';
import { connect } from 'react-redux';
import history from '../../history';
import { withRouter, useHistory} from 'react-router';
//import * as actions from '../../Store/Actions/index';
//---------------------------- CSS ----------------------------------------
import './userHeader.css';
import 'Shared/style.css';
//--------------------------- COMPONENTS ----------------------------------
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart, faList, faClock, faPhone, faTimes, faSearch, faCartPlus, faCarAlt, faUser } from '@fortawesome/free-solid-svg-icons';
import NavigationItems from './NavigationItems/navigationItems';
import { baseUrl, ImageLogoLink, ImageProductLink } from 'Shared/server';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';

class UserHeader extends Component{

    state = {
        showSearch: false,
        searchValue: '',
        showSideMenu: false,
        width: 0,
    }

    // contextTypes = {
    //     router: React.PropTypes.func.isRequired
    // };

    componentDidMount(){
        this.props.onUserCheckTimeOut();
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth});
        console.log(window.innerWidth);
    }

    // set state
    toggleSearch = ()=>{
        this.setState({showSearch: !this.state.showSearch})
    }
    toggleShowSideMenu = () => {
        this.setState({showSideMenu: !this.state.showSideMenu});
    }

    // event
    changeSearchFormValue = (event) => {
        const value = event.target.value;
        this.setState({searchValue: value});

        this.props.onfetchProductsUser([], 'DEFAULT', '', value, 1);
        this.props.onFetchProductsSearch(value);
    }

    orderHandler = (event) => {
        event.preventDefault();
        history.push('/products?key=' + this.state.searchValue);
    }

    onUserLogoutHandler = (event) => {
        event.preventDefault();
        this.props.onUserLogout();
    
    }

    render(){
        let logo = null;
        if (this.props.logo && this.props.logo.length > 0){
            logo = (
                <div className="logo">
                    <img 
                        width="150px" 
                        src= {ImageLogoLink + "logo.png"}
                    />
                    
                </div>
            )
        }

        const listItemSearch = this.props.list_search.map(ig => {
            return (
                <Link to = {"/product-detail/" + ig.id}
                    onClick = {this.toggleSearch}
                >
                    <div className = "image">
                        <img width="100%" src= {baseUrl + "/" + JSON.parse(ig.images)[0].url} />
                    </div>
                    <p>{ig.name}</p>
                </Link>
            )
        })

        return(
            <header className = {this.props.show ? "": "visible"}>
                <div className={["userHeader", this.props.page == "" ? "homeHeader":""].join(" ")}>
                    <div className="contentBg"></div>
                    <div className="container content">
                        <div className = "menuIcon"
                            onClick = {this.toggleShowSideMenu}
                        >
                            <FontAwesomeIcon icon = {faList} />
                        </div>

                        {logo}

                        <div className = {["sideMenuBg", !this.state.showSideMenu ? "visible" : ""].join(" ")}>
                            <BackDrop
                                show = {this.state.showSideMenu}
                                clicked = {this.toggleShowSideMenu}
                            />
                            <div className = "sideMenu">
                                <div className = "deleteBtn">
                                    <span
                                        onClick = {this.toggleShowSideMenu}
                                    >X</span>
                                </div>
                                <NavigationItems isUser = {this.props.isUser}/>
                            </div>
                        </div>
                        
                        <div className = "menu">
                            <NavigationItems isUser = {this.props.isUser}/>
                        </div>
                        <div className = "icon">
                            <div className="search">
                                <span className = "iconItem"
                                    onClick = {this.toggleSearch}
                                ><FontAwesomeIcon icon={faSearch} /></span>
                            </div>
                            {
                                this.props.isUser ? 
                                    <div className="userProfile itemMenu">
                                        <div className = "avatar">
                                            <span className = "image">
                                                <img width="100%" src="https://cdn.trackdays.co.uk/cdn-cgi/image/format=auto,fit=contain/products/400/audi-r8-blast-4404-main.jpg" />
                                            </span>
                                            <span className = "userName">Thành Công</span>
                                        </div>
                                        <div className = "subMenu">
                                            <div></div>
                                            <ul>
                                                <li>
                                                    <Link to = "/profile">Tài khoản</Link>
                                                </li>
                                                <li>
                                                    <Link to = "/envoices">Danh sách đơn hàng</Link>
                                                </li>
                                                <li>
                                                    <button
                                                        onClick = {(event) => this.onUserLogoutHandler(event)}
                                                    >
                                                        Đăng xuất
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                :   <div className="profile itemMenu">
                                        <span className = "iconItem"><FontAwesomeIcon icon={faUser} /></span>
                                        <div className = "subMenu">
                                            <div></div>
                                            <ul>
                                                <li>
                                                    <Link to = "/sign-in">Đăng Nhập</Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                            }
                            
                            <div className = "cart">
                                <Link to="/cart">
                                    <span className = "iconItem">
                                        <FontAwesomeIcon icon = {faCartPlus} />
                                    </span>
                                    <span className = "quantity">{this.props.quantityProductsCart}</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                    {
                        this.state.showSearch ?
                        <div className = "searchForm">
                            <div>
                                <form onSubmit={this.orderHandler}>
                                    <input 
                                        type="text"
                                        placeholder = "Tên sản phẩm, danh mục"
                                        onChange = {(event) => this.changeSearchFormValue(event)}
                                    />
                                    <span onClick = {this.orderHandler}>
                                        <FontAwesomeIcon icon={faSearch} />
                                    </span>
                                </form>
                            </div>
                            <div className = {["recomendProduct", this.props.list_search.length == 0 ? "visible" : ""].join(" ")}>
                                {listItemSearch}
                            </div>
                        </div>
                        :''
                    }
                    
                </div>
                
            </header>
        );
    }
}

export default withRouter(UserHeader);