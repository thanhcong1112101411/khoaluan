import React from 'react';
import { Route } from 'react-router-dom';
import Auxx from 'Hoc/Auxx/auxx';

import Dashboard from 'Containers/Admin/Dashboard/dashboard';

import brands from 'Containers/Admin/ProductManagement/Brands/Main/brands';
import catagories from 'Containers/Admin/ProductManagement/Catagories/Main/catagories';
import discounts from 'Containers/Admin/ProductManagement/Discounts/Main/discounts';
import prices from 'Containers/Admin/ProductManagement/Prices/Main/prices';
import products from 'Containers/Admin/ProductManagement/Products/Main/products';
import units from 'Containers/Admin/ProductManagement/Units/Main/units';

import accounts from 'Containers/Admin/AccountManagement/Accounts/Main/accounts';
import users from 'Containers/Admin/AccountManagement/Users/Main/users';

import export_envoice from 'Containers/Admin/EnvoiceManagement/ExportEnvoice/Main/exportEnvoice';

import contents from 'Containers/Admin/Interfaces/Contents/contents';
import images from 'Containers/Admin/Interfaces/Images/images';

import news from 'Containers/Admin/News/Main/news';


import feedbacks from 'Containers/Admin/Others/FeedBacks/feedbacks';
import history from 'Containers/Admin/Others/History/history';


import recomend_system from 'Containers/Admin/Reports/RecomendSystem/recomendSystem';
import public_reports from 'Containers/Admin/Reports/PublicReport/publicReport';
import revenue from 'Containers/Admin/Reports/Revenue/revenue';

import decentralization from 'Containers/Admin/RoleManagement/Decentralization/decentralization';
import roles from 'Containers/Admin/RoleManagement/Roles/Main/roles';

//---------------------------------- INSERT , UPDATE, DETAIL -----------------------------------------------
import roles_update from 'Containers/Admin/RoleManagement/Roles/Update/update';
import roles_insert from 'Containers/Admin/RoleManagement/Roles/Insert/insert';

import accounts_insert from 'Containers/Admin/AccountManagement/Accounts/Insert/insert';
import accounts_detail from 'Containers/Admin/AccountManagement/Accounts/Detail/detail';
import accounts_update from 'Containers/Admin/AccountManagement/Accounts/Update/update';

import users_detail from 'Containers/Admin/AccountManagement/Users/Detail/detail';
import users_update from 'Containers/Admin/AccountManagement/Users/Update/update';

import catagory_update from 'Containers/Admin/ProductManagement/Catagories/Update/update';
import catagory_add from "Containers/Admin/ProductManagement/Catagories/Add/add";

import addNews from 'Containers/Admin/News/Add/addNew';
import newsUpdate from 'Containers/Admin/News/Update/newsUpdate';

import addBrand from 'Containers/Admin/ProductManagement/Brands/Add/addBrand';
import brandsUpdate from 'Containers/Admin/ProductManagement/Brands/Update/brandsUpdate';

import addUnit from 'Containers/Admin/ProductManagement/Units/Add/addUnit';
import unitsUpdate from 'Containers/Admin/ProductManagement/Units/Update/unitsUpdate';

import products_add from 'Containers/Admin/ProductManagement/Products/Add/addProducts';
import products_update from 'Containers/Admin/ProductManagement/Products/Update/update';

import price_detail from 'Containers/Admin/ProductManagement/Prices/Details/detail';
import price_update from 'Containers/Admin/ProductManagement/Prices/Update/update';

import discount_add from 'Containers/Admin/ProductManagement/Discounts/Add/addDiscount';
import discount_detail from 'Containers/Admin/ProductManagement/Discounts/Detail/detail';

import exportEnvoice_detail from 'Containers/Admin/EnvoiceManagement/ExportEnvoice/Detail/detail';

const RouteItem = (props) => {

    let item = null;
    switch(props.component){
        case 'brands':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {brands} />
                    <Route path = {"/admin/" + props.component + "/update/:id"} component= {brandsUpdate} />
                    <Route path = {"/admin/" + props.component + '/insert'} component= {addBrand} />
                </Auxx>
            )
            break;
        case 'catagories':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {catagories} />
                    <Route path = {"/admin/" + props.component + "/update/:id"} component= {catagory_update} />
                    <Route path = {"/admin/" + props.component + "/insert"} exact component= {catagory_add} />
                </Auxx>
            )

            break;
        case 'discounts':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {discounts} />
                    {/* <Route path = {"/admin/" + props.component + "/update"} component= {catagory_update} /> */}
                    <Route path = {"/admin/" + props.component + "/insert"} exact component= {discount_add} />
                    <Route path = {"/admin/" + props.component + "/detail/:id"} component= {discount_detail} />
                </Auxx>
            )
            break;
        case 'prices':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {prices} />
                    <Route path = {"/admin/" + props.component + "/detail/:id"} component= {price_detail} />
                    <Route path = {"/admin/" + props.component + "/update/:id"} component= {price_update} />
                </Auxx>
            )
            break;
        case 'products':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {products} />
                    <Route path = {"/admin/" + props.component + "/insert"} exact component= {products_add} />
                    <Route path = {"/admin/" + props.component + "/update/:id"} component= {products_update} />
                </Auxx>
            )
            break;
        
        case 'units':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {units} />
                    <Route path = {"/admin/" + props.component + "/update/:id"} component= {unitsUpdate} />
                    <Route path = {"/admin/" + props.component + "/insert"} exact component= {addUnit} />
                </Auxx>
            )
            break;

        case 'accounts':
            item = (
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {accounts} />
                    <Route path = {"/admin/" + props.component + "/insert"} component= {accounts_insert} />
                    <Route path = {"/admin/" + props.component + "/detail"} component= {accounts_detail} />
                    <Route path = {"/admin/" + props.component + "/update"} component= {accounts_update} />
                </Auxx>
            )
            break;
        case 'users':
            item = (
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {users} />
                    <Route path = {"/admin/" + props.component + '/detail'} component= {users_detail} />
                    <Route path = {"/admin/" + props.component + '/update'} component= {users_update} />
                </Auxx>
            )
            break;
        
        case 'export_envoice':
            item = (
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {export_envoice} />
                    <Route path = {"/admin/" + props.component + '/detail/:id'} component= {exportEnvoice_detail} />
                </Auxx>
            )
            break;

        case 'contents':
            item = (<Route path = {"/admin/" + props.component} exact component= {contents} />)
            break;
        case 'images':
            item = (
                <Route path = {"/admin/" + props.component} exact component= {images} />
            );
            break;
        
        case 'news':
            item=(
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {news} />
                    <Route path = {"/admin/" + props.component + '/insert'} component= {addNews} />
                    <Route path = {"/admin/" + props.component + '/update'} component= {newsUpdate} />
                </Auxx>
            )
            break;

        case 'feedbacks':
            item = (<Route path = {"/admin/" + props.component} exact component= {feedbacks} />)
            break;

        case 'history':
            item = (<Route path = {"/admin/" + props.component} exact component= {history} />)
            break;

        case 'recomend-system':
            item = (<Route path = {"/admin/" + props.component} exact component= {recomend_system} />)
            break;

        case 'public_reports':
            item = (<Route path = {"/admin/" + props.component} exact component= {public_reports} />)
            break;

        case 'revenue':
            item = ( <Route path = {"/admin/" + props.component} exact component= {revenue} /> )
            break;

        case 'decentralization':
            item = ( <Route path = {"/admin/" + props.component} exact component= {decentralization} /> )
            break;
        case 'roles':
            item = (
                <Auxx>
                    <Route path = {"/admin/" + props.component} exact component= {roles} />
                    <Route path = {"/admin/" + props.component + "/update"} component= {roles_update} />
                    <Route path = {"/admin/" + props.component + "/insert"} component= {roles_insert} />
                </Auxx>
            )
            break;

        default:
            item = (
                <Route path = {"/admin" + props.component} exact component= {Dashboard} />
            )
            break;
            
    }

    return item;
}

export default RouteItem;