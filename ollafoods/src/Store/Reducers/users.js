import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list_active: [],
    count_active: 0,
    load_more_active: false,
    pages_active: 0,

    list_inactive: [],
    count_inactive: 0,
    load_more_inactive: false,
    pages_inactive: 0,

    detail: null,
    
    alert_fetch_active: null,
    alert_fetch_inactive: null,
    alert_get_user_detail: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_ACTIVE_USERS_SUCCESS:
            return updateObject(state,{
                list_active: action.data.data.results,
                count_active: action.data.data.count,
                load_more_active: action.data.data.load_more,
                pages_active: action.data.data.pages,
            });
        case actionTypes.FETCH_ACTIVE_USERS_FAIL:
            return updateObject(state,{
                alert_fetch_active: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_INACTIVE_USERS_SUCCESS:
            return updateObject(state,{
                list_inactive: action.data.data.results,
                count_inactive: action.data.data.count,
                load_more_inactive: action.data.data.load_more,
                pages_inactive: action.data.data.pages,
            });
        case actionTypes.FETCH_INACTIVE_USERS_FAIL:
            return updateObject(state,{
                alert_fetch_inactive: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.GET_USER_DETAIL_SUCCESS:
            return updateObject(state,{
                detail: action.data.data,
            });
        case actionTypes.GET_USER_DETAIL_FAIL:
            return updateObject(state,{
                alert_get_user_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });
        
        default:
            return state;
    }
}
export default reducer;