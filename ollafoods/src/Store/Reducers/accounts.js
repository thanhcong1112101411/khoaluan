import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list_active: [],
    count_active: 0,
    load_more_active: false,
    pages_active: 0,

    list_inactive: [],
    count_inactive: 0,
    load_more_inactive: false,
    pages_inactive: 0,

    detail: null,
    
    alert_fetch_active_accounts: null,
    alert_fetch_inactive_accounts: null,
    alert_change_role_group: null,
    alert_add_account: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_ACTIVE_ACCOUNTS_SUCCESS:
            return updateObject(state,{
                list_active: action.data.data.results,
                count_active: action.data.data.count,
                load_more_active: action.data.data.load_more,
                pages_active: action.data.data.pages,
            });
        case actionTypes.FETCH_ACTIVE_ACCOUNTS_FAIL:
            return updateObject(state,{
                alert_fetch_active_accounts: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_INACTIVE_ACCOUNTS_SUCCESS:
            return updateObject(state,{
                list_inactive: action.data.data.results,
                count_inactive: action.data.data.count,
                load_more_inactive: action.data.data.load_more,
                pages_inactive: action.data.data.pages,
            });
        case actionTypes.FETCH_INACTIVE_ACCOUNTS_FAIL:
            return updateObject(state,{
                alert_fetch_inactive_accounts: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.CHANGE_ROLE_GROUP_SUCCESS:
            return updateObject(state,{
                alert_change_role_group: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.CHANGE_ROLE_GROUP_FAIL:
            return updateObject(state,{
                alert_change_role_group: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_CHANGE_ROLE_GROUP:
            return updateObject(state,{
                alert_change_role_group: null,
            });
        
        case actionTypes.ADD_ACCOUNT_SUCCESS:
            return updateObject(state,{
                alert_add_account: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.ADD_ACCOUNT_FAIL:
            return updateObject(state,{
                alert_add_account: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_ACCOUNT:
            return updateObject(state,{
                alert_add_account: null,
            });
        
        default:
            return state;
    }
}
export default reducer;