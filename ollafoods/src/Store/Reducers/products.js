import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list_waiting: [],
    count_waiting: 0,
    load_more_waiting: false,
    pages_waiting: 0,

    list_selling: [],
    count_selling: 0,
    load_more_selling: false,
    pages_selling: 0,

    list_lock: [],
    count_lock: 0,
    load_more_lock: false,
    pages_lock: 0,

    list_product_user: [],
    count_product_user: 0,
    load_more_product_user: false,
    pages_product_user: 0,

    detail: null,

    list_newest: [],

    catagory_list:null,
    brand_list:null,
    unit_list:null,

    recomend_list_detail: [],

    list_search: [],
    
    alert_fetch_waiting: null,
    alert_fetch_selling: null,
    alert_fetch_lock: null,
    alert_fetch_product_user: null,
    alert_update_product: null,
    alert_add_product: null,
    alert_delete_product: null,
    alert_set_status_product:null,
    alert_get_catagory_name:null,
    alert_get_brand_name:null,
    alert_get_unit_name:null,
    alert_get_products_detail:null,
    alert_fetch_recomend_product_detail: null,
    alert_fetch_top_newest: null,
    alert_fetch_search: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_PRODUCTS_WAITING_SUCCESS:
            return updateObject(state,{
                list_waiting: action.data.data.results,
                count_waiting: action.data.data.count,
                load_more_waiting: action.data.data.load_more,
                pages_waiting: action.data.data.pages,
            });
        case actionTypes.FETCH_PRODUCTS_WAITING_FAIL:
            return updateObject(state,{
                alert_fetch_waiting: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_PRODUCTS_SELLING_SUCCESS:
            return updateObject(state,{
                list_selling: action.data.data.results,
                count_selling: action.data.data.count,
                load_more_selling: action.data.data.load_more,
                pages_selling: action.data.data.pages,
            });
        case actionTypes.FETCH_PRODUCTS_SELLING_FAIL:
            return updateObject(state,{
                alert_fetch_selling: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_PRODUCTS_LOCK_SUCCESS:
            return updateObject(state,{
                list_lock: action.data.data.results,
                count_lock: action.data.data.count,
                load_more_lock: action.data.data.load_more,
                pages_lock: action.data.data.pages,
            });
        case actionTypes.FETCH_PRODUCTS_LOCK_FAIL:
            return updateObject(state,{
                alert_fetch_lock: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.ADD_PRODUCTS_SUCCESS:
            return updateObject(state,{
                alert_add_product: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.ADD_PRODUCTS_FAIL:
            return updateObject(state,{
                alert_add_product: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_PRODUCTS:
            return updateObject(state,{
                alert_add_product: null,
            });

        case actionTypes.UPDATE_PRODUCTS_SUCCESS:
            return updateObject(state,{
                alert_update_product: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.UPDATE_PRODUCTS_FAIL:
            return updateObject(state,{
                alert_update_product: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_UPDATE_PRODUCTS:
            return updateObject(state,{
                alert_update_product: null,
            });

        case actionTypes.SET_STATUS_PRODUCT_SUCCESS:
            return updateObject(state,{
                alert_set_status_product: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.SET_STATUS_PRODUCT_FAIL:
            return updateObject(state,{
                alert_set_status_product: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_SET_STATUS_PRODUCT:
            return updateObject(state,{
                alert_set_status_product: null,
            });
        case actionTypes.GET_CATAGORY_NAME_SUCCESS:
            return updateObject(state,{
                alert_get_catagory_name: null,
                catagory_list:action.data.data,
            });
        case actionTypes.GET_CATAGORY_NAME_FAIL:
            return updateObject(state,{
                alert_get_catagory_name: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.GET_BRAND_NAME_SUCCESS:
            return updateObject(state,{
                alert_get_brand_name: null,
                brand_list:action.data.data,
            });
        case actionTypes.GET_BRAND_NAME_FAIL:
            return updateObject(state,{
                alert_get_brand_name: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.GET_UNIT_NAME_SUCCESS:
            return updateObject(state,{
                alert_get_unit_name: null,
                unit_list:action.data.data,
            });
        case actionTypes.GET_UNIT_NAME_FAIL:
            return updateObject(state,{
                alert_get_unit_name: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.FETCH_PRODUCTS_USER_SUCCESS:
            return updateObject(state,{
                list_product_user: action.data.data.results,
                count_product_user: action.data.data.count,
                load_more_product_user: action.data.data.load_more,
                pages_product_user: action.data.data.pages,
                alert_fetch_product_user: null,
            });
        case actionTypes.FETCH_PRODUCTS_USER_FAIL:
            return updateObject(state,{
                alert_fetch_product_user: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.GET_PRODUCTS_DETAIL_SUCCESS:
            return updateObject(state,{
                detail: action.data.data,
            });
        case actionTypes.GET_PRODUCTS_DETAIL_FAIL:
            return updateObject(state,{
                alert_get_products_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });

         case actionTypes.FETCH_RECOMEND_PRODUCT_DETAIL_SUCCESS:
            return updateObject(state,{
                recomend_list_detail: action.data.data,
            });
        case actionTypes.FETCH_RECOMEND_PRODUCT_DETAIL_FAIL:
            return updateObject(state,{
                alert_fetch_recomend_product_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_TOP_PRODUCTS_NEWEST_SUCCESS:
            return updateObject(state,{
                list_newest: action.data.data,
                alert_fetch_top_newest: null,
            });
        case actionTypes.FETCH_TOP_PRODUCTS_NEWEST_FAIL:
            return updateObject(state,{
                alert_fetch_top_newest: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_PRODUCTS_SEARCH_SUCCESS:
            return updateObject(state,{
                list_search: action.data.data.results,
                alert_fetch_search: null,
            });
        case actionTypes.FETCH_PRODUCTS_SEARCH_FAIL:
            return updateObject(state,{
                alert_fetch_search: {
                    varient: "danger",
                    content: action.error,
                }
            });

        default:
            return state;
    }
}
export default reducer;