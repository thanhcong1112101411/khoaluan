import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list_products: [],
    count_products: 0,
    load_more_products: false,
    pages_products: 0,

    detail: null,

    alert_fetch_products_prices:null,
    alert_add_prices: null,
    alert_get_prices_detail:null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_PRODUCTS_PRICES_SUCCESS:
            return updateObject(state,{
                list_products: action.data.data.results,
                count_products: action.data.data.count,
                load_more_products: action.data.data.load_more,
                pages_products: action.data.data.pages,
            });
        case actionTypes.FETCH_PRODUCTS_PRICES_FAIL:
            return updateObject(state,{
                alert_fetch_products_prices: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.GET_PRICES_DETAIL_SUCCESS:
            return updateObject(state,{
                detail: action.data.data,
            });
        case actionTypes.GET_PRICES_DETAIL_FAIL:
            return updateObject(state,{
                alert_get_prices_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.ADD_PRICES_SUCCESS:
            return updateObject(state,{
                alert_add_prices: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.ADD_PRICES_FAIL:
            return updateObject(state,{
                alert_add_prices: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_PRICES:
            return updateObject(state, {
                alert_add_prices: null,
            });
        default:
            return state;
    }
}
export default reducer;