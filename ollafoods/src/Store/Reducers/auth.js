import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    currentPath: "",
    uuid_admin: "",
    token_admin: "",
    info_admin: [],

    uuid_user: "",
    token_user: "",
    info_user: [],

    screens: [],

    city: [],
    country: [],
    ward: [],
    
    alert_user_login: null,
    alert_user_signup: null,
    alert_admin_login: null,
    alert_reset_password : null,
    alert_change_password_token: null,
    alert_fetch_screens: null,
    alert_set_status_user: null,
    alert_update_user: null,

    error: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.SET_PATH:
            return updateObject(state,{
                currentPath: action.path
            });

        case actionTypes.USER_LOGIN_SUCCESS:
            return updateObject(state,{
                uuid_user : action.data.data.uuid,
                token_user: action.data.data._token,
                info_user : action.data.data,
                alert_user_login: {
                    varient: "success",
                    content: "",
                },
            });
        case actionTypes.USER_LOGIN_FAIL:
            return updateObject(state,{
                alert_user_login: {
                    varient: "danger",
                    content: action.error
                }
            });

        case actionTypes.USER_SIGNUP_SUCCESS:
            return updateObject(state,{
                alert_user_signup: {
                    varient: "success",
                    content: action.data
                },
            });
        case actionTypes.USER_SIGNUP_FAIL:
            return updateObject(state,{
                alert_user_signup: {
                    varient: "danger",
                    content: action.error
                }
            });

        case actionTypes.USER_LOGOUT_SUCCESS:
            return updateObject(state,{
                uuid_user : "",
                token_user : "",
                info_user : [],
            });
        case actionTypes.RESET_PASSSWORD_SUCCESS:
            return updateObject(state,{
                alert_reset_password: {
                    content: action.data,
                    varient: "success",
                }
            });
        case actionTypes.RESET_PASSSWORD_FAIL:
            return updateObject(state,{
                alert_reset_password: {
                    content: action.error,
                    varient: "danger",
                }
            });
        case actionTypes.CHANGE_PASSSWORD_TOKEN_SUCCESS:
            return updateObject(state,{
                alert_change_password_token: {
                    content: action.data,
                    varient: "success",
                }
            });
        case actionTypes.CHANGE_PASSSWORD_TOKEN_FAIL:
            return updateObject(state,{
                alert_change_password_token: {
                    content: action.error,
                    varient: "danger",
                }
            });
         case actionTypes.ADMIN_LOGIN_SUCCESS:
            return updateObject(state,{
                uuid_admin : action.data.data.uuid,
                token_admin: action.data.data._token,
                info_admin : action.data.data,
            });
        case actionTypes.ADMIN_LOGIN_FAIL:
            return updateObject(state,{
                alert_admin_login: {
                    varient: "danger",
                    content: action.error
                }
            });
        case actionTypes.ADMIN_LOGOUT_SUCCESS:
            return updateObject(state,{
                uuid_admin : "",
                token_admin : "",
                info_admin : [],
            });
        case actionTypes.FETCH_SCREENS_SUCCESS:
            return updateObject(state,{
                screens : action.data
            });
        case actionTypes.FETCH_SCREENS_FAIL:
            return updateObject(state,{
                alert_fetch_screens: {
                    varient: "danger",
                    content: action.error
                }
            });

        case actionTypes.SET_STATUS_USER_SUCCESS:
            return updateObject(state,{
                alert_set_status_user: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.SET_STATUS_USER_FAIL:
            return updateObject(state,{
                alert_set_status_user: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_SET_STATUS_USER:
            return updateObject(state,{
                alert_set_status_user: null,
            });

        case actionTypes.GET_CITY_SUCCESS:
            return updateObject(state,{
                city: action.data.data,
                error: null,
            });
        case actionTypes.GET_CITY_FAIL:
            return updateObject(state,{
                error: action.error,
            });

        case actionTypes.GET_DISTRICT_SUCCESS:
            return updateObject(state,{
                district: action.data.data,
                error: null,
            });
        case actionTypes.GET_DISTRICT_FAIL:
            return updateObject(state,{
                error: action.error,
            });
        case actionTypes.GET_WARD_SUCCESS:
            return updateObject(state,{
                ward: action.data.data,
                error: null,
            });
        case actionTypes.GET_WARD_FAIL:
            return updateObject(state,{
                error: action.error,
            });

        case actionTypes.UPDATE_USER_SUCCESS:
            return updateObject(state,{
                alert_update_user: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.UPDATE_USER_FAIL:
            return updateObject(state,{
                alert_update_user: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_UPDATE_USER:
            return updateObject(state,{
                alert_update_user: null,
            });
        
        default:
            return state;
    }
}
export default reducer;