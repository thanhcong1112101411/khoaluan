import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list_waiting: [],
    count_waiting: 0,
    load_more_waiting: false,
    pages_waiting: 0,

    list_completed: [],
    count_completed: 0,
    load_more_completed: false,
    pages_completed: 0,

    list_canceled: [],
    count_canceled: 0,
    load_more_canceled: false,
    pages_canceled: 0,

    list_user: [],

    detail: null,

    // alert_fetch_export_envoice:null,
    alert_fetch_export_envoice_waiting: null,
    alert_fetch_export_envoice_completed: null,
    alert_fetch_export_envoice_canceled: null,
    alert_update_export_envoice: null,
    alert_add_export_envoice: null,
    alert_delete_export_envoice: null,
    alert_set_status: null,
    alert_get_detail: null,
    alert_fetch_envoice_user: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_EXPORT_ENVOICE_WAITING_SUCCESS:
            return updateObject(state,{
                list_waiting: action.data.data.results,
                count_waiting: action.data.data.count,
                load_more_waiting: action.data.data.load_more,
                pages_waiting: action.data.data.pages,
            });
        case actionTypes.FETCH_EXPORT_ENVOICE_WAITING_FAIL:
            return updateObject(state,{
                alert_fetch_export_envoice_waiting: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.FETCH_EXPORT_ENVOICE_CANCELED_SUCCESS:
            return updateObject(state,{
                list_canceled: action.data.data.results,
                count_clist_canceled: action.data.data.count,
                load_more_clist_canceled: action.data.data.load_more,
                pages_clist_canceled: action.data.data.pages,
            });
        case actionTypes.FETCH_EXPORT_ENVOICE_CANCELED_FAIL:
            return updateObject(state,{
                alert_fetch_export_envoice_clist_canceled: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.FETCH_EXPORT_ENVOICE_COMPLETED_SUCCESS:
            return updateObject(state,{
                list_completed: action.data.data.results,
                count_completed: action.data.data.count,
                load_more_completed: action.data.data.load_more,
                pages_completed: action.data.data.pages,
            });
        case actionTypes.FETCH_EXPORT_ENVOICE_COMPLETED_FAIL:
            return updateObject(state,{
                alert_fetch_export_envoice_completed: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.UPDATE_EXPORT_ENVOICE_SUCCESS:
            return updateObject(state,{
                alert_update_export_envoice: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.UPDATE_EXPORT_ENVOICE_FAIL:
            return updateObject(state,{
                alert_update_export_envoice: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_UPDATE_EXPORT_ENVOICE:
            return updateObject(state, {
                alert_update_export_envoice: null,
            });
        case actionTypes.ADD_EXPORT_ENVOICE_SUCCESS:
            return updateObject(state,{
                alert_add_export_envoice: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.ADD_EXPORT_ENVOICE_FAIL:
            return updateObject(state,{
                alert_add_export_envoice: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_EXPORT_ENVOICE:
            return updateObject(state, {
                alert_add_export_envoice: null,
            });

        case actionTypes.DELETE_EXPORT_ENVOICE_SUCCESS:
            return updateObject(state,{
                alert_delete_export_envoice: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.DELETE_EXPORT_ENVOICE_FAIL:
            return updateObject(state,{
                alert_delete_export_envoice: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_DELETE_EXPORT_ENVOICE:
            return updateObject(state, {
                alert_delete_export_envoice: null,
            });

        case actionTypes.SET_STATUS_EXPORT_ENVOICE_SUCCESS:
            return updateObject(state,{
                alert_set_status: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.SET_STATUS_EXPORT_ENVOICE_FAIL:
            return updateObject(state,{
                alert_set_status: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_SET_STATUS_EXPORT_ENVOICE:
            return updateObject(state, {
                alert_set_status: null,
            });

        case actionTypes.GET_DETAIL_EXPORT_ENVOICE_SUCCESS:
            return updateObject(state,{
                detail: action.data.data,
                alert_get_detail: null,
            });
        case actionTypes.GET_DETAIL_EXPORT_ENVOICE_FAIL:
            return updateObject(state,{
                alert_get_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });

         case actionTypes.FETCH_ENVOICE_USER_SUCCESS:
            return updateObject(state,{
                list_user: action.data.data,
                alert_fetch_envoice_user: null,
            });
        case actionTypes.FETCH_ENVOICE_USER_FAIL:
            return updateObject(state,{
                alert_fetch_envoice_user: {
                    varient: "danger",
                    content: action.error,
                }
            });

        default:
            return state;
    }
}
export default reducer;