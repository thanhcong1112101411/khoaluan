import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,
    detail: null,

    alert_update_catagory: null,
    alert_add_catagory: null,
    alert_delete_catagory: null,
    alert_get_detail: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_CATAGORIES_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
            });
        case actionTypes.FETCH_CATAGORIES_FAIL:
            return updateObject(state,{
                alert_fetch_catagories: {
                    varient: "danger",
                    content: action.error,
                }
            });
            case actionTypes.UPDATE_CATAGORY_SUCCESS:
                return updateObject(state,{
                    alert_update_catagory: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.UPDATE_CATAGORY_FAIL:
                return updateObject(state,{
                    alert_update_catagory: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_UPDATE_CATAGORY:
                return updateObject(state, {
                    alert_update_catagory: null,
                });
            case actionTypes.ADD_CATAGORIES_SUCCESS:
                return updateObject(state,{
                    alert_add_catagory: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.ADD_CATAGORIES_FAIL:
                return updateObject(state,{
                    alert_add_catagory: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_ADD_CATAGORIES:
                return updateObject(state, {
                    alert_add_catagory: null,
                });

            case actionTypes.DELETE_CATAGORY_SUCCESS:
                return updateObject(state,{
                    alert_delete_catagory: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.DELETE_CATAGORY_FAIL:
                return updateObject(state,{
                    alert_delete_catagory: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_DELETE_CATAGORY:
                return updateObject(state, {
                    alert_delete_catagory: null,
                });

            case actionTypes.GET_CATAGORY_DETAIL_SUCCESS:
                return updateObject(state,{
                    detail: action.data.data,
                    alert_get_detail: null,
                });
            case actionTypes.GET_CATAGORY_DETAIL_FAIL:
                return updateObject(state,{
                    alert_get_detail: {
                        varient: "danger",
                        content: action.error,
                    }
                });
        default:
            return state;
    }
}
export default reducer;