import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    banner: [],
    content_top: [],
    logo: [],

    alert_add_images: null,
    alert_delete_images: null,
    error: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.ADD_IMAGES_SUCCESS:
            return updateObject(state,{
                alert_add_images: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.ADD_IMAGES_FAIL:
            return updateObject(state,{
                alert_add_images: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_IMAGES:
            return updateObject(state,{
                alert_add_images: null,
            });
        
        case actionTypes.FETCH_BANNER_IMAGES_SUCCESS:
            return updateObject(state,{
                banner: action.data.data,
                error: null,
            });
        case actionTypes.FETCH_BANNER_IMAGES_FAIL:
            return updateObject(state,{
                error: action.error,
            });

        case actionTypes.DELETE_IMAGES_SUCCESS:
            return updateObject(state,{
                alert_delete_images: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.DELETE_IMAGES_FAIL:
            return updateObject(state,{
                alert_delete_images: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_DELETE_IMAGES:
            return updateObject(state,{
                alert_delete_images: null,
            });

        case actionTypes.FETCH_CONTENT_TOP_IMAGES_SUCCESS:
            return updateObject(state,{
                content_top: action.data.data,
                error: null,
            });
        case actionTypes.FETCH_CONTENT_TOP_IMAGES_FAIL:
            return updateObject(state,{
                error: action.error,
            });

        case actionTypes.FETCH_LOGO_IMAGES_SUCCESS:
            return updateObject(state,{
                logo: action.data.data,
                error: null,
            });
        case actionTypes.FETCH_LOGO_IMAGES_FAIL:
            return updateObject(state,{
                error: action.error,
            });
        
        default:
            return state;
    }
}
export default reducer;