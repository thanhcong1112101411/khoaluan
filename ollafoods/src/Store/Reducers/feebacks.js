import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,

    alert_fetch_feedbacks: null,
    alert_add_feedbacks: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_FEEDBACKS_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
                alert_fetch_feedbacks: null,
            });
        case actionTypes.FETCH_FEEDBACKS_FAIL:
            return updateObject(state,{
                alert_fetch_feedbacks: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.ADD_FEEDBACKS_SUCCESS:
            return updateObject(state,{
                alert_add_feedbacks: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.ADD_FEEDBACKS_FAIL:
            return updateObject(state,{
                alert_add_feedbacks: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_FEEDBACKS:
            return updateObject(state,{
                alert_add_feedbacks: null,
            });
        
        default:
            return state;
    }
}
export default reducer;