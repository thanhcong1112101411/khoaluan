import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,

    detail:null,

    list_newest: [],

    image_upload: '',

    // alert_fetch_news:null,
    alert_update_news: null,
    alert_add_news: null,
    alert_delete_news: null,
    alert_get_news_detail: null,
    alert_fetch_top_newest: null,
    alert_upload: null,
    alert_increase: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_NEWS_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
            });
        case actionTypes.FETCH_NEWS_FAIL:
            return updateObject(state,{
                alert_fetch_news: {
                    varient: "danger",
                    content: action.error,
                }
            });
            case actionTypes.UPDATE_NEWS_SUCCESS:
                return updateObject(state,{
                    alert_update_news: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.UPDATE_NEWS_FAIL:
                return updateObject(state,{
                    alert_update_news: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_UPDATE_NEWS:
                return updateObject(state, {
                    alert_update_news: null,
                });
            case actionTypes.ADD_NEWS_SUCCESS:
                return updateObject(state,{
                    alert_add_news: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.ADD_NEWS_FAIL:
                return updateObject(state,{
                    alert_add_news: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_ADD_NEWS:
                return updateObject(state, {
                    alert_add_news: null,
                });

            case actionTypes.DELETE_NEWS_SUCCESS:
                return updateObject(state,{
                    alert_delete_news: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.DELETE_NEWS_FAIL:
                return updateObject(state,{
                    alert_delete_news: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_DELETE_NEWS:
                return updateObject(state, {
                    alert_delete_news: null,
                });
            case actionTypes.GET_NEWS_DETAIL_SUCCESS:
                return updateObject(state,{
                    detail: action.data.data,
                });
            case actionTypes.GET_NEWS_DETAIL_FAIL:
                return updateObject(state,{
                    alert_get_news_detail: {
                        varient: "danger",
                        content: action.error,
                    }
                });

        case actionTypes.FETCH_TOP_NEWS_NEWEST_SUCCESS:
            return updateObject(state,{
                list_newest: action.data.data,
                alert_fetch_top_newest: null,
            });
        case actionTypes.FETCH_TOP_NEWS_NEWEST_FAIL:
            return updateObject(state,{
                alert_fetch_top_newest: {
                    varient: "danger",
                    content: action.error,
                }
            });
        
        case actionTypes.UPLOAD_IMAGE_SUCCESS:
            return updateObject(state,{
                image_upload: action.data.data,
                alert_upload: null,
            });
        case actionTypes.UPLOAD_IMAGE_FAIL:
            return updateObject(state,{
                alert_upload: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_UPLOAD_IMAGE:
            return updateObject(state,{
                alert_upload: null,
            });

        case actionTypes.INCREASE_SEEN_NEWS_SUCCESS:
            return updateObject(state,{
                alert_increase: {
                    varient: "success",
                    content: action.data,
                }
            });
        case actionTypes.INCREASE_SEEN_NEWS_FAIL:
            return updateObject(state,{
                alert_increase: {
                    varient: "danger",
                    content: action.error,
                }
            });
        
        default:
            return state;
    }
}
export default reducer;