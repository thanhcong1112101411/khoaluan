import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    envoice_amount: 0,
    loading: false,

    alert_get_envoice_amount: null,
    alert_analytic_hui: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.GET_ENVOICE_AMOUNT_SUCCESS:
            return updateObject(state,{
                envoice_amount: action.data.data,
                alert_get_envoice_amount: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.GET_ENVOICE_AMOUNT_FAIL:
            return updateObject(state,{
                alert_get_envoice_amount: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_GET_ENVOICE_AMOUNT:
            return updateObject(state,{
                alert_get_envoice_amount: null,
            });

        case actionTypes.ANALYTIC_HUI_START:
            return updateObject(state,{
                alert_analytic_hui: null,
                loading: true,
            });
        case actionTypes.ANALYTIC_HUI_SUCCESS:
            return updateObject(state,{
                loading: false,
                alert_analytic_hui: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.ANALYTIC_HUI_FAIL:
            return updateObject(state,{
                loading: false,
                alert_analytic_hui: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ANALYTIC_HUI:
            return updateObject(state,{
                alert_analytic_hui: null,
            });
        
        default:
            return state;
    }
}
export default reducer;