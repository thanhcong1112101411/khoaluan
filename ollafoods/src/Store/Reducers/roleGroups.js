import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,
    all_screens: [],
    detail: null,
    
    alert_fetch_role_groups: null,
    alert_fetch_all_screens: null,
    alert_add_role_group: null,
    alert_delete_role_group: null,
    alert_update_role_group: null,
    alert_get_role_group_detail: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_ROLE_GROUPS_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
            });
        case actionTypes.FETCH_ROLE_GROUPS_FAIL:
            return updateObject(state,{
                alert_fetch_role_groups: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_ALL_SCREENS_SUCCESS:
            return updateObject(state,{
                all_screens: action.data.data,
            });
        case actionTypes.FETCH_ALL_SCREENS_FAIL:
            return updateObject(state,{
                alert_fetch_all_screens: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.ADD_ROLE_GROUP_SUCCESS:
            return updateObject(state,{
                alert_add_role_group: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.ADD_ROLE_GROUP_FAIL:
            return updateObject(state,{
                alert_add_role_group: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_ROLE_GROUP:
            return updateObject(state, {
                alert_add_role_group: null,
            });
        
        case actionTypes.DELETE_ROLE_GROUP_SUCCESS:
            return updateObject(state,{
                alert_delete_role_group: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.DELETE_ROLE_GROUP_FAIL:
            return updateObject(state,{
                alert_delete_role_group: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_DELETE_ROLE_GROUP:
            return updateObject(state, {
                alert_delete_role_group: null,
            });
        
        case actionTypes.UPDATE_ROLE_GROUP_SUCCESS:
            return updateObject(state,{
                alert_update_role_group: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.UPDATE_ROLE_GROUP_FAIL:
            return updateObject(state,{
                alert_update_role_group: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_UPDATE_ROLE_GROUP:
            return updateObject(state, {
                alert_update_role_group: null,
            });

        case actionTypes.GET_ROLE_GROUP_DETAIL_SUCCESS:
            return updateObject(state,{
                detail : action.data.data,
                alert_get_role_group_detail: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.GET_ROLE_GROUP_DETAIL_FAIL:
            return updateObject(state,{
                alert_get_role_group_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });
        
        default:
            return state;
    }
}
export default reducer;