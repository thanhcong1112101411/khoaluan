import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list_waiting: [],
    count_waiting: 0,
    load_more_waiting: false,
    pages_waiting: 0,

    list_appling: [],
    count_appling: 0,
    load_more_appling: false,
    pages_appling: 0,

    list_applied: [],
    count_applied: 0,
    load_more_applied: false,
    pages_applied: 0,

    list_deleted: [],
    count_deleted: 0,
    load_more_deleted: false,
    pages_deleted: 0,

    detail: null,

    discounts_product :[],
    count_discounts_product: 0,
    load_more_discounts_product: false,
    pages_discounts_product: 0,

    picking_products: [],
    
    alert_fetch_waiting: null,
    alert_fetch_appling: null,
    alert_fetch_applied: null,
    alert_fetch_deleted: null,
    alert_update_discounts: null,
    alert_add_discounts: null,
    alert_delete_discounts: null,
    alert_set_status_discounts:null,
    alert_fetch_discounts_product:null,
    alert_fetch_picking_product: null,
    alert_get_detail: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_DISCOUNTS_WAITING_SUCCESS:
            return updateObject(state,{
                list_waiting: action.data.data.results,
                count_waiting: action.data.data.count,
                load_more_waiting: action.data.data.load_more,
                pages_waiting: action.data.data.pages,
            });
        case actionTypes.FETCH_DISCOUNTS_WAITING_FAIL:
            return updateObject(state,{
                alert_fetch_waiting: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.FETCH_DISCOUNTS_APPLING_SUCCESS:
            return updateObject(state,{
                list_appling: action.data.data.results,
                count_appling: action.data.data.count,
                load_more_appling: action.data.data.load_more,
                pages_appling: action.data.data.pages,
            });
        case actionTypes.FETCH_DISCOUNTS_APPLING_FAIL:
            return updateObject(state,{
                alert_fetch_appling: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.FETCH_DISCOUNTS_APPLIED_SUCCESS:
            return updateObject(state,{
                list_applied: action.data.data.results,
                count_applied: action.data.data.count,
                load_more_applied: action.data.data.load_more,
                pages_applied: action.data.data.pages,
            });
        case actionTypes.FETCH_DISCOUNTS_APPLIED_FAIL:
            return updateObject(state,{
                alert_fetch_applied: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.ADD_DISCOUNTS_SUCCESS:
            return updateObject(state,{
                alert_add_discounts: {
                    varient: "success",
                    content: action.data.message,
                }
            });
        case actionTypes.ADD_DISCOUNTS_FAIL:
            return updateObject(state,{
                alert_add_discounts: {
                    varient: "danger",
                    content: action.error,
                }
            });
        case actionTypes.SET_ALERT_ADD_DISCOUNTS:
            return updateObject(state,{
                alert_add_discounts: null,
            });
        
        case actionTypes.FETCH_PRODUCTS_DISCOUNTS_SUCCESS:
            return updateObject(state,{
                discounts_product: action.data.data.results,
                count_discounts_product: action.data.data.count,
                load_more_discounts_product: action.data.data.load_more,
                pages_discounts_product: action.data.data.pages,
            });
        case actionTypes.FETCH_PRODUCTS_DISCOUNTS_FAIL:
            return updateObject(state,{
                alert_fetch_discounts_product: {
                    varient: "danger",
                    content: action.error,
                }
            });
    
        case actionTypes.FETCH_PRODUCTS_PICKING_DISCOUNTS_SUCCESS:
            return updateObject(state,{
                picking_products: action.data.data,
            });
        case actionTypes.FETCH_PRODUCTS_PICKING_DISCOUNTS_FAIL:
            return updateObject(state,{
                alert_fetch_picking_product: {
                    varient: "danger",
                    content: action.error,
                }
            });

        case actionTypes.GET_DISCOUNTS_DETAIL_SUCCESS:
            return updateObject(state,{
                detail: action.data.data[0],
            });
        case actionTypes.GET_DISCOUNTS_DETAIL_FAIL:
            return updateObject(state,{
                alert_get_detail: {
                    varient: "danger",
                    content: action.error,
                }
            });
        default:
            return state;
    }
}
export default reducer;