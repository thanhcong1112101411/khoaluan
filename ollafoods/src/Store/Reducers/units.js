import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,

    detail: null,

    alert_fetch_units:null,
    alert_update_units: null,
    alert_add_units: null,
    alert_delete_units: null,
    alert_get_detail: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_UNITS_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
            });
        case actionTypes.FETCH_UNITS_FAIL:
            return updateObject(state,{
                alert_fetch_units: {
                    varient: "danger",
                    content: action.error,
                }
            });
            case actionTypes.UPDATE_UNITS_SUCCESS:
                return updateObject(state,{
                    alert_update_units: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.UPDATE_UNITS_FAIL:
                return updateObject(state,{
                    alert_update_units: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_UPDATE_UNITS:
                return updateObject(state, {
                    alert_update_units: null,
                });
            case actionTypes.ADD_UNITS_SUCCESS:
                return updateObject(state,{
                    alert_add_units: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.ADD_UNITS_FAIL:
                return updateObject(state,{
                    alert_add_units: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_ADD_UNITS:
                return updateObject(state, {
                    alert_add_units: null,
                });

            case actionTypes.DELETE_UNITS_SUCCESS:
                return updateObject(state,{
                    alert_delete_units: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.DELETE_UNITS_FAIL:
                return updateObject(state,{
                    alert_delete_units: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_DELETE_UNITS:
                return updateObject(state, {
                    alert_delete_units: null,
                });

            case actionTypes.GET_UNIT_DETAIL_SUCCESS:
                return updateObject(state,{
                    detail: action.data.data,
                    alert_get_detail: null,
                });
            case actionTypes.GET_UNIT_DETAIL_FAIL:
                return updateObject(state,{
                    alert_get_detail: {
                        varient: "danger",
                        content: action.error,
                    }
                });
        default:
            return state;
    }
}
export default reducer;