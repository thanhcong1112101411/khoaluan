import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    cart : [],
    totalMoney : 0,
    error: null,
    quantityProducts: 0,
    //alert
    orderAlert: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_CART_SUCCESS:
            let Money = 0;
            action.data.data.map(key =>{
                console.log("key", key);
                if (key.discountAmount){
                    Money += key.exportPrice * key.quantityProduct * ( 1 - key.discountAmount / 100);
                }else{
                    Money += key.exportPrice * key.quantityProduct;
                }
                
            });
            return updateObject(state,{
                cart: action.data.data,
                totalMoney: Money,
                quantityProducts: action.data.data.length
            });
            
        case actionTypes.FETCH_CART_FAIL:
            return updateObject(state,{
                error: action.error
            });
        case actionTypes.ORDER_SERVICE_SUCCESS:
            return updateObject(state,{
                orderAlert: {
                    varient: "success",
                    content: action.data.message,
                }
            })
        case actionTypes.ORDER_SERVICE_FAIL:
            return updateObject(state,{
                orderAlert: {
                    varient: "danger",
                    content: action.error,
                }
            })
        case actionTypes.SET_ORDER_SERVICE_ALERT:
            return updateObject(state,{
                orderAlert: null,
            })
        default:
            return state;
    }
}
export default reducer;