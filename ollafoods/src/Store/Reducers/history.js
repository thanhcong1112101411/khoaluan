import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,

    alert_fetch_history: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_HISTORY_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
            });
        case actionTypes.FETCH_HISTORY_FAIL:
            return updateObject(state,{
                alert_fetch_history: {
                    varient: "danger",
                    content: action.error,
                }
            });

        
        default:
            return state;
    }
}
export default reducer;