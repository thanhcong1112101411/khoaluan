import * as actionTypes from '../Actions/actionTypes'
import {updateObject} from '../../Shared/utility';

const initialState = {
    list: [],
    count: 0,
    load_more: false,
    pages: 0,

    detail: null,

    alert_update_brands: null,
    alert_add_brands: null,
    alert_delete_brands: null,
    alert_get_detail: null,
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.FETCH_BRANDS_SUCCESS:
            return updateObject(state,{
                list: action.data.data.results,
                count: action.data.data.count,
                load_more: action.data.data.load_more,
                pages: action.data.data.pages,
            });
        case actionTypes.FETCH_BRANDS_FAIL:
            return updateObject(state,{
                alert_fetch_brands: {
                    varient: "danger",
                    content: action.error,
                }
            });
            case actionTypes.UPDATE_BRANDS_SUCCESS:
                return updateObject(state,{
                    alert_update_brands: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.UPDATE_BRANDS_FAIL:
                return updateObject(state,{
                    alert_update_brands: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_UPDATE_BRANDS:
                return updateObject(state, {
                    alert_update_brands: null,
                });
            case actionTypes.ADD_BRANDS_SUCCESS:
                return updateObject(state,{
                    alert_add_brands: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.ADD_BRANDS_FAIL:
                return updateObject(state,{
                    alert_add_brands: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_ADD_BRANDS:
                return updateObject(state, {
                    alert_add_brands: null,
                });

            case actionTypes.DELETE_BRANDS_SUCCESS:
                return updateObject(state,{
                    alert_delete_brands: {
                        varient: "success",
                        content: action.data.message,
                    }
                });
            case actionTypes.DELETE_BRANDS_FAIL:
                return updateObject(state,{
                    alert_delete_brands: {
                        varient: "danger",
                        content: action.error,
                    }
                });
            case actionTypes.SET_ALERT_DELETE_BRANDS:
                return updateObject(state, {
                    alert_delete_brands: null,
                });

            case actionTypes.GET_BRAND_DETAIL_SUCCESS:
                return updateObject(state,{
                    detail: action.data.data,
                    alert_get_detail: null,
                });
            case actionTypes.GET_BRAND_DETAIL_FAIL:
                return updateObject(state,{
                    alert_get_detail: {
                        varient: "danger",
                        content: action.error,
                    }
                });
        default:
            return state;
    }
}
export default reducer;