import * as actionTypes from './actionTypes';
//----------------------------------- FETCH ACTIVE USERS --------------------------------------------------------
export const fetchActiveUsers = (email, name, current_page, token) =>{
	return{
        type: actionTypes.FETCH_ACTIVE_USERS,
        email,
        name,
		current_page,
        token: token,
	};
}

export const fetchActiveUsersSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_ACTIVE_USERS_SUCCESS,
        data: data
	};
}

export const fetchActiveUsersFail = (error) =>{
	return{
		type: actionTypes.FETCH_ACTIVE_USERS_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH INACTIVE USERS --------------------------------------------------------
export const fetchInactiveUsers = (email, name, current_page, token) =>{
	return{
        type: actionTypes.FETCH_INACTIVE_USERS,
        email,
        name,
		current_page,
        token: token,
	};
}

export const fetchInactiveUsersSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_INACTIVE_USERS_SUCCESS,
        data: data
	};
}

export const fetchInactiveUsersFail = (error) =>{
	return{
		type: actionTypes.FETCH_INACTIVE_USERS_FAIL,
        error: error,
	};
}

//----------------------------------- GET USER DETAIL --------------------------------------------------------
export const getUserDetail = (uuid, token) =>{
	return{
        type: actionTypes.GET_USER_DETAIL,
		uuid,
		token,
	};
}

export const getUserDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_USER_DETAIL_SUCCESS,
        data: data
	};
}

export const getUserDetailFail = (error) =>{
	return{
		type: actionTypes.GET_USER_DETAIL_FAIL,
        error: error,
	};
}