import * as actionTypes from './actionTypes';
//----------------------------------- FETCH ROLE GROUPS --------------------------------------------------------
export const fetchRoleGroups = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_ROLE_GROUPS,
        name,
		current_page,
        token: token,
	};
}

export const fetchRoleGroupsSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_ROLE_GROUPS_SUCCESS,
        data: data
	};
}

export const fetchRoleGroupsFail = (error) =>{
	return{
		type: actionTypes.FETCH_ROLE_GROUPS_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH ALL SCREENS --------------------------------------------------------
export const fetchAllScreens = (token) =>{
	return{
        type: actionTypes.FETCH_ALL_SCREENS,
        token: token,
	};
}

export const fetchAllScreensSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_ALL_SCREENS_SUCCESS,
        data: data
	};
}

export const fetchAllScreensFail = (error) =>{
	return{
		type: actionTypes.FETCH_ALL_SCREENS_FAIL,
        error: error,
	};
}
//----------------------------------- ADD ROLE GROUP --------------------------------------------------------
export const addRoleGroup = (data, token) =>{
	return{
		type: actionTypes.ADD_ROLE_GROUP,
		data: data,
        token: token,
	};
}

export const addRoleGroupSuccess = (data) =>{
	return{
		type: actionTypes.ADD_ROLE_GROUP_SUCCESS,
        data: data
	};
}

export const addRoleGroupFail = (error) =>{
	return{
		type: actionTypes.ADD_ROLE_GROUP_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD ROLE GROUP -----------------//
export const setAlertAddRoleGroup = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_ROLE_GROUP,
	};
}

//----------------------------------- DELETE ROLE GROUP --------------------------------------------------------
export const deleteRoleGroup = (id, token) =>{
	return{
		type: actionTypes.DELETE_ROLE_GROUP,
		id: id,
        token: token,
	};
}

export const deleteRoleGroupSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_ROLE_GROUP_SUCCESS,
        data: data
	};
}

export const deleteRoleGroupFail = (error) =>{
	return{
		type: actionTypes.DELETE_ROLE_GROUP_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD ROLE GROUP -----------------//
export const setAlertDeleteRoleGroup = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_ROLE_GROUP,
	};
}

//----------------------------------- UPDATE ROLE GROUP --------------------------------------------------------
export const updateRoleGroup = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_ROLE_GROUP,
		id: id,
		data: data,
        token: token,
	};
}

export const updateRoleGroupSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_ROLE_GROUP_SUCCESS,
        data: data
	};
}

export const updateRoleGroupFail = (error) =>{
	return{
		type: actionTypes.UPDATE_ROLE_GROUP_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD ROLE GROUP -----------------//
export const setAlertUpdateRoleGroup = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_ROLE_GROUP,
	};
}

//----------------------------------- GET ROLE GROUP DETAIL --------------------------------------------------------
export const getRoleGroupDetail = (id, token) =>{
	return{
		type: actionTypes.GET_ROLE_GROUP_DETAIL,
		id: id,
        token: token,
	};
}

export const getRoleGroupDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_ROLE_GROUP_DETAIL_SUCCESS,
        data: data
	};
}

export const getRoleGroupDetailFail = (error) =>{
	return{
		type: actionTypes.GET_ROLE_GROUP_DETAIL_FAIL,
        error: error,
	};
}