import * as actionTypes from './actionTypes';
//----------------------------------- FETCH EXPORT_ENVOICE COMPLETED  --------------------------------------------------------
export const fetchExportEnvoiceCompleted = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_COMPLETED,
        name,
		current_page,
        token: token,
	};
}

export const fetchExportEnvoiceCompletedSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_COMPLETED_SUCCESS,
        data: data
	};
}

export const fetchExportEnvoiceCompletedFail = (error) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_COMPLETED_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH EXPORT_ENVOICE_WAITING --------------------------------------------------------
export const fetchExportEnvoiceWaiting = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_WAITING,
        name,
		current_page,
        token: token,
	};
}

export const fetchExportEnvoiceWaitingSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_WAITING_SUCCESS,
        data: data
	};
}

export const fetchExportEnvoiceWaitingFail = (error) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_WAITING_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH EXPORT_ENVOICE_CANCELED --------------------------------------------------------
export const fetchExportEnvoiceCanceled = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_CANCELED,
        name,
		current_page,
        token: token,
	};
}

export const fetchExportEnvoiceCanceledSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_CANCELED_SUCCESS,
        data: data
	};
}

export const fetchExportEnvoiceCanceledFail = (error) =>{
	return{
		type: actionTypes.FETCH_EXPORT_ENVOICE_CANCELED_FAIL,
        error: error,
	};
}

//----------------------------------- UPDATE EXPORT_ENVOICE --------------------------------------------------------
export const updateExportEnvoice = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_EXPORT_ENVOICE,
		id: id,
		data: data,
        token: token,
	};
}

export const updateExportEnvoiceSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_EXPORT_ENVOICE_SUCCESS,
        data: data
	};
}

export const updateExportEnvoiceFail = (error) =>{
	return{
		type: actionTypes.UPDATE_EXPORT_ENVOICE_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE EXPORT_ENVOICE-----------------//
export const setAlertUpdateExportEnvoice = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_EXPORT_ENVOICE,
	};
}


//--------------- ADD EXPORT_ENVOICE-----------------//

export const addExportEnvoice = (data, token) =>{
	return{
		type: actionTypes.ADD_EXPORT_ENVOICE,
		data: data,
        token: token,
	};
}

export const addExportEnvoiceSuccess = (data) =>{
	return{
		type: actionTypes.ADD_EXPORT_ENVOICE_SUCCESS,
        data: data
	};
}

export const addExportEnvoiceFail = (error) =>{
	return{
		type: actionTypes.ADD_EXPORT_ENVOICE_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD EXPORT_ENVOICE -----------------//
export const setAlertAddExportEnvoice = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_EXPORT_ENVOICE,
	};
}
//----------------------------------- DELETE EXPORT_ENVOICE --------------------------------------------------------
export const deleteExportEnvoice = (id, token) =>{
	return{
		type: actionTypes.DELETE_EXPORT_ENVOICE,
		id: id,
        token: token,
	};
}

export const deleteExportEnvoiceSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_EXPORT_ENVOICE_SUCCESS,
        data: data
	};
}

export const deleteExportEnvoiceFail = (error) =>{
	return{
		type: actionTypes.DELETE_EXPORT_ENVOICE_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE EXPORT_ENVOICE -----------------//
export const setAlertDeleteExportEnvoice = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_EXPORT_ENVOICE,
	};
}

//----------------------------------- SET STATUS EXPORT_ENVOICE --------------------------------------------------------
export const setStatusExportEnvoice = (id, status, token) =>{
	return{
		type: actionTypes.SET_STATUS_EXPORT_ENVOICE,
		id: id,
		status,
        token: token,
	};
}

export const setStatusExportEnvoiceSuccess = (data) =>{
	return{
		type: actionTypes.SET_STATUS_EXPORT_ENVOICE_SUCCESS,
        data: data
	};
}

export const setStatusExportEnvoiceFail = (error) =>{
	return{
		type: actionTypes.SET_STATUS_EXPORT_ENVOICE_FAIL,
        error: error,
	};
}


//--------------- SET ALERT SET STATUS EXPORT_ENVOICE -----------------//
export const setAlertSetStatusExportEnvoice = () =>{
	return{
		type: actionTypes.SET_ALERT_SET_STATUS_EXPORT_ENVOICE,
	};
}

//----------------------------------- GET EXPORT ENVOICE DETAIL --------------------------------------------------------
export const getExportEnvoiceDetail = (id, token) =>{
	return{
        type: actionTypes.GET_DETAIL_EXPORT_ENVOICE,
		id,
		token,
	};
}

export const getExportEnvoiceDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_DETAIL_EXPORT_ENVOICE_SUCCESS,
        data: data
	};
}

export const getExportEnvoiceDetailFail = (error) =>{
	return{
		type: actionTypes.GET_DETAIL_EXPORT_ENVOICE_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH ENVOICE USER  --------------------------------------------------------
export const fetchEnvoiceUser = (uuid, token) =>{
	return{
		type: actionTypes.FETCH_ENVOICE_USER,
        uuid,
        token: token,
	};
}

export const fetchEnvoiceUserSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_ENVOICE_USER_SUCCESS,
        data: data
	};
}

export const fetchEnvoiceUserFail = (error) =>{
	return{
		type: actionTypes.FETCH_ENVOICE_USER_FAIL,
        error: error,
	};
}