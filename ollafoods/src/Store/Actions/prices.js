import * as actionTypes from './actionTypes';
//----------------------------------- FETCH PRODUCT PRICES --------------------------------------------------------
export const fetchProductsPrices = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_PRICES,
        name,
		current_page,
        token: token,
	};
}

export const fetchProductsPricesSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_PRICES_SUCCESS,
        data: data
	};
}

export const fetchProductsPricesFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_PRICES_FAIL,
        error: error,
	};
}

//----------------------------------- GET PRICES DETAIL --------------------------------------------------------
export const getPricesDetail = (id, token) =>{
	return{
        type: actionTypes.GET_PRICES_DETAIL,
		id,
		token,
	};
}

export const getPricesDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_PRICES_DETAIL_SUCCESS,
        data: data
	};
}

export const getPricesDetailFail = (error) =>{
	return{
		type: actionTypes.GET_PRICES_DETAIL_FAIL,
        error: error,
	};
}

//--------------- ADD PRICES-----------------//

export const addPrices = (data, token) =>{
	return{
		type: actionTypes.ADD_PRICES,
		data: data,
        token: token,
	};
}

export const addPricesSuccess = (data) =>{
	return{
		type: actionTypes.ADD_PRICES_SUCCESS,
        data: data
	};
}

export const addPricesFail = (error) =>{
	return{
		type: actionTypes.ADD_PRICES_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD PRICES -----------------//
export const setAlertAddPrices = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_PRICES,
	};
}