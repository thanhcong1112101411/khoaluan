import * as actionTypes from './actionTypes';
//----------------------------------- FETCH Catagories --------------------------------------------------------
export const fetchCatagories = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_CATAGORIES,
        name,
		current_page,
        token: token,
	};
}

export const fetchCatagoriesSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_CATAGORIES_SUCCESS,
        data: data
	};
}

export const fetchCatagoriesFail = (error) =>{
	return{
		type: actionTypes.FETCH_CATAGORIES_FAIL,
        error: error,
	};
}


//----------------------------------- UPDATE CATAGORY --------------------------------------------------------
export const updateCatagory = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_CATAGORY,
		id: id,
		data: data,
        token: token,
	};
}

export const updateCatagorySuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_CATAGORY_SUCCESS,
        data: data
	};
}

export const updateCatagoryFail = (error) =>{
	return{
		type: actionTypes.UPDATE_CATAGORY_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE CATAGORY-----------------//
export const setAlertUpdateCatagory = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_CATAGORY,
	};
}


//--------------- ADD CATAGORY-----------------//

export const addCatagory = (data, token) =>{
	return{
		type: actionTypes.ADD_CATAGORIES,
		data: data,
        token: token,
	};
}

export const addCatagorySuccess = (data) =>{
	return{
		type: actionTypes.ADD_CATAGORIES_SUCCESS,
        data: data
	};
}

export const addCatagoryFail = (error) =>{
	return{
		type: actionTypes.ADD_CATAGORIES_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD CATAGORY -----------------//
export const setAlertAddCatagory = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_CATAGORIES,
	};
}
//----------------------------------- DELETE CATAGORY --------------------------------------------------------
export const deleteCatagory = (id, token) =>{
	return{
		type: actionTypes.DELETE_CATAGORY,
		id: id,
        token: token,
	};
}

export const deleteCatagorySuccess = (data) =>{
	return{
		type: actionTypes.DELETE_CATAGORY_SUCCESS,
        data: data
	};
}

export const deleteCatagoryFail = (error) =>{
	return{
		type: actionTypes.DELETE_CATAGORY_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE CATAGORY -----------------//
export const setAlertDeleteCatagory = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_CATAGORY,
	};
}


//----------------------------------- GET CATAGORY DETAIL --------------------------------------------------------
export const getCatagoryDetail = (id) =>{
	return{
        type: actionTypes.GET_CATAGORY_DETAIL,
		id,
	};
}

export const getCatagoryDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_CATAGORY_DETAIL_SUCCESS,
        data: data
	};
}

export const getCatagoryDetailFail = (error) =>{
	return{
		type: actionTypes.GET_CATAGORY_DETAIL_FAIL,
        error: error,
	};
}
