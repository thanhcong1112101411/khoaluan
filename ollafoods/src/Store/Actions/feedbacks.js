import * as actionTypes from './actionTypes';

//----------------------------------- ADD FEEDBACKS --------------------------------------------------------
export const addFeedbacks = (data, token) =>{
	return{
		type: actionTypes.ADD_FEEDBACKS,
        data,
        token,
	};
}

export const addFeedbacksSuccess = (data) =>{
	return{
		type: actionTypes.ADD_FEEDBACKS_SUCCESS,
        data: data
	};
}

export const addFeedbacksFail = (error) =>{
	return{
		type: actionTypes.ADD_FEEDBACKS_FAIL,
        error: error,
	};
}

export const setAlertAddFeedback = () => {
    return{
        type: actionTypes.SET_ALERT_ADD_FEEDBACKS,
    }
}

//----------------------------------- FETCH HISTORY --------------------------------------------------------
export const fetchFeedbacks = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_FEEDBACKS,
        name,
		current_page,
        token: token,
	};
}

export const fetchFeedbacksSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_FEEDBACKS_SUCCESS,
        data: data
	};
}

export const fetchFeedbacksFail = (error) =>{
	return{
		type: actionTypes.FETCH_FEEDBACKS_FAIL,
        error: error,
	};
}