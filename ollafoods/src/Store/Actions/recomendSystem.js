import * as actionTypes from './actionTypes';
//----------------------------------- GET ENVOICE AMOUNT --------------------------------------------------------
export const getEnvoiceAmount = (date_from, date_to, token) =>{
	return{
		type: actionTypes.GET_ENVOICE_AMOUNT,
        date_from,
        date_to,
        token: token,
	};
}

export const getEnvoiceAmountSuccess = (data) =>{
	return{
		type: actionTypes.GET_ENVOICE_AMOUNT_SUCCESS,
        data: data
	};
}

export const getEnvoiceAmountFail = (error) =>{
	return{
		type: actionTypes.GET_ENVOICE_AMOUNT_FAIL,
        error: error,
	};
}

export const setAlertGetEnvoiceAmount = () =>{
	return{
		type: actionTypes.SET_ALERT_GET_ENVOICE_AMOUNT,
	};
}

//----------------------------------- ANALYTIC HUI --------------------------------------------------------
export const analyticHUI = (data) =>{
	return{
		type: actionTypes.ANALYTIC_HUI,
        data,
	};
}

export const analyticHUIStart = () =>{
	return{
		type: actionTypes.ANALYTIC_HUI_START,
	};
}

export const analyticHUISuccess = (data) =>{
	return{
		type: actionTypes.ANALYTIC_HUI_SUCCESS,
        data: data
	};
}

export const analyticHUIFail = (error) =>{
	return{
		type: actionTypes.ANALYTIC_HUI_FAIL,
        error: error,
	};
}

export const setAlertAnalyticHUI = () =>{
	return{
		type: actionTypes.SET_ALERT_ANALYTIC_HUI,
	};
}