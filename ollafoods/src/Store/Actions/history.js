import * as actionTypes from './actionTypes';
//----------------------------------- FETCH HISTORY --------------------------------------------------------
export const fetchHistory = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_HISTORY,
        name,
		current_page,
        token: token,
	};
}

export const fetchHistorySuccess = (data) =>{
	return{
		type: actionTypes.FETCH_HISTORY_SUCCESS,
        data: data
	};
}

export const fetchHistoryFail = (error) =>{
	return{
		type: actionTypes.FETCH_HISTORY_FAIL,
        error: error,
	};
}