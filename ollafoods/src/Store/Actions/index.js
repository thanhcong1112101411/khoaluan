export{
    setPath,

    userLogin,
    userLoginFail,
    userLoginSuccess,

    userLoginSocial,

    userSignUp,
    userSignUpFail,
    userSignUpSuccess,

    userLogout,
    userLogoutSuccess,

    userCheckTimeOut,

    resetPassword,
    resetPasswordFail,
    resetPasswordSuccess,

    changePasswordToken,
    changePasswordTokenFail,
    changePasswordTokenSuccess,

    adminLogin,
    adminLoginFail,
    adminLoginSuccess,

    adminLogout,
    adminLogoutSuccess,

    adminCheckTimeOut,

    fetchScreensFail, // in login
    fetchScreensSuccess, // in login

    setStatusUser,
    setStatusUserFail,
    setStatusUserSuccess,
    setAlertSetStatusUser,

    getCity,
    getCityFail,
    getCitySuccess,

    getDistrict,
    getDistrictFail,
    getDistrictSuccess,

    getWard,
    getWardFail,
    getWardSuccess,

    updateUser,
    updateUserFail,
    updateUserSuccess,
    setAlertUpdateUser,

} from './auth';

export {
    fetchHistory,
    fetchHistoryFail,
    fetchHistorySuccess,
} from './history';

export {
    fetchRoleGroups,
    fetchRoleGroupsFail,
    fetchRoleGroupsSuccess,

    fetchAllScreens,
    fetchAllScreensFail,
    fetchAllScreensSuccess,

    addRoleGroup,
    addRoleGroupFail,
    addRoleGroupSuccess,
    setAlertAddRoleGroup,

    deleteRoleGroup,
    deleteRoleGroupFail,
    deleteRoleGroupSuccess,
    setAlertDeleteRoleGroup,

    getRoleGroupDetail,
    getRoleGroupDetailFail,
    getRoleGroupDetailSuccess,

    updateRoleGroup,
    updateRoleGroupFail,
    updateRoleGroupSuccess,
    setAlertUpdateRoleGroup,

} from './roleGroups';

export {
    fetchActiveAccounts,
    fetchActiveAccountsFail,
    fetchActiveAccountsSuccess,

    fetchInactiveAccounts,
    fetchInactiveAccountsFail,
    fetchInactiveAccountsSuccess,

    changeRoleGroup,
    changeRoleGroupSuccess,
    changeRoleGroupFail,
    setAlertChangeRoleGroup,

    addAccount,
    addAccountFail,
    addAccountSuccess,
    setAlertAddAccount,

} from './accounts';

export {
    fetchActiveUsers,
    fetchActiveUsersFail,
    fetchActiveUsersSuccess,

    fetchInactiveUsers,
    fetchInactiveUsersFail,
    fetchInactiveUsersSuccess,

    getUserDetail,
    getUserDetailFail,
    getUserDetailSuccess,
} from './users';

export {
    addImages,
    addImagesFail,
    addImagesSuccess,
    setAlertAddImages,

    fetchLogoImages,
    fetchLogoImagesFail,
    fetchLogoImagesSuccess,

    fetchBannerImages,
    fetchBannerImagesFail,
    fetchBannerImagesSuccess,

    fetchContentTopImages,
    fetchContentTopImagesFail,
    fetchContentTopImagesSuccess,

    deleteImages,
    deleteImagesFail,
    deleteImagesSuccess,
    setAlertdeleteImages,
} from './images';

export {
    fetchFeedbacks,
    fetchFeedbacksFail,
    fetchFeedbacksSuccess,

    addFeedbacks,
    addFeedbacksFail,
    addFeedbacksSuccess,
    setAlertAddFeedback,
} from './feedbacks';

export {
    fetchCatagories,
    fetchCatagoriesFail,
    fetchCatagoriesSuccess,

    updateCatagory,
    updateCatagorySuccess,
    updateCatagoryFail,
    setAlertUpdateCatagory,
    
    addCatagory,
    addCatagorySuccess,
    addCatagoryFail,
    setAlertAddCatagory,

    deleteCatagory,
    deleteCatagorySuccess,
    deleteCatagoryFail,
    setAlertDeleteCatagory,

    getCatagoryDetail,
    getCatagoryDetailFail,
    getCatagoryDetailSuccess,
} from './catagories';

export {
    fetchNews,
    fetchNewsFail,
    fetchNewsSuccess,

    addNews,
    addNewsFail,
    addNewsSuccess,
    setAlertAddNews,

    updateNews,
    updateNewsFail,
    updateNewsSuccess,
    setAlertUpdateNews,

    deleteNews,
    deleteNewsFail,
    deleteNewsSuccess,
    setAlertDeleteNews,

    getNewsDetail,
    getNewsDetailFail,
    getNewsDetailSuccess,

    fetchTopNewsNewest,
    fetchTopNewsNewestFail,
    fetchTopNewsNewestSuccess,

    uploadImage,
    uploadImageFail,
    uploadImageSuccess,
    setAlertUploadImage,

    increaseSeenNews,
    increaseSeenNewsFail,
    increaseSeenNewsSuccess,
} from './news';

export {
    fetchBrands,
    fetchBrandsFail,
    fetchBrandsSuccess,

    addBrands,
    addBrandsFail,
    addBrandsSuccess,
    setAlertAddBrands,

    deleteBrands,
    deleteBrandsFail,
    deleteBrandsSuccess,
    setAlertDeleteBrands,

    updateBrands,
    updateBrandsFail,
    updateBrandsSuccess,
    setAlertUpdateBrands,

    getBrandDetail,
    getBrandDetailFail,
    getBrandDetailSuccess,
} from './brands';

export {
    fetchUnits,
    fetchUnitsFail,
    fetchUnitsSuccess,

    addUnits,
    addUnitsFail,
    addUnitsSuccess,
    setAlertAddUnits,

    deleteUnits,
    deleteUnitsFail,
    deleteUnitsSuccess,
    setAlertDeleteUnits,

    updateUnits,
    updateUnitsFail,
    updateUnitsSuccess,
    setAlertUpdateUnits,

    getUnitDetail,
    getUnitDetailFail,
    getUnitDetailSuccess,
} from './units';

export {
    fetchProductsPrices,
    fetchProductsPricesFail,
    fetchProductsPricesSuccess,

    getPricesDetail,
    getPricesDetailFail,
    getPricesDetailSuccess,

    addPrices,
    addPricesFail,
    addPricesSuccess,
    setAlertAddPrices,
} from './prices';

export {
    fetchProductsWaiting,
    fetchProductsWaitingFail,
    fetchProductsWaitingSuccess,

    fetchProductsSelling,
    fetchProductsSellingFail,
    fetchProductsSellingSuccess,

    fetchProductsLock,
    fetchProductsLockFail,
    fetchProductsLockSuccess,

    addProducts,
    addProductsFail,
    addProductsSuccess,
    setAlertAddProducts,

    deleteProducts,
    deleteProductsFail,
    deleteProductsSuccess,
    setAlertDeleteProducts,

    updateProducts,
    updateProductsFail,
    updateProductsSuccess,
    setAlertUpdateProducts,

    setStatusProduct,
    setStatusProductFail,
    setStatusProductSuccess,
    setAlertSetStatusProduct,

    getCatagoryName,
    getCatagoryNameFail,
    getCatagoryNameSuccess,

    getBrandName,
    getBrandNameFail,
    getBrandNameSuccess,

    getUnitName,
    getUnitNameFail,
    getUnitNameSuccess,

    fetchProductsUser,
    fetchProductsUserFail,
    fetchProductsUserSuccess,

    getProductsDetail,
    getProductsDetailFail,
    getProductsDetailSuccess,

    fetchRecomendProductDetail,
    fetchRecomendProductDetailFail,
    fetchRecomendProductDetailSuccess,

    fetchTopProductsNewest,
    fetchTopProductsNewestFail,
    fetchTopProductsNewestSuccess,

    fetchProductsSearch,
    fetchProductsSearchFail,
    fetchProductsSearchSuccess,
} from './products';

export{
    fetchDiscountsAppling,
    fetchDiscountsApplingFail,
    fetchDiscountsApplingSuccess,

    fetchDiscountsApplied,
    fetchDiscountsAppliedFail,
    fetchDiscountsAppliedSuccess,

    fetchDiscountsWaiting,
    fetchDiscountsWaitingFail,
    fetchDiscountsWaitingSuccess,

    fetchDiscountsDeleted,
    fetchDiscountsDeletedFail,
    fetchDiscountsDeletedSuccess,

    addDiscounts,
    addDiscountsFail,
    addDiscountsSuccess,
    setAlertAddDiscounts,

    updateDiscounts,
    updateDiscountsFail,
    updateDiscountsSuccess,

    deleteDiscounts,
    deleteDiscountsFail,
    deleteDiscountsSuccess,

    fetchProductsDiscounts,
    fetchProductsDiscountsFail,
    fetchProductsDiscountsSuccess,

    fetchProductsPickingDiscounts,
    fetchProductsPickingDiscountsFail,
    fetchProductsPickingDiscountsSuccess,

    getDiscountDetail,
    getDiscountDetailFail,
    getDiscountDetailSuccess,
} from './discounts';

export{
    fetchExportEnvoiceWaiting,
    fetchExportEnvoiceWaitingFail,
    fetchExportEnvoiceWaitingSuccess,

    fetchExportEnvoiceCompleted,
    fetchExportEnvoiceCompletedFail,
    fetchExportEnvoiceCompletedSuccess,

    fetchExportEnvoiceCanceled,
    fetchExportEnvoiceCanceledFail,
    fetchExportEnvoiceCanceledSuccess,

    addExportEnvoice,
    addExportEnvoiceFail,
    addExportEnvoiceSuccess,
    setAlertAddExportEnvoice,

    deleteExportEnvoice,
    deleteExportEnvoiceFail,
    deleteExportEnvoiceSuccess,
    setAlertDeleteExportEnvoice,

    updateExportEnvoice,
    updateExportEnvoiceFail,
    updateExportEnvoiceSuccess,
    setAlertUpdateExportEnvoice,

    setStatusExportEnvoice,
    setStatusExportEnvoiceFail,
    setStatusExportEnvoiceSuccess,
    setAlertSetStatusExportEnvoice,

    getExportEnvoiceDetail,
    getExportEnvoiceDetailFail,
    getExportEnvoiceDetailSuccess,

    fetchEnvoiceUser,
    fetchEnvoiceUserFail,
    fetchEnvoiceUserSuccess,
} from './exportEnvoice';

export{
    fetchCart,
    fetchCartSuccess,
    fetchCartFail,
    
    addToCart,

    deleteCartItem,

    changeQuantityProductCart,

    orderService,
    orderServiceSuccess,
    orderServiceFail,
    setOrderServiceAlert
} from './cart';

export {
    getEnvoiceAmount,
    getEnvoiceAmountFail,
    getEnvoiceAmountSuccess,
    setAlertGetEnvoiceAmount,

    analyticHUI,
    analyticHUIFail,
    analyticHUIStart,
    analyticHUISuccess,
    setAlertAnalyticHUI,
} from './recomendSystem';