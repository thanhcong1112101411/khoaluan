import * as actionTypes from './actionTypes';
//----------------------------------- FETCH News --------------------------------------------------------
export const fetchNews = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_NEWS,
        name,
		current_page,
        token: token,
	};
}

export const fetchNewsSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_NEWS_SUCCESS,
        data: data
	};
}

export const fetchNewsFail = (error) =>{
	return{
		type: actionTypes.FETCH_NEWS_FAIL,
        error: error,
	};
}

//----------------------------------- UPDATE NEWS --------------------------------------------------------
export const updateNews = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_NEWS,
		id: id,
		data: data,
        token: token,
	};
}

export const updateNewsSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_NEWS_SUCCESS,
        data: data
	};
}

export const updateNewsFail = (error) =>{
	return{
		type: actionTypes.UPDATE_NEWS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE NEWS-----------------//
export const setAlertUpdateNews = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_NEWS,
	};
}


//--------------- ADD NEWS-----------------//

export const addNews = (data, token) =>{
	return{
		type: actionTypes.ADD_NEWS,
		data: data,
        token: token,
	};
}

export const addNewsSuccess = (data) =>{
	return{
		type: actionTypes.ADD_NEWS_SUCCESS,
        data: data
	};
}

export const addNewsFail = (error) =>{
	return{
		type: actionTypes.ADD_NEWS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD NEWS -----------------//
export const setAlertAddNews = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_NEWS,
	};
}
//----------------------------------- DELETE NEWS --------------------------------------------------------
export const deleteNews = (id, token) =>{
	return{
		type: actionTypes.DELETE_NEWS,
		id: id,
        token: token,
	};
}

export const deleteNewsSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_NEWS_SUCCESS,
        data: data
	};
}

export const deleteNewsFail = (error) =>{
	return{
		type: actionTypes.DELETE_NEWS_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE NEWS -----------------//
export const setAlertDeleteNews = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_NEWS,
	};
}


//----------------------------------- GET NEWS DETAIL --------------------------------------------------------
export const getNewsDetail = (id) =>{
	return{
        type: actionTypes.GET_NEWS_DETAIL,
		id,
	};
}

export const getNewsDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_NEWS_DETAIL_SUCCESS,
        data: data
	};
}

export const getNewsDetailFail = (error) =>{
	return{
		type: actionTypes.GET_NEWS_DETAIL_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH TOP NEWSET --------------------------------------------------------
export const fetchTopNewsNewest = () =>{
	return{
		type: actionTypes.FETCH_TOP_NEWS_NEWEST,
	};
}

export const fetchTopNewsNewestSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_TOP_NEWS_NEWEST_SUCCESS,
        data: data
	};
}

export const fetchTopNewsNewestFail = (error) =>{
	return{
		type: actionTypes.FETCH_TOP_NEWS_NEWEST_FAIL,
        error: error,
	};
}

//----------------------------------- UPLOAD IMAGE --------------------------------------------------------
export const uploadImage = (data, token) =>{
	return{
		type: actionTypes.UPLOAD_IMAGE,
		data,
		token,
	};
}

export const uploadImageSuccess = (data) =>{
	return{
		type: actionTypes.UPLOAD_IMAGE_SUCCESS,
        data: data
	};
}

export const uploadImageFail = (error) =>{
	return{
		type: actionTypes.UPLOAD_IMAGE_FAIL,
        error: error,
	};
}
export const setAlertUploadImage = () => {
    return{
        type: actionTypes.SET_ALERT_UPLOAD_IMAGE,
    }
}

//----------------------------------- INCREASE SEEN NEWS --------------------------------------------------------
export const increaseSeenNews = (id) =>{
	return{
		type: actionTypes.INCREASE_SEEN_NEWS,
        id,
	};
}

export const increaseSeenNewsSuccess = (data) =>{
	return{
		type: actionTypes.INCREASE_SEEN_NEWS_SUCCESS,
        data: data
	};
}

export const increaseSeenNewsFail = (error) =>{
	return{
		type: actionTypes.INCREASE_SEEN_NEWS_FAIL,
        error: error,
	};
}