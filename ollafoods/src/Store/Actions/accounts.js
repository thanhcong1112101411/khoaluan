import * as actionTypes from './actionTypes';
//----------------------------------- FETCH ACTIVE ACCOUNTS --------------------------------------------------------
export const fetchActiveAccounts = (email, name, current_page, token) =>{
	return{
        type: actionTypes.FETCH_ACTIVE_ACCOUNTS,
        email,
        name,
		current_page,
        token: token,
	};
}

export const fetchActiveAccountsSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_ACTIVE_ACCOUNTS_SUCCESS,
        data: data
	};
}

export const fetchActiveAccountsFail = (error) =>{
	return{
		type: actionTypes.FETCH_ACTIVE_ACCOUNTS_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH INACTIVE ACCOUNTS --------------------------------------------------------
export const fetchInactiveAccounts = (email, name, current_page, token) =>{
	return{
        type: actionTypes.FETCH_INACTIVE_ACCOUNTS,
        email,
        name,
		current_page,
        token: token,
	};
}

export const fetchInactiveAccountsSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_INACTIVE_ACCOUNTS_SUCCESS,
        data: data
	};
}

export const fetchInactiveAccountsFail = (error) =>{
	return{
		type: actionTypes.FETCH_INACTIVE_ACCOUNTS_FAIL,
        error: error,
	};
}

//---------------------------------- CHANGE ROLE GROUP ----------------------------------------------------
export const changeRoleGroup = (id, uuid, token) =>{
	return{
        type: actionTypes.CHANGE_ROLE_GROUP,
		uuid,
		id,
		token,
	};
}

export const changeRoleGroupSuccess = (data) =>{
	return{
		type: actionTypes.CHANGE_ROLE_GROUP_SUCCESS,
        data: data
	};
}

export const changeRoleGroupFail = (error) =>{
	return{
		type: actionTypes.CHANGE_ROLE_GROUP_FAIL,
        error: error,
	};
}

export const setAlertChangeRoleGroup = () =>{
	return{
		type: actionTypes.SET_ALERT_CHANGE_ROLE_GROUP,
	};
}

//---------------------------------- ADD ACCOUNT ------------------------------------------------
export const addAccount = (data, token) =>{
    return{
        type: actionTypes.ADD_ACCOUNT,
		data,
		token,
    }
}
export const addAccountSuccess = (data) =>{
	return{
		type: actionTypes.ADD_ACCOUNT_SUCCESS,
        data: data,
	};
};

export const addAccountFail = (error) =>{
	return{
		type: actionTypes.ADD_ACCOUNT_FAIL,
		error: error
	};
};

export const setAlertAddAccount = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_ACCOUNT,
	};
};

