import * as actionTypes from './actionTypes';
//----------------------------------- FETCH Brand --------------------------------------------------------
export const fetchBrands = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_BRANDS,
        name,
		current_page,
        token: token,
	};
}

export const fetchBrandsSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_BRANDS_SUCCESS,
        data: data
	};
}

export const fetchBrandsFail = (error) =>{
	return{
		type: actionTypes.FETCH_BRANDS_FAIL,
        error: error,
	};
}


//----------------------------------- UPDATE CATAGORY --------------------------------------------------------
export const updateBrands = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_BRANDS,
		id: id,
		data: data,
        token: token,
	};
}

export const updateBrandsSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_BRANDS_SUCCESS,
        data: data
	};
}

export const updateBrandsFail = (error) =>{
	return{
		type: actionTypes.UPDATE_BRANDS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE BRANDS-----------------//
export const setAlertUpdateBrands = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_BRANDS,
	};
}


//--------------- ADD BRANDS-----------------//

export const addBrands = (data, token) =>{
	return{
		type: actionTypes.ADD_BRANDS,
		data: data,
        token: token,
	};
}

export const addBrandsSuccess = (data) =>{
	return{
		type: actionTypes.ADD_BRANDS_SUCCESS,
        data: data
	};
}

export const addBrandsFail = (error) =>{
	return{
		type: actionTypes.ADD_BRANDS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD BRANDS -----------------//
export const setAlertAddBrands = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_BRANDS,
	};
}
//----------------------------------- DELETE BRANDS --------------------------------------------------------
export const deleteBrands = (id, token) =>{
	return{
		type: actionTypes.DELETE_BRANDS,
		id: id,
        token: token,
	};
}

export const deleteBrandsSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_BRANDS_SUCCESS,
        data: data
	};
}

export const deleteBrandsFail = (error) =>{
	return{
		type: actionTypes.DELETE_BRANDS_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE BRANDS -----------------//
export const setAlertDeleteBrands = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_BRANDS,
	};
}

//----------------------------------- GET BRAND DETAIL --------------------------------------------------------
export const getBrandDetail = (id) =>{
	return{
        type: actionTypes.GET_BRAND_DETAIL,
		id,
	};
}

export const getBrandDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_BRAND_DETAIL_SUCCESS,
        data: data
	};
}

export const getBrandDetailFail = (error) =>{
	return{
		type: actionTypes.GET_BRAND_DETAIL_FAIL,
        error: error,
	};
}