import * as actionTypes from './actionTypes';
//----------------------------------- FETCH WAITING PRODUCTS --------------------------------------------------------
export const fetchProductsWaiting = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_WAITING,
        name,
		current_page,
        token: token,
	};
}

export const fetchProductsWaitingSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_WAITING_SUCCESS,
        data: data
	};
}

export const fetchProductsWaitingFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_WAITING_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH SELLING PRODUCTS --------------------------------------------------------
export const fetchProductsSelling = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_SELLING,
        name,
		current_page,
        token: token,
	};
}

export const fetchProductsSellingSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_SELLING_SUCCESS,
        data: data
	};
}

export const fetchProductsSellingFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_SELLING_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH LOCK PRODUCTS --------------------------------------------------------
export const fetchProductsLock = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_LOCK,
        name,
		current_page,
        token: token,
	};
}

export const fetchProductsLockSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_LOCK_SUCCESS,
        data: data
	};
}

export const fetchProductsLockFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_LOCK_FAIL,
        error: error,
	};
}

//----------------------------------- GET CATAGORY NAME --------------------------------------------------------
export const getCatagoryName = () =>{
	return{
		type: actionTypes.GET_CATAGORY_NAME,
	};
}

export const getCatagoryNameSuccess = (data) =>{
	
	return{
		type: actionTypes.GET_CATAGORY_NAME_SUCCESS,
        data: data
	};
}

export const getCatagoryNameFail = (error) =>{
	return{
		type: actionTypes.GET_CATAGORY_NAME_FAIL,
        error: error,
	};
}

//----------------------------------- GET BRAND NAME --------------------------------------------------------
export const getBrandName = () =>{
	return{
		type: actionTypes.GET_BRAND_NAME,
	};
}

export const getBrandNameSuccess = (data) =>{
	
	return{
		type: actionTypes.GET_BRAND_NAME_SUCCESS,
        data: data
	};
}

export const getBrandNameFail = (error) =>{
	return{
		type: actionTypes.GET_CATAGORY_NAME_FAIL,
        error: error,
	};
}

//----------------------------------- GET UNIT NAME --------------------------------------------------------
export const getUnitName = () =>{
	return{
		type: actionTypes.GET_UNIT_NAME,
	};
}

export const getUnitNameSuccess = (data) =>{
	
	return{
		type: actionTypes.GET_UNIT_NAME_SUCCESS,
        data: data
	};
}

export const getUnitNameFail = (error) =>{
	return{
		type: actionTypes.GET_UNIT_NAME_FAIL,
        error: error,
	};
}

//----------------------------------- UPDATE PRODUCTS --------------------------------------------------------
export const updateProducts = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_PRODUCTS,
		id: id,
		data: data,
        token: token,
	};
}

export const updateProductsSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_PRODUCTS_SUCCESS,
        data: data
	};
}

export const updateProductsFail = (error) =>{
	return{
		type: actionTypes.UPDATE_PRODUCTS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE PRODUCTS-----------------//
export const setAlertUpdateProducts = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_PRODUCTS,
	};
}


//--------------- ADD PRODUCTS-----------------//

export const addProducts = (data, token) =>{
	return{
		type: actionTypes.ADD_PRODUCTS,
		data: data,
        token: token,
	};
}

export const addProductsSuccess = (data) =>{
	return{
		type: actionTypes.ADD_PRODUCTS_SUCCESS,
        data: data
	};
}

export const addProductsFail = (error) =>{
	return{
		type: actionTypes.ADD_PRODUCTS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD Products -----------------//
export const setAlertAddProducts = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_PRODUCTS,
	};
}
//----------------------------------- DELETE PRODUCTS --------------------------------------------------------
export const deleteProducts = (id, token) =>{
	return{
		type: actionTypes.DELETE_PRODUCTS,
		id: id,
        token: token,
	};
}

export const deleteProductsSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_PRODUCTS_SUCCESS,
        data: data
	};
}

export const deleteProductsFail = (error) =>{
	return{
		type: actionTypes.DELETE_PRODUCTS_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE PRODUCTS -----------------//
export const setAlertDeleteProducts = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_PRODUCTS,
	};
}

//---------------------------------- SET STATUS PRODUCT ------------------------------------------------
export const setStatusProduct = (id, token) =>{
    return{
        type: actionTypes.SET_STATUS_PRODUCT,
		id,
		token,
    }
}
export const setStatusProductSuccess = (data) =>{
	return{
		type: actionTypes.SET_STATUS_PRODUCT_SUCCESS,
        data: data,
	};
};

export const setStatusProductFail = (error) =>{
	return{
		type: actionTypes.SET_STATUS_PRODUCT_FAIL,
		error: error
	};
};

export const setAlertSetStatusProduct = () =>{
	return{
		type: actionTypes.SET_ALERT_SET_STATUS_PRODUCT,
	};
};

//---------------------------------- SET STATUS LOCK PRODUCT ------------------------------------------------
export const setStatusLockProduct = (id, token) =>{
    return{
        type: actionTypes.SET_STATUS_LOCK_PRODUCT,
		id,
		token,
    }
}
export const setStatusLockProductSuccess = (data) =>{
	return{
		type: actionTypes.SET_STATUS_LOCK_PRODUCT_SUCCESS,
        data: data,
	};
};

export const setStatusLockProductFail = (error) =>{
	return{
		type: actionTypes.SET_STATUS_LOCK_PRODUCT_FAIL,
		error: error
	};
};

export const setAlertSetStatusLockProduct = () =>{
	return{
		type: actionTypes.SET_ALERT_SET_STATUS_LOCK_PRODUCT,
	};
};


//----------------------------------- FETCH  PRODUCTS USER -------------------------------------
export const fetchProductsUser = (brand, price, catagory, name, current_page) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_USER,
		brand,
		price,
		catagory,
		name,
		current_page,
	};
}

export const fetchProductsUserSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_USER_SUCCESS,
        data: data
	};
}

export const fetchProductsUserFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_USER_FAIL,
        error: error,
	};
}

//------------------------------- GET PRODUCT DETAIL --------------------------------------------

export const getProductsDetail = (id) =>{
	return{
        type: actionTypes.GET_PRODUCTS_DETAIL,
		id,
	};
}

export const getProductsDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_PRODUCTS_DETAIL_SUCCESS,
        data: data
	};
}

export const getProductsDetailFail = (error) =>{
	return{
		type: actionTypes.GET_PRODUCTS_DETAIL_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH RECOMEND PRODUCTS DETAIL -------------------------------------
export const fetchRecomendProductDetail = (id) =>{
	return{
		type: actionTypes.FETCH_RECOMEND_PRODUCT_DETAIL,
		id,
	};
}

export const fetchRecomendProductDetailSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_RECOMEND_PRODUCT_DETAIL_SUCCESS,
        data: data
	};
}

export const fetchRecomendProductDetailFail = (error) =>{
	return{
		type: actionTypes.FETCH_RECOMEND_PRODUCT_DETAIL_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH TOP NEWSET --------------------------------------------------------
export const fetchTopProductsNewest = () =>{
	return{
		type: actionTypes.FETCH_TOP_PRODUCTS_NEWEST,
	};
}

export const fetchTopProductsNewestSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_TOP_PRODUCTS_NEWEST_SUCCESS,
        data: data
	};
}

export const fetchTopProductsNewestFail = (error) =>{
	return{
		type: actionTypes.FETCH_TOP_PRODUCTS_NEWEST_FAIL,
        error: error,
	};
}
//----------------------------------- FETCH  PRODUCTS SEARCH -------------------------------------
export const fetchProductsSearch = (name) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_SEARCH,
		name,
	};
}

export const fetchProductsSearchSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_SEARCH_SUCCESS,
        data: data
	};
}

export const fetchProductsSearchFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_SEARCH_FAIL,
        error: error,
	};
}