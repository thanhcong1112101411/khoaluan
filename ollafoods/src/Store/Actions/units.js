import * as actionTypes from './actionTypes';
//----------------------------------- FETCH UNITS --------------------------------------------------------
export const fetchUnits = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_UNITS,
        name,
		current_page,
        token: token,
	};
}

export const fetchUnitsSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_UNITS_SUCCESS,
        data: data
	};
}

export const fetchUnitsFail = (error) =>{
	return{
		type: actionTypes.FETCH_UNITS_FAIL,
        error: error,
	};
}


//----------------------------------- UPDATE CATAGORY --------------------------------------------------------
export const updateUnits = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_UNITS,
		id: id,
		data: data,
        token: token,
	};
}

export const updateUnitsSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_UNITS_SUCCESS,
        data: data
	};
}

export const updateUnitsFail = (error) =>{
	return{
		type: actionTypes.UPDATE_UNITS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE UNITS-----------------//
export const setAlertUpdateUnits = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_UNITS,
	};
}


//--------------- ADD UNITS-----------------//

export const addUnits = (data, token) =>{
	return{
		type: actionTypes.ADD_UNITS,
		data: data,
        token: token,
	};
}

export const addUnitsSuccess = (data) =>{
	return{
		type: actionTypes.ADD_UNITS_SUCCESS,
        data: data
	};
}

export const addUnitsFail = (error) =>{
	return{
		type: actionTypes.ADD_UNITS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD UNITS -----------------//
export const setAlertAddUnits = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_UNITS,
	};
}
//----------------------------------- DELETE UNITS --------------------------------------------------------
export const deleteUnits = (id, token) =>{
	return{
		type: actionTypes.DELETE_UNITS,
		id: id,
        token: token,
	};
}

export const deleteUnitsSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_UNITS_SUCCESS,
        data: data
	};
}

export const deleteUnitsFail = (error) =>{
	return{
		type: actionTypes.DELETE_UNITS_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE UNITS -----------------
export const setAlertDeleteUnits = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_UNITS,
	};
}

//----------------------------------- GET UNIT DETAIL --------------------------------------------------------
export const getUnitDetail = (id) =>{
	return{
        type: actionTypes.GET_UNIT_DETAIL,
		id,
	};
}

export const getUnitDetailSuccess = (data) =>{
	return{
		type: actionTypes.GET_UNIT_DETAIL_SUCCESS,
        data: data
	};
}

export const getUnitDetailFail = (error) =>{
	return{
		type: actionTypes.GET_UNIT_DETAIL_FAIL,
        error: error,
	};
}