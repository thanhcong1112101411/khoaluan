import * as actionTypes from './actionTypes';

//---------------------------------------- ADD TO CART --------------------------------------
export const addToCart = (id, quantity) =>{
    return{
        type: actionTypes.ADD_TO_CART,
        id: id,
        quantity,
    }
}
//---------------------------------------- FETCH CART --------------------------------------
export const fetchCart = () =>{
    return{
        type: actionTypes.FETCH_CART
    }
}
export const fetchCartSuccess = (data) =>{
    return{
        type: actionTypes.FETCH_CART_SUCCESS,
        data: data,
    }
}
export const fetchCartFail = (error) =>{
    return{
        type: actionTypes.FETCH_CART_FAIL,
        error: error
    }
}
//---------------------------------------- DELETE CART ITEM --------------------------------------
export const deleteCartItem = (id) =>{
    return{
        type: actionTypes.DELETE_CART_ITEM,
        id: id
    }
}
//---------------------------------------- CHANGE QUANTITY PRODUCT --------------------------------------
export const changeQuantityProductCart = (id,quantity) =>{
    return{
        type: actionTypes.CHANGE_QUANTITY_PRODUCT_CART,
        id: id,
        quantity: quantity
    }
}
//------------------------------------------ ORDER SERVICE ----------------------------------------
export const orderService = (uuid, token, data) =>{
    return{
        type: actionTypes.ORDER_SERVICE,
        uuid,
        token,
        data,
    }
}
export const orderServiceSuccess = (data) =>{
    return{
        type: actionTypes.ORDER_SERVICE_SUCCESS,
        data: data,
    }
}
export const orderServiceFail = (error) =>{
    return{
        type: actionTypes.ORDER_SERVICE_FAIL,
        error: error
    }
}
// set order service alert
export const setOrderServiceAlert = () =>{
    return{
        type: actionTypes.SET_ORDER_SERVICE_ALERT
    }
}