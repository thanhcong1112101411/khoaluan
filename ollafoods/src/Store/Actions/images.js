import * as actionTypes from './actionTypes';
//----------------------------------- ADD IMAGES --------------------------------------------------------
export const addImages = (data, token) =>{
	return{
		type: actionTypes.ADD_IMAGES,
		data,
        token: token,
	};
}

export const addImagesSuccess = (data) =>{
	return{
		type: actionTypes.ADD_IMAGES_SUCCESS,
        data: data
	};
}

export const addImagesFail = (error) =>{
	return{
		type: actionTypes.ADD_IMAGES_FAIL,
        error: error,
	};
}

export const setAlertAddImages = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_IMAGES,
	};
}

//----------------------------------- FETCH BANNER IMAGES --------------------------------------------------------
export const fetchBannerImages = () =>{
	return{
		type: actionTypes.FETCH_BANNER_IMAGES,
	};
}

export const fetchBannerImagesSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_BANNER_IMAGES_SUCCESS,
        data: data
	};
}

export const fetchBannerImagesFail = (error) =>{
	return{
		type: actionTypes.FETCH_BANNER_IMAGES_FAIL,
        error: error,
	};
}

//----------------------------------- DELETE IMAGES --------------------------------------------------------
export const deleteImages = (id, token) =>{
	return{
		type: actionTypes.DELETE_IMAGES,
		id,
        token,
	};
}

export const deleteImagesSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_IMAGES_SUCCESS,
        data: data
	};
}

export const deleteImagesFail = (error) =>{
	return{
		type: actionTypes.DELETE_IMAGES_FAIL,
        error: error,
	};
}

export const setAlertdeleteImages = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_IMAGES,
	};
}

//----------------------------------- FETCH CONTENT TOP IMAGES --------------------------------------------------------
export const fetchContentTopImages = () =>{
	return{
		type: actionTypes.FETCH_CONTENT_TOP_IMAGES,
	};
}

export const fetchContentTopImagesSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_CONTENT_TOP_IMAGES_SUCCESS,
        data: data
	};
}

export const fetchContentTopImagesFail = (error) =>{
	return{
		type: actionTypes.FETCH_CONTENT_TOP_IMAGES_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH LOGO IMAGES --------------------------------------------------------
export const fetchLogoImages = () =>{
	return{
		type: actionTypes.FETCH_LOGO_IMAGES,
	};
}

export const fetchLogoImagesSuccess = (data) =>{
	return{
		type: actionTypes.FETCH_LOGO_IMAGES_SUCCESS,
        data: data
	};
}

export const fetchLogoImagesFail = (error) =>{
	return{
		type: actionTypes.FETCH_LOGO_IMAGES_FAIL,
        error: error,
	};
}