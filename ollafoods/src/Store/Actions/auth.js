import * as actionTypes from './actionTypes';
//----------------------------------- SET PATH --------------------------------------------------------
export const setPath = (path) =>{
	return{
		type: actionTypes.SET_PATH,
		path: path
	};
}

//---------------------------------- USER LOGIN ------------------------------------------------
export const userLogin = (username, password) =>{
    return{
        type: actionTypes.USER_LOGIN,
        username: username,
        password: password
    }
}
export const userLoginSuccess = (data) =>{
	return{
		type: actionTypes.USER_LOGIN_SUCCESS,
        data: data
	};
};

export const userLoginFail = (error) =>{
	return{
		type: actionTypes.USER_LOGIN_FAIL,
		error: error
	};
};

//---------------------------------- USER LOGIN SOCIAL ------------------------------------------------
export const userLoginSocial = (data) =>{
    return{
        type: actionTypes.USER_LOGIN_SOCIAL,
        data,
    }
}

//---------------------------------- USER LOGOUT ------------------------------------------------
export const userLogout = ()=>{
    return{
        type: actionTypes.USER_LOGOUT
    }
}
export const userLogoutSuccess = () =>{
    return{
        type: actionTypes.USER_LOGOUT_SUCCESS
    }
}
//--------------------------------- CHECK TIME OUT --------------------------------------------
export const userCheckTimeOut = () =>{
    return{
        type: actionTypes.USER_CHECK_TIMEOUT
    }
}
//---------------------------------- SIGN UP ------------------------------------------------
export const userSignUp = (data) =>{
    return{
        type: actionTypes.USER_SIGNUP,
        data: data,
    }
}
export const userSignUpSuccess = (data) =>{
	return{
		type: actionTypes.USER_SIGNUP_SUCCESS,
        data: data,
	};
};

export const userSignUpFail = (error) =>{
	return{
		type: actionTypes.USER_SIGNUP_FAIL,
		error: error
	};
};
//--------------------------------- RESET PASSWORD --------------------------------------------
export const resetPassword = (email) =>{
    return{
        type: actionTypes.RESET_PASSSWORD,
        email: email,
    }
}
export const resetPasswordSuccess = (data) =>{
	return{
		type: actionTypes.RESET_PASSSWORD_SUCCESS,
        data: data,
	};
};

export const resetPasswordFail = (error) =>{
	return{
		type: actionTypes.RESET_PASSSWORD_FAIL,
		error: error
	};
};
//------------------------------- CHANGE PASSWORD TOKEN ---------------------------------------
export const changePasswordToken = (password, repassword, token) =>{
    return{
        type: actionTypes.CHANGE_PASSSWORD_TOKEN,
        password: password,
        repassword: repassword,
        token: token,
    }
}
export const changePasswordTokenSuccess = (data) =>{
	return{
		type: actionTypes.CHANGE_PASSSWORD_TOKEN_SUCCESS,
        data: data,
	};
};

export const changePasswordTokenFail = (error) =>{
	return{
		type: actionTypes.CHANGE_PASSSWORD_TOKEN_FAIL,
		error: error
	};
};

//---------------------------------- ADMIN LOGIN ------------------------------------------------
export const adminLogin = (username, password) =>{
    return{
        type: actionTypes.ADMIN_LOGIN,
        username: username,
        password: password
    }
}
export const adminLoginSuccess = (data) =>{
	return{
		type: actionTypes.ADMIN_LOGIN_SUCCESS,
        data: data
	};
};

export const adminLoginFail = (error) =>{
	return{
		type: actionTypes.ADMIN_LOGIN_FAIL,
		error: error
	};
};

//---------------------------------- ADMIN LOGOUT ------------------------------------------------
export const adminLogout = ()=>{
    return{
        type: actionTypes.ADMIN_LOGOUT
    }
}
export const adminLogoutSuccess = () =>{
    return{
        type: actionTypes.ADMIN_LOGOUT_SUCCESS
    }
}

//--------------------------------- ADMIN CHECK TIME OUT --------------------------------------------
export const adminCheckTimeOut = () =>{
    return{
        type: actionTypes.ADMIN_CHECK_TIMEOUT
    }
}

//---------------------------------- FETCH SCREENS --------------------------------------------------

export const fetchScreensSuccess = (data) =>{
    return{
        type: actionTypes.FETCH_SCREENS_SUCCESS,
        data: data,
    }
}
export const fetchScreensFail = (error) =>{
    return{
        type: actionTypes.FETCH_SCREENS_FAIL,
        error: error,
    }
}

//---------------------------------- SET STATUS USER ------------------------------------------------
export const setStatusUser = (uuid, token) =>{
    return{
        type: actionTypes.SET_STATUS_USER,
		uuid,
		token,
    }
}
export const setStatusUserSuccess = (data) =>{
	return{
		type: actionTypes.SET_STATUS_USER_SUCCESS,
        data: data,
	};
};

export const setStatusUserFail = (error) =>{
	return{
		type: actionTypes.SET_STATUS_USER_FAIL,
		error: error
	};
};

export const setAlertSetStatusUser = () =>{
	return{
		type: actionTypes.SET_ALERT_SET_STATUS_USER,
	};
};

//---------------------------------- GET CITY ------------------------------------------------
export const getCity  = () =>{
    return{
        type: actionTypes.GET_CITY,
    }
}
export const getCitySuccess = (data) =>{
	return{
		type: actionTypes.GET_CITY_SUCCESS,
        data: data,
	};
};

export const getCityFail = (error) =>{
	return{
		type: actionTypes.GET_CITY_FAIL,
		error: error
	};
};

//---------------------------------- GET COUNTRY ------------------------------------------------
export const getDistrict  = (id) =>{
    return{
        type: actionTypes.GET_DISTRICT,
        id,
    }
}
export const getDistrictSuccess = (data) =>{
	return{
		type: actionTypes.GET_DISTRICT_SUCCESS,
        data: data,
	};
};

export const getDistrictFail = (error) =>{
	return{
		type: actionTypes.GET_DISTRICT_FAIL,
		error: error
	};
};

//---------------------------------- GET WARD ------------------------------------------------
export const getWard  = (id) =>{
    return{
        type: actionTypes.GET_WARD,
        id,
    }
}
export const getWardSuccess = (data) =>{
	return{
		type: actionTypes.GET_WARD_SUCCESS,
        data: data,
	};
};

export const getWardFail = (error) =>{
	return{
		type: actionTypes.GET_WARD_FAIL,
		error: error
	};
};

//---------------------------------- UPDATE USER ------------------------------------------------
export const updateUser  = (id, data, token) =>{
    return{
        type: actionTypes.UPDATE_USER,
        id,
        data,
        token,
    }
}
export const updateUserSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_USER_SUCCESS,
        data: data,
	};
};

export const updateUserFail = (error) =>{
	return{
		type: actionTypes.UPDATE_USER_FAIL,
		error: error
	};
};

export const setAlertUpdateUser = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_USER,
	};
};

