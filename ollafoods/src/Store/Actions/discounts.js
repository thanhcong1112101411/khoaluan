import * as actionTypes from './actionTypes';
//----------------------------------- FETCH WAITING DISCOUNTS --------------------------------------------------------
export const fetchDiscountsWaiting = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_WAITING,
        name,
		current_page,
        token: token,
	};
}

export const fetchDiscountsWaitingSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_DISCOUNTS_WAITING_SUCCESS,
        data: data
	};
}

export const fetchDiscountsWaitingFail = (error) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_WAITING_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH APPLING DISCOUNTS --------------------------------------------------------
export const fetchDiscountsAppling = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_APPLING,
        name,
		current_page,
        token: token,
	};
}

export const fetchDiscountsApplingSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_DISCOUNTS_APPLING_SUCCESS,
        data: data
	};
}

export const fetchDiscountsApplingFail = (error) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_APPLING_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH APPLIED DISCOUNTS --------------------------------------------------------
export const fetchDiscountsApplied = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_APPLIED,
        name,
		current_page,
        token: token,
	};
}

export const fetchDiscountsAppliedSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_DISCOUNTS_APPLIED_SUCCESS,
        data: data
	};
}

export const fetchDiscountsAppliedFail = (error) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_APPLIED_FAIL,
        error: error,
	};
}
//----------------------------------- FETCH DELETED DISCOUNTS --------------------------------------------------------
export const fetchDiscountsDeleted = (name, current_page, token) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_DELETED,
        name,
		current_page,
        token: token,
	};
}

export const fetchDiscountsDeletedSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_DISCOUNTS_DELETED_SUCCESS,
        data: data
	};
}

export const fetchDiscountsDeletedFail = (error) =>{
	return{
		type: actionTypes.FETCH_DISCOUNTS_DELETED_FAIL,
        error: error,
	};
}


//----------------------------------- UPDATE DISCOUNTS --------------------------------------------------------
export const updateDiscounts = (id, data, token) =>{
	return{
		type: actionTypes.UPDATE_DISCOUNTS,
		id: id,
		data: data,
        token: token,
	};
}

export const updateDiscountsSuccess = (data) =>{
	return{
		type: actionTypes.UPDATE_DISCOUNTS_SUCCESS,
        data: data
	};
}

export const updateDiscountsFail = (error) =>{
	return{
		type: actionTypes.UPDATE_DISCOUNTS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT UPDATE DISCOUNTS-----------------//
export const setAlertUpdateDiscounts = () =>{
	return{
		type: actionTypes.SET_ALERT_UPDATE_DISCOUNTS,
	};
}


//--------------- ADD Discounts-----------------//

export const addDiscounts = (data, token) =>{
	return{
		type: actionTypes.ADD_DISCOUNTS,
		data: data,
        token: token,
	};
}

export const addDiscountsSuccess = (data) =>{
	return{
		type: actionTypes.ADD_DISCOUNTS_SUCCESS,
        data: data
	};
}

export const addDiscountsFail = (error) =>{
	return{
		type: actionTypes.ADD_DISCOUNTS_FAIL,
        error: error,
	};
}

//--------------- SET ALERT ADD DISCOUNTS -----------------//
export const setAlertAddDiscounts = () =>{
	return{
		type: actionTypes.SET_ALERT_ADD_DISCOUNTS,
	};
}
//----------------------------------- DELETE DISCOUNTS --------------------------------------------------------
export const deleteDiscounts = (id, token) =>{
	return{
		type: actionTypes.DELETE_DISCOUNTS,
		id: id,
        token: token,
	};
}

export const deleteDiscountsSuccess = (data) =>{
	return{
		type: actionTypes.DELETE_DISCOUNTS_SUCCESS,
        data: data
	};
}

export const deleteDiscountsFail = (error) =>{
	return{
		type: actionTypes.DELETE_DISCOUNTS_FAIL,
        error: error,
	};
}


//--------------- SET ALERT DELETE DISCOUNTS -----------------//
export const setAlertDeleteDiscounts = () =>{
	return{
		type: actionTypes.SET_ALERT_DELETE_DISCOUNTS,
	};
}

export const fetchProductsDiscounts = (name, current_page, date_from, date_to, token) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_DISCOUNTS,
		name,
		current_page,
		date_from,
		date_to,
        token: token,
	};
}

export const fetchProductsDiscountsSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_DISCOUNTS_SUCCESS,
        data: data
	};
}

export const fetchProductsDiscountsFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_DISCOUNTS_FAIL,
        error: error,
	};
}

//----------------------------------- FETCH PRODUCTS PICKING DISCOUNTS --------------------------------------------------------
export const fetchProductsPickingDiscounts = (data, token) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_PICKING_DISCOUNTS,
        data,
        token: token,
	};
}

export const fetchProductsPickingDiscountsSuccess = (data) =>{
	
	return{
		type: actionTypes.FETCH_PRODUCTS_PICKING_DISCOUNTS_SUCCESS,
        data: data
	};
}

export const fetchProductsPickingDiscountsFail = (error) =>{
	return{
		type: actionTypes.FETCH_PRODUCTS_PICKING_DISCOUNTS_FAIL,
        error: error,
	};
}

//----------------------------------- GET DISCOUNT DETAIL --------------------------------------------------------
export const getDiscountDetail = (id, token) =>{
	return{
		type: actionTypes.GET_DISCOUNTS_DETAIL,
        id,
        token: token,
	};
}

export const getDiscountDetailSuccess = (data) =>{
	
	return{
		type: actionTypes.GET_DISCOUNTS_DETAIL_SUCCESS,
        data: data
	};
}

export const getDiscountDetailFail = (error) =>{
	return{
		type: actionTypes.GET_DISCOUNTS_DETAIL_FAIL,
        error: error,
	};
}