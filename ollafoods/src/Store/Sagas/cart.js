import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from '../Actions/index';

export function* addToCartSaga(action){
    const cart = yield localStorage.getItem("cart");
    const cartItem = {
        id: action.id,
        quantity: action.quantity,
    };
    if(!cart){
        const arrayCart = [];
        arrayCart.push(cartItem);
        yield localStorage.setItem("cart",JSON.stringify(arrayCart));
    }else{
        let updateCart = JSON.parse(cart);
        let index = -1;
        for(let i=0; i<updateCart.length; i++){
            if(updateCart[i].id === action.id){
                index = i;
            }
        }
        if(index === -1){
            updateCart.push(cartItem);
        }else{
            updateCart[index].quantity += 1;
        }
        yield localStorage.setItem("cart",JSON.stringify(updateCart));
    }
    yield call(fetchCartSaga);
}
export function* fetchCartSaga(){
    try{
        const cart = yield localStorage.getItem("cart");
        const data = {
            products: JSON.parse(cart)
        };
        // if(data.products.length == 0){
        //     yield put(actions.fetchCartSuccess([]));
        //     return;
        // }
        const res = yield axios.post('/api/products/get-product-cart', data);
        if (res.data.status){
            yield put(actions.fetchCartSuccess(res.data));
        }else{
            yield put(actions.fetchCartFail(res.data.message));
        } 
        
    }catch(err){
        yield put(actions.fetchCartFail(err.message));
    }
}
export function* deleteCartItemSaga(action){
    let cart = yield localStorage.getItem("cart");
    cart = JSON.parse(cart);
    var index = -1;
    for(var i=0; i<cart.length; i++){
        if(cart[i].id === action.id){
            index = i;
            break;
        }
    }
    cart.splice(index,1);
    yield localStorage.setItem("cart",JSON.stringify(cart));
    yield call(fetchCartSaga);
}
export function* changeQuantityProductCartSaga(action){
    let cart = yield localStorage.getItem("cart");
    cart = JSON.parse(cart);
    for(var i=0; i<cart.length; i++){
        if(cart[i].id === action.id){
            cart[i].quantity = parseInt(action.quantity);
        }
    }
    yield localStorage.setItem("cart",JSON.stringify(cart));
    yield call(fetchCartSaga);
}

export function* orderServiceSaga(action){
	try{
        let products = [];
        action.data.forEach(element => {
            products.push(
                {
                    id: element.id,
                    quantity: element.quantityProduct,
                }
            );
        });

        const data = {
            userId: action.uuid,
            note: "",
            products: products,
        }

        const res = yield axios.post('/api/envoices', data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.orderServiceSuccess(res.data));
            yield localStorage.setItem("cart",JSON.stringify([]));
            yield call(fetchCartSaga);
        }else{
            yield put(actions.orderServiceFail(res.data.message));
        }
	}catch(err){
		yield put(actions.orderServiceFail(err.message));
	}
}
