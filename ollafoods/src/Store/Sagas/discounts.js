import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus, DiscountType } from 'Utils/enum.utils';

export function* fetchDiscountsWaitingSaga(action){
	try{
        const limit = 10;
        const type = DiscountType.WATING;

        let link = '/api/discounts/get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchDiscountsWaitingSuccess(res.data));
        }else{
            yield put(actions.fetchDiscountsWaitingFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchDiscountsWaitingFail(error.message));
	}
}

export function* fetchDiscountsApplingSaga(action){
	try{
        const limit = 10;
        const type = DiscountType.APPLING;

        let link = '/api/discounts/get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchDiscountsApplingSuccess(res.data));
        }else{
            yield put(actions.fetchDiscountsApplingFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchDiscountsApplingFail(error.message));
	}
}

export function* fetchDiscountsAppliedSaga(action){
	try{
        const limit = 10;

        const type = DiscountType.APPLIED;

        let link = '/api/discounts/get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchDiscountsAppliedSuccess(res.data));
        }else{
            yield put(actions.fetchDiscountsAppliedFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchDiscountsAppliedFail(error.message));
	}
}

export function* fetchDiscountsDeletedSaga(action){
	try{
        const limit = 10;

        const type = DiscountType.DELETED;

        let link = '/api/discounts/get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchDiscountsDeletedSuccess(res.data));
        }else{
            yield put(actions.fetchDiscountsDeletedFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchDiscountsDeletedFail(error.message));
	}
}


export function* updateDiscountsSaga(action){
	try{
        const res = yield axios.put('/api/catagories/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateDiscountsSuccess(res.data));
        }else{
            yield put(actions.updateDiscountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateDiscountsFail(error.message));
	}
}
export function* addDiscountsaga(action){
	try{
        const res = yield axios.post('/api/Discounts', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addDiscountsSuccess(res.data));
        }else{
            yield put(actions.addDiscountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addDiscountsFail(error.message));
	}
}


export function* deleteDiscountsaga(action){
	try{
        const res = yield axios.put('/api/Discounts/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteDiscountsSuccess(res.data));
        }else{
            yield put(actions.deleteDiscountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteDiscountsFail(error.message));
	}
}


export function* fetchDiscountsProductSaga(action){
	try{
        const limit = 10;

        let link = '/api/products/admin-get-product-none-discount?date_from=' + action.date_from + '&date_to=' + action.date_to;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchProductsDiscountsSuccess(res.data));
        }else{
            yield put(actions.fetchProductsDiscountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsDiscountsFail(error.message));
	}
}

export function* fetchProductPickingDiscountSaga(action){

    const data = {
        products: action.data
    }
	try{

        const res = yield axios.post('/api/products/get-product-add-discount', data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchProductsPickingDiscountsSuccess(res.data));
        }else{
            yield put(actions.fetchProductsPickingDiscountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsPickingDiscountsFail(error.message));
	}
}

export function* getDiscountDetailSaga(action){
	try{
        const res = yield axios.get('/api/discounts/get-detail/' + action.id, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.getDiscountDetailSuccess(res.data));
        }else{
            yield put(actions.getDiscountDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getDiscountDetailFail(error.message));
	}
}