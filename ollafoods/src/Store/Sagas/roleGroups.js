import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType } from 'Utils/enum.utils';

export function* fetchRoleGroupsSaga(action){
	try{
        const limit = 10;
        let link = '/api/role-groups?limit=' + limit.toString();
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page.toString();
        }
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchRoleGroupsSuccess(res.data));
        }else{
            yield put(actions.fetchRoleGroupsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchRoleGroupsFail(error.message));
	}
}

export function* fetchAllScreensSaga(action){
	try{
        const res = yield axios.get('/api/screens', {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchAllScreensSuccess(res.data));
        }else{
            yield put(actions.fetchAllScreensFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchAllScreensFail(error.message));
	}
}
export function* addRoleGroupSaga(action){
	try{
        const res = yield axios.post('/api/role-groups', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addRoleGroupSuccess(res.data));
        }else{
            yield put(actions.addRoleGroupFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addRoleGroupFail(error.message));
	}
}

export function* deleteRoleGroupSaga(action){
	try{
        const res = yield axios.put('/api/role-groups/delete-role/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.deleteRoleGroupSuccess(res.data));
        }else{
            yield put(actions.deleteRoleGroupFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteRoleGroupFail(error.message));
	}
}

export function* updateRoleGroupSaga(action){
	try{
        const res = yield axios.put('/api/role-groups/update-role/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateRoleGroupSuccess(res.data));
        }else{
            yield put(actions.updateRoleGroupFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateRoleGroupFail(error.message));
	}
}

export function* getRoleGroupDetailSaga(action){
    try{
        const res = yield axios.get('/api/role-groups/' + action.id,{
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        if (res.data.status){
            yield put(actions.getRoleGroupDetailSuccess(res.data));
        }else{
            yield put(actions.getRoleGroupDetailFail(res.data.message));
        } 
    }catch(error){
        yield put(actions.getRoleGroupDetailFail(error.message));
    }
}

