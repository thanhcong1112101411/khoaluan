import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus, CountryCode, RoleDefault } from 'Utils/enum.utils';

export function* fetchActiveAccountSaga(action){
	try{
        const limit = 10;
        const user_type = UserType.ADMIN;
        const status = UserStatus.ACTIVE;

        let link = '/api/users?limit=' + limit.toString() + '&user_type=' + user_type + '&status=' + status;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page.toString();
        }
        if (action.email.trim().length > 0){
            link += '&email=' + action.email;
        }
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchActiveAccountsSuccess(res.data));
        }else{
            yield put(actions.fetchActiveAccountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchActiveAccountsFail(error.message));
	}
}

export function* fetchInactiveAccountSaga(action){
	try{
        const limit = 10;
        const user_type = UserType.ADMIN;
        const status = UserStatus.INACTIVE;

        let link = '/api/users?limit=' + limit.toString() + '&user_type=' + user_type + '&status=' + status;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page.toString();
        }
        if (action.email.trim().length > 0){
            link += '&email=' + action.email;
        }
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchInactiveAccountsSuccess(res.data));
        }else{
            yield put(actions.fetchInactiveAccountsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchInactiveAccountsFail(error.message));
	}
}

export function* changeRoleGroupSaga(action){
	try{
        const res = yield axios.put('/api/users/change-role/'+ action.uuid + '?role=' + action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.changeRoleGroupSuccess(res.data.message));
        }else{
            yield put(actions.changeRoleGroupFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.changeRoleGroupFail(error.message));
	}
}

export function* addAccountSaga(action){
    const data = {
        ...action.data,
        role: RoleDefault.NONE,
        country_code: CountryCode,
    };
    console.log(data);

	try{
        const res = yield axios.post('/api/users/admin-register', data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addAccountSuccess(res.data.message));
        }else{
            yield put(actions.addAccountFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addAccountFail(error.message));
	}
}