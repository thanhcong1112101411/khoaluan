import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchBrandsSaga(action){
	try{
        const limit = 10;

        let link = '/api/brands/get-all?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchBrandsSuccess(res.data));
        }else{
            yield put(actions.fetchBrandsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchBrandsFail(error.message));
	}
}

export function* updateBrandsSaga(action){
	try{
        const res = yield axios.put('/api/brands/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        console.log(res.data);
        if (res.data.status){
            yield put(actions.updateBrandsSuccess(res.data));
        }else{
            yield put(actions.updateBrandsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateBrandsFail(error.message));
	}
}
export function* addBrandsSaga(action){
	try{
        const res = yield axios.post('/api/brands', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addBrandsSuccess(res.data));
        }else{
            yield put(actions.addBrandsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addBrandsSuccess(error.message));
	}
}


export function* deleteBrandsSaga(action){
	try{
        const res = yield axios.put('/api/brands/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteBrandsSuccess(res.data));
        }else{
            yield put(actions.deleteBrandsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteBrandsFail(error.message));
	}
}

export function* getBrandDetailsSaga(action){
	try{
        const res = yield axios.get('/api/brands/get-detail/' + action.id);

        if (res.data.status){
            yield put(actions.getBrandDetailSuccess(res.data));
        }else{
            yield put(actions.getBrandDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getBrandDetailFail(error.message));
	}
}