import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchFeedbackSaga(action){
	try{
        const limit = 10;

        let link = '/api/feedbacks?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchFeedbacksSuccess(res.data));
        }else{
            yield put(actions.fetchFeedbacksFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchFeedbacksFail(error.message));
	}
}

export function* addFeedbackSaga(action){
	try{
        const res = yield axios.post('/api/feedbacks', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addFeedbacksSuccess(res.data.message));
        }else{
            yield put(actions.addFeedbacksFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addFeedbacksFail(error.message));
	}
}