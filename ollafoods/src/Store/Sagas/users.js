import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchActiveUsersSaga(action){
	try{
        const limit = 10;
        const user_type = UserType.USER;
        const status = UserStatus.ACTIVE;

        let link = '/api/users?limit=' + limit.toString() + '&user_type=' + user_type + '&status=' + status;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page.toString();
        }
        if (action.email.trim().length > 0){
            link += '&email=' + action.email;
        }
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchActiveUsersSuccess(res.data));
        }else{
            yield put(actions.fetchActiveUsersFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchActiveUsersFail(error.message));
	}
}

export function* fetchInactiveUsersSaga(action){
	try{
        const limit = 10;
        const user_type = UserType.USER;
        const status = UserStatus.INACTIVE;

        let link = '/api/users?limit=' + limit.toString() + '&user_type=' + user_type + '&status=' + status;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page.toString();
        }
        if (action.email.trim().length > 0){
            link += '&email=' + action.email;
        }
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchInactiveUsersSuccess(res.data));
        }else{
            yield put(actions.fetchInactiveUsersFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchInactiveUsersFail(error.message));
	}
}

export function* getUserDetailSaga(action){
	try{
        const res = yield axios.get('/api/users/' + action.uuid, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.getUserDetailSuccess(res.data));
        }else{
            yield put(actions.getUserDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getUserDetailFail(error.message));
	}
}