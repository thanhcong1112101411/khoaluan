import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';

export function* getEnvoiceAmountSaga(action){
	try{
        const limit = 10;

        let link = '/api/envoices/get-amount?date_from=' + action.date_from + '&date_to=' + action.date_to;
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.getEnvoiceAmountSuccess(res.data));
        }else{
            yield put(actions.getEnvoiceAmountFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getEnvoiceAmountFail(error.message));
	}
}

export function* analyticHUISaga(action){
	try{
        let link = '/api/recomender-system/hui?date_from=' + action.data.date_from + '&date_to=' + action.data.date_to + '&minUtility=' + action.data.utility;

        yield put(actions.analyticHUIStart());
        
        const res = yield axios.get(link);

        if (res.data.status){
            yield put(actions.analyticHUISuccess(res.data));
        }else{
            yield put(actions.analyticHUIFail(res.data.message));
        }
        
	}catch(error){
		yield put(actions.analyticHUIFail(error.message));
	}
}