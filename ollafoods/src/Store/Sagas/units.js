import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchUnitsSaga(action){
	try{
        const limit = 10;

        let link = '/api/units/get-all?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchUnitsSuccess(res.data));
        }else{
            yield put(actions.fetchUnitsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchUnitsFail(error.message));
	}
}

export function* updateUnitsSaga(action){
	try{
        const res = yield axios.put('/api/units/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateUnitsSuccess(res.data));
        }else{
            yield put(actions.updateUnitsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateUnitsFail(error.message));
	}
}
export function* addUnitsSaga(action){
	try{
        const res = yield axios.post('/api/units', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addUnitsSuccess(res.data));
        }else{
            yield put(actions.addUnitsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addUnitsFail(error.message));
	}
}


export function* deleteUnitsSaga(action){
	try{
        const res = yield axios.put('/api/units/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteUnitsSuccess(res.data));
        }else{
            yield put(actions.deleteUnitsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteUnitsFail(error.message));
	}
}

export function* getUnitDetailsSaga(action){
	try{
        const res = yield axios.get('/api/units/get-detail/' + action.id);

        if (res.data.status){
            yield put(actions.getUnitDetailSuccess(res.data));
        }else{
            yield put(actions.getUnitDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getUnitDetailFail(error.message));
	}
}