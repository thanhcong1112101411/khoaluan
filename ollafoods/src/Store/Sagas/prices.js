import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchProductsPricesSaga(action){
	try{
        const limit = 10;

        let link = '/api/products/admin-get-all?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchProductsPricesSuccess(res.data));
        }else{
            yield put(actions.fetchProductsPricesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsPricesFail(error.message));
	}
}

export function* getPricesDetailsSaga(action){
	try{
        const res = yield axios.get('/api/prices/' + action.id, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.getPricesDetailSuccess(res.data));
        }else{
            yield put(actions.getPricesDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getPricesDetailFail(error.message));
	}
}

export function* addPricesSaga(action){
	try{
        const res = yield axios.post('/api/prices', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addPricesSuccess(res.data));
        }else{
            yield put(actions.addPricesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addPricesFail(error.message));
	}
}