import {takeEvery} from 'redux-saga/effects';

import * as actionTypes from 'Store/Actions/actionTypes';

import{
    userLoginSaga,
    userLoginSocialSaga,
    userLogoutSaga,
    resetPasswordSaga,
    changePasswordTokenSaga,
    userCheckTimeOutSaga,
    userSignUpSaga,

    adminLoginSaga,
    adminLogoutSaga,
    adminCheckTimeOutSaga,

    setStatusUserSaga,
    
    getCitySaga,
    getDistrictSaga,
    getWardSaga,

    updateUserSaga,

} from './auth';

import {
    fetchHistorySaga
} from './history';

import {
    fetchRoleGroupsSaga,
    fetchAllScreensSaga,
    addRoleGroupSaga,
    deleteRoleGroupSaga,
    updateRoleGroupSaga,
    getRoleGroupDetailSaga,
} from './roleGroups';

import {
    fetchActiveAccountSaga,
    fetchInactiveAccountSaga,
    changeRoleGroupSaga,
    addAccountSaga,
} from './accounts';

import {
    fetchActiveUsersSaga,
    fetchInactiveUsersSaga,
    getUserDetailSaga,
} from './users';

import {
    addImageSaga, fetchBannerImageSaga, deleteImageSaga, fetchContentTopImageSaga, fetchLogoImageSaga,
} from './images';

import{
    addFeedbackSaga,
    fetchFeedbackSaga,
} from './feedbacks';
import{
    fetchCatagoriesSaga,
    updateCatagorySaga,
    addCatagorySaga,
    deleteCatagorySaga,
    getCatagoryDetailsSaga,
}from './catagories';
import{
    fetchNewsSaga,
    deleteNewsSaga,
    addNewsSaga,
    updateNewsSaga,
    getNewsDetailsSaga,
    fetchTopNewsNewestSaga,
    uploadImageSaga,
    increaseSeenNewsSaga,
}from './news';
import{
    fetchBrandsSaga,
    addBrandsSaga,
    updateBrandsSaga,
    deleteBrandsSaga,
    getBrandDetailsSaga,
}from './brands';
import{
    fetchUnitsSaga,
    addUnitsSaga,
    updateUnitsSaga,
    deleteUnitsSaga,
    getUnitDetailsSaga,
}from './units';
import{
    fetchProductsPricesSaga,
    getPricesDetailsSaga,
    addPricesSaga,
}from './prices';
import{
    fetchProductsWaitingSaga,
    fetchProductsSellingSaga,
    fetchProductsLockSaga,
    addProductSaga,
    setStatusProductSaga,
    getCatagoryNameSaga,
    getBrandNameSaga,
    getUnitNameSaga,
    fetchProductsUserSaga,
    getProductsDetailsSaga,
    updateProductsSaga,
    fetchRecomendProductDetailSaga,
    fetchTopProductsNewestSaga,
    fetchProductsSearchSaga,
}from './products';
import{
    fetchDiscountsApplingSaga,
    fetchDiscountsAppliedSaga,
    fetchDiscountsWaitingSaga,
    fetchDiscountsDeletedSaga,
    fetchDiscountsProductSaga,
    fetchProductPickingDiscountSaga,
    addDiscountsaga,
    getDiscountDetailSaga,
}from './discounts';
import{
    fetchExportEnvoiceCanceledSaga,
    fetchExportEnvoiceCompletedSaga,
    fetchExportEnvoiceWaitingSaga,
    setStatusExportEnvoiceSaga,
    getExportEnvoiceDetailsSaga,
    fetchEnvoiceUserSaga,
}from './exportEnvoice';
import { updateCatagory, getPricesDetailFail, getCatagoryName, fetchProductsUser, fetchProductsDiscountsSuccess, getProductsDetail } from 'Store/Actions';
import { fetchCartSaga, changeQuantityProductCartSaga, deleteCartItemSaga, orderServiceSaga, addToCartSaga } from './cart';
import { getEnvoiceAmountSaga, analyticHUISaga } from './recomendSystem';

export function* watchAuth(){
    yield takeEvery(actionTypes.USER_LOGIN, userLoginSaga);
    yield takeEvery(actionTypes.USER_LOGIN_SOCIAL, userLoginSocialSaga);
    yield takeEvery(actionTypes.USER_LOGOUT, userLogoutSaga);
    yield takeEvery(actionTypes.RESET_PASSSWORD, resetPasswordSaga);
    yield takeEvery(actionTypes.CHANGE_PASSSWORD_TOKEN, changePasswordTokenSaga);
    yield takeEvery(actionTypes.USER_CHECK_TIMEOUT, userCheckTimeOutSaga);
    yield takeEvery(actionTypes.USER_SIGNUP, userSignUpSaga);

    yield takeEvery(actionTypes.ADMIN_LOGIN, adminLoginSaga);
    yield takeEvery(actionTypes.ADMIN_LOGOUT, adminLogoutSaga);
    yield takeEvery(actionTypes.ADMIN_CHECK_TIMEOUT, adminCheckTimeOutSaga);

    yield takeEvery(actionTypes.SET_STATUS_USER, setStatusUserSaga);

    yield takeEvery(actionTypes.GET_CITY, getCitySaga);
    yield takeEvery(actionTypes.GET_DISTRICT, getDistrictSaga);
    yield takeEvery(actionTypes.GET_WARD, getWardSaga);

    yield takeEvery(actionTypes.UPDATE_USER, updateUserSaga);

}

export function* watchHistory(){
    yield takeEvery(actionTypes.FETCH_HISTORY, fetchHistorySaga);
}

export function* watchRoleGroup(){
    yield takeEvery(actionTypes.FETCH_ROLE_GROUPS, fetchRoleGroupsSaga);
    yield takeEvery(actionTypes.FETCH_ALL_SCREENS, fetchAllScreensSaga);
    yield takeEvery(actionTypes.ADD_ROLE_GROUP, addRoleGroupSaga);
    yield takeEvery(actionTypes.DELETE_ROLE_GROUP, deleteRoleGroupSaga);
    yield takeEvery(actionTypes.UPDATE_ROLE_GROUP, updateRoleGroupSaga);
    yield takeEvery(actionTypes.GET_ROLE_GROUP_DETAIL, getRoleGroupDetailSaga);
}

export function* watchAccount(){
    yield takeEvery(actionTypes.FETCH_ACTIVE_ACCOUNTS, fetchActiveAccountSaga);
    yield takeEvery(actionTypes.FETCH_INACTIVE_ACCOUNTS, fetchInactiveAccountSaga);
    yield takeEvery(actionTypes.CHANGE_ROLE_GROUP, changeRoleGroupSaga);
    yield takeEvery(actionTypes.ADD_ACCOUNT, addAccountSaga);
}

export function* watchUser(){
    yield takeEvery(actionTypes.FETCH_ACTIVE_USERS, fetchActiveUsersSaga);
    yield takeEvery(actionTypes.FETCH_INACTIVE_USERS, fetchInactiveUsersSaga);
    yield takeEvery(actionTypes.GET_USER_DETAIL, getUserDetailSaga);
}

export function* watchImage(){
    yield takeEvery(actionTypes.ADD_IMAGES, addImageSaga);
    yield takeEvery(actionTypes.FETCH_BANNER_IMAGES, fetchBannerImageSaga);
    yield takeEvery(actionTypes.FETCH_CONTENT_TOP_IMAGES, fetchContentTopImageSaga);
    yield takeEvery(actionTypes.FETCH_LOGO_IMAGES, fetchLogoImageSaga);
    yield takeEvery(actionTypes.DELETE_IMAGES, deleteImageSaga);
}

export function* watchFeedback(){
    yield takeEvery(actionTypes.ADD_FEEDBACKS, addFeedbackSaga);
    yield takeEvery(actionTypes.FETCH_FEEDBACKS, fetchFeedbackSaga);
}

export function* watchCatagory(){
    yield takeEvery(actionTypes.FETCH_CATAGORIES, fetchCatagoriesSaga);
    yield takeEvery(actionTypes.UPDATE_CATAGORY, updateCatagorySaga);
    yield takeEvery(actionTypes.ADD_CATAGORIES, addCatagorySaga);
    yield takeEvery(actionTypes.DELETE_CATAGORY, deleteCatagorySaga);
    yield takeEvery(actionTypes.GET_CATAGORY_DETAIL, getCatagoryDetailsSaga);
}

export function* watchNews(){
    yield takeEvery(actionTypes.FETCH_NEWS, fetchNewsSaga);
    yield takeEvery(actionTypes.ADD_NEWS, addNewsSaga);
    yield takeEvery(actionTypes.UPDATE_NEWS, updateNewsSaga);
    yield takeEvery(actionTypes.DELETE_NEWS, deleteNewsSaga);
    yield takeEvery(actionTypes.GET_NEWS_DETAIL, getNewsDetailsSaga);
    yield takeEvery(actionTypes.FETCH_TOP_NEWS_NEWEST, fetchTopNewsNewestSaga);
    yield takeEvery(actionTypes.UPLOAD_IMAGE, uploadImageSaga);
    yield takeEvery(actionTypes.INCREASE_SEEN_NEWS, increaseSeenNewsSaga);
}

export function* watchBrands(){
    yield takeEvery(actionTypes.FETCH_BRANDS, fetchBrandsSaga);
    yield takeEvery(actionTypes.ADD_BRANDS, addBrandsSaga);
    yield takeEvery(actionTypes.UPDATE_BRANDS, updateBrandsSaga);
    yield takeEvery(actionTypes.DELETE_BRANDS, deleteBrandsSaga);
    yield takeEvery(actionTypes.GET_BRAND_DETAIL, getBrandDetailsSaga);
}

export function* watchUnits(){
    yield takeEvery(actionTypes.FETCH_UNITS, fetchUnitsSaga);
    yield takeEvery(actionTypes.ADD_UNITS, addUnitsSaga);
    yield takeEvery(actionTypes.UPDATE_UNITS, updateUnitsSaga);
    yield takeEvery(actionTypes.DELETE_UNITS, deleteUnitsSaga);
    yield takeEvery(actionTypes.GET_UNIT_DETAIL, getUnitDetailsSaga);
}

export function* watchPrices(){
    yield takeEvery(actionTypes.FETCH_PRODUCTS_PRICES, fetchProductsPricesSaga);
    yield takeEvery(actionTypes.GET_PRICES_DETAIL, getPricesDetailsSaga);
    yield takeEvery(actionTypes.ADD_PRICES, addPricesSaga);
}

export function* watchProducts(){
    yield takeEvery(actionTypes.FETCH_PRODUCTS_WAITING,fetchProductsWaitingSaga);
    yield takeEvery(actionTypes.FETCH_PRODUCTS_SELLING,fetchProductsSellingSaga);
    yield takeEvery(actionTypes.FETCH_PRODUCTS_LOCK,fetchProductsLockSaga);
    yield takeEvery(actionTypes.ADD_PRODUCTS,addProductSaga);
    yield takeEvery(actionTypes.SET_STATUS_PRODUCT,setStatusProductSaga);
    yield takeEvery(actionTypes.GET_CATAGORY_NAME,getCatagoryNameSaga);
    yield takeEvery(actionTypes.GET_BRAND_NAME,getBrandNameSaga);
    yield takeEvery(actionTypes.GET_UNIT_NAME,getUnitNameSaga);
    yield takeEvery(actionTypes.FETCH_PRODUCTS_USER,fetchProductsUserSaga);
    yield takeEvery(actionTypes.GET_PRODUCTS_DETAIL,getProductsDetailsSaga);
    yield takeEvery(actionTypes.UPDATE_PRODUCTS, updateProductsSaga);
    yield takeEvery(actionTypes.FETCH_RECOMEND_PRODUCT_DETAIL, fetchRecomendProductDetailSaga);
    yield takeEvery(actionTypes.FETCH_TOP_PRODUCTS_NEWEST, fetchTopProductsNewestSaga);
    yield takeEvery(actionTypes.FETCH_PRODUCTS_SEARCH, fetchProductsSearchSaga);
}

export function* watchDiscounts(){
    yield takeEvery(actionTypes.FETCH_DISCOUNTS_WAITING, fetchDiscountsWaitingSaga);
    yield takeEvery(actionTypes.FETCH_DISCOUNTS_APPLING, fetchDiscountsApplingSaga);
    yield takeEvery(actionTypes.FETCH_DISCOUNTS_APPLIED, fetchDiscountsAppliedSaga);
    yield takeEvery(actionTypes.FETCH_DISCOUNTS_DELETED, fetchDiscountsDeletedSaga);
    yield takeEvery(actionTypes.FETCH_PRODUCTS_DISCOUNTS, fetchDiscountsProductSaga);
    yield takeEvery(actionTypes.FETCH_PRODUCTS_PICKING_DISCOUNTS, fetchProductPickingDiscountSaga);
    yield takeEvery(actionTypes.ADD_DISCOUNTS, addDiscountsaga);
    yield takeEvery(actionTypes.GET_DISCOUNTS_DETAIL, getDiscountDetailSaga);
}

export function* watchExportEnvoice(){
    yield takeEvery(actionTypes.FETCH_EXPORT_ENVOICE_WAITING, fetchExportEnvoiceWaitingSaga);
    yield takeEvery(actionTypes.FETCH_EXPORT_ENVOICE_CANCELED, fetchExportEnvoiceCanceledSaga);
    yield takeEvery(actionTypes.FETCH_EXPORT_ENVOICE_COMPLETED, fetchExportEnvoiceCompletedSaga);
    yield takeEvery(actionTypes.SET_STATUS_EXPORT_ENVOICE, setStatusExportEnvoiceSaga);
    yield takeEvery(actionTypes.GET_DETAIL_EXPORT_ENVOICE, getExportEnvoiceDetailsSaga);
    yield takeEvery(actionTypes.FETCH_ENVOICE_USER, fetchEnvoiceUserSaga);
}

export function* watchCart(){
    yield takeEvery(actionTypes.FETCH_CART, fetchCartSaga);
    yield takeEvery(actionTypes.ADD_TO_CART, addToCartSaga);
    yield takeEvery(actionTypes.CHANGE_QUANTITY_PRODUCT_CART, changeQuantityProductCartSaga);
    yield takeEvery(actionTypes.DELETE_CART_ITEM, deleteCartItemSaga);
    yield takeEvery(actionTypes.ORDER_SERVICE, orderServiceSaga);
}
export function* watchRecomendSystem(){
    yield takeEvery(actionTypes.GET_ENVOICE_AMOUNT, getEnvoiceAmountSaga);
    yield takeEvery(actionTypes.ANALYTIC_HUI, analyticHUISaga);
}