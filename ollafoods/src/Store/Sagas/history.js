import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchHistorySaga(action){
	try{
        const limit = 10;

        let link = '/api/history?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchHistorySuccess(res.data));
        }else{
            yield put(actions.fetchHistoryFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchHistorySuccess(error.message));
	}
}