import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchNewsSaga(action){
	try{
        const limit = 10;

        let link = '/api/news/get-all?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchNewsSuccess(res.data));
        }else{
            yield put(actions.fetchNewsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchNewsFail(error.message));
	}
}

export function* updateNewsSaga(action){
	try{
        const res = yield axios.put('/api/news/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateNewsSuccess(res.data));
        }else{
            yield put(actions.updateNewsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateNewsFail(error.message));
	}
}
export function* addNewsSaga(action){
	try{
        const res = yield axios.post('/api/news', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addNewsSuccess(res.data));
        }else{
            yield put(actions.addNewsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addNewsFail(error.message));
	}
}


export function* deleteNewsSaga(action){
	try{
        const res = yield axios.put('/api/news/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteNewsSuccess(res.data));
        }else{
            yield put(actions.deleteNewsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteNewsFail(error.message));
	}
}

export function* getNewsDetailsSaga(action){
	try{
        const res = yield axios.get('/api/news/get-detail/' + action.id);

        if (res.data.status){
            yield put(actions.getNewsDetailSuccess(res.data));
        }else{
            yield put(actions.getNewsDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getNewsDetailFail(error.message));
	}
}

export function* fetchTopNewsNewestSaga(action){
	try{
        const res = yield axios.get("/api/news/get-top-new");

        if (res.data.status){
            yield put(actions.fetchTopNewsNewestSuccess(res.data));
        }else{
            yield put(actions.fetchTopNewsNewestFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchTopNewsNewestFail(error.message));
	}
}

export function* uploadImageSaga(action){
	try{
        const res = yield axios.post('/api/upload-images', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.uploadImageSuccess(res.data));
        }else{
            yield put(actions.uploadImageFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.uploadImageFail(error.message));
	}
}

export function* increaseSeenNewsSaga(action){
	try{
        const res = yield axios.put('/api/news/increase-click/' + action.id, null);

        if (res.data.status){
            yield put(actions.increaseSeenNewsSuccess(res.data.message));
        }else{
            yield put(actions.increaseSeenNewsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.increaseSeenNewsFail(error.message));
	}
}