import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus, ExportEnvoiceType } from 'Utils/enum.utils';

export function* fetchExportEnvoiceWaitingSaga(action){
	try{
        const limit = 10;
        const type = ExportEnvoiceType.WATING;

        let link = '/api/envoices/admin-get-all?status=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchExportEnvoiceWaitingSuccess(res.data));
        }else{
            yield put(actions.fetchExportEnvoiceWaitingFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchExportEnvoiceWaitingFail(error.message));
	}
}

export function* fetchExportEnvoiceCompletedSaga(action){
	try{
        const limit = 10;
        const type = ExportEnvoiceType.COMPLETED;

        let link = '/api/envoices/admin-get-all?status=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchExportEnvoiceCompletedSuccess(res.data));
        }else{
            yield put(actions.fetchExportEnvoiceCompletedFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchExportEnvoiceCompletedFail(error.message));
	}
}

export function* fetchExportEnvoiceCanceledSaga(action){
	try{
        const limit = 10;
        const type = ExportEnvoiceType.CANCELED;

        let link = '/api/envoices/admin-get-all?status=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchExportEnvoiceCanceledSuccess(res.data));
        }else{
            yield put(actions.fetchExportEnvoiceCanceledFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchExportEnvoiceCanceledFail(error.message));
	}
}





export function* updateExportEnvoiceSaga(action){
	try{
        const res = yield axios.put('/api/envoices/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateExportEnvoiceSuccess(res.data));
        }else{
            yield put(actions.updateExportEnvoiceFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateExportEnvoiceFail(error.message));
	}
}
export function* addExportEnvoiceSaga(action){
	try{
        const res = yield axios.post('/api/envoices/add', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addExportEnvoiceSuccess(res.data));
        }else{
            yield put(actions.addExportEnvoiceFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addExportEnvoiceFail(error.message));
	}
}


export function* deleteExportEnvoiceSaga(action){
	try{
        const res = yield axios.put('/api/envoices/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteExportEnvoiceSuccess(res.data));
        }else{
            yield put(actions.deleteExportEnvoiceFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteExportEnvoiceFail(error.message));
	}
}

export function* setStatusExportEnvoiceSaga(action){
    const data = {
        status: action.status,
    }

	try{
        const res = yield axios.put('/api/envoices/set-status/'+ action.id, data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.setStatusExportEnvoiceSuccess(res.data));
        }else{
            yield put(actions.setStatusExportEnvoiceFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.setStatusExportEnvoiceFail(error.message));
	}
}

export function* getExportEnvoiceDetailsSaga(action){
	try{
        const res = yield axios.get('/api/envoices/get-detail/' + action.id, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.getExportEnvoiceDetailSuccess(res.data));
        }else{
            yield put(actions.getExportEnvoiceDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getExportEnvoiceDetailFail(error.message));
	}
}

export function* fetchEnvoiceUserSaga(action){
	try{
        const res = yield axios.get('/api/envoices/get-list-envoice/'+ action.uuid,{
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchEnvoiceUserSuccess(res.data));
        }else{
            yield put(actions.fetchEnvoiceUserFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchEnvoiceUserFail(error.message));
	}
}