import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus, ProductType } from 'Utils/enum.utils';

export function* fetchProductsWaitingSaga(action){
	try{
        const limit = 10;
        const type = ProductType.WATING;

        let link = '/api/products/admin-get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchProductsWaitingSuccess(res.data));
        }else{
            yield put(actions.fetchProductsWaitingFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsWaitingFail(error.message));
	}
}

export function* fetchProductsSellingSaga(action){
	try{
        const limit = 10;
        const type = ProductType.ACTIVE;

        let link = '/api/products/admin-get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchProductsSellingSuccess(res.data));
        }else{
            yield put(actions.fetchProductsSellingFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsSellingFail(error.message));
	}
}

export function* fetchProductsLockSaga(action){
	try{
        const limit = 10;

        const type = ProductType.INACTIVE;

        let link = '/api/products/admin-get-all?type=' + type + '&limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchProductsLockSuccess(res.data));
        }else{
            yield put(actions.fetchProductsLockFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsLockFail(error.message));
	}
}


export function* updateProductsSaga(action){
	try{
        const res = yield axios.put('/api/products/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateProductsSuccess(res.data));
        }else{
            yield put(actions.updateProductsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateProductsFail(error.message));
	}
}
export function* addProductSaga(action){
	try{
        const res = yield axios.post('/api/products', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addProductsSuccess(res.data));
        }else{
            yield put(actions.addProductsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addProductsFail(error.message));
	}
}


export function* deleteProductSaga(action){
	try{
        const res = yield axios.put('/api/products/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteProductsSuccess(res.data));
        }else{
            yield put(actions.deleteProductsFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteProductsFail(error.message));
	}
}
export function* setStatusProductSaga(action){
	try{
        const res = yield axios.put('/api/products/set-status/' + action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.setStatusProductSuccess(res.data.message));
        }else{
            yield put(actions.setStatusProductFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.setStatusUserFail(error.message));
	}
}

export function* getCatagoryNameSaga(action){
    try{
        const res = yield axios.get('/api/catagories/get-all-catagory');

        if (res.data.status){
            yield put(actions.getCatagoryNameSuccess(res.data));
        }else{
            yield put(actions.getCatagoryNameFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getCatagoryNameFail(error.message));
	}
}

export function* getBrandNameSaga(action){
    try{
        const res = yield axios.get('/api/brands/get-all-brand');

        if (res.data.status){
            yield put(actions.getBrandNameSuccess(res.data));
        }else{
            yield put(actions.getBrandNameFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getBrandNameFail(error.message));
	}
}

export function* getUnitNameSaga(action){
    try{
        const res = yield axios.get('/api/units/get-all-unit');

        if (res.data.status){
            yield put(actions.getUnitNameSuccess(res.data));
        }else{
            yield put(actions.getUnitNameFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getUnitNameFail(error.message));
	}
}

// for user

export function* fetchProductsUserSaga(action){
	try{
        const limit = 12;

        let link = '/api/products/customer-get-all?limit=' + limit;
        
        if (action.brand.length > 0){
            link += '&brand=' + JSON.stringify(action.brand);
        }
        if (action.price == 1 || action.price == 2){
            link += '&price=' + action.price;
        }
        if (action.catagory != '' && action.catagory != -1){
            link += '&catagory=' + action.catagory;
        }
        if (action.name.length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0 && action.current_page != ''){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link);

        if (res.data.status){
            yield put(actions.fetchProductsUserSuccess(res.data));
        }else{
            yield put(actions.fetchProductsUserFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsUserFail(error.message));
	}
}


export function* getProductsDetailsSaga(action){
	try{
        const res = yield axios.get('/api/products/customer-get-detail/' + action.id);

        if (res.data.status){
            yield put(actions.getProductsDetailSuccess(res.data));
        }else{
            yield put(actions.getProductsDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getProductsDetailFail(error.message));
	}
}

export function* fetchRecomendProductDetailSaga(action){
	try{
        const res = yield axios.get('/api/products/recomend-product-detail/' + action.id);

        if (res.data.status){
            yield put(actions.fetchRecomendProductDetailSuccess(res.data));
        }else{
            yield put(actions.fetchRecomendProductDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchRecomendProductDetailFail(error.message));
	}
}

export function* fetchTopProductsNewestSaga(action){
	try{
        const res = yield axios.get("/api/products/get-top-new");

        if (res.data.status){
            yield put(actions.fetchTopProductsNewestSuccess(res.data));
        }else{
            yield put(actions.fetchTopProductsNewestFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchTopProductsNewestFail(error.message));
	}
}

export function* fetchProductsSearchSaga(action){
	try{
        const limit = 10;

        let link = '/api/products/customer-get-all?limit=' + limit;

        if (action.name.length > 0){
            link += '&name=' + action.name;
        }

        const res = yield axios.get(link);

        if (res.data.status){
            yield put(actions.fetchProductsSearchSuccess(res.data));
        }else{
            yield put(actions.fetchProductsSearchFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchProductsSearchFail(error.message));
	}
}
