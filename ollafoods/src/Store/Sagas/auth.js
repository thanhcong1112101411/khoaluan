import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';
import countryCodeAxios from 'axios';

import * as actions from 'Store/Actions/index';
import { UserType, RoleDefault, CountryCode } from 'Utils/enum.utils';

export function* userLoginSaga(action){
    const data = {
        username: action.username,
        password: action.password,
        type: UserType.USER,
    };
    
	try{
        const res = yield axios.post('/api/auth/login', data);

        if (res.data.status){
            const now = new Date();
            const expirationDate = yield new Date(now.setDate(now.getDate()+30));
            localStorage.setItem("uuid", res.data.data.uuid);
            localStorage.setItem("token",res.data.data._token);
            localStorage.setItem("expirationDate", expirationDate);

            yield put(actions.userLoginSuccess(res.data));
        }else{
            yield put(actions.userLoginFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.userLoginFail(error.message));
	}
}

export function* userLoginSocialSaga(action){
    console.log(action.data);
	try{
        const res = yield axios.post('/api/auth/social-login', action.data);
        console.log(res.data);
        if (res.data.status){
            const now = new Date();
            const expirationDate = yield new Date(now.setDate(now.getDate()+30));
            localStorage.setItem("uuid", res.data.data.uuid);
            localStorage.setItem("token",res.data.data._token);
            localStorage.setItem("expirationDate", expirationDate);

            yield put(actions.userLoginSuccess(res.data));
        }else{
            yield put(actions.userLoginFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.userLoginFail(error.message));
	}
}

export function* userLogoutSaga(){
	
    localStorage.removeItem('uuid');
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    yield put(actions.userLogoutSuccess());
    
}

export function* userCheckTimeOutSaga(){
    const uuid = yield localStorage.getItem("uuid");
    const token = yield localStorage.getItem("token");
    if(!uuid || !token){
        yield put(actions.userLogout());
    }else{
        const expirationDate = yield localStorage.getItem("expirationDate");
        if(expirationDate <= new Date()){
            yield put(actions.userLogout());
        }else{
            try{
                const res = yield axios.get('/api/auth/check-user?uuid=' + uuid + '&token=' + token);
                if(res.data.status){
                    yield put(actions.userLoginSuccess(res.data));
                }else {
                    actions.userLogout();
                }
                
            }catch(err){
                actions.userLogout();
            }
        }
    }
}

export function* resetPasswordSaga(action){
    const data = {
        email: action.email
    };

	try{
        const res = yield axios.post('/api/reset-password', data);

        if (res.data.status){
            yield put(actions.resetPasswordSuccess(res.data.message));
        }else{
            yield put(actions.resetPasswordFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.resetPasswordFail(error.message));
	}
}

export function* changePasswordTokenSaga(action){
    const data = {
        password: action.password,
        password_confirmation: action.repassword
    };

	try{
        const res = yield axios.put('/api/reset-password/change_password/' + action.token, data);

        if (res.data.status){
            yield put(actions.changePasswordTokenSuccess(res.data.message));
        }else{
            yield put(actions.changePasswordTokenFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.changePasswordTokenFail(error.message));
	}
}

export function* adminLoginSaga(action){
    const data = {
        username: action.username,
        password: action.password,
        type: UserType.ADMIN,
    };
    
	try{
        const res = yield axios.post('/api/auth/login', data);
        if (res.data.status){
            sessionStorage.setItem("uuid", res.data.data.uuid);
            sessionStorage.setItem("token",res.data.data._token);

            yield put(actions.adminLoginSuccess(res.data));

            // fetch screens
            yield fetchScreensSaga(res.data.data.role.id, res.data.data._token);

        }else{
            yield put(actions.adminLoginFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.adminLoginFail(error.message));
	}
}

export function* adminLogoutSaga(){
	
    sessionStorage.removeItem('uuid');
    sessionStorage.removeItem('token');
    yield put(actions.adminLogoutSuccess());
}

export function* adminCheckTimeOutSaga(){
    const uuid = yield sessionStorage.getItem("uuid");
    const token = yield sessionStorage.getItem("token");
    if(!uuid || !token){
        yield put(actions.adminLogout());
    }else{
        try{
            const res = yield axios.get('/api/auth/check-user?uuid=' + uuid + '&token=' + token);
            if(res.data.status){
                yield put(actions.adminLoginSuccess(res.data));

                // fetch screens
                yield fetchScreensSaga(res.data.data.role.id, res.data.data._token);
            }else {
                actions.adminLogout();
            }  
        }catch(err){
            actions.adminLogout();
        }
        
    }
}

function* fetchScreensSaga(roleId, token){
    try{
        const resScreen = yield axios.get('/api/role-groups/' + roleId,{
            headers: {
                "Authorization": "bearer " + token
            }
        });
        if (resScreen.data.status){
            yield put(actions.fetchScreensSuccess(resScreen.data.data.screens));
        }else{
            yield put(actions.fetchScreensFail(resScreen.data.message));
        } 
    }catch(error){
        yield put(actions.fetchScreensFail(error.message));
    }
}

export function* userSignUpSaga(action){
    const data = {
        ...action.data,
        country_code: CountryCode,
        role: RoleDefault.USER,
    };

	try{
        const res = yield axios.post('/api/users/user-register', data);

        if (res.data.status){
            yield put(actions.userSignUpSuccess(res.data.message));
        }else{
            yield put(actions.userSignUpFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.userSignUpFail(error.message));
	}
}

export function* setStatusUserSaga(action){
	try{
        const res = yield axios.put('/api/users/set-status-user/' + action.uuid, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.setStatusUserSuccess(res.data.message));
        }else{
            yield put(actions.setStatusUserFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.setStatusUserFail(error.message));
	}
}

export function* getCitySaga(){
	try{
        const res = yield axios.get('/api/general-api/city');

        if (res.data.status){
            yield put(actions.getCitySuccess(res.data));
        }else{
            yield put(actions.getCityFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getCityFail(error.message));
	}
}

export function* getDistrictSaga(action){
	try{
        const res = yield axios.get('/api/general-api/district/' + action.id);

        if (res.data.status){
            yield put(actions.getDistrictSuccess(res.data));
        }else{
            yield put(actions.getDistrictFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getDistrictFail(error.message));
	}
}

export function* getWardSaga(action){
	try{
        const res = yield axios.get('/api/general-api/ward/' + action.id);

        if (res.data.status){
            yield put(actions.getWardSuccess(res.data));
        }else{
            yield put(actions.getWardFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getWardFail(error.message));
	}
}

export function* updateUserSaga(action){
    const data = {
        ...action.data,
        country_code: CountryCode,
    }
	try{
        const res = yield axios.put('/api/users/' + action.id, data, {
            headers: {
                "Authorization": "bearer " + action.token,
            }
        });

        if (res.data.status){
            yield put(actions.updateUserSuccess(res.data.message));
        }else{
            yield put(actions.updateUserFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateUserFail(error.message));
	}
}

