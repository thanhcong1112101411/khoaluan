import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { Imagetype } from 'Utils/enum.utils';

export function* addImageSaga(action){
	try{
        const res = yield axios.post('/api/images/add-image', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addImagesSuccess(res.data.message));
        }else{
            yield put(actions.addImagesSuccess(res.data.message));
        } 
	}catch(error){
		yield put(actions.addImagesFail(error.message));
	}
}

export function* fetchBannerImageSaga(action){
	try{
        const res = yield axios.get('/api/images/' + Imagetype.BANNER);

        if (res.data.status){
            yield put(actions.fetchBannerImagesSuccess(res.data));
        }else{
            yield put(actions.fetchBannerImagesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchBannerImagesFail(error.message));
	}
}

export function* fetchContentTopImageSaga(action){
	try{
        const res = yield axios.get('/api/images/' + Imagetype.CONTENT_TOP);

        if (res.data.status){
            yield put(actions.fetchContentTopImagesSuccess(res.data));
        }else{
            yield put(actions.fetchContentTopImagesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchContentTopImagesFail(error.message));
	}
}

export function* fetchLogoImageSaga(action){
	try{
        const res = yield axios.get('/api/images/' + Imagetype.LOGO);

        if (res.data.status){
            yield put(actions.fetchLogoImagesSuccess(res.data));
        }else{
            yield put(actions.fetchLogoImagesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchLogoImagesFail(error.message));
	}
}

export function* deleteImageSaga(action){
	try{
        const res = yield axios.delete('/api/images/delete-image/' + action.id, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.deleteImagesSuccess(res.data.message));
        }else{
            yield put(actions.deleteImagesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteImagesFail(error.message));
	}
}