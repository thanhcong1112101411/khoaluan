import {put, call} from 'redux-saga/effects';
import axios from 'axios-api';

import * as actions from 'Store/Actions/index';
import { UserType, UserStatus } from 'Utils/enum.utils';

export function* fetchCatagoriesSaga(action){
	try{
        const limit = 10;

        let link = '/api/catagories/get-all?limit=' + limit;
        
        if (action.name.trim().length > 0){
            link += '&name=' + action.name;
        }
        if (action.current_page != 0){
            link += '&current_page=' + action.current_page;
        }
        
        const res = yield axios.get(link, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.fetchCatagoriesSuccess(res.data));
        }else{
            yield put(actions.fetchCatagoriesFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.fetchCatagoriesFail(error.message));
	}
}

export function* updateCatagorySaga(action){
	try{
        const res = yield axios.put('/api/catagories/update/'+ action.id, action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.updateCatagorySuccess(res.data));
        }else{
            yield put(actions.updateCatagoryFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.updateCatagoryFail(error.message));
	}
}
export function* addCatagorySaga(action){
	try{
        const res = yield axios.post('/api/catagories', action.data, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });

        if (res.data.status){
            yield put(actions.addCatagorySuccess(res.data));
        }else{
            yield put(actions.addCatagoryFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.addCatagoryFail(error.message));
	}
}


export function* deleteCatagorySaga(action){
	try{
        const res = yield axios.put('/api/catagories/delete/'+ action.id, null, {
            headers: {
                "Authorization": "bearer " + action.token
            }
        });
        
        if (res.data.status){
            yield put(actions.deleteCatagorySuccess(res.data));
        }else{
            yield put(actions.deleteCatagoryFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.deleteCatagoryFail(error.message));
	}
}

export function* getCatagoryDetailsSaga(action){
	try{
        const res = yield axios.get('/api/catagories/get-detail/' + action.id);

        if (res.data.status){
            yield put(actions.getCatagoryDetailSuccess(res.data));
        }else{
            yield put(actions.getCatagoryDetailFail(res.data.message));
        } 
	}catch(error){
		yield put(actions.getCatagoryDetailFail(error.message));
	}
}