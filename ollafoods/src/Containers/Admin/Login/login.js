import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Alert from 'react-bootstrap/Alert';
// ------------------------ CSS -----------------------------------------
import './login.css';
import 'Shared/style.css';

//-------------------------- COMPONENTS -------------------------------
import {updateObject, checkValidity, ImageSliderLink} from 'Shared/utility';
import SignInput from 'Components/UI/UIS/InputFirst/inputFirst';
import { Link } from 'react-router-dom';

class Login extends Component{
    state = {
		controls:{
			username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'username',
                    icon : 'user',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'password',
                    icon : 'password',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3
                },
                valid: false,
                touched: false
            }
		},
		formIsValid: false,
		isSignup: true
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
    }
    componentWillReceiveProps = (nextProps) => {
        if(nextProps.isAdmin){
            this.props.history.push("/admin");
        }
    }

    inputChangedHandler = (event, controlName) => {
        event.preventDefault();
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }
    loginClick = (event) =>{
        event.preventDefault();
        this.props.onAdminLogin(this.state.controls.username.value, this.state.controls.password.value);
    }

    render(){
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        let form = "";
        form = formElementsArray.map(formElement => (
        	<SignInput
                key = {formElement.id}
                icon = {formElement.config.elementConfig.icon}
        		elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
        	/>
        ));

        return(
            <div className = "LoginAdmin">
                <div className = "Content">
                    <div>
                        <h1>Tài Khoản Admin</h1>
                        {form}
                        <span><Link to="/forget-account">Quên mật khẩu?</Link></span>
                        <p style={{color: "red"}}>{this.props.error}</p>
                        <button
                             disabled = {!this.state.formIsValid}
                             onClick = {(event) => this.loginClick(event)}
                        >Đăng Nhập</button>
                    </div>
                    <div className="alert">
                        {
                            this.props.alert ?
                            <Alert variant={this.props.alert.varient}>
                                {this.props.alert.content}
                            </Alert>
                            : ""
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
        alert: state.auth.alert_admin_login,
        isAdmin: state.auth.uuid_admin != "",
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onAdminLogin: (username,password) =>dispatch(actions.adminLogin(username,password)),
    };  
};

export default connect(mapStateToProps,mapDispatchToProps)(Login);