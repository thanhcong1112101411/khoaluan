import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import HistoryItem from 'Components/Admin/Others/History/HistoryItem/historyItem';

class history extends Component {
    state = {
        searchValue: '',
        current_page: 1,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchHistory(this.state.searchValue, this.state.current_page, this.props.token);
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchHistory(this.state.searchValue,pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchHistory(value,1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return (
                <HistoryItem
                    key = {ig.id}
                    index = {index + 1}
                    name = {ig.user.first_name}
                    table = {ig.table}
                    action = {ig.action}
                    created_at = {ig.created_at}
                />
            )
        })

        return (
            <Auxx>
                <TitleAdmin>Lịch sử hoạt động</TitleAdmin>
                <Search
                    holder = "Họ và tên, tên dữ liệu"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Họ và tên</th><th>dữ liệu</th><th>Hành động</th><th>Ngày thực hiện</th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",
        
        list: state.history.list,
        load_more: state.history.load_more,
        pages: state.history.pages,
        alert: state.history.alert_fetch_history,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchHistory: (name, current_page, token) => dispatch(actions.fetchHistory(name, current_page, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(history);