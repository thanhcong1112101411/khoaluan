import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import FeedbackItem from 'Components/Admin/Others/Feedback/FeedbackItem/feedbackItem';
import { socket } from 'Shared/server';

class feedbacks extends Component {
    state = {
        searchValue: '',
        current_page: 1,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchFeedbacks(this.state.searchValue, this.state.current_page, this.props.token);
        
        // socket io
        socket.on("server-send-fetch-feedbacks", () => {
            this.props.onFetchFeedbacks(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchFeedbacks(this.state.searchValue,pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchFeedbacks(value,1, this.props.token);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return (
                <FeedbackItem
                    key = {ig.id}
                    index = {index + 1}
                    title = {ig.title}
                    content = {ig.content}
                    created_at = {ig.created_at}
                    name = {ig.user.first_name}
                    email = {ig.user.email}
                    uuid = {ig.user.uuid}
                />
            )
        })

        return (
            <Auxx>
                <TitleAdmin>Phản hồi</TitleAdmin>
                <Search
                    holder = "Tiêu đề"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Tiêu đề</th><th>Nội dung</th><th>Người gửi</th><th>Ngày thực hiện</th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,

        list: state.feedback.list,
        pages: state.feedback.pages,
        alert: state.feedback.alert_fetch_feedbacks,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchFeedbacks: (name, current_page, token) => dispatch(actions.fetchFeedbacks(name, current_page, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(feedbacks);