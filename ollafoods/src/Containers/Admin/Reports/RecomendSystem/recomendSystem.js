import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';


//----------------------------------- COMPONENTS ------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import {updateObject, checkValidity, getDateNow} from 'Shared/utility';
import Loading from 'Components/UI/UIS/Loading/loading';


class RecomendSystem extends Component {
    state = {
        controls:{
            date_from: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Từ ngày',
                },
                value: getDateNow() ,
                validation: {},
                valid: true,
                touched: false
            },
            date_to: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Đến ngày',
                },
                value: getDateNow() ,
                validation: {},
                valid: true,
                touched: false
            },
            utility: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Lợi nhuận nhỏ nhất',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
        },
        formIsValid: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        this.props.onGetEnvoiceAmount(getDateNow(), getDateNow(), this.props.token);

    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        
        let value= event.target.value;
        if (controlName == "date_from"){
            this.props.onGetEnvoiceAmount(value, this.state.controls.date_to.value, this.props.token);
            
        }else if(controlName == "date_to") {
            this.props.onGetEnvoiceAmount(this.state.controls.date_from.value, value, this.props.token);
        }
        else{
            value = event.target.value;
        }
        
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) => {
        event.preventDefault();
        let formData = {};
        for (let formElementIdentifier in this.state.controls) {
            formData[formElementIdentifier] = this.state.controls[formElementIdentifier].value;
        }

        this.props.onAnalyticHUI(formData);
    }

    // alert event
    setAlertAnalytic = (event) => {
        event.preventDefault();
        this.props.onSetAlertAnalyticHUI();
    }

    setAlertGetEnvoiceAmount = (event) => {
        event.preventDefault();
        this.props.onSetAlertGetEnvoiceAmount();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
            </form>
        );

        return (
            <Auxx>
                <TitleAdmin>Phân tích tập hữu ích</TitleAdmin>
                    <hr/>
                <div className="infostyle">
                    {form}
                </div>
                <p style = {{fontSize: "20px", margin: "20px 5px"}}>Số lượng hóa đơn: {" " + this.props.envoice_amount}</p>
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Phân tích</Button>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertAnalytic}
                    />
                    : ""
                }

                {/* {
                    !!this.props.alert_get_envoice_amount ?
                    <Alert
                        show = {!!this.props.alert_get_envoice_amount}
                        data = {this.props.alert_get_envoice_amount}
                        clicked = {this.setAlertGetEnvoiceAmount}
                    />
                    : ""
                } */}

                <Loading
                    show = {this.props.loading}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        alert_get_envoice_amount: state.recomendSystem.alert_get_envoice_amount,

        envoice_amount: state.recomendSystem.envoice_amount,
        alert: state.recomendSystem.alert_analytic_hui,
        loading: state.recomendSystem.loading,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetEnvoiceAmount: (date_from, date_to, token) => dispatch(actions.getEnvoiceAmount(date_from, date_to, token)),
        onSetAlertGetEnvoiceAmount: () => dispatch(actions.setAlertGetEnvoiceAmount()),
        onAnalyticHUI: (data) => dispatch(actions.analyticHUI(data)),
        onSetAlertAnalyticHUI: () => dispatch(actions.setAlertAnalyticHUI()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(RecomendSystem);