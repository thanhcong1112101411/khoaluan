import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- COMPONENTS --------------------------------------

class publicReport extends Component {
    state = {  }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

    }

    render() {
        return (
            <div>
                <div>
                publicReport
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(publicReport);