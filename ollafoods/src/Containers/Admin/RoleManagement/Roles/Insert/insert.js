import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';
import './insert.css';

//----------------------------------- COMPONENTS ------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';

class insert extends Component {
    state = { 
        controls:{
			name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tên nhóm quyền',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            screens: {
                elementType: 'checkbox',
                elementConfig: {
                    type: 'checkbox',
                    options: [],
                    placeholder: "Danh sách quyền"
                },
                value: [],
                validation: {
                    required: true,
                },
                valid: false
            },

		},
		formIsValid: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        this.props.onFetchAllScreens(this.props.token);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
        
        let allScreens = nextProps.allScreens;

        if (allScreens && allScreens.length > 0){
            let controls = this.state.controls;
            controls.screens.elementConfig.options = allScreens;
            this.setState({controls: controls});
        }

        // socket io
        if (nextProps.alert){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-role-group", null);
            }
        }
    }
    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = event.target.value;

        if (controlName == "screens"){
            let roleValue = this.state.controls[controlName].value;
            if(event.target.checked){
                roleValue.push(parseInt(value));
            }else{
                const index = roleValue.findIndex((item)=>{
                    return item == value;
                });
                roleValue.splice(index,1);
            }
            value = roleValue;
        }

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = async(event) =>{
        event.preventDefault();
        let formData = {};
        for (let formElementIdentifier in this.state.controls) {
            formData[formElementIdentifier] = this.state.controls[formElementIdentifier].value;
        }
        await this.props.onAddRoleGroup(formData, this.props.token);
    }

    //--------------------- alert event -------------------

    setAlertAddRoleGroupHandler = () => {
        this.props.onSetAlertAddRoleGroup();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Thêm</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Thêm nhóm quyền</TitleAdmin>
                <div className = "insertRoleDesgin">
                    {form}
                </div>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertAddRoleGroupHandler}
                    />
                    : ""
                }
                
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        allScreens: state.roleGroup.all_screens,
        alert: state.roleGroup.alert_add_role_group,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchAllScreens: (token) => dispatch(actions.fetchAllScreens(token)),
        onAddRoleGroup: (data, token) => dispatch(actions.addRoleGroup(data, token)),
        onSetAlertAddRoleGroup: () => dispatch(actions.setAlertAddRoleGroup()),

    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(insert);