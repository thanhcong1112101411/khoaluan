import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import { Link } from 'react-router-dom';

//----------------------------- CSs --------------------------------------------
import './roles.css';
import 'Shared/style.css';
// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import RoleGroupItem from 'Components/Admin/RoleManagement/Roles/RoleGroupItem/roleGroupItem';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import { socket } from 'Shared/server';

class roles extends Component {
    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
        deleteConfirm: false,
        id_using: 0,
        
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        this.props.onFetchRoleGroups(this.state.searchValue, this.state.current_page, this.props.token);
        // socket io
        socket.on("server-send-fetch-role-group", () => {
            this.props.onFetchRoleGroups(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-role-group", null);
                this.setState({isLoad: false});
            }
        }
    }

    // toggle
    toggleDeleteConfirm = () => {
        this.setState({deleteConfirm: !this.state.deleteConfirm});
    }

    // set id using
    setIdUsing = (id) => {
        this.setState({id_using: id});
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchRoleGroups(this.state.searchValue,pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchRoleGroups(value,1, this.props.token);
    }
    // confirm event
    confirmDeleteRoleGroupHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteRoleGroup(this.state.id_using, this.props.token);
        
        this.toggleDeleteConfirm();
        this.setState({isLoad: true});
    }

    // alert event
    setAlertDeleteRoleGroupHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertDeleteRoleGroup();
    }

    render() {

        const listItem = this.props.listRoleGroup.map((ig, index) => {
            return(
                <RoleGroupItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    name = {ig.name}
                    created_at = {ig.created_at}
                    screens = {ig.screens}
                    url = {this.props.location.pathname + "/update/" + ig.id }
                    // event
                    toggleDeleteConfirm = {this.toggleDeleteConfirm}
                    setIdUsing = {this.setIdUsing}
                />
            );
        })

        return (
            <Auxx>
                <TitleAdmin
                    type = "add"
                    url = {this.props.location.pathname + "/insert"}
                >Quản lý nhóm quyền</TitleAdmin>
                <Search
                    holder = "Tên nhóm quyền"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Name</th><th>Quyền</th><th>Ngày tạo</th><th></th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
                <Confirm
                    show = {this.state.deleteConfirm}
                    content = "Bạn có chắc muốn xóa nhóm quyền này?"
                    onCancelClick = {this.toggleDeleteConfirm}
                    onConfirmClick = {(event) => this.confirmDeleteRoleGroupHandler(event)}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {(event) => this.setAlertDeleteRoleGroupHandler(event)}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        listRoleGroup: state.roleGroup.list,
        load_more: state.roleGroup.load_more,
        pages: state.roleGroup.pages,
        alert: state.roleGroup.alert_delete_role_group,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchRoleGroups: (name, current_page, token) => dispatch(actions.fetchRoleGroups(name, current_page, token)),
        onDeleteRoleGroup: (id, token) => dispatch(actions.deleteRoleGroup(id, token)),
        onSetAlertDeleteRoleGroup: () => dispatch(actions.setAlertDeleteRoleGroup()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(roles);