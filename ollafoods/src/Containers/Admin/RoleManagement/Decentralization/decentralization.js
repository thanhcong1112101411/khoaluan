import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- CSS --------------------------------------
import 'Shared/style.css';
// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import RoleGroupItem from 'Components/Admin/RoleManagement/Roles/RoleGroupItem/roleGroupItem';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import Item from 'Components/Admin/RoleManagement/Decentralization/DecentralizationItem/DecentralizationItem';
import UpdateModel from 'Components/Admin/RoleManagement/Decentralization/UpdateModel/updateModel';
import { socket } from 'Shared/server';
import {AccountDefault} from 'Utils/enum.utils';

class decentralization extends Component {
    state = {
        isLoad: false,
        searchValue : '',
        current_page : 1,

        showUpdateModel: false,
        using_id : 1,
        using_uuid: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        this.props.onFetchActiveAccount(
            this.state.searchValue, 
            this.state.searchValue, 
            this.state.current_page,
            this.props.token
        );
        this.props.onFetchRoleGroups('',0, this.props.token);

        // socket-io
        socket.on("server-send-change-role-group", () => {
            this.props.onFetchActiveAccount(
                this.state.searchValue, 
                this.state.searchValue, 
                this.state.current_page,
                this.props.token
            );
        })
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-change-role-group", null);
                this.setStateIsLoad();
            }
        }
    }

    // toggle
    toggleUpdateModel = () => {
        this.setState({showUpdateModel: !this.state.showUpdateModel});
    }

    // set state
    setStateIsLoad = () => {
        this.setState({isLoad: !this.state.isLoad});
    }

    // set using id
    setUsingId = (id, uuid) => {
        this.setState({using_id: id});
        this.setState({using_uuid: uuid});
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchActiveAccount(this.state.searchValue, this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchActiveAccount(value, value, 1, this.props.token);
    }

    // alert event
    setAlertChangeRoleGroupHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertChangeRoleGroup();
        this.toggleUpdateModel();
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            if (ig.id !== AccountDefault){
                return(
                    <Item
                        key = {ig.uuid}
                        id = {ig.role.id}
                        uuid = {ig.uuid}
                        index = {index + 1}
                        name = {ig.first_name}
                        email = {ig.email}
                        role = {ig.role.name}
    
                        toggleUpdateModel = {this.toggleUpdateModel}
                        setUsingId = {this.setUsingId}
                    />  
                )
            }
        })

        return (
            <Auxx>
                <TitleAdmin>Phân quyền tài khoản</TitleAdmin>
                <Search
                    holder = "Họ tên, email tài khoản"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Họ tên</th><th>Email</th><th>Nhóm quyền</th><th></th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
                <UpdateModel
                    show = {this.state.showUpdateModel}
                    listRoleGroup = {this.props.listRoleGroup}
                    toggleUpdateModel = {this.toggleUpdateModel}
                    setStateIsLoad = {this.setStateIsLoad}
                    using_id = {this.state.using_id}
                    using_uuid = {this.state.using_uuid}
                    token = {this.props.token}
                    onChangeRoleGroup = {this.props.onChangeRoleGroup}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {(event) => this.setAlertChangeRoleGroupHandler(event)}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list: state.accounts.list_active,
        pages: state.accounts.pages_active,

        listRoleGroup: state.roleGroup.list,
        alert: state.accounts.alert_change_role_group,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchActiveAccount: (email, name, current_page, token) => dispatch(actions.fetchActiveAccounts(email, name, current_page, token)),
        onFetchRoleGroups: (name, current_page, token) => dispatch(actions.fetchRoleGroups(name, current_page, token)),
        onChangeRoleGroup: (uuid, id, token) => dispatch(actions.changeRoleGroup(uuid, id, token)),
        onSetAlertChangeRoleGroup: () => dispatch(actions.setAlertChangeRoleGroup()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(decentralization);