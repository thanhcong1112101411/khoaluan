import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';

class update extends Component {
    state = {
        controls:{
			name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tên danh mục',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: true,
                touched: false
            },
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: "Hình ảnh"
                },
                value: [],
                validation: {
                    required: true,
                },
                valid: true
            },

		},
        formIsValid: false,
        using_id: 0,
        is_load: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
        
        const id = this.props.match.params.id;
        this.setState({using_id: id});

        this.props.onGetCatagoryDetail(id);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        if (nextProps.detail){
            let controls = this.state.controls;
            controls.name.value = nextProps.detail.name;

            this.setState({controls: controls});

        }

        // socket io
        if (nextProps.alert){
            if (nextProps.alert.varient == "success" && this.state.is_load){
                socket.emit("client-send-fetch-catagories", null);
                this.props.onGetCatagoryDetail(this.state.using_id);
                this.setState({is_load: false});
            }
        }
                
        
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        
        let value = '';
        if (controlName == "images"){
            value = event.target.files[0];
        }else{
            value = event.target.value;
        }
        
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) =>{
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
        }
        
        this.props.onUpdateCatagory(this.state.using_id, form, this.props.token);
        this.setState({is_load: true});
    }

    //--------------------- alert event -------------------

    setAlertUpdateCatagoryHandler = () => {
        
        this.props.onSetAlertUpdateCatagory();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Cập nhật</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Cập nhật danh mục sản phẩm</TitleAdmin>
                <div className = "insertRoleDesgin">
                    {form}
                </div>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertUpdateCatagoryHandler}
                    />
                    : ""
                }
                
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        detail: state.catagory.detail,
        alert: state.catagory.alert_update_catagory,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onUpdateCatagory: (id,name,token)=>dispatch(actions.updateCatagory(id,name,token)),
        onSetAlertUpdateCatagory: () => dispatch(actions.setAlertUpdateCatagory()),
        onGetCatagoryDetail: (id) => dispatch(actions.getCatagoryDetail(id)),

    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(update);