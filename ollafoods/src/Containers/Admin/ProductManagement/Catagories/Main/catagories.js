import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

//----------------------------- CSs --------------------------------------------
import 'Shared/style.css';
import './catagories.css';


// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import CatagoryItem from "Components/Admin/ProductManagement/Catagories/CatagoryItem/catagoryItem";
import { socket } from 'Shared/server';

class catagories extends Component {
    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
        deleteConfirm: false,
        id_using: 0,

    }

    
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchCatagories(this.state.searchValue,this.state.current_page,this.props.token);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        socket.on("server-send-fetch-catagories", () => {
            this.props.onFetchCatagories(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-catagories", null);
                this.setState({isLoad: false});
            }
        }
    }

    // toggle
    toggleDeleteConfirm = () => {
        this.setState({deleteConfirm: !this.state.deleteConfirm});
    }

    // set id using
    setIdUsing = (id) => {
        this.setState({id_using: id});
    }

    // confirm event
    confirmDeleteCatagoryHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteCatagory(this.state.id_using, this.props.token);
        
        this.toggleDeleteConfirm();
        this.setState({isLoad: true});
    }
    // alert event
    setAlertDeleteCatagoryHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertDeleteCatagory();
    }

     // pagination, search
     nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchCatagories(this.state.searchValue,pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchCatagories(value,1, this.props.token);
    }
    
    render() {
        
        const listItem = this.props.list.map((ig, index) => {
            return(
                <CatagoryItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    name = {ig.name}
                    created_at = {ig.created_at}
                    image = {ig.image}
                    url = {this.props.location.pathname + "/update/" + ig.id }
                    // event
                     toggleDeleteConfirm = {this.toggleDeleteConfirm}
                     setIdUsing = {this.setIdUsing}
                />
            );
        })
        return (
            <Auxx>
                <TitleAdmin
                    type = "add"
                    url = {this.props.location.pathname + "/insert"}
                >Danh Mục Sản Phẩm</TitleAdmin>
                <Search
                    holder = "Tên danh mục"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Tên</th><th>Hình Ảnh</th><th>Ngày tạo</th><th></th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
                 <Confirm
                    show = {this.state.deleteConfirm}
                    content = "Bạn có chắc muốn xóa danh mục này?"
                    onCancelClick = {this.toggleDeleteConfirm}
                    onConfirmClick = {(event) => this.confirmDeleteCatagoryHandler(event)}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {(event) => this.setAlertDeleteCatagoryHandler(event)}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list:state.catagory.list,
        load_more: state.catagory.load_more,
        pages: state.catagory.pages,
        alert: state.catagory.alert_delete_catagory,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchCatagories: (name, current_page, token) => dispatch(actions.fetchCatagories(name, current_page, token)),
        onDeleteCatagory: (id, token) => dispatch(actions.deleteCatagory(id, token)),
        onSetAlertDeleteCatagory: () => dispatch(actions.setAlertDeleteCatagory()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(catagories);