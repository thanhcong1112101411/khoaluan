import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------

import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import UpdateItem from 'Components/Admin/ProductManagement/Prices/Update/updateItem';
import { PriceType } from 'Utils/enum.utils';

class update extends Component {

    state = {
        using_id: null,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        const id = this.props.match.params.id;
        this.setState({using_id: id});
    }

    orderHandler = (price, type) => {
        const data = {
            price: price,
            type: type,
            productId: this.state.using_id,
        };

        this.props.onAddPrices(data, this.props.token);
    }
    
    setAlertAddPrices = () => {
        this.props.onSetAlertAddPrices();
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin>Cập nhật giá</TitleAdmin>
                <hr/>
                <UpdateItem
                    title = "Giá nhập"
                    type = {PriceType.IMPORT}
                    orderHandler = {this.orderHandler}
                />  
                <UpdateItem
                    title = "Giá bán"
                    type = {PriceType.EXPORT}
                    orderHandler = {this.orderHandler}
                />  
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertAddPrices}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        alert: state.prices.alert_add_prices,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onAddPrices: (data, token) => dispatch(actions.addPrices(data, token)),
        onSetAlertAddPrices: () => dispatch(actions.setAlertAddPrices()),

    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(update);