import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

//----------------------------- CSs --------------------------------------------
import 'Shared/style.css';

// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import { socket } from 'Shared/server';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import ImportPrice from 'Components/Admin/ProductManagement/Prices/ImportPrice/importPrice';
import ExportPrice from 'Components/Admin/ProductManagement/Prices/ExportPrice/exportPrice';

class pricesDetails extends Component {
    state = {
        using_id : null,
    }

    
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
        const id = this.props.match.params.id;
        this.setState({using_id: id});
        this.props.onGetPricesDetail(id, this.props.token);
       
        // socket io
        socket.on("server-send-fetch-prices", () => {
            this.props.onGetPricesDetail(id, this.props.token);
        })
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-prices", null);
                this.setState({isLoad: false});
            }
        }
    }



     // pagination, search
     nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchPrices(this.state.searchValue,pageNumber, this.props.token);
    }
    
    
    render() {
        return (
            <Auxx>
                <TitleAdmin>Chi Tiết Giá Sản Phẩm</TitleAdmin>
                <Tabs id="controlled-tab-example" className= "tabStyle">
                    <Tab eventKey="importPrice" title="Giá nhập">
                        <ImportPrice
                            list = {this.props.detail ? this.props.detail.importPriceList : []}
                        />
                    </Tab>
                    <Tab eventKey="exportPrice" title="Giá bán" >
                        <ExportPrice
                            list = {this.props.detail ? this.props.detail.exportPriceList : []}
                        />
                    </Tab>
                </Tabs>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        detail: state.prices.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetPricesDetail: (id, token) => dispatch(actions.getPricesDetail(id, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(pricesDetails);