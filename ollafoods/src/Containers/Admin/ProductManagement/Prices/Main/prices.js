import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

//----------------------------- CSs --------------------------------------------
import 'Shared/style.css';


// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import PriceItem from "Components/Admin/ProductManagement/Prices/PriceItem/priceItem";
import { socket } from 'Shared/server';

class prices extends Component {
    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
        deleteConfirm: false,
        id_using: 0,

    }
    
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchProductsPrices(this.state.searchValue,this.state.current_page,this.props.token);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        socket.on("server-send-fetch-prices", () => {
            this.props.onFetchProductsPrices(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // // socket io
        // if (nextProps.alert && this.state.isLoad){
        //     if (nextProps.alert.varient == "success"){
        //         socket.emit("client-send-fetch-prices", null);
        //         this.setState({isLoad: false});
        //     }
        // }
    }



     // pagination, search
     nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchProductsPrices(this.state.searchValue, pageNumber,this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchProductsPrices(value,1 ,this.props.token);
    }
    
    render() {
        
        const listItem = this.props.list.map((ig, index) => {
            return(
                <PriceItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    code = {ig.code}
                    name = {ig.name}
                    created_at = {ig.created_at}
                    image = {JSON.parse(ig.images)[0]}
                    importPrice = {ig.importPrice}
                    exportPrice = {ig.exportPrice}
                    status = {ig.status}

                    // url
                    update_url = {this.props.location.pathname + "/update/" + ig.id }
                    detail_url = {this.props.location.pathname + "/detail/" + ig.id }

                    // event
                     toggleDeleteConfirm = {this.toggleDeleteConfirm}
                     setIdUsing = {this.setIdUsing}
                />
            );
        })
        return (
            <Auxx>
                <TitleAdmin>Giá Sản Phẩm</TitleAdmin>
                <Search
                    holder = "Tên sản phẩm, mã sản phẩm, tên danh mục"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Mã SP</th><th colSpan = "2">Sản phẩm</th><th>Giá Nhập</th><th>Giá bán</th><th>Lợi nhuận</th><th>Trạng thái</th><th></th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list:state.prices.list_products,
        load_more: state.prices.load_more_products,
        pages: state.prices.pages_products,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchProductsPrices: (name, current_page, token) => dispatch(actions.fetchProductsPrices(name, current_page, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(prices);