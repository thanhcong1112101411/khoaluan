import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------

import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import AlertAdd from 'Components/UI/UIS/AlertSecond/alertSecond';
import {updateObject, checkValidity, getDateNow} from 'Shared/utility';
import { socket } from 'Shared/server';
import DiscountInf from 'Components/Admin/ProductManagement/Discounts/Add/DiscountInfo/discountInfo';
import DiscountProducts from 'Components/Admin/ProductManagement/Discounts/Add/DiscountProducts/discountProducts';
import AddProductModel from 'Components/Admin/ProductManagement/Discounts/Add/AddProductModel/addProductModel';
class addDiscount extends Component {
    state = {
        controls:{
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tên giảm giá',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
            amout: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Phần trăm giảm giá',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
            date_from: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Từ ngày',
                },
                value: getDateNow() ,
                validation: {},
                valid: true,
                touched: false
            },
            date_to: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Đến ngày',
                },
                value: getDateNow() ,
                validation: {},
                valid: true,
                touched: false
            },
        },
        productsPicking: [],
        formIsValid: false,

        showAlertAdd: false,
        showProductModel: false,

        // error
        showErrorNonProduct: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
        this.props.onFetchProductsPickingDiscounts(this.state.productsPicking, this.props.token);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // // socket io
        // if (nextProps.alert){
        //     if (nextProps.alert.varient == "success"){
        //         socket.emit("client-send-fetch-discounts", null);
        //     }
        // }
    }

    // toggle
    toggleProductModel = () =>{
        this.setState({showProductModel: !this.state.showProductModel})
    }
    toggleShowErrorNonProduct = () => {
        this.setState({showErrorNonProduct: false});
    }

    onFetchProductsDiscountsHandler = (name, current_page) => {
        this.props.onFetchProductsDiscounts(name, current_page, this.state.controls.date_from.value, this.state.controls.date_to.value, this.props.token);
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        
        let value = '';
        if (controlName == "date_from" || controlName == "date_to"){
            value = event.target.value;
            this.setState({productsPicking: []});
            this.props.onFetchProductsPickingDiscounts([], this.props.token);
            
        }else{
            value = event.target.value;
        }
        
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) => {
        event.preventDefault();

        if (this.state.productsPicking.length == 0){
            this.setState({showErrorNonProduct: true});
            return;
        }

        let formData = {};
        for (let formElementIdentifier in this.state.controls) {
            formData[formElementIdentifier] = this.state.controls[formElementIdentifier].value;
        }
        formData.products = this.state.productsPicking;

        this.props.onAddDiscounts(formData, this.props.token);
    }

    // alert event
    
    setAlertAddDiscounts = () => {
        this.props.onSetAlertAddDiscounts();
    }

    // add products picking
    addProductPicking = (id) =>{
        
        let productsPicking = this.state.productsPicking;
        if(productsPicking.length == 0){
            productsPicking.push(id);
        }else{
            let index = -1;
            for(let i=0; i<productsPicking.length; i++){
                if(productsPicking[i].id === id){
                    index = i;
                }
            }
            if(index == -1){
                productsPicking.push(id);
            }else{
                console.log("Sản phẩm này đã được chọn");
            }
        }
        this.setState({productsPicking: productsPicking});
        this.props.onFetchProductsPickingDiscounts(productsPicking, this.props.token);
        //alert
        this.setState({showAlertAdd: true});
        setTimeout(() => {
            this.setState({showAlertAdd: false})
        }, 2000);
    }

    deleteProductPicking = (id) =>{
        let productsPicking = this.state.productsPicking;
        let index = -1;
        for(let i=0; i<productsPicking.length; i++){
            if(productsPicking[i].id === id){
                index = i;
            }
        }
        productsPicking.splice(index, 1);
        this.setState({productsPicking: productsPicking});
        this.props.onFetchProductsPickingDiscounts(this.state.productsPicking, this.props.token);
    }

    onFetchProductsPickingDiscountsHandler = () => {
        this.props.onFetchProductsPickingDiscounts(this.state.productsPicking, this.props.token);
    }


    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        
        return (
            <Auxx>
                <TitleAdmin>Thêm giảm giá sản phẩm</TitleAdmin>
                <hr/>
                <DiscountInf 
                    formElementsArray = {formElementsArray}
                    inputChangedHandler = {this.inputChangedHandler}
                />
                <DiscountProducts
                    toggleProductModel = {this.toggleProductModel}
                    productsPicking = {this.props.productsPicking}
                    deleteProductPicking = {this.deleteProductPicking}
                    onFetchProductsDiscountsHandler = {this.onFetchProductsDiscountsHandler}
                />
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Thêm</Button>
                <AddProductModel
                    token = {this.props.token}
                    show= {this.state.showProductModel}
                    toggleProductModel = {this.toggleProductModel}
                    onFetchProductsDiscountsHandler = {this.onFetchProductsDiscountsHandler}
                    productsAdd = {this.props.productsDiscounts}
                    productsPicking = {this.state.productsPicking}
                    pages = {this.props.pages_discounts_product}
                    // pages = {this.props.productsAddPage}
                    addProductPicking = {this.addProductPicking}
                    onFetchProductsPickingDiscountsHandler = {this.onFetchProductsPickingDiscountsHandler}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertAddDiscounts}
                    />
                    : ""
                }
                <AlertAdd
                    title="Đã thêm sản phẩm"
                    show={this.state.showAlertAdd}
                />

                {/* error alert */}
                <Alert
                    show = {this.state.showErrorNonProduct}
                    data = {{
                        varient: "danger",
                        content: "Không có sản phẩm được chọn"
                    }}
                    clicked = {this.toggleShowErrorNonProduct}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        alert: state.discounts.alert_add_discounts,

        productsDiscounts: state.discounts.discounts_product,
        pages_discounts_product: state.discounts.pages_discounts_product,

        productsPicking: state.discounts.picking_products,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchProductsDiscounts: (name, current_page, date_from, date_to, token) => dispatch(actions.fetchProductsDiscounts(name, current_page, date_from, date_to, token)),
        onAddDiscounts: (data, token) => dispatch(actions.addDiscounts(data, token)),
        onSetAlertAddDiscounts: () => dispatch(actions.setAlertAddDiscounts()),
        onFetchProductsPickingDiscounts: (data, token) => dispatch(actions.fetchProductsPickingDiscounts(data, token)),
        
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(addDiscount);