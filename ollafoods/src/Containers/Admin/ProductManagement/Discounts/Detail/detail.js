import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------

import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import AlertAdd from 'Components/UI/UIS/AlertSecond/alertSecond';
import {updateObject, checkValidity, getDateNow, formatDateYYMMDD} from 'Shared/utility';
import { socket } from 'Shared/server';
import DiscountInf from 'Components/Admin/ProductManagement/Discounts/Add/DiscountInfo/discountInfo';
import DiscountProducts from 'Components/Admin/ProductManagement/Discounts/Add/DiscountProducts/discountProducts';
import AddProductModel from 'Components/Admin/ProductManagement/Discounts/Add/AddProductModel/addProductModel';
import ProductList from 'Components/Admin/ProductManagement/Discounts/Detail/ProductList/productList';

class Detail extends Component {
    state = {
        controls:{
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tên giảm giá',
                    note: '',
                    disabled: "true",
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: '',
                    disabled: "true",
                },
                valid: false,
                touched: false
            },
            amout: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Phần trăm giảm giá',
                    note: '',
                    disabled: "true",
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                },
                valid: false,
                touched: false
            },
            date_from: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Từ ngày',
                    disabled: "true",
                },
                value: getDateNow() ,
                validation: {},
                valid: true,
                touched: false
            },
            date_to: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Đến ngày',
                    disabled: "true",
                },
                value: getDateNow() ,
                validation: {},
                valid: true,
                touched: false
            },
        },
        productsPicking: [],
        formIsValid: false,

        showAlertAdd: false,
        showProductModel: false,

        // error
        showErrorNonProduct: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        this.props.onGetDiscountDetail(this.props.match.params.id, this.props.token);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        const detail_next = nextProps.detail;
        if (detail_next){
            let controls = this.state.controls;
            controls.name.value = detail_next.name;
            controls.amout.value = detail_next.amount;
            controls.date_from.value = formatDateYYMMDD(detail_next.date_from);
            controls.date_to.value = formatDateYYMMDD(detail_next.date_to);

            this.setState({controls: controls});
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        
        return (
            <Auxx>
                <TitleAdmin>Chi tiết giảm giá</TitleAdmin>
                <hr/>
                <DiscountInf 
                    formElementsArray = {formElementsArray}
                    inputChangedHandler = {this.inputChangedHandler}
                />
                <ProductList
                    list = {this.props.detail && this.props.detail.products ? this.props.detail.products : []}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        alert: state.discounts.alert_get_detail,
        detail: state.discounts.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetDiscountDetail: (id, token) => dispatch(actions.getDiscountDetail(id, token)),
    
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);