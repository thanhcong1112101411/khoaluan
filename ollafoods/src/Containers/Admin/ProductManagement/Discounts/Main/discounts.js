import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import DiscountsAppling from 'Components/Admin/ProductManagement/Discounts/DiscountsAppling/discountsAppling';
import DiscountsApplied from 'Components/Admin/ProductManagement/Discounts/DiscountsApplied/discountsApplied';
import DiscountsDeleted from 'Components/Admin/ProductManagement/Discounts/DiscountsDeleted/discountsDeleted';
import DiscountsWaiting from 'Components/Admin/ProductManagement/Discounts/DiscountsWaiting/discountWaiting';
import { socket } from 'Shared/server';


class discounts extends Component {
    state = {
        isLoad: false,
        confirm_set_status_block: false,
        confirm_set_status_open: false,
        using_uuid: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }


    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-discounts", null);
                this.setState({isLoad: false});
            }
        }
    }

    // toggle
    toggleConfirmSetStatusBlock = () => {
        this.setState({confirm_set_status_block: !this.state.confirm_set_status_block});
    }

    toggleConfirmSetStatusOpen = () => {
        this.setState({confirm_set_status_open: !this.state.confirm_set_status_open});
    }

    // set using id
    setUsingUuid = (uuid) => {
        this.setState({using_uuid: uuid});
    }

    // confirm event
    onConfirmSetStatusDiscountsBlock = async() => {
        await this.props.onSetStatusDiscounts(this.state.using_uuid, this.props.token);
        this.toggleConfirmSetStatusBlock();

        // socket io
        this.setState({isLoad: true});
    }

    onConfirmSetStatusDiscountsOpen = async() => {
        await this.props.onSetStatusDiscounts(this.state.using_uuid, this.props.token);
        this.toggleConfirmSetStatusOpen();

        // socket io
        this.setState({isLoad: true});
    }

    // alert event
    onSetAlertSetStatusDiscountsHandler = () => {
        this.props.onSetAlertSetStatusDiscounts();
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin
                    type = "add"
                    url = {this.props.location.pathname + "/insert"}
                >Giảm giá</TitleAdmin>

                <Tabs id="controlled-tab-example" className= "tabStyle">
                    <Tab eventKey="home" title="Đang áp dụng">
                        <DiscountsAppling 
                            onFetchDiscounts = {this.props.onFetchDiscountsAppling}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_appling}
                            list = {this.props.list_appling}

                            // event
                            toggleConfirmSetStatusBlock = {this.toggleConfirmSetStatusBlock}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                    <Tab eventKey="applied" title="Đã áp dụng" >
                        <DiscountsApplied
                            onFetchDiscounts = {this.props.onFetchDiscountsApplied}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_applied}
                            list = {this.props.list_applied}

                            // event
                            toggleConfirmSetStatusOpen = {this.toggleConfirmSetStatusOpen}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                    <Tab eventKey="waiting" title="Chờ áp dụng" >
                        <DiscountsWaiting
                            onFetchDiscounts = {this.props.onFetchDiscountsWaiting}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_waiting}
                            list = {this.props.list_waiting}

                            // event
                            toggleConfirmSetStatusOpen = {this.toggleConfirmSetStatusOpen}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                    <Tab eventKey="delete" title="Đã xóa" >
                        <DiscountsDeleted
                            onFetchDiscounts = {this.props.onFetchDiscountsDeleted}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_deleted}
                            list = {this.props.list_deleted}

                            // event
                            toggleConfirmSetStatusOpen = {this.toggleConfirmSetStatusOpen}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                </Tabs>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.onSetAlertSetStatusDiscountsHandler}
                    />
                    : ""
                }

                {/* confirm block account */}
                <Confirm
                    show = {this.state.confirm_set_status_block}
                    content = "Bạn có chắc muốn khóa sản phẩm này?"
                    onCancelClick = {this.toggleConfirmSetStatusBlock}
                    onConfirmClick = {this.onConfirmSetStatusDiscountsBlock}
                />

                {/* confirm open account */}
                <Confirm
                    show = {this.state.confirm_set_status_open}
                    content = "Bạn có chắc muốn mở khóa sản phẩm này?"
                    onCancelClick = {this.toggleConfirmSetStatusOpen}
                    onConfirmClick = {this.onConfirmSetStatusDiscountsOpen}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list_waiting: state.discounts.list_waiting,
        pages_waiting: state.discounts.pages_waiting,

        list_appling: state.discounts.list_appling,
        pages_appling: state.discounts.pages_appling,

        list_applied: state.discounts.list_applied,
        pages_applied: state.discounts.pages_applied,
        
        list_deleted: state.discounts.list_deleted,
        pages_deleted: state.discounts.pages_deleted,

        alert: state.discounts.alert_set_status_discounts,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchDiscountsWaiting: ( name, current_page, token) => dispatch(actions.fetchDiscountsWaiting( name, current_page, token)),
        onFetchDiscountsAppling: ( name, current_page, token) => dispatch(actions.fetchDiscountsAppling( name, current_page, token)),
        onFetchDiscountsApplied: ( name, current_page, token) => dispatch(actions.fetchDiscountsApplied( name, current_page, token)),
        onFetchDiscountsDeleted: ( name, current_page, token) => dispatch(actions.fetchDiscountsDeleted( name, current_page, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(discounts);