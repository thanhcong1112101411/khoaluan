import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import UnitItem from 'Components/Admin/ProductManagement/UnitsItem/unitItem';
import { socket } from 'Shared/server';

class units extends Component {
    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
        deleteConfirm: false,
        id_using: 0,

    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-units", null);
                this.setState({isLoad: false});
            }
        }
    }

    // toggle
    toggleDeleteConfirm = () => {
        this.setState({deleteConfirm: !this.state.deleteConfirm});
    }

    // set id using
    setIdUsing = (id) => {
        this.setState({id_using: id});
    }

    // confirm event
    confirmDeleteUnitsHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteUnits(this.state.id_using, this.props.token);
        
        this.toggleDeleteConfirm();
        this.setState({isLoad: true});
    }
    // alert event
    setAlertDeleteUnitsHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertDeleteUnits();
    }

     // pagination, search
     nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchCatagories(this.state.searchValue,pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchUnits(value,1, this.props.token);
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchUnits(this.state.searchValue,this.state.current_page,this.props.token);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        socket.on("server-send-fetch-units", () => {
            this.props.onFetchUnits(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <UnitItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    name = {ig.name}
                    created_at = {ig.created_at}
                    image = {ig.image}
                    url = {this.props.location.pathname + "/update/" + ig.id }
                    // event
                     toggleDeleteConfirm = {this.toggleDeleteConfirm}
                     setIdUsing = {this.setIdUsing}
                />
            );
        })
        return (
            <div>
                <div>
                <Auxx>
                <TitleAdmin
                    type = "add"
                    url = {this.props.location.pathname + "/insert"}
                >Đơn vị tính</TitleAdmin>
                <Search
                    holder = "Tên đơn vị tính"
                    value = {this.state.searchValue}
                    changed = {(event) => this.onChangeSearchValue(event)}
                />
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>STT</th><th>Tên</th><th>Ngày tạo</th><th></th></tr>
                    </thead>
                    <tbody>
                        {listItem}
                    </tbody>
                </table>
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
                 <Confirm
                    show = {this.state.deleteConfirm}
                    content = "Bạn có chắc muốn xóa thương hiệu này?"
                    onCancelClick = {this.toggleDeleteConfirm}
                    onConfirmClick = {(event) => this.confirmDeleteUnitsHandler(event)}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {(event) => this.setAlertDeleteUnitsHandler(event)}
                    />
                    : ""
                }
            </Auxx>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list:state.units.list,
        load_more: state.units.load_more,
        pages: state.units.pages,
        alert: state.units.alert_delete_units,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchUnits: (name, current_page, token) => dispatch(actions.fetchUnits(name, current_page, token)),
        onDeleteUnits: (id, token) => dispatch(actions.deleteUnits(id, token)),
        onSetAlertDeleteUnits: () => dispatch(actions.setAlertDeleteUnits()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(units);