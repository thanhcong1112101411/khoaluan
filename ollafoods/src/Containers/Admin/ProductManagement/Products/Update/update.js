import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';
import './update.css';

//----------------------------------- COMPONENTS ------------------------------

import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import {updateObject, checkValidity} from 'Shared/utility';
import ProductImageItem from 'Components/Admin/ProductManagement/Products/ProductImageItem/productImageItem';
import ProductImageAddedItem from 'Components/Admin/ProductManagement/Products/ProductImageAddedItem/productImageAddedItem';
import { socket } from 'Shared/server';

class update extends Component {
    state = {
        controls:{
            code: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mã sản phẩm',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tên sản phẩm',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            catagory_id:{
                elementType: 'selectDefault',
                elementConfig: {
                    placeholder: 'Tên danh mục',
                    options: [],
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            brand_id:{
                elementType: 'selectDefault',
                elementConfig: {
                    placeholder: 'Thương hiệu',
                    options:[],
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            unit_id:{
                elementType: 'selectDefault',
                elementConfig: {
                    placeholder: 'Đơn vị tính',
                    options:[],
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            min: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Số lượng mua nhỏ nhất',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            description_first: {
                elementType: 'ckeditor',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mô tả 1',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            description_second: {
                elementType: 'ckeditor',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mô tả 2',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: 'File Ảnh'
                },
                value: [],
                validation: {
                    required: true
                },
                valid: true,
                touched: false
            },
        },
        image_added: [],
        formIsValid: false,
        using_id: null,
        is_load: false,
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onGetCatagoryName();
        this.props.onGetBrandName();
        this.props.onGetUnitName();
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        const id = this.props.match.params.id;
        this.setState({using_id: id});

        this.props.onGetProductsDetail(id);

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.catagory_list){
            let catagoryArr = nextProps.catagory_list;
            catagoryArr=catagoryArr.map(ig=>{
                return {
                    value:ig.id,
                    displayValue:ig.name,
                }
            })
            
            let controls=this.state.controls;
            controls.catagory_id.elementConfig.options=catagoryArr;
            this.setState({controls:controls})
        }
        if(nextProps.brand_list){
            let brandArr = nextProps.brand_list;
            brandArr=brandArr.map((ig)=>{
                return {
                    value:ig.id,
                    displayValue:ig.name,
                }
            })
            
            let controls = this.state.controls;
            controls.brand_id.elementConfig.options = brandArr;
            this.setState({controls:controls})
        }
        if(nextProps.unit_list){
            let unitArr = nextProps.unit_list;
            unitArr=unitArr.map((ig)=>{
                return {
                    value:ig.id,
                    displayValue:ig.name,
                }
            })
            
            let controls=this.state.controls;
            controls.unit_id.elementConfig.options=unitArr;
            this.setState({controls:controls})
        }

        const detail_next = nextProps.detail;
        if (detail_next){
            let controls = this.state.controls;
            controls.name.value = detail_next.name;
            controls.code.value = detail_next.code;
            controls.catagory_id.value = detail_next.catagoryId;
            controls.unit_id.value = detail_next.unitId;
            controls.brand_id.value = detail_next.brandId;
            controls.min.value = detail_next.min;
            controls.description_first.value = detail_next.description_first;
            controls.description_second.value = detail_next.description_second;

            this.setState({controls: controls});
            this.setState({image_added: JSON.parse(detail_next.images)})

        }

        // socket io
        if (nextProps.alert){
            if (nextProps.alert.varient == "success" && this.state.is_load){
                socket.emit("client-send-fetch-products", null);
                let controls=this.state.controls;
                controls.images.value = [];
                this.setState({controls:controls})
                this.props.onGetProductsDetail(this.state.using_id);
                this.setState({is_load: false});
            }
        }
    }
    orderHandler = (event) => {
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            if (formElementIdentifier == "images"){
                console.log("images", this.state.controls.images.value);
                this.state.controls[formElementIdentifier].value.forEach((item) => {
                    form.append('images', item.value);
                })
            } else{
                form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
            }
        }
        form.append("quantity", 0);
        form.append("rrp", 0);
        form.append("image_added", JSON.stringify(this.state.image_added));
        
        this.props.onUpdateProduct(this.state.using_id, form, this.props.token);
        this.setState({is_load: true})
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();

        let value = '';
        let valid = '';
        if (controlName == "images"){
            let imgArr = this.state.controls.images.value;
            let id;
            if (imgArr.length == 0){
                id = 0;
            }else{
                id = imgArr[imgArr.length-1].id + 1;
            }

            let local = "";
            let file = event.target.files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                local = reader.result;
                imgArr.push({
                    id : id,
                    value : file,
                    local : local,
                })
                value = imgArr;
                valid = value;

                this.updateControls(value, valid, controlName);
            }

        }else if(controlName == "description_first" || controlName == "description_second"){
            value = event.editor.getData();
            valid = value;

            this.updateControls(value, valid, controlName);
        }
        else{
            value = event.target.value;
            valid = event.target.value;

            this.updateControls(value, valid, controlName);
        }
    }

    updateControls = (value, valid, controlName) => {
        const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(valid, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    deleteImageItem=(index)=>{
        let controls=this.state.controls;
        
        controls.images.value.splice(index,1);
        this.setState({
            controls:controls,
         })
    }
    deleteImageAddedItem = (index)=>{
        let imagesAdded = this.state.image_added;
        imagesAdded.splice(index,1);

        this.setState({image_added: imagesAdded, formIsValid: true})
    }
    
    // alert event
    setAlertUpdate = () => {
        this.props.onSetAlertUpdateProduct();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let imageTag="";
        if(this.state.controls.images.value.length != 0){
            imageTag=this.state.controls.images.value.map((ig,index)=>{
                
               return (
                     <ProductImageItem 
                        key={index}
                        name={ig.name}
                        index = {index}
                        image_url = {ig.local}
                        deleteImageItem={this.deleteImageItem} 
                    />
               )
            })
        }   

        let imageAddedTag = null;
        if(this.state.image_added.length != 0){
            imageAddedTag = this.state.image_added.map((ig,index)=>{
               return (
                    <ProductImageAddedItem
                        key={index}
                        image_url = {ig.url}
                        index = {index}
                        deleteImageAddedItem = {this.deleteImageAddedItem}
                    />
               );
            })
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <div className = "productListImageAdd">
                    {imageTag}
                </div>
                <div className = "productListImageAdd">
                    {imageAddedTag}
                </div>
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Cập nhật</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Cập nhật sản phẩm</TitleAdmin>
                    <hr/>
                <div className="insertRoleDesgin">
                    {form}
                
                </div>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertUpdate}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        catagory_list:state.products.catagory_list,
        brand_list:state.products.brand_list,
        unit_list:state.products.unit_list,
        alert: state.products.alert_update_product,
        detail: state.products.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onUpdateProduct: (id, data, token) => dispatch(actions.updateProducts(id, data, token)),
        onSetAlertUpdateProduct: () => dispatch(actions.setAlertUpdateProducts()),
        onGetCatagoryName: () => dispatch(actions.getCatagoryName()),
        onGetBrandName: () => dispatch(actions.getBrandName()),
        onGetUnitName: () => dispatch(actions.getUnitName()),
        onGetProductsDetail: (id) => dispatch(actions.getProductsDetail(id)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(update);