import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';
import './addProduct.css';

//----------------------------------- COMPONENTS ------------------------------

import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import {updateObject, checkValidity} from 'Shared/utility';
import ProductImageItem from 'Components/Admin/ProductManagement/Products/ProductImageItem/productImageItem';



class addProduct extends Component {
    state = {
        controls:{
            code: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mã sản phẩm',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tên sản phẩm',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
            catagory_id:{
                elementType: 'selectDefault',
                elementConfig: {
                    placeholder: 'Tên danh mục',
                    options: [],
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            brand_id:{
                elementType: 'selectDefault',
                elementConfig: {
                    placeholder: 'Thương hiệu',
                    options:[],
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            unit_id:{
                elementType: 'selectDefault',
                elementConfig: {
                    placeholder: 'Đơn vị tính',
                    options:[],
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            min: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Số lượng mua nhỏ nhất',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: false,
                touched: false
            },
            description_first: {
                elementType: 'ckeditor',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mô tả 1',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            description_second: {
                elementType: 'ckeditor',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mô tả 2',
                    note: ''
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    note: ''
                },
                valid: true,
                touched: false
            },
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: 'File Ảnh'
                },
                value: [],
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
        },
        formIsValid: false,
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onGetCatagoryName();
        this.props.onGetBrandName();
        this.props.onGetUnitName();
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.catagory_list){
            let catagoryArr=nextProps.catagory_list;
            let firstId=null;
            catagoryArr=catagoryArr.map((ig,index)=>{
                if(index==0){
                    firstId=ig.id;
                }
                return {
                    value:ig.id,
                    displayValue:ig.name,
                }
            })
            
            let controls=this.state.controls;
            controls.catagory_id.elementConfig.options=catagoryArr;
            controls.catagory_id.value=firstId;
            this.setState({
               controls:controls,
            })
        }
        if(nextProps.brand_list){
            let brandArr=nextProps.brand_list;
            let firstId=null;
            brandArr=brandArr.map((ig,index)=>{
                if(index==0){
                    firstId=ig.id;
                }
                return {
                    value:ig.id,
                    displayValue:ig.name,
                }
            })
            
            let controls=this.state.controls;
            controls.brand_id.elementConfig.options=brandArr;
            controls.brand_id.value=firstId;
            this.setState({
               controls:controls,
            })
        }
        if(nextProps.unit_list){
            let unitArr=nextProps.unit_list;
            let firstId=null;
            unitArr=unitArr.map((ig,index)=>{
                if(index==0){
                    firstId=ig.id;
                }
                return {
                    value:ig.id,
                    displayValue:ig.name,
                }
            })
            
            let controls=this.state.controls;
            controls.unit_id.elementConfig.options=unitArr;
            controls.unit_id.value=firstId;
            this.setState({
               controls:controls,
            })
        }
    }
    orderHandler = (event) => {
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            if (formElementIdentifier == "images"){
                console.log("images", this.state.controls.images.value);
                this.state.controls[formElementIdentifier].value.forEach((item) => {
                    form.append('images', item.value);
                })
            } else{
                form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
            }
        }
        form.append("quantity", 0);
        form.append("rrp", 0);
        this.props.onAddProduct(form, this.props.token);
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();

        let value = '';
        let valid = '';
        if (controlName == "images"){
            let imgArr = this.state.controls.images.value;
            let id;
            if (imgArr.length == 0){
                id = 0;
            }else{
                id = imgArr[imgArr.length-1].id + 1;
            }

            let local = "";
            let file = event.target.files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                local = reader.result;
                imgArr.push({
                    id : id,
                    value : file,
                    local : local,
                })
                value = imgArr;
                valid = value;

                this.updateControls(value, valid, controlName);
            }

        }else if(controlName == "description_first" || controlName == "description_second"){
            value = event.editor.getData();
            valid = value;

            this.updateControls(value, valid, controlName);
        }
        else{
            value = event.target.value;
            valid = event.target.value;

            this.updateControls(value, valid, controlName);
        }
        
    }

    updateControls = (value, valid, controlName) => {
        const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(valid, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    deleteImageItem=(key)=>{
        let controls=this.state.controls;
        
        controls.images.value.splice(key,1);
        this.setState({
            controls:controls,
         })
    }
    
    // alert event
    setAlertAddProduct = () => {
        this.props.onSetAlertAddProduct();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        let imageTag = "";
        
        if(this.state.controls.images.value.length != 0){
            imageTag = this.state.controls.images.value.map((ig,index)=>{
               return (
                     <ProductImageItem 
                        name={ig.name}
                        image_url = {ig.local}
                        deleteImageItem={()=>this.deleteImageItem(index)}
                        key={index}>
                           
                    </ProductImageItem>
               )
            })
        }   
        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <div className = "productListImageAdd">
                    {imageTag}
                </div>
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Thêm</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Thêm sản phẩm</TitleAdmin>
                    <hr/>
                <div className="insertRoleDesgin">
                    {form}
                
                </div>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertAddProduct}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        catagory_list:state.products.catagory_list,
        brand_list:state.products.brand_list,
        unit_list:state.products.unit_list,
        alert: state.products.alert_add_product,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onAddProduct: (data, token) => dispatch(actions.addProducts(data, token)),
        onSetAlertAddProduct: () => dispatch(actions.setAlertAddProducts()),
        onGetCatagoryName: () => dispatch(actions.getCatagoryName()),
        onGetBrandName: () => dispatch(actions.getBrandName()),
        onGetUnitName: () => dispatch(actions.getUnitName()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(addProduct);