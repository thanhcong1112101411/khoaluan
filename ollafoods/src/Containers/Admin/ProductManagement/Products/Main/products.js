import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import ProductLock from 'Components/Admin/ProductManagement/Products/ProductLock/productLock';
import ProductSelling from 'Components/Admin/ProductManagement/Products/ProductSelling/productSelling';
import ProductWaiting from 'Components/Admin/ProductManagement/Products/ProductWaiting/ProductWaiting';
import { socket } from 'Shared/server';


class product extends Component {
    state = {
        isLoad: false,
        confirm_set_status_block: false,
        confirm_set_status_open: false,
        using_id: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }


    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-products", null);
                this.setState({isLoad: false});
            }
        }
    }

    // toggle
    toggleConfirmSetStatusBlock = () => {
        this.setState({confirm_set_status_block: !this.state.confirm_set_status_block});
    }

    toggleConfirmSetStatusOpen = () => {
        this.setState({confirm_set_status_open: !this.state.confirm_set_status_open});
    }
    // set using id
    setUsingId = (id) => {
        this.setState({using_id: id});
    }

    // confirm event
    onConfirmSetStatusProductBlock = () => {
        this.toggleConfirmSetStatusBlock();
        this.props.onSetStatusProduct(this.state.using_id, this.props.token);
        // socket io
        this.setState({isLoad: true});
    }

    onConfirmSetStatusProductOpen = () => {

        this.toggleConfirmSetStatusOpen();
        this.props.onSetStatusProduct(this.state.using_id, this.props.token);
        // socket io
        this.setState({isLoad: true});
    }

    // alert event
    onSetAlertSetStatusProductHandler = () => {
        this.props.onSetAlertSetStatusProduct();
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin
                    type = "add"
                    url = {this.props.location.pathname + "/insert"}
                >Danh sách sản phẩm</TitleAdmin>

                <Tabs id="controlled-tab-example" className= "tabStyle">
                    <Tab eventKey="home" title="Đang bán">
                        <ProductSelling
                            onFetchProducts = {this.props.onFetchProductsSelling}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_selling}
                            list = {this.props.list_selling}

                            // event
                            toggleConfirmSetStatusBlock = {this.toggleConfirmSetStatusBlock}
                            setUsingId = {this.setUsingId}
                        />
                    </Tab>
                    <Tab eventKey="profile" title="Chờ cập nhật giá" >
                        <ProductWaiting
                            onFetchProducts = {this.props.onFetchProductsWaiting}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_waiting}
                            list = {this.props.list_waiting}

                            // event
                            toggleConfirmSetStatusBlock = {this.toggleConfirmSetStatusBlock}
                            setUsingId = {this.setUsingId}
                        />
                    </Tab>
                    <Tab eventKey="khoa" title="Đang khóa" >
                        <ProductLock
                            onFetchProducts = {this.props.onFetchProductsLock}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_lock}
                            list = {this.props.list_lock}

                            // event
                            toggleConfirmSetStatusOpen = {this.toggleConfirmSetStatusOpen}
                            setUsingId = {this.setUsingId}
                        />
                    </Tab>
                </Tabs>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.onSetAlertSetStatusProductHandler}
                    />
                    : ""
                }

                {/* confirm block account */}
                <Confirm
                    show = {this.state.confirm_set_status_block}
                    content = "Bạn có chắc muốn khóa sản phẩm này?"
                    onCancelClick = {this.toggleConfirmSetStatusBlock}
                    onConfirmClick = {this.onConfirmSetStatusProductBlock}
                />

                {/* confirm open account */}
                <Confirm
                    show = {this.state.confirm_set_status_open}
                    content = "Bạn có chắc muốn mở khóa sản phẩm này?"
                    onCancelClick = {this.toggleConfirmSetStatusOpen}
                    onConfirmClick = {this.onConfirmSetStatusProductOpen}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list_waiting: state.products.list_waiting,
        pages_waiting: state.products.pages_waiting,

        list_selling: state.products.list_selling,
        pages_selling: state.products.pages_selling,

        list_lock: state.products.list_lock,
        pages_lock: state.products.pages_lock,

        alert: state.products.alert_set_status_product,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchProductsWaiting: ( name, current_page, token) => dispatch(actions.fetchProductsWaiting( name, current_page, token)),
        onFetchProductsSelling: ( name, current_page, token) => dispatch(actions.fetchProductsSelling( name, current_page, token)),
        onFetchProductsLock: ( name, current_page, token) => dispatch(actions.fetchProductsLock( name, current_page, token)),
        onSetStatusProduct: (id, token) => dispatch(actions.setStatusProduct(id, token)),
        onSetAlertSetStatusProduct: () => dispatch(actions.setAlertSetStatusProduct()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(product);