import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- COMPONENTS --------------------------------------
import BannerImage from 'Components/Admin/Interfaces/Images/Banner/banner';
import ContentTopImage from 'Components/Admin/Interfaces/Images/ContentTop/contentTop'
import LogoImage from 'Components/Admin/Interfaces/Images/Logo/logo'
import { socket } from 'Shared/server';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';

class images extends Component {
    state = {
        isLoad: false,
        deleteConfirm: false,
        id_using: 0,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        this.props.onFetchBannerImages();
        this.props.onFetchContentTopImages();
        this.props.onFetchLogoImages();

        // socket io
        socket.on("server-send-fetch-images", () => {
            this.props.onFetchBannerImages();
            this.props.onFetchContentTopImages();
            this.props.onFetchLogoImages();   
        })
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert_add && this.state.isLoad){
            if (nextProps.alert_add.varient == "success"){
                socket.emit("client-send-fetch-images", null);
                this.setState({isLoad: false});
            }
        }

        if (nextProps.alert_delete && this.state.isLoad){
            if (nextProps.alert_delete.varient == "success"){
                socket.emit("client-send-fetch-images", null);
                this.setState({isLoad: false});
            }
        }

    }

    // toggle
    toggleDeleteConfirm = () => {
        this.setState({deleteConfirm: !this.state.deleteConfirm});
    }

    // set id using
    setIdUsing = (id) => {
        this.setState({id_using: id});
    }

    // confirm event
    confirmDeleteImageHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteImage(this.state.id_using, this.props.token);
        
        this.toggleDeleteConfirm();
        this.setState({isLoad: true});
    }
    
    // alert event
    setAlertDeleteImageHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertDeleteImage();
    }

    onAddImagesHandler = (data) => {
        this.props.onAddImages(data, this.props.token);
        this.setState({isLoad: true});
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin>Quản lý hình ảnh</TitleAdmin>
                <LogoImage
                    list = {this.props.logo} 
                    onAddImagesHandler = {this.onAddImagesHandler}
                    alert_add = {this.props.alert_add}
                    onSetAlertAddImage = {this.props.onSetAlertAddImage}
                    toggleDeleteConfirm = {this.toggleDeleteConfirm}
                    setIdUsing = {this.setIdUsing}
                />
                <BannerImage 
                    list = {this.props.banner} 
                    onAddImagesHandler = {this.onAddImagesHandler}
                    alert_add = {this.props.alert_add}
                    onSetAlertAddImage = {this.props.onSetAlertAddImage}
                    toggleDeleteConfirm = {this.toggleDeleteConfirm}
                    setIdUsing = {this.setIdUsing}
                />
                <ContentTopImage 
                    list = {this.props.content_top} 
                    onAddImagesHandler = {this.onAddImagesHandler}
                    alert_add = {this.props.alert_add}
                    onSetAlertAddImage = {this.props.onSetAlertAddImage}
                    toggleDeleteConfirm = {this.toggleDeleteConfirm}
                    setIdUsing = {this.setIdUsing}
                />

                <Confirm
                    show = {this.state.deleteConfirm}
                    content = "Bạn có chắc muốn xóa hình ảnh này?"
                    onCancelClick = {this.toggleDeleteConfirm}
                    onConfirmClick = {(event) => this.confirmDeleteImageHandler(event)}
                />
                {
                    !!this.props.alert_delete ?
                    <Alert
                        show = {!!this.props.alert_delete}
                        data = {this.props.alert_delete}
                        clicked = {(event) => this.setAlertDeleteImageHandler(event)}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        isAdmin: state.auth.uuid_admin != "",
        token: state.auth.token_admin,

        alert_add: state.image.alert_add_images,
        alert_delete: state.image.alert_delete_images,
        banner: state.image.banner,
        content_top: state.image.content_top,
        logo: state.image.logo,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onAddImages: (data, token) => dispatch(actions.addImages(data, token)),
        onSetAlertAddImage: () => dispatch(actions.setAlertAddImages()),
        onFetchBannerImages: () => dispatch(actions.fetchBannerImages()),
        onFetchContentTopImages: () => dispatch(actions.fetchContentTopImages()),
        onFetchLogoImages: () => dispatch(actions.fetchLogoImages()),
        onDeleteImage: (id, token) => dispatch(actions.deleteImages(id, token)),
        onSetAlertDeleteImage: () => dispatch(actions.setAlertdeleteImages()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(images);