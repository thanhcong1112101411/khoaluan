import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';

// --------------------------- COMPONENTS --------------------------------------

class contents extends Component {
    state = {  }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin>Quản lý nội dung</TitleAdmin>
                <table className = "tableDesign roleTableDesign">
                    <thead>
                        <tr><th>ID</th><th>Tiêu đề</th><th>Mô tả</th><th>Nội dung</th><th></th></tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        isAdmin: state.auth.uuid_admin != "",
        token: state.auth.token_admin,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(contents);