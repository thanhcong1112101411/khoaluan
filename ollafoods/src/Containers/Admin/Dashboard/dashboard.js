import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import { Redirect } from 'react-router-dom';
import { socket, ImageLogoLink } from 'Shared/server';

// --------------------------- CSs -------------------------------
import './dashboard.css';

class Dashboard extends Component {
    state = {  }

    componentDidMount(){ 
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
    }
    componentWillReceiveProps = (nextProps) => {
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
    }

    render() {
        return (
            <div className = "dashboard">
                <div>
                    <img width = "200px" src={ImageLogoLink + "logo.png"}/>
                    <p>Công ty cung cấp thực phẩm sạch an toàn, chất lượng hàng đầu Việt Nam</p>
                </div>
            </div>
            
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        isAdmin: state.auth.uuid_admin != "",
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);