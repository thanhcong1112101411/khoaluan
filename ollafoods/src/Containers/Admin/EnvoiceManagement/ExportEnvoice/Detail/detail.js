import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';
import EnvoiceInformation from 'Components/Admin/ProductManagement/ExportEnvoice/Detail/EnvoiceInformation/envoiceInformation';
import UserInformation from 'Components/Admin/ProductManagement/ExportEnvoice/Detail/UserInformation/userInformation';
import ProductInformation from 'Components/Admin/ProductManagement/ExportEnvoice/Detail/ProductInformation/productInformation';


class Detail extends Component {
    state = {
        is_load: false,
        using_id: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

        const id = this.props.match.params.id;
        this.setState({using_id: id});

        this.props.onGetExportEnvoiceDetail(id, this.props.token);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // // socket io
        // if (nextProps.alert && this.state.is_load){
        //     if (nextProps.alert.varient == "success"){
        //         socket.emit("client-send-fetch-export-envoices", null);
        //         this.setState({is_load: false});
        //     }
        // }
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin>Chi tiết hóa đơn</TitleAdmin>
                <EnvoiceInformation
                    detail = {this.props.detail}
                />
                <UserInformation
                    detail = {this.props.detail ? this.props.detail.user : null}
                />
                <ProductInformation
                    list = {this.props.detail ? this.props.detail.envoiceDetails : []}
                />  
                
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        detail: state.exportEnvoice.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetExportEnvoiceDetail: (id, token) => dispatch(actions.getExportEnvoiceDetail(id, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);