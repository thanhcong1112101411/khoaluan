import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import ExportEnvoiceWaiting from 'Components/Admin/ProductManagement/ExportEnvoice/ExportEnvoiceWaiting/exportEnvoiceWaiting';
import ExportEnvoiceCompleted from 'Components/Admin/ProductManagement/ExportEnvoice/ExportEnvoiceCompleted/exportEnvoiceCompleted';
import ExportEnvoiceCanceled from 'Components/Admin/ProductManagement/ExportEnvoice/ExportEnvoiceCanceled/exportEnvoiceCanceled';
import { socket } from 'Shared/server';
import { ExportEnvoiceType } from 'Utils/enum.utils';


class exportEnvoice extends Component {
    state = {
        is_load: false,
        using_id: '',

        confirm_set_status_completed: false,
        confirm_set_status_canceled: false,
        confirm_set_status_wating: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }


    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.is_load){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-export-envoices", null);
                this.setState({is_load: false});
            }
        }
    }

    // toggle
    toggleConfirmSetStatusCompleted = () => {
        this.setState({confirm_set_status_completed: !this.state.confirm_set_status_completed});
    }

    toggleConfirmSetStatusCanceled = () => {
        this.setState({confirm_set_status_canceled: !this.state.confirm_set_status_canceled});
    }

    toggleConfirmSetStatusWating = () => {
        this.setState({confirm_set_status_wating: !this.state.confirm_set_status_wating});
    }

    // set using id
    setUsingId = (id) => {
        this.setState({using_id: id});
    }

    // confirm event
    onConfirmSetStatusCompleted = () => {
        this.props.onSetStatusExportEnvoice(this.state.using_id, ExportEnvoiceType.COMPLETED, this.props.token);
        this.toggleConfirmSetStatusCompleted();

        this.setState({is_load: true});
    }

    onConfirmSetStatusCanceled = () => {
        this.props.onSetStatusExportEnvoice(this.state.using_id, ExportEnvoiceType.CANCELED, this.props.token);
        this.toggleConfirmSetStatusCanceled();

        this.setState({is_load: true});
    }

    onConfirmSetStatusWating = () => {
        this.props.onSetStatusExportEnvoice(this.state.using_id, ExportEnvoiceType.WATING, this.props.token);
        this.toggleConfirmSetStatusWating();

        this.setState({is_load: true});
    }

    // alert event
    onSetAlertSetStatusHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertSetStatusExportEnvoice();
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin>Danh sách hóa đơn</TitleAdmin>

                <Tabs id="controlled-tab-example" className= "tabStyle">
                    <Tab eventKey="home" title="Đang chờ xử lý">
                        <ExportEnvoiceWaiting 
                            onFetchExportEnvoice = {this.props.onFetchExportEnvoiceWaiting}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_waiting}
                            list = {this.props.list_waiting}

                            // event
                            toggleConfirmSetStatusCanceled = {this.toggleConfirmSetStatusCanceled}
                            toggleConfirmSetStatusCompleted = {this.toggleConfirmSetStatusCompleted}
                            setUsingId = {this.setUsingId}
                        />
                    </Tab>
                    <Tab eventKey="completed" title="Hoàn thành" >
                        <ExportEnvoiceCompleted
                            onFetchExportEnvoice = {this.props.onFetchExportEnvoiceCompleted}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_completed}
                            list = {this.props.list_completed}

                            // event
                            toggleConfirmSetStatusCanceled = {this.toggleConfirmSetStatusCanceled}
                            toggleConfirmSetStatusWating = {this.toggleConfirmSetStatusWating}
                            setUsingId = {this.setUsingId}
                        />
                    </Tab>
                    <Tab eventKey="canceled" title="Đã hủy" >
                        <ExportEnvoiceCanceled
                            onFetchExportEnvoice = {this.props.onFetchExportEnvoiceCanceled}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_canceled}
                            list = {this.props.list_canceled}

                            // event
                            toggleConfirmSetStatusWating = {this.toggleConfirmSetStatusWating}
                            toggleConfirmSetStatusCompleted = {this.toggleConfirmSetStatusCompleted}
                            setUsingId = {this.setUsingId}
                        />
                    </Tab>
                </Tabs>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.onSetAlertSetStatusHandler}
                    />
                    : ""
                }

                {/* confirm completed */}
                <Confirm
                    show = {this.state.confirm_set_status_completed}
                    content = "Hóa đơn đã hoàn thành?"
                    onCancelClick = {this.toggleConfirmSetStatusCompleted}
                    onConfirmClick = {this.onConfirmSetStatusCompleted}
                />

                {/* confirm canceled */}
                <Confirm
                    show = {this.state.confirm_set_status_canceled}
                    content = "Bạn có chắc muốn hủy hóa đơn này?"
                    onCancelClick = {this.toggleConfirmSetStatusCanceled}
                    onConfirmClick = {this.onConfirmSetStatusCanceled}
                />

                {/* confirm wating */}
                <Confirm
                    show = {this.state.confirm_set_status_wating}
                    content = "Bạn muốn chuyển hóa đơn sang trạng thái chưa xử lý?"
                    onCancelClick = {this.toggleConfirmSetStatusWating}
                    onConfirmClick = {this.onConfirmSetStatusWating}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list_waiting: state.exportEnvoice.list_waiting,
        pages_waiting: state.exportEnvoice.pages_waiting,

        list_canceled: state.exportEnvoice.list_canceled,
        pages_canceled: state.exportEnvoice.pages_canceled,

        list_completed: state.exportEnvoice.list_completed,
        pages_completed: state.exportEnvoice.pages_completed,

        alert: state.exportEnvoice.alert_set_status,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchExportEnvoiceWaiting: ( name, current_page, token) => dispatch(actions.fetchExportEnvoiceWaiting( name, current_page, token)),
        onFetchExportEnvoiceCompleted: ( name, current_page, token) => dispatch(actions.fetchExportEnvoiceCompleted( name, current_page, token)),
        onFetchExportEnvoiceCanceled: ( name, current_page, token) => dispatch(actions.fetchExportEnvoiceCanceled( name, current_page, token)),
        onSetStatusExportEnvoice: ( id, status, token) => dispatch(actions.setStatusExportEnvoice( id, status, token)),
        onSetAlertSetStatusExportEnvoice: () => dispatch(actions.setAlertSetStatusExportEnvoice()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(exportEnvoice);