import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';
import UploadImageModel from 'Components/Admin/NewsManagement/News/UploadImageModel/uploadImageModel';

class newsUpdate extends Component {
    state = {
        controls:{
			title: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tiêu đề',
                    rows: 4,
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1
                },
                valid: true,
                touched: false
            },
            
            content: {
                elementType: 'ckeditor',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Nội dung',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: true,
                touched: false
            },
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: "Hình ảnh"
                },
                value: [],
                validation: {
                    required: true,
                },
                valid: true
            },

		},
        formIsValid: false,
        using_id: 0,
        is_load: false,
        showUploadImageModel: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
        let pathName = this.props.location.pathname.split("/");
        const id = pathName[pathName.length - 1];

        this.setState({using_id: id});

        this.props.onGetNewsDetail(id);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        if (nextProps.detail){
            let controls = this.state.controls;
            controls.content.value = nextProps.detail.content;
            controls.title.value = nextProps.detail.title;

            this.setState({controls: controls});

        }

        // socket io
        if (nextProps.alert){
            if (nextProps.alert.varient == "success" && this.state.is_load){
                socket.emit("client-send-fetch-news", null);
                this.props.onGetNewsDetail(this.state.using_id);
                this.setState({is_load: false});
            }
        }
                
        
    }

    // toggle
    toggleUploadImagemodel = () => {
        this.setState({showUploadImageModel: !this.state.showUploadImageModel})
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        
        let value = '';
        let valid = '';
        if (controlName == "images"){
            value = event.target.files[0];
            valid = event.target.value;
        }else if(controlName == "content"){
            value = event.editor.getData();
            valid = value;
        }
        else{
            value = event.target.value;
            valid = event.target.value;
        }

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(valid, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) =>{
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
        }
        
        this.props.onUpdateNews(this.state.using_id, form, this.props.token);
        this.setState({is_load: true});
    }

    onUploadImagesHandler = (data) => {
        this.props.onUploadImages(data, this.props.token);
    }

    //--------------------- alert event -------------------

    setAlertUpdateHandler = () => {
        this.props.onSetAlertUpdateNews();
    }

    setAlertUploadImageHandler = () => {
        this.props.onSetAlertUploadImage();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Cập nhật</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Cập nhật tin tức</TitleAdmin>
                <hr/>
                <Button
                    type = "main"
                    align = "right"
                    disabled = {false}
                    clicked = {this.toggleUploadImagemodel}
                >Upload Image</Button>
                <div className = "insertRoleDesgin">
                    {form}
                </div>
                <UploadImageModel 
                    show = {this.state.showUploadImageModel}
                    toggle = {this.toggleUploadImagemodel}
                    onUploadImagesHandler = {this.onUploadImagesHandler}
                    image_upload = {this.props.image_upload}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertUpdateHandler}
                    />
                    : ""
                }
                {
                    !!this.props.alert_upload_image ?
                    <Alert
                        show = {!!this.props.alert_upload_image}
                        data = {this.props.alert_upload_image}
                        clicked = {this.setAlertUploadImageHandler}
                    />
                    : ""
                }
                
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        detail: state.news.detail,
        alert: state.news.alert_update_news,

        alert_upload_image: state.news.alert_upload,
        image_upload : state.news.image_upload,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onUpdateNews:(id,name,token)=>dispatch(actions.updateNews(id,name,token)),
        onSetAlertUpdateNews: () => dispatch(actions.setAlertUpdateNews()),
        onGetNewsDetail: (id) => dispatch(actions.getNewsDetail(id)),
        onUploadImages: (data, token) => dispatch(actions.uploadImage(data, token)),
        onSetAlertUploadImage: () => dispatch(actions.setAlertUploadImage()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(newsUpdate);