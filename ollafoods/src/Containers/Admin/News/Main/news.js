import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
//----------------------------- CSs --------------------------------------------
import 'Shared/style.css';

// --------------------------- COMPONENTS --------------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Search from 'Components/UI/UIS/Search/search';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import { socket } from 'Shared/server';
import NewItem from 'Components/Admin/NewItem/NewItem';
import { baseUrl } from 'Shared/server';


class news extends Component {
    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
        deleteConfirm: false,
        id_using: 0,

        alert_update_news: null,
        alert_add_news: null,
        alert_delete_news: null,

    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
        this.props.onFetchNews(this.state.searchValue,this.state.current_page,this.props.token);

        // socket io
        socket.on("server-send-fetch-news", () => {
            this.props.onFetchNews(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }
        // toggle
        toggleDeleteConfirm = () => {
            this.setState({deleteConfirm: !this.state.deleteConfirm});
        }
    
        // set id using
        setIdUsing = (id) => {
            this.setState({id_using: id});
        }
    
        // confirm event
        confirmDeleteNewsHandler = (event) => {
            event.preventDefault();
            this.props.onDeleteNews(this.state.id_using, this.props.token);
            
            this.toggleDeleteConfirm();
            this.setState({isLoad: true});
        }
        // alert event
        setAlertDeleteNewsHandler = (event) => {
            event.preventDefault();
            this.props.onSetAlertDeleteNews();
        }
    
         // pagination, search
         nextPage = (pageNumber) =>{
            this.setState({current_page: pageNumber});
            this.props.onFetchNews(this.state.searchValue,pageNumber, this.props.token);
        }
        onChangeSearchValue = (event) =>{
            event.preventDefault();
            let value = event.target.value;
            this.setState({searchValue: value});
            this.props.onFetchNews(value,1, this.props.token);
        }
        componentWillReceiveProps(nextProps){
            if(!nextProps.isAdmin){
                this.props.history.push('/admin/login');
            }
    
            // socket io
            if (nextProps.alert && this.state.isLoad){
                if (nextProps.alert.varient == "success"){
                    socket.emit("client-send-fetch-news", null);
                    this.setState({isLoad: false});
                }
            }
        }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <NewItem
                    key = {ig.id}
                    id = {ig.id}
                    index = {index + 1}
                    title = {ig.title}
                    click={ig.click}
                    created_at = {ig.created_at}
                    image = {ig.image}
                    url = {this.props.location.pathname + "/update/" + ig.id }
                    // event
                     toggleDeleteConfirm = {this.toggleDeleteConfirm}
                     setIdUsing = {this.setIdUsing}
                />
            );
        })
        return (
            <Auxx>
            <TitleAdmin
                type = "add"
                url = {this.props.location.pathname + "/insert"}
            >Tin Tức</TitleAdmin>
            <Search
                holder = "Tiêu đề"
                value = {this.state.searchValue}
                changed = {(event) => this.onChangeSearchValue(event)}
            />
            <table className = "tableDesign roleTableDesign">
                <thead>
                    <tr><th>STT</th><th>Hình</th><th>Tiêu đề</th><th>Lượt xem</th><th>Ngày viết</th><th></th></tr>
                </thead>
                <tbody>
                    {listItem}
                </tbody>
            </table>
            <Pagination
                pages = {this.props.pages}
                currentPage = {this.state.current_page}
                nextPage = {this.nextPage}
            />
            <Confirm
                show = {this.state.deleteConfirm}
                content = "Bạn có chắc muốn xóa tin này?"
                onCancelClick = {this.toggleDeleteConfirm}
                onConfirmClick = {(event) => this.confirmDeleteNewsHandler(event)}
            />
            {
                !!this.props.alert ?
                <Alert
                    show = {!!this.props.alert}
                    data = {this.props.alert}
                    clicked = {(event) => this.setAlertDeleteNewsHandler(event)}
                />
                : ""
            }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list: state.news.list,
        load_more: state.news.load_more,
        pages: state.news.pages,
        alert: state.news.alert_delete_news,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchNews: (name, current_page, token) => dispatch(actions.fetchNews(name, current_page, token)),
        onDeleteNews: (id, token) => dispatch(actions.deleteNews(id, token)),
        onSetAlertDeleteNews: () => dispatch(actions.setAlertDeleteNews()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(news);