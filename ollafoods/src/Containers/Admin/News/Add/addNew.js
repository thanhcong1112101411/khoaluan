import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------

import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import BackDrop from 'Components/UI/UIS/BackDrop/backDrop';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import {updateObject, checkValidity} from 'Shared/utility';
import UploadImageModel from 'Components/Admin/NewsManagement/News/UploadImageModel/uploadImageModel';

class addNew extends Component {
    state = {
        controls:{
            title: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tiêu đề',
                    rows: 4,
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1
                },
                valid: false,
                touched: false
            },
            
            content: {
                elementType: 'ckeditor',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Nội dung',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            images: {
                elementType: 'inputFile',
                elementConfig: {
                    type: 'file',
                    placeholder: 'File Ảnh'
                },
                value: [],
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
        },
        formIsValid: false,
        showUploadImageModel: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }

    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
    }

    // toggle
    toggleUploadImagemodel = () => {
        this.setState({showUploadImageModel: !this.state.showUploadImageModel})
    }

    orderHandler = (event) => {
        event.preventDefault();
        let form = new FormData();
        for (let formElementIdentifier in this.state.controls) {
            form.append(formElementIdentifier, this.state.controls[formElementIdentifier].value);
        }
        this.props.onAddNews(form, this.props.token);
    }
    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = '';
        let valid = '';
        if (controlName == "images"){
            value = event.target.files[0];
            valid = event.target.value;
        }else if(controlName == "content"){
            value = event.editor.getData();
            valid = value;
        }
        else{
            value = event.target.value;
            valid = event.target.value;
        }
        
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(valid, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    onUploadImagesHandler = (data) => {
        this.props.onUploadImages(data, this.props.token);
    }

    // alert event
    setAlertAddNews = () => {
        this.props.onSetAlertAddNews();
    }
    setAlertUploadImageHandler = () => {
        this.props.onSetAlertUploadImage();
    }
    
    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        options = {formElement.options}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Thêm</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Thêm tin tức</TitleAdmin>
                    <hr/>
                <Button
                    type = "main"
                    align = "right"
                    disabled = {false}
                    clicked = {this.toggleUploadImagemodel}
                >Upload Image</Button>
                <div className="insertRoleDesgin">
                    {form}
                </div>
                <UploadImageModel 
                    show = {this.state.showUploadImageModel}
                    toggle = {this.toggleUploadImagemodel}
                    onUploadImagesHandler = {this.onUploadImagesHandler}
                    image_upload = {this.props.image_upload}
                />
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertAddNews}
                    />
                    : ""
                }
                {
                    !!this.props.alert_upload_image ?
                    <Alert
                        show = {!!this.props.alert_upload_image}
                        data = {this.props.alert_upload_image}
                        clicked = {this.setAlertUploadImageHandler}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        alert: state.news.alert_add_news,
        alert_upload_image: state.news.alert_upload,
        image_upload : state.news.image_upload,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onAddNews: (data, token) => dispatch(actions.addNews(data, token)),
        onSetAlertAddNews: () => dispatch(actions.setAlertAddNews()),
        onUploadImages: (data, token) => dispatch(actions.uploadImage(data, token)),
        onSetAlertUploadImage: () => dispatch(actions.setAlertUploadImage()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(addNew);