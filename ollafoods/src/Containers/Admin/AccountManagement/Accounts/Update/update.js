import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------------- CSS ----------------------------------
import 'Shared/style.css';

//----------------------------------- COMPONENTS ------------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import {updateObject, checkValidity, formatDateYYMMDD} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Button from 'Components/UI/UIType/ButtonFirst/buttonFirst';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { GenderType } from 'Utils/enum.utils';
import { socket } from 'Shared/server';

class update extends Component {
    state = { 
        controls:{
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Họ tên',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                },
                valid: true,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'tel',
                    placeholder: 'Số điện thoại',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                },
                valid: true,
                touched: false
            },
			email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Email đăng nhập',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    isEmail: true
                },
                valid: true,
                touched: false
            },
            bod: {
                elementType: 'input',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Ngày sinh',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            gender: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        { value: GenderType.MALE, displayValue: "Nam"},
                        { value: GenderType.FEMALE, displayValue: "Nữ"},
                        { value: GenderType.OTHER, displayValue: "Khác"},
                    ],
                    placeholder: "Giới tính",
                },
                value: GenderType.MALE,
                valid: true
            },
            city: {
                elementType: 'select',
                elementConfig: {
                    options: [],
                    placeholder: "Tỉnh/thành phố",
                },
                value: 0,
                valid: false,
            },
            district: {
                elementType: 'select',
                elementConfig: {
                    options: [],
                    placeholder: "Quận/huyện",
                },
                value: 0,
                valid: false,
            },
            ward: {
                elementType: 'select',
                elementConfig: {
                    options: [],
                    placeholder: "Phường/xã/thị trấn",
                },
                value: 0,
                valid: false,
            },
            address: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Địa chỉ (số nhà, tên đường)',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3
                },
                valid: false,
                touched: false
            },
            

		},
		formIsValid: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onGetCity();

        const pathName = this.props.location.pathname.split("/");
        this.props.onGetUserDetail(pathName[pathName.length-1], this.props.token);
    }
    componentWillReceiveProps(nextProps){
        const city = nextProps.city;
        if (city && city.length > 0){
            let controls = this.state.controls
            let cityArr = [];
            city.forEach(element => {
                let item = new Object();
                item.value = element.ID;
                item.displayValue = element.Title;
                cityArr.push(item);
            });
            controls.city.elementConfig.options = cityArr;
            this.setState({controls: controls});
        }

        const detail = nextProps.detail;
        if (detail && !!detail && this.state.controls.district.value == 0){
            let controls = this.state.controls;
            controls.name.value = detail.first_name;
            controls.email.value = detail.email;
            controls.phone.value = "0" + detail.phone;
            if (detail.address){
                controls.address.value = detail.address;
                controls.address.valid = true;
            }
            if (detail.bod){
                controls.bod.value = formatDateYYMMDD(detail.bod);
                controls.bod.valid = true;
            }
            if (detail.gender){
                controls.gender.value = detail.gender;
            }
        
            if (detail.city_id){
                controls.city.value = detail.city_id;
                controls.district.value = detail.district_id;
                controls.ward.value = detail.ward_id;

                controls.city.valid = true;
                controls.district.valid = true;
                controls.ward.valid = true;

                this.props.onGetDistrict(detail.city_id);
                this.props.onGetWard(detail.district_id);
            }

            this.setState({controls: controls});
        }

        const district = nextProps.district;
        if (district && district.length > 0){
            let controls = this.state.controls;
            let districtArr = [];
            district.forEach(element => {
                let item = new Object();
                item.value = element.ID;
                item.displayValue = element.Title;
                districtArr.push(item);
            });
            controls.district.elementConfig.options = districtArr;

            this.setState({controls: controls});
        }

        const ward = nextProps.ward;
        if (ward && ward.length > 0){
            let controls = this.state.controls;
            let wardArr = [];
            ward.forEach(element => {
                let item = new Object();
                item.value = element.ID;
                item.displayValue = element.Title;
                wardArr.push(item);
            });
            controls.ward.elementConfig.options = wardArr;

            this.setState({controls: controls});
        }

        // socket io
        if (nextProps.alert){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-account", null);
            }
        }
    }

    inputChangedHandler = (event, controlName) => {
        //event.preventDefault();
        let value = event.target.value;
        if (controlName == "city" || controlName == "district" || controlName == "ward" || controlName == "gender"){
            value = parseInt(value);
        }
        if (controlName == "city"){
            this.props.onGetDistrict(value);
        }
        if (controlName == "district"){
            this.props.onGetWard(value);
        }

    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: value,
            valid: checkValidity(value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) =>{
        event.preventDefault();
        let formData = {};
        for (let formElementIdentifier in this.state.controls) {
            formData[formElementIdentifier] = this.state.controls[formElementIdentifier].value;
        }
        const pathName = this.props.location.pathname.split("/");
        this.props.onUpdateUser(pathName[pathName.length-1], formData, this.props.token);
    }

    //--------------------- alert event -------------------
    setAlertUpdateUserHandler = () => {
        this.props.onSetAlertUpdateUser();
    }
    

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = (
            <form>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button
                    type = "main"
                    align = "center"
                    disabled = {!this.state.formIsValid}
                    clicked = {(event) => this.orderHandler(event)}
                >Cập nhật</Button>
            </form>
        );
        return (
            <Auxx>
                <TitleAdmin>Cập nhật tài khoản</TitleAdmin>
                <div className = "insertRoleDesgin">
                    {form}
                </div>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.setAlertUpdateUserHandler}
                    />
                    : ""
                }
                
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,

        city: state.auth.city,
        district: state.auth.district,
        ward: state.auth.ward,
        detail: state.users.detail,
        alert: state.auth.alert_update_user,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetUserDetail: (uuid, token) => dispatch(actions.getUserDetail(uuid, token)),
        onGetCity: () => dispatch(actions.getCity()),
        onGetDistrict: (id) => dispatch(actions.getDistrict(id)),
        onGetWard: (id) => dispatch(actions.getWard(id)),
        onUpdateUser: (id, data, token) => dispatch(actions.updateUser(id, data, token)),
        onSetAlertUpdateUser: () => dispatch(actions.setAlertUpdateUser()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(update);