import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import AccountActive from 'Components/Admin/AccountManagement/Accounts/AccountActive/accountActive';
import AccountInactive from 'Components/Admin/AccountManagement/Accounts/AccountInactive/accountInactive';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';

class accounts extends Component {
    state = {
        confirm_set_status_block: false,
        confirm_set_status_open: false,
        using_uuid: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

    }

    // toggle
    toggleConfirmSetStatusBlock = () => {
        this.setState({confirm_set_status_block: !this.state.confirm_set_status_block});
    }

    toggleConfirmSetStatusOpen = () => {
        this.setState({confirm_set_status_open: !this.state.confirm_set_status_open});
    }

    // set using id
    setUsingUuid = (uuid) => {
        this.setState({using_uuid: uuid});
    }

    // confirm event
    onConfirmSetStatusUserBlock = async() => {
        await this.props.onSetStatusUser(this.state.using_uuid, this.props.token);
        this.toggleConfirmSetStatusBlock();

        // socket io
        socket.emit("client-send-fetch-account", null);
    }

    onConfirmSetStatusUserOpen = async() => {
        await this.props.onSetStatusUser(this.state.using_uuid, this.props.token);
        this.toggleConfirmSetStatusOpen();

        // socket io
        socket.emit("client-send-fetch-account", null);
    }

    // alert event
    onSetAlertSetStatusUserHandler = () => {
        this.props.onSetAlertSetStatusUser();
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin
                    type = "add"
                    url = {this.props.location.pathname + "/insert"}
                >Tài khoản quản trị</TitleAdmin>

                <Tabs id="controlled-tab-example" className= "tabStyle">
                    <Tab eventKey="home" title="Hoạt động">
                        <AccountActive 
                            onFetchAccount = {this.props.onFetchActiveAccount}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_active}
                            list = {this.props.list_active}

                            // event
                            toggleConfirmSetStatusBlock = {this.toggleConfirmSetStatusBlock}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                    <Tab eventKey="profile" title="Không hoạt động" >
                        <AccountInactive
                            onFetchAccount = {this.props.onFetchInactiveAccount}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_inactive}
                            list = {this.props.list_inactive}

                            // event
                            toggleConfirmSetStatusOpen = {this.toggleConfirmSetStatusOpen}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                    
                </Tabs>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.onSetAlertSetStatusUserHandler}
                    />
                    : ""
                }

                {/* confirm block account */}
                <Confirm
                    show = {this.state.confirm_set_status_block}
                    content = "Bạn có chắc muốn khóa tài khoản này?"
                    onCancelClick = {this.toggleConfirmSetStatusBlock}
                    onConfirmClick = {this.onConfirmSetStatusUserBlock}
                />

                {/* confirm open account */}
                <Confirm
                    show = {this.state.confirm_set_status_open}
                    content = "Bạn có chắc muốn mở khóa tài khoản này?"
                    onCancelClick = {this.toggleConfirmSetStatusOpen}
                    onConfirmClick = {this.onConfirmSetStatusUserOpen}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,

        list_active: state.accounts.list_active,
        pages_active: state.accounts.pages_active,

        list_inactive: state.accounts.list_inactive,
        pages_inactive: state.accounts.pages_inactive,
        alert: state.auth.alert_set_status_user,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchActiveAccount: (email, name, current_page, token) => dispatch(actions.fetchActiveAccounts(email, name, current_page, token)),
        onFetchInactiveAccount: (email, name, current_page, token) => dispatch(actions.fetchInactiveAccounts(email, name, current_page, token)),
        onSetStatusUser: (uuid, token) => dispatch(actions.setStatusUser(uuid, token)),
        onSetAlertSetStatusUser: () => dispatch(actions.setAlertSetStatusUser()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(accounts);