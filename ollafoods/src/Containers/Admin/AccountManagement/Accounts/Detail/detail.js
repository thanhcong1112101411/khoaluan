import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// ------------------------- CSS ----------------------------------
import 'Shared/style.css';

// ------------------------- COMPONENTS ---------------------------
import Auxx from 'Hoc/Auxx/auxx';
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import { formatDate, formatPhone } from 'Shared/utility';
import { socket } from 'Shared/server';
import { GenderType } from 'Utils/enum.utils';


class Detail extends Component {
    state = {  }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }
        const pathName = this.props.location.pathname.split("/");
        this.props.onGetUserDetail(pathName[pathName.length-1], this.props.token);

        // socket-io
        socket.on("server-send-change-role-group", () => {
            this.props.onGetUserDetail(pathName[pathName.length-1], this.props.token);
        })

    }

    componentWillReceiveProps = (nextProps) => {
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }
    }

    render() {
        let gender = null;
        if (this.props.detail){
            if (this.props.detail.gender == GenderType.MALE){
                gender = (
                    <span>Nam</span>
                )
            } else if(this.props.detail.gender == GenderType.FEMALE){
                gender = (
                    <span>Nữ</span>
                )
            } else{
                gender = (
                    <span>Khác</span>
                )
            }
        }
        return (
            <Auxx>
                <TitleAdmin>Thông tin tài khoản</TitleAdmin>
                <div className = "userInfo">
                    <p>
                        <span>Họ và Tên: </span>
                        {this.props.detail ? this.props.detail.first_name : ""}
                    </p>
                    <p>
                        <span>Email: </span>
                        {this.props.detail ? this.props.detail.email : ""}
                    </p>
                    <p>
                        <span>Số điện thoại: </span>
                        {this.props.detail ? formatPhone(this.props.detail.phone) : ""}
                        </p>
                    <p>
                        <span>Địa Chỉ: </span>
                        {this.props.detail ? this.props.detail.fullAddress : ""}
                    </p>
                    <p>
                        <span>Giới tính: </span> {gender}
                    </p>
                    <p>
                        <span>Năm sinh: </span>
                        {this.props.detail ? this.props.detail.bod : ""
                    }</p>
                    <p>
                        <span>Ngày tạo: </span>
                        {this.props.detail ? formatDate(this.props.detail.created_at) : ""}
                    </p>
                    <p>
                        <span>Quyền: </span>
                        {this.props.detail ? this.props.detail.role.name : ""}
                    </p>
                    
                </div>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        detail: state.users.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetUserDetail: (uuid, token) => dispatch(actions.getUserDetail(uuid, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);