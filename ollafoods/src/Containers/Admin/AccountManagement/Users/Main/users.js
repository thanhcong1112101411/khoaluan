import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

// --------------------------- COMPONENTS --------------------------------------
import TitleAdmin from 'Components/UI/UIS/TitleAdmin/titleAdmin';
import Confirm from 'Components/UI/UIS/Confirm/confirm';
import Auxx from 'Hoc/Auxx/auxx';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import UserInactive from 'Components/Admin/AccountManagement/Users/UserInactive/userInactive';
import UserActive from 'Components/Admin/AccountManagement/Users/UserActive/userActive';

import { socket } from 'Shared/server';


class users extends Component {
    state = {
        isLoad: false,
        confirm_set_status_block: false,
        confirm_set_status_open: false,
        using_uuid: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        if(!this.props.isAdmin){
            this.props.history.push('/admin/login');
        }


    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.isAdmin){
            this.props.history.push('/admin/login');
        }

        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-user", null);
                this.setState({isLoad: false});
            }
        }
    }

    // toggle
    toggleConfirmSetStatusBlock = () => {
        this.setState({confirm_set_status_block: !this.state.confirm_set_status_block});
    }

    toggleConfirmSetStatusOpen = () => {
        this.setState({confirm_set_status_open: !this.state.confirm_set_status_open});
    }

    // set using id
    setUsingUuid = (uuid) => {
        this.setState({using_uuid: uuid});
    }

    // confirm event
    onConfirmSetStatusUserBlock = async() => {
        await this.props.onSetStatusUser(this.state.using_uuid, this.props.token);
        this.toggleConfirmSetStatusBlock();

        // socket io
        this.setState({isLoad: true});
    }

    onConfirmSetStatusUserOpen = async() => {
        await this.props.onSetStatusUser(this.state.using_uuid, this.props.token);
        this.toggleConfirmSetStatusOpen();

        // socket io
        this.setState({isLoad: true});
    }

    // alert event
    onSetAlertSetStatusUserHandler = () => {
        this.props.onSetAlertSetStatusUser();
    }

    render() {
        return (
            <Auxx>
                <TitleAdmin>Tài khoản khách hàng</TitleAdmin>

                <Tabs id="controlled-tab-example" className= "tabStyle">
                    <Tab eventKey="home" title="Hoạt động">
                        <UserActive 
                            onFetchUser = {this.props.onFetchActiveUsers}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_active}
                            list = {this.props.list_active}

                            // event
                            toggleConfirmSetStatusBlock = {this.toggleConfirmSetStatusBlock}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                    <Tab eventKey="profile" title="Không hoạt động" >
                        <UserInactive
                            onFetchUser = {this.props.onFetchInactiveUsers}
                            token = {this.props.token}
                            pathName = {this.props.currentPath}
        
                            pages = {this.props.pages_inactive}
                            list = {this.props.list_inactive}

                            // event
                            toggleConfirmSetStatusOpen = {this.toggleConfirmSetStatusOpen}
                            setUsingUuid = {this.setUsingUuid}
                        />
                    </Tab>
                </Tabs>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.onSetAlertSetStatusUserHandler}
                    />
                    : ""
                }

                {/* confirm block account */}
                <Confirm
                    show = {this.state.confirm_set_status_block}
                    content = "Bạn có chắc muốn khóa tài khoản này?"
                    onCancelClick = {this.toggleConfirmSetStatusBlock}
                    onConfirmClick = {this.onConfirmSetStatusUserBlock}
                />

                {/* confirm open account */}
                <Confirm
                    show = {this.state.confirm_set_status_open}
                    content = "Bạn có chắc muốn mở khóa tài khoản này?"
                    onCancelClick = {this.toggleConfirmSetStatusOpen}
                    onConfirmClick = {this.onConfirmSetStatusUserOpen}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list_active: state.users.list_active,
        pages_active: state.users.pages_active,

        list_inactive: state.users.list_inactive,
        pages_inactive: state.users.pages_inactive,
        
        alert: state.auth.alert_set_status_user,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchActiveUsers: (email, name, current_page, token) => dispatch(actions.fetchActiveUsers(email, name, current_page, token)),
        onFetchInactiveUsers: (email, name, current_page, token) => dispatch(actions.fetchInactiveUsers(email, name, current_page, token)),
        onSetStatusUser: (uuid, token) => dispatch(actions.setStatusUser(uuid, token)),
        onSetAlertSetStatusUser: () => dispatch(actions.setAlertSetStatusUser()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(users);
