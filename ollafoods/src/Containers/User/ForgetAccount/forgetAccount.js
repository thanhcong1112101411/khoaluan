import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Alert from 'react-bootstrap/Alert';

// -------------------------- CSS -------------------------------
import '../SignIn/signIn.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';

class ForgetAccount extends Component {
    state = { 
        controls:{
			email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Email đăng nhập',
                    icon : 'user',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
		},
		formIsValid: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

    }

    inputChangedHandler = (event, controlName) => {
        event.preventDefault();
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    forgeAccountHandler = (event) =>{
        event.preventDefault();
        console.log("forget account");
        this.props.onResetPassword(this.state.controls.email.value);
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = "";
        form = formElementsArray.map(formElement => (
        	<Input
                key = {formElement.id}
                icon = {formElement.config.elementConfig.icon}
        		elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
        	/>
        ));

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Quên mật khẩu</Breadcrumb.Item>
                </Breadcrumb>
                <div className = "formSign">
                    <div className = "container">
                        <div className = "content">
                            <h1>Quên mật khẩu</h1>
                            <form>
                                {form}
                                <button
                                    disabled = {!this.state.formIsValid}
                                    onClick = {(event) => this.forgeAccountHandler(event)}
                                >Gửi</button>
                            </form>
                        </div>
                        <div className="alert">
                            {
                                this.props.alert ?
                                <Alert variant={this.props.alert.varient}>
                                    {this.props.alert.content}
                                </Alert>
                                : ""
                            }
                        </div>
                    </div>
                </div>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        alert: state.auth.alert_reset_password,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onResetPassword: (email) => dispatch(actions.resetPassword(email)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgetAccount);