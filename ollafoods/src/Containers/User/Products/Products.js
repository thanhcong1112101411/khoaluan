import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
// -------------------------- CSS -------------------------------
import 'Shared/style.css';
import './products.css';

//-------------------------- COMPONENT -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import { socket } from 'Shared/server';
import ProductItem from "../../../Components/UI/UIType/ProductItem/productItem";
import Pagination from 'Components/UI/UIS/Pagination/pagination';
import BrandFilter from 'Components/User/Products/BrandFilter/brandFilter';
import CatagoryFilter from 'Components/User/Products/CatagoryFilter/catagoryFilter';
import PriceFilter from 'Components/User/Products/PriceFilter/priceFilter';
import brands from 'Containers/Admin/ProductManagement/Brands/Main/brands';

class Products extends Component {

    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
        brand: [],
        price: 0,
        catagory: '',
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onGetCatagoryName();
        this.props.onGetBrandName();

        const locationSearch = this.props.location.search.split("=");
        const search = locationSearch[1] ? locationSearch[1] : '';
        this.setState({searchValue: search});
        
        this.props.onfetchProductsUser(this.state.brand, this.state.price, this.state.catagory, search, this.state.current_page);
        // // socket io
        // socket.on("server-send-fetch-news", () => {
        //     this.props.onfetchProductsUser(this.state.searchValue, this.state.current_page);
        // })
    }

    // set state
    setStateBrand = (brands) => {
        this.setState({brand: brands});
        this.props.onfetchProductsUser(brands, this.state.price, this.state.catagory, this.state.searchValue, this.state.current_page);
    }
    setStatePrice = (price) => {
        this.setState({price: price});
        this.props.onfetchProductsUser(this.state.brand, price, this.state.catagory, this.state.searchValue, this.state.current_page);
    }
    setStateCatagory = (catagory) => {
        this.setState({catagory: catagory});
        this.props.onfetchProductsUser(this.state.brand, this.state.price, catagory, this.state.searchValue, this.state.current_page);
    }

    // search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onfetchProductsUser(this.state.brand, this.state.price, this.state.catagory, this.state.searchValue, pageNumber);
    }

    render() {
        const listItem = this.props.list.map((ig, index) => {
            return(
                <div key = {ig.id} className="col-lg-3 col-md-4 col-sm-6">
                    <ProductItem
                        key = {ig.id}
                        id = {ig.id}
                        index = {index + 1}
                        name = {ig.name}
                        created_at = {ig.created_at}
                        images = {JSON.parse(ig.images)[0]}
                        exportPrice={ig.exportPrice}
                        discountAmount = {ig.discountAmount}
                        // event
                        
                    />
                </div>
            );
        })
        
        return (
            <Auxx>
                <CatagoryFilter
                    list = {this.props.catagory_list}
                    setStateCatagory = {this.setStateCatagory}
                />
                <div className = "container filterProductList">
                    <BrandFilter 
                        list = {this.props.brand_list}
                        setStateBrand = {this.setStateBrand}
                    />
                    <PriceFilter 
                        setStatePrice = {this.setStatePrice}
                    />
                </div>
                
                <div className="container">
                        <div className="row">
                            {listItem}
                        </div>
                        <Pagination
                            pages = {this.props.pages}
                            currentPage = {this.state.current_page}
                            nextPage = {this.nextPage}
                        />  
                </div>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_admin,
        isAdmin: state.auth.uuid_admin != "",

        list: state.products.list_product_user,
        load_more: state.products.load_more,
        pages: state.products.pages_product_user,

        catagory_list:state.products.catagory_list,
        brand_list:state.products.brand_list,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onfetchProductsUser: (brand, price, catagory, name, current_page) => dispatch(actions.fetchProductsUser(brand, price, catagory, name, current_page)),
        onGetCatagoryName: () => dispatch(actions.getCatagoryName()),
        onGetBrandName: () => dispatch(actions.getBrandName()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);