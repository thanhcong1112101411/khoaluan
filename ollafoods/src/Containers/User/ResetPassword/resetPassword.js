import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Alert from 'react-bootstrap/Alert';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';

class ResetPassword extends Component {
    state = {
        controls:{
			password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Mật khẩu',
                    icon : 'password',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3
                },
                valid: false,
                touched: false
            },
            repassword: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Nhập lại mật khẩu',
                    icon : 'password',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3
                },
                valid: false,
                touched: false
            }
		},
		formIsValid: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
    }

    inputChangedHandler = (event, controlName) => {
        event.preventDefault();
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    resetPasswordHandler = (event) =>{
        event.preventDefault();
        const pathName = this.props.location.pathname.split("/");
        this.props.onChangePasswordToken(this.state.controls.password.value, this.state.controls.repassword.value,pathName[pathName.length-1]);
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        
        let form = "";
        form = formElementsArray.map(formElement => (
        	<Input
                key = {formElement.id}
                icon = {formElement.config.elementConfig.icon}
        		elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
        	/>
        ));

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Đăng Nhập</Breadcrumb.Item>
                </Breadcrumb>
                <div className = "formSign">
                    <div className = "container">
                        <div className = "content">
                            <h1>Đặt lại mật khẩu</h1>
                            <form>
                                {form}
                                <button
                                    disabled = {!this.state.formIsValid}
                                    onClick = {(event) => this.resetPasswordHandler(event)}
                                >Gửi</button>
                            </form>
                        </div>
                        <div className="alert">
                            {
                                this.props.alert ?
                                <Alert variant={this.props.alert.varient}>
                                    {this.props.alert.content}
                                </Alert>
                                : ""
                            }
                        </div>
                    </div>
                </div>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        alert: state.auth.alert_change_password_token,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onChangePasswordToken : (password, repassword, token) => dispatch(actions.changePasswordToken(password, repassword, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);