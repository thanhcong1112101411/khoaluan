import React, { Component } from 'react';
import Auxx from 'Hoc/Auxx/auxx';
import { socket } from 'Shared/server';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import { baseUrl } from 'Shared/server';

// ----------------------------- CSS -------------------------
import './newDetail.css';
import 'Shared/style.css';

class newDetail extends Component {

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        this.props.onGetNewsDetail(this.props.match.params.id);
        this.props.onIncreaseSeenNews(this.props.match.params.id);
    }

    render() {
        
        return (
            <Auxx>
                <div className="container newDetail">
                    <h1>{this.props.detail ? this.props.detail.title : ""}</h1>
                    {
                        this.props.detail ?
                        <div dangerouslySetInnerHTML={{__html: this.props.detail.content}} />
                        : ""
                    }
                    
                </div>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,

        list: state.news.list,
        alert: state.news.alert_get_news_detail,

        detail: state.news.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetNewsDetail: (id) => dispatch(actions.getNewsDetail(id)),
        onIncreaseSeenNews: (id) => dispatch(actions.increaseSeenNews(id)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(newDetail);