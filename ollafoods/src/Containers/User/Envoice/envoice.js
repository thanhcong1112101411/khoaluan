import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- CSS ---------------------------
import '../SignIn/signIn.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';
import EnvoiceList from 'Components/User/Envoice/EnvoiceList/envoiceList';

class Envoice extends Component {
    state = {
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchEnvoiceUser(this.props.uuid, this.props.token);
    }

    render() {

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="/">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Hóa đơn</Breadcrumb.Item>
                </Breadcrumb>
                <EnvoiceList
                    list = {this.props.list}
                    path = {this.props.currentPath}
                />
            </Auxx>
        );
    }
}
const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_user,
        uuid: state.auth.uuid_user,

        list: state.exportEnvoice.list_user,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchEnvoiceUser: (uuid, token) => dispatch(actions.fetchEnvoiceUser(uuid, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Envoice);