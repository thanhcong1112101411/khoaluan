import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Alert from 'react-bootstrap/Alert';
// ---------------------------- CSS ----------------------------
import '../SignIn/signIn.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import { Link } from 'react-router-dom';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';

class SignUp extends Component {
    state = {  
        controls:{
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Họ tên',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                },
                valid: false,
                touched: false
            },
			email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Email đăng nhập',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'tel',
                    placeholder: 'Số điện thoại',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Mật khẩu',
                    icon : 'password',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3
                },
                valid: false,
                touched: false
            }
		},
		formIsValid: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

    }
    
    inputChangedHandler = (event, controlName) => {
        event.preventDefault();
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    signUpHandler = (event) =>{
        event.preventDefault();
        let formData = {};
        for (let formElementIdentifier in this.state.controls) {
            formData[formElementIdentifier] = this.state.controls[formElementIdentifier].value;
        }
        this.props.onUserSignUp(formData);
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = "";
        form = formElementsArray.map(formElement => (
        	<Input
                key = {formElement.id}
                icon = {formElement.config.elementConfig.icon}
        		elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
        	/>
        ));

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Đăng Ký</Breadcrumb.Item>
                </Breadcrumb>
                <div className = "formSign">
                    <div className = "container">
                        <div className = "content">
                            <h1>Đăng ký</h1>
                            <form>
                                {form}
                                <button
                                    disabled = {!this.state.formIsValid}
                                    onClick = {(event) => this.signUpHandler(event)}
                                >Đăng ký</button>
                            </form>
                            <p>Bạn đã có tài khoản? <Link to="/sign-in"> Đăng nhập</Link></p>
                        </div>
                        <div className="alert">
                            {
                                this.props.alert ?
                                <Alert variant={this.props.alert.varient}>
                                    {this.props.alert.content}
                                </Alert>
                                : ""
                            }
                        </div>
                    </div>
                </div>
                
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        alert: state.auth.alert_user_signup,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onUserSignUp: (data) => dispatch(actions.userSignUp(data)),
    
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);