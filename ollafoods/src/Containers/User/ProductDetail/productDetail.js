import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// -------------------------- CSS ------------------------------
import 'Shared/style.css';
// /-------------------------- COMPONENT -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import ProductInfo from 'Components/User/ProductDetail/Information/information';
import Description from 'Components/User/ProductDetail/Description/description';
import Alert from 'Components/UI/UIS/AlertSecond/alertSecond';
import RecomendProduct from 'Components/User/ProductDetail/TrendingProducts/trendingProducts';

class ProductDetail extends Component {
    state = {
        id: -1,
        showAlertAdd: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        const id = this.props.match.params.id;
        this.setState({id: id});
        this.props.onGetProductsDetail(id);
        this.props.onFetchRecomendProductDetail(id);
    }

    componentWillReceiveProps(nextProps){
        const nextId = nextProps.match.params.id;
        if(nextId != this.state.id){
            this.setState({id: nextId});
            this.props.onGetProductsDetail(nextId);
            this.props.onFetchRecomendProductDetail(nextId);
        }
    }

    onAddProductToCartHandler = (id) =>{
        this.props.onAddProductToCart(id, this.props.detail.min);

        this.setState({showAlertAdd: true});
        setTimeout(() => {
            this.setState({showAlertAdd: false})
        }, 2000);
    }

    render() {
        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item href="/products">
                        Sản phẩm
                    </Breadcrumb.Item>
                    <Breadcrumb.Item active>{this.props.detail ? this.props.detail.name : ""}</Breadcrumb.Item>
                </Breadcrumb>
                <ProductInfo
                    images ={this.props.detail ? JSON.parse(this.props.detail.images) : ""}
                    detail = { this.props.detail ? this.props.detail : ''}

                    // event
                    onAddProductToCart = {this.onAddProductToCartHandler}
                />
                <RecomendProduct
                    list = {this.props.recomend_list_detail}
                />
                <Description 
                    desFirst={this.props.detail ? this.props.detail.description_first : ""}
                    desSecond={this.props.detail ? this.props.detail.description_second : ""}
                />
                <Alert 
                    title="Đã thêm sản phẩm vào giỏ hàng!"
                    show={this.state.showAlertAdd}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        
        detail: state.products.detail,
        recomend_list_detail: state.products.recomend_list_detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetProductsDetail: (id) => dispatch(actions.getProductsDetail(id)),
        onAddProductToCart : (id, quantity) => dispatch(actions.addToCart(id, quantity)),
        onFetchRecomendProductDetail : (id) => dispatch(actions.fetchRecomendProductDetail(id)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);