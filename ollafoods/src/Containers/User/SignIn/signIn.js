import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import Alert from 'react-bootstrap/Alert';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

// --------------------------- CSS ---------------------------
import './signIn.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import { Link } from 'react-router-dom';
import { SocialType } from 'Utils/enum.utils';

class signIn extends Component {
    state = {
        controls:{
			username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Email đăng nhập',
                    icon : 'user',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Mật khẩu',
                    icon : 'password',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3
                },
                valid: false,
                touched: false
            }
		},
        formIsValid: false,
        data: null,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
    }

    inputChangedHandler = (event, controlName) => {
        event.preventDefault();
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    signInHandler = (event) =>{
        event.preventDefault();
        this.props.onSignInHandler(this.state.controls.username.value, this.state.controls.password.value);
    }

    responseFacebook = (response) => {
        let formData = new Object();
        formData.email = response.email;
        formData.name = response.name;
        formData.id = response.id;
        formData.type = SocialType.FB;

        this.props.onUserLoginSocial(formData);
    }

    responseGoogle = (response) => {
        let formData = new Object();
        formData.email = response.profileObj.email;
        formData.name = response.profileObj.name;
        formData.id = response.profileObj.googleId;
        formData.type = SocialType.GG;

        this.props.onUserLoginSocial(formData);
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = "";
        form = formElementsArray.map(formElement => (
        	<Input
                key = {formElement.id}
                icon = {formElement.config.elementConfig.icon}
        		elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
        	/>
        ));

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Đăng Nhập</Breadcrumb.Item>
                </Breadcrumb>
                <div className = "formSign">
                    <div className = "container">
                        <div className = "content">
                            <h1>Đăng nhập</h1>
                            <form>
                                {form}
                                <span><Link to="/forget-account">Quên mật khẩu?</Link></span>
                                <button
                                    disabled = {!this.state.formIsValid}
                                    onClick = {(event) => this.signInHandler(event)}
                                >Đăng Nhập</button>
                            </form>
                            <p>Bạn chưa có tài khoản? <Link to="sign-up"> tạo tài khoản</Link></p>
                        </div>
                        <div className="alert">
                            {
                                this.props.alert && this.props.alert.varient == "danger" ?
                                <Alert variant={this.props.alert.varient}>
                                    {this.props.alert.content}
                                </Alert>
                                : ""
                            }
                        </div>
                        <div className = "socialStyle">
                            <div className = "facebook">
                                <FacebookLogin
                                    appId="2876006122464885"
                                    autoLoad={false}
                                    fields="name,email,picture"
                                    onClick = {this.componentClicked}
                                    callback={this.responseFacebook}
                                />
                            </div>
                            <div className = "google">
                                <GoogleLogin
                                    clientId="3379086529-7o4ug88i1457npi4cdknd4ahi2u82kuc.apps.googleusercontent.com"
                                    buttonText="Login"
                                    onSuccess={this.responseGoogle}
                                    onFailure={this.responseGoogle}
                                    cookiePolicy={'single_host_origin'}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        alert: state.auth.alert_user_login,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onSignInHandler : (username, password) => dispatch(actions.userLogin(username, password)),
        onUserLoginSocial : (data) => dispatch(actions.userLoginSocial(data)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(signIn);