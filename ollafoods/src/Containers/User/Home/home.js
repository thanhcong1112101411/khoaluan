import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import history from '../../../history';

//-------------------------- COMPONENT -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import Banner from 'Components/User/Home/Banner/banner';
import ContentTop from 'Components/User/Home/ContentTop/contentTop';
import TrendingProduct from 'Components/User/Home/TrendingProducts/trendingProducts';
import Testimonials from 'Components/User/Home/Testimonials/testimonials';
import BestSellers from 'Components/User/Home/BestSellers/bestSellers';
import LatestNews from 'Components/User/Home/LatestNews/latestNews';
import { withRouter } from 'react-router';



class home extends Component {

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onFetchBannerImages();
        this.props.onFetchContentTopImages();
        this.props.onFetchTopNewsNewest();
        this.props.onFetchTopProductsNewest();

    }

    // componentWillReceiveProps(nextProps){
    //     console.log('location', history.location);
    //     if (history.location.pathname != nextProps.currentPath){
    //         console.log("aaaaaaaaa");
    //     }
    //     // console.log("aaaaaaaaaaaaaaaa", nextProps);
    //     // if (nextProps.location.pathname != this.props.location.pathname){
    //     //     window.location.href(nextProps.location.pathname);
    //     // }
    // }

    render() {

        return (
            <Auxx>
                <Banner 
                    list = {this.props.banner}
                />
                <ContentTop 
                    list = {this.props.content_top}
                />
                <TrendingProduct 
                    list = {this.props.products_newest}
                />
                <Testimonials />
                {/* <BestSellers /> */}
                <LatestNews 
                    list = {this.props.news_newest}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        banner: state.image.banner,
        content_top: state.image.content_top,

        news_newest: state.news.list_newest,
        products_newest: state.products.list_newest,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchBannerImages: () => dispatch(actions.fetchBannerImages()),
        onFetchContentTopImages: () => dispatch(actions.fetchContentTopImages()),
        onFetchTopNewsNewest: () => dispatch(actions.fetchTopNewsNewest()),
        onFetchTopProductsNewest: () => dispatch(actions.fetchTopProductsNewest()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(home);