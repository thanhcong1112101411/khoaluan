import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// ------------------------- COMPONENTS ---------------------------
import Auxx from 'Hoc/Auxx/auxx';
import NewsItem from 'Components/User/News/NewItem/NewItem';
import { socket } from 'Shared/server';
import NewsList from 'Components/User/News/NewsList/newsList';
import Pagination from 'Components/UI/UIS/Pagination/pagination';

class news extends Component {
    state = {
        isLoad: false,
        searchValue: '',
        current_page: 1,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        this.props.onFetchNews(this.state.searchValue,this.state.current_page,this.props.token);

        // socket io
        socket.on("server-send-fetch-news", () => {
            this.props.onFetchNews(this.state.searchValue, this.state.current_page, this.props.token);
        })
    }

    // pagination, search
    nextPage = (pageNumber) =>{
        this.setState({current_page: pageNumber});
        this.props.onFetchNews(this.state.searchValue, pageNumber, this.props.token);
    }
    onChangeSearchValue = (event) =>{
        event.preventDefault();
        let value = event.target.value;
        this.setState({searchValue: value});
        this.props.onFetchNews(value,1, this.props.token);
    }

    render() {
        return (
            <Auxx>
                <NewsList 
                    list = {this.props.list}
                />
                <Pagination
                    pages = {this.props.pages}
                    currentPage = {this.state.current_page}
                    nextPage = {this.nextPage}
                />
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        
        list: state.news.list,
        load_more: state.news.load_more,
        pages: state.news.pages,
        alert: state.news.alert_delete_news,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onFetchNews: (name, current_page, token) => dispatch(actions.fetchNews(name, current_page, token)),
    };  
};



export default connect(mapStateToProps, mapDispatchToProps)(news);