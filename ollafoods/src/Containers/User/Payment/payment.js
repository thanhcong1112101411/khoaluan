import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

// --------------------------- CSS ---------------------------
import './payment.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import { Link } from 'react-router-dom';
import { SocialType } from 'Utils/enum.utils';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket, ImageLogoLink } from 'Shared/server';

class Payment extends Component {
    state = {
        choosing: 1,
        is_load: false,
        showAlertLogin: false,
        showAlertNonProduct: false,
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
    }

    componentWillReceiveProps(nextProps){
        // socket io
        if (nextProps.alert && this.state.is_load){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-export-envoices", null);
                this.setState({is_load: false});
            }
        }
    }

    setStateChoosing = (event, id) => {
        event.preventDefault();
        this.setState({choosing: id});
    }

    orderService = (event) => {
        event.preventDefault();
        if (this.props.cart.length == 0){
            this.setState({showAlertNonProduct: true});
            return;
        }
        if (this.props.uuid_user == ""){
            this.setState({showAlertLogin: true})
            return;
        }

        this.props.onOrderService(this.props.uuid_user, this.props.token_user, this.props.cart);
        this.setState({is_load: true});
    }

     // exit state
     exitShowAlertLogin = () => {
        this.setState({showAlertLogin: false});
    }
    exitShowAlertNonProduct = () => {
        this.setState({showAlertNonProduct: false});
    }

    // alert event
    onSetAlertSetStatusOrder = () => {
        this.props.onSetOrderServiceAlert();
    }

    render() {
        
        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Thanh toán</Breadcrumb.Item>
                </Breadcrumb>
                
                <div className = "paymentChoose">
                    <div className = "container">
                        <div className="content">
                            <h1>Phương thức thanh toán</h1>

                            <div className = "item">
                                <h2>1. Thanh toán khi nhận hàng</h2>
                                
                            </div>
                            <div className = "item">
                                <h2>2. Thanh toán bằng thẻ quốc tế Visa, Master, JCB</h2>
                                
                            </div>
                            <div className = "item">
                                <h2>3. Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)</h2>
                                <div className = "images">
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/1.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/2.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/3.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/4.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/5.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/6.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/7.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/8.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/9.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/10.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/11.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/12.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/13.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/14.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/15.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/16.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/17.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/18.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/19.jpg"} />
                                    </div>
                                    <div className = "image">
                                        <img width = "100%" src = {ImageLogoLink + "/20.jpg"} />
                                    </div>
                                </div>
                            </div>

                            <button 
                                onClick = {this.orderService}
                            >Thanh toán</button>
                        </div>
                    </div>
                    
                </div>
                {
                    !!this.props.alert ?
                    <Alert
                        show = {!!this.props.alert}
                        data = {this.props.alert}
                        clicked = {this.onSetAlertSetStatusOrder}
                    />
                    : ""
                }
                {
                    this.state.showAlertLogin ?
                    <Alert
                        show = {this.state.showAlertLogin}
                        data = {{
                            varient: "danger",
                            content: "Cần đăng nhập trước khi đặt hàng"
                        }}
                        clicked = {this.exitShowAlertLogin}
                    />
                    : ""
                }

                {
                    this.state.showAlertNonProduct ?
                    <Alert
                        show = {this.state.showAlertNonProduct}
                        data = {{
                            varient: "danger",
                            content: "Không có sản phẩm trong giỏ hàng"
                        }}
                        clicked = {this.exitShowAlertNonProduct}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        uuid_user: state.auth.uuid_user,
        token_user: state.auth.token_user,

        cart: state.cart.cart,
        alert: state.cart.orderAlert,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onOrderService: (uuid, token, data) => dispatch(actions.orderService(uuid, token, data)),
        onSetOrderServiceAlert: () => dispatch(actions.setOrderServiceAlert()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);