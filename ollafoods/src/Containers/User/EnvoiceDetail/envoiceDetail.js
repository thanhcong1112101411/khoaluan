import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- CSS ---------------------------
import '../SignIn/signIn.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';
import ProductInformation from 'Components/User/Envoice/ProductInformation/productInformation';

class EnvoiceDetail extends Component {
    state = {
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
        this.props.onGetExportEnvoiceDetail(this.props.match.params.id, this.props.token);
    }

    render() {

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="/">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item href="/envoices">Hóa đơn</Breadcrumb.Item>
                    <Breadcrumb.Item active>Chi tiết hóa đơn</Breadcrumb.Item>
                </Breadcrumb>
                <ProductInformation
                    list = {this.props.detail ? this.props.detail.envoiceDetails : []}
                /> 
            </Auxx>
        );
    }
}
const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_user,
        uuid: state.auth.uuid_user,

        detail: state.exportEnvoice.detail,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onGetExportEnvoiceDetail: (id, token) => dispatch(actions.getExportEnvoiceDetail(id, token)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(EnvoiceDetail);