import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------- components ------------------------
import Auxx from 'Hoc/Auxx/auxx';
import ProductList from 'Components/User/Cart/ProductList/productList';
import Payment from 'Components/User/Cart/Payment/payment';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';

class cart extends Component {
    state = {
        showAlertLogin: false,
        showAlertNonProduct: false,
        is_load: false,
    }

    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);

        console.log(this.props);
    }

    // exit state
    exitShowAlertLogin = () => {
        this.setState({showAlertLogin: false});
    }
    exitShowAlertNonProduct = () => {
        this.setState({showAlertNonProduct: false});
    }

    changeQuantityProductHandler = (id, quantity) =>{
        this.props.onChangeQuantityProductCart(id, quantity);
    }

    orderServiceHandler = () => {
        if (this.props.cart.length == 0){
            this.setState({showAlertNonProduct: true});
            return;
        }
        if (this.props.uuid_user == ""){
            this.setState({showAlertLogin: true})
            return;
        }

        this.props.history.push("/payment");
    }

    render() {
        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Giỏ hàng</Breadcrumb.Item>
                </Breadcrumb>
                {
                    this.props.cart.length > 0 ?
                    <ProductList 
                        cart = {this.props.cart}

                        // event
                        changeQuantityProductClicked = {this.changeQuantityProductHandler}
                        onDeleteCartItemClicked = {this.props.onDeleteCartItem}
                    />
                    :
                    <div className = "container">
                        <p style = {{fontSize: "25px"}}>Không có sản phẩm trong giỏ hàng</p>
                    </div>
                }
                
                <Payment
                    totalMoney = {this.props.totalMoney}
                    onOrderServiceHandler = {this.orderServiceHandler}
                />

                {
                    this.state.showAlertLogin ?
                    <Alert
                        show = {this.state.showAlertLogin}
                        data = {{
                            varient: "danger",
                            content: "Cần đăng nhập trước khi đặt hàng"
                        }}
                        clicked = {this.exitShowAlertLogin}
                    />
                    : ""
                }

                {
                    this.state.showAlertNonProduct ?
                    <Alert
                        show = {this.state.showAlertNonProduct}
                        data = {{
                            varient: "danger",
                            content: "Không có sản phẩm trong giỏ hàng"
                        }}
                        clicked = {this.exitShowAlertNonProduct}
                    />
                    : ""
                }
            </Auxx>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        uuid_user: state.auth.uuid_user,
        token_user: state.auth.token_user,

        cart: state.cart.cart,
        totalMoney: state.cart.totalMoney,
        alert: state.cart.orderAlert
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onChangeQuantityProductCart : (id, quantity) => dispatch(actions.changeQuantityProductCart(id, quantity)),
        onDeleteCartItem : (id) => dispatch(actions.deleteCartItem(id)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(cart);