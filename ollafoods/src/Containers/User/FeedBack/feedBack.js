import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// --------------------------- CSS ---------------------------
import '../SignIn/signIn.css';
import 'Shared/style.css';

// -------------------------- COMPONENTS -------------------------
import Auxx from 'Hoc/Auxx/auxx';
import {updateObject, checkValidity} from 'Shared/utility';
import Input from 'Components/UI/UIS/InputSecond/inputSecond';
import Alert from 'Components/UI/UIS/AlertFirst/alertFirst';
import { socket } from 'Shared/server';

class FeedBack extends Component {
    state = {
        controls:{
			title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Tiêu đề',
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                },
                valid: false,
                touched: false
            },
            content: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Nội dung',
                    rows: 10,
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1
                },
                valid: false,
                touched: false
            }
		},
        formIsValid: false,
        data: null,
        isLoad: false,
    }
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
    }

    componentWillReceiveProps(nextProps){
        // socket io
        if (nextProps.alert && this.state.isLoad){
            if (nextProps.alert.varient == "success"){
                socket.emit("client-send-fetch-feedbacks", null);
                this.setState({isLoad: false});
            }
        }
    }

    inputChangedHandler = (event, controlName) => {
        event.preventDefault();
    	const updatedElement = updateObject(this.state.controls[controlName],{
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
    	});
    	const updatedControls = updateObject(this.state.controls, {
    		[controlName]: updatedElement
    	});

        this.setState({controls: updatedControls});

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({controls: updatedControls, formIsValid: formIsValid});
    }

    orderHandler = (event) =>{
        event.preventDefault();
        let formData = {};
        for (let formElementIdentifier in this.state.controls) {
            formData[formElementIdentifier] = this.state.controls[formElementIdentifier].value;
        }

        this.props.onAddFeedback(formData, this.props.token);
        this.setState({isLoad: true});
    }

    //--------------------- alert event -------------------
    setAlertAddFeedbackHandler = (event) => {
        event.preventDefault();
        this.props.onSetAlertAddFeedback();
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = "";
        form = formElementsArray.map(formElement => (
        	<Input
                key = {formElement.id}
                icon = {formElement.config.elementConfig.icon}
        		elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
        	/>
        ));

        return (
            <Auxx>
                <Breadcrumb className="breadCrump">
                    <Breadcrumb.Item href="#">Trang chủ</Breadcrumb.Item>
                    <Breadcrumb.Item active>Phản hồi</Breadcrumb.Item>
                </Breadcrumb>
                <div className = "formSign">
                    <div className = "container">
                        <div className = "content">
                            <h1>Phản hồi</h1>
                            <form>
                                {form}
                                <button
                                    disabled = {!this.state.formIsValid}
                                    onClick = {(event) => this.orderHandler(event)}
                                >Gửi</button>
                            </form>
                        </div>
                        {
                            !!this.props.alert ?
                            <Alert
                                show = {!!this.props.alert}
                                data = {this.props.alert}
                                clicked = {(event) => this.setAlertAddFeedbackHandler(event)}
                            />
                            : ""
                        }
                    </div>
                </div>
            </Auxx>
        );
    }
}
const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
        token: state.auth.token_user,

        alert: state.feedback.alert_add_feedbacks,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
        onAddFeedback: (data, token) => dispatch(actions.addFeedbacks(data, token)),
        onSetAlertAddFeedback: () => dispatch(actions.setAlertAddFeedback()),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedBack);