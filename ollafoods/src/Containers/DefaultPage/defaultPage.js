import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Store/Actions/index';

// ---------------------------- CSS ---------------------------------
import './defaultPage.css';
import 'Shared/style.css';

class defaultPage extends Component {
    
    componentDidMount(){
        this.props.onSetPath(this.props.location.pathname);
    }

    render() {
        return (
            <div className="defaultStyle">
                <h1>Lỗi !</h1>
                <ul>
                    <li>Đường đẫn không tồn tại</li>
                    <li>Hoặc</li>
                    <li>Bạn không có quyền truy cập đường dẫn này</li>
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return {
        currentPath: state.auth.currentPath,
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        onSetPath: (path) => dispatch(actions.setPath(path)),
    };  
};

export default connect(mapStateToProps, mapDispatchToProps)(defaultPage);