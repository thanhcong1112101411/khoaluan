import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import history from './history';

//--------------------------------- react-router ----------------------------------
import { BrowserRouter, Router} from 'react-router-dom';
//---------------------------------------------------------------------------------
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose , combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';

//-------------------------------------- REDUCERS----------------------------------
import authReducers from './Store/Reducers/auth';
import roleGroupReducers from './Store/Reducers/roleGroups';
import accountReducers from './Store/Reducers/accounts';
import userReducers from './Store/Reducers/users';
import historyReducers from './Store/Reducers/history';
import imageReducers from './Store/Reducers/images';
import feedbackReducers from './Store/Reducers/feebacks';
import catagoryReducers from "./Store/Reducers/catagories";
import newsReducers from "./Store/Reducers/news";
import brandsReducers from "./Store/Reducers/brands";
import unitsReducers from "./Store/Reducers/units";
import pricesReducers from "./Store/Reducers/prices";
import productsReducers from "./Store/Reducers/products";
import discountReducers from './Store/Reducers/discounts';
import exportEnvoiceReducers from './Store/Reducers/exportEnvoice';
import cartReducers from './Store/Reducers/cart';
import recomendSystemReducers from './Store/Reducers/recomendSystem';

//-------------------------------------- SAGAS --------------------------------
import { 
    watchAuth,
    watchRoleGroup,
    watchAccount,
    watchUser,
    watchHistory,
    watchImage,
    watchFeedback,
    watchCatagory,
    watchNews,
    watchBrands,
    watchUnits,
    watchPrices,
    watchProducts,
    watchDiscounts,
    watchExportEnvoice,
    watchCart,
    watchRecomendSystem,

} from './Store/Sagas';
import discountsDeleted from 'Components/Admin/ProductManagement/Discounts/DiscountsDeleted/discountsDeleted';


//--------------------------------------------------------------------------------
const rootReducer = combineReducers({
    // ------------- USER ------------
    // ------------- ADMIN ------------
    auth: authReducers,
    roleGroup: roleGroupReducers,
    accounts: accountReducers,
    users: userReducers,
    history: historyReducers,
    image: imageReducers,
    feedback: feedbackReducers,
    catagory: catagoryReducers,
    news:newsReducers,
    brands:brandsReducers,
    units:unitsReducers,
    prices:pricesReducers,
    products:productsReducers,
    discounts:discountReducers,
    exportEnvoice:exportEnvoiceReducers,
    cart: cartReducers,
    recomendSystem: recomendSystemReducers,
});

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__: null || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(sagaMiddleware)
));

sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchRoleGroup);
sagaMiddleware.run(watchAccount);
sagaMiddleware.run(watchUser);
sagaMiddleware.run(watchHistory);
sagaMiddleware.run(watchImage);
sagaMiddleware.run(watchFeedback);
sagaMiddleware.run(watchCatagory);
sagaMiddleware.run(watchNews);
sagaMiddleware.run(watchBrands);
sagaMiddleware.run(watchUnits);
sagaMiddleware.run(watchPrices);
sagaMiddleware.run(watchProducts);
sagaMiddleware.run(watchDiscounts);
sagaMiddleware.run(watchExportEnvoice);
sagaMiddleware.run(watchCart);
sagaMiddleware.run(watchRecomendSystem);

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
   </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
