require('dotenv').config();
import {join} from "path";
export default {
    mainPort: Number(process.env.ENV_IDENTITY_MAIN_PORT).valueOf() || 8200,
    env: {
        type: 'mysql',
        host: process.env.ENV_IDENTITY_HOST || 'localhost',
        port: Number(process.env.ENV_IDENTITY_PORT).valueOf() || 3306,
        username: process.env.ENV_IDENTITY_USERNAME || 'root',
        password: process.env.ENV_IDENTITY_PASSWORD || '',
        database: process.env.ENV_IDENTITY_DATABASE || 'ollafoods',
    },
    jwtSecret: process.env.JWT_SECRET || 'debitnationSecretKey', // jwtSecret
    jwtExpiresIn: process.env.JWT_EXPIRES_IN || '48h', // 2 day
    urlRestPassword: process.env.URL_RESET_PASSWORD || 'http://localhost:3000/reset-password/', // URL reset password
    emailSetting: {
        from: process.env.EMAIL_FROM || 'ollafoods@gmail.com',
        name: process.env.EMAIL_NAME || 'OllaFoods',
        SENDGRID_API_KEY: process.env.SENDGRID_API_KEY ||
            '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',
        mailGun: {
            host: process.env.MG_HOST || 'smtp.mailgun.org',
            port: process.env.MG_PORT || 587,
            secure: process.env.MG_SECURE || false,
            auth: {
                user: process.env.MG_USER || 'vitop@dqmail.net',
                pass: process.env.MG_PASS || '6e1239e63b2608ed5fe9f4006b949b18-5645b1f9-3ad2a5dc',
            },
        },
    },
    redisConfig: {
        connect: {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '0.0.0.0',
            password: process.env.ENV_REDIS_PASSWORD || null,
        },
        redisExpireOtp: process.env.ENV_IDENTITY_REDIS_EXPIRE_OTP || 600,
    },
    apiCheckTokenFacebook: process.env.API_CHECK_TOKEN_FACEBOOK || 'https://graph.facebook.com/me?fields=name,first_name,last_name,email&access_token=',
    apiCheckTokenGoogle: process.env.API_CHECK_TOKEN_GOOLE || 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=',
    urlVerify: process.env.URL_RESET_PASSWORD || 'http://localhost:8200/mail-verification/', // URL verify
    uploadConfig: {
        FILE_UPLOAD_PATH: join( './', 'public/assets'),
        FILE_UPLOAD_URL: '',
    },
    apiUrl: process.env.URL_API_URL || 'http://localhost:8200',
};
