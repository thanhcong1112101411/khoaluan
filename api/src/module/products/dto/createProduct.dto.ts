import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreateProductDto {

    @IsNotEmpty({ message: 'code is require' })
    @IsNotBlank('code', { message: 'code is not white space' })
    @ApiModelProperty({ example: 'SP001' })
    code: string;

    @IsNotEmpty({ message: 'name is require' })
    @IsNotBlank('name', { message: 'name is not white space' })
    @ApiModelProperty({ example: 'Tôm' })
    name: string;

    @IsNotEmpty({ message: 'quantity is require' })
    @ApiModelProperty({ example: 10 })
    quantity: number;

    @IsNotEmpty({ message: 'min is require' })
    @ApiModelProperty({ example: 1 })
    min: number;

    @IsNotEmpty({ message: 'rrp is require' })
    @ApiModelProperty({ example: 0 })
    rrp: number;

    @IsNotEmpty({ message: 'description_first is require' })
    @IsNotBlank('description_first', { message: 'description_first is not white space' })
    @ApiModelProperty({ example: 'Hải sản' })
    description_first: string;

    @IsNotEmpty({ message: 'description_second is require' })
    @IsNotBlank('description_first', { message: 'description_first is not white space' })
    @ApiModelProperty({ example: 'Hải sản' })
    description_second: string;

    @IsNotEmpty({ message: 'unit_id is require' })
    @ApiModelProperty({ example: 0 })
    unit_id: number;

    @IsNotEmpty({ message: 'brand_id is require' })
    @ApiModelProperty({ example: 0 })
    brand_id: number;

    @IsNotEmpty({ message: 'catagory_id is require' })
    @ApiModelProperty({ example: 0 })
    catagory_id: number;
}
