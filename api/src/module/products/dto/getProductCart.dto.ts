import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class GetProductCartDto {
    @IsNotEmpty({ message: 'products is require' })
    @IsNotBlank('products', { message: 'products is not white space' })
    @ApiModelProperty({ example: [{id:1, quantity:5}, {id:2, quantity:5}] })
    products: any;
}
