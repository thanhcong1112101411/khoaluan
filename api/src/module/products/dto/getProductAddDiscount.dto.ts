import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class GetProductAddDiscountDto {
    @IsNotEmpty({ message: 'products is require' })
    @IsNotBlank('products', { message: 'products is not white space' })
    @ApiModelProperty({ example: [3,4] })
    products: any;
}
