import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get, UseInterceptors, UploadedFiles, Req, HttpException, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam, ApiImplicitFile, ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import {FilesInterceptor} from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import {extname, join} from "path";
import * as fs from 'fs';
import {diskStorage} from 'multer';
import config from '../../../config/config';
import * as jwt from 'jsonwebtoken';
import { AuthGuard } from '@nestjs/passport';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/createProduct.dto';
import { UpdateProductDto } from './dto/updateProduct.dto';
import { ProductStatus } from '../../utils/enum.util';
import { CreatePriceDto } from '../prices/dto/createPrice.dto';
import { GetProductCartDto } from './dto/getProductCart.dto';
import { AnyRecordWithTtl } from 'dns';
import { GetProductAddDiscountDto } from './dto/getProductAddDiscount.dto';

@ApiUseTags('products')
@Controller('api/products')
export class ProductsController {
    constructor(
        private readonly productsService: ProductsService,
    ) { }

    @Roles(RolesType.All)
    @Post('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add product (code: ?, name: ?, images: ?, quantity: ?, min = ?, rrp = ?, description_first = ?, description_second = ?) - Root | product Management' })
    @ApiImplicitFile({name: 'images', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images', 20,
        {
            limits: {
                fileSize: 3000000,
            },
            fileFilter: (req, images, cb) => {
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'products');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const randomName = Date.now();
                    return cb(null, `${randomName}${extname(images.originalname).toLowerCase()}`);
                },
            }),
        },
    ))
    async addProduct(@Req() request: Request, @UploadedFiles() images, @Res() res: Response) {
        
        // validate
        if (typeof images === 'undefined' || images.length == 0)
            return res.status(HttpStatus.OK).send(dataError('File is require', null));

        const image = [];
        images.map((item, index) => {
            let url = config.uploadConfig.FILE_UPLOAD_PATH.replace('public/', '') + '/products/' + item.filename;
            image.push({id: index, url: url});
        })
        
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
    
        try {
            let body: CreateProductDto;
            body = request.body;
            let data = await this.productsService.insert(body, JSON.stringify(image), token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Thêm sản phẩm thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }


    @Roles(RolesType.All)
    @Put('update/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'update product (name: ?, image_added: ?, images: ?, quantity: ?, min = ?, rrp = ?, description_first = ?, description_second = ?) - Root | product Management' })
    @ApiImplicitFile({name: 'images', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images', 20,
        {
            limits: {
                fileSize: 3000000,
            },
            fileFilter: (req, images, cb) => {
                if (typeof(images) === 'undefined'){
                    return cb(null, true);
                }
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'products');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const randomName = Date.now();
                    return cb(null, `${randomName}${extname(images.originalname).toLowerCase()}`);
                },
            }),
        },
    ))
    async updateProduct(@Param('id') id: number, @Req() request: Request, @UploadedFiles() images, @Res() res: Response) {
        
        let body: UpdateProductDto;
        body = request.body;
        let image_added = JSON.parse(body.image_added);

        // if images and image_added null
        if ((typeof images === 'undefined' || images.length == 0) && !image_added){
            return res.status(HttpStatus.OK).send(dataError('Không có ảnh sản phẩm', null));
        }

        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        let images_new = [];
        if (image_added){
            image_added.map(item => {
                images_new.push({id: images_new.length , url: item.url});
            })
        }
        if(images){
            images.map((item) => {
                let url = config.uploadConfig.FILE_UPLOAD_PATH.replace('public/', '') + '/products/' + item.filename;
                images_new.push({id: images_new.length , url: url});
            })
        }
        
        try {
            const data = await this.productsService.update(id, body, token_decoded, JSON.stringify(images_new));
            return res.status(HttpStatus.OK).send(dataSuccess('Cập nhật sản phẩm thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('set-status/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'enable/disable product - Root | product management'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async setStatusUser(@Param('id') id: number, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            let data;
            data = await this.productsService.setStatus(token_decoded, id);
       
            if (data.status == ProductStatus.ACTIVE){
                return res.status(HttpStatus.OK).send(dataSuccess('Đã mở khóa sản phẩm', null));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Đã khóa sản phẩm', null));
            
        } catch (error){
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('admin-get-all')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List product - ROOT | PRODUCT MANAGEMENT | PRICE MANAGEMENT'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name of product code, product name, catagory name, brand name', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'catagory', description: 'catagory id', required: false, type: Number})
    @ApiImplicitQuery({name: 'type', description: 'type of product (1 - dang ban || 3 - cho cap nhat gia || 2 - dang khoa)', required: false, type: Number})
    async adminListProduct(@Req() request, @Res() res: Response) {
        let limit, current_page,catagory, type;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        if (request.query.catagory){
            catagory = Number(request.query.catagory);
            if (isNaN(catagory)) {
                return res.status(HttpStatus.OK).send(dataError('catagory should be a number', null));
            }
        }

        if (request.query.type){
            type = Number(request.query.type);
            if (isNaN(type)) {
                return res.status(HttpStatus.OK).send(dataError('type should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
            type,
            catagory,
        };

        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            let data_results = await this.productsService.adminFindAll(options, token_decoded);
            return res.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('admin-get-product-none-discount')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List product none discount in date - ROOT | Discount management'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name of product code, product name, catagory name, brand name', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'catagory', description: 'catagory id', required: false, type: Number})
    @ApiImplicitQuery({name: 'date_from', description: 'date from search. ex: 2020-05-07', required: true, type: 'string'})
    @ApiImplicitQuery({name: 'date_to', description: 'date to search. ex: 2020-05-07', required: true, type: 'string'})
    async adminListProductNonDiscountInDate(@Req() request, @Res() res: Response) {
        let limit, current_page,catagory;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        if (request.query.catagory){
            catagory = Number(request.query.catagory);
            if (isNaN(catagory)) {
                return res.status(HttpStatus.OK).send(dataError('catagory should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
            catagory,
            date_from: request.query.date_from,
            date_to: request.query.date_to,
        };

        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            let data_results = await this.productsService.adminGetProductNoneDiscountInDate(options, token_decoded);
            return res.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('customer-get-all')
    @ApiOperation({title: 'Get List product - ALL'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name of product code, product name, catagory name, brand name', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'catagory', description: 'catagory id', required: false, type: Number})
    @ApiImplicitQuery({name: 'price', description: 'ORDER BY PRICE : 1 - ASC ; 2 - DESC', required: false, type: Number})
    @ApiImplicitQuery({name: 'brand', description: 'BRAND id array. ex: [1,2,3]', required: false, type: 'string'})
    async customerListProduct(@Req() request, @Res() res: Response) {
        let limit, current_page,catagory, price;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        if (request.query.catagory){
            catagory = Number(request.query.catagory);
            if (isNaN(catagory)) {
                return res.status(HttpStatus.OK).send(dataError('catagory should be a number', null));
            }
        }

        if (request.query.price){
            price = Number(request.query.price);
            if (isNaN(price)) {
                return res.status(HttpStatus.OK).send(dataError('price should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
            price,
            brand: request.query.brand,
            catagory,
        };

        try {
            let data_results = await this.productsService.customerFindAll(options);
            return res.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('customer-get-catagory/:catagoryId')
    @ApiOperation({title: 'Get List product - ALL'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'price', description: 'ORDER BY PRICE : 1 - ASC ; 2 - DESC', required: false, type: Number})
    @ApiImplicitQuery({name: 'brand', description: 'BRAND id array. ex: [1,2,3]', required: false, type: 'string'})
    async customerListProductCatagory(@Param('catagoryId') catagoryId: number, @Req() request, @Res() res: Response) {
        let limit, current_page, price;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        if (request.query.price){
            price = Number(request.query.price);
            if (isNaN(price)) {
                return res.status(HttpStatus.OK).send(dataError('price should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            price,
            brand: request.query.brand,
        };

        try {
            let data_results = await this.productsService.customerCatagoryFindAll(catagoryId, options);
            return res.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('admin-get-detail/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get product detail - ROOT | Product management ' })
    async getProductDetailAdmin(@Param('id') id: number, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.productsService.adminGetDetail(id, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('customer-get-detail/:id')
    @ApiOperation({ title: 'get product detail - All ' })
    async getProductDetailCustomer(@Param('id') id: number, @Req() request, @Res() res: Response) {
        
        try {
            const data = await this.productsService.customerGetDetail(id);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Post('get-product-cart')
    @ApiOperation({ title: 'get product cart - ALL ' })
    async getProductCart( @Body() body: GetProductCartDto, @Req() request, @Res() res: Response) {
        try {
            const data = await this.productsService.getProductCart(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Post('get-product-add-discount')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get product add discount - ROOT || Products management || export envoice management ' })
    async getProductAddDiscount( @Body() body: GetProductAddDiscountDto, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.productsService.getProductAddDiscount(body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('recomend-product-detail/:id')
    @ApiOperation({ title: 'get product recomend for detail - All ' })
    async getProductRecomendDetail(@Param('id') id: number, @Req() request, @Res() res: Response) {
        try {
            const data = await this.productsService.getRecomendDetail(id);
            
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-top-new')
    @ApiOperation({title: 'Get List product top new - ALL'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async getTopNew(@Req() request, @Res() res: Response) {
        try {
            const data = await this.productsService.getTopNew();
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
