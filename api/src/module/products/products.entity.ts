import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';
import { UsersEntity } from '../users/users.entity';
import { ScreensEntity } from '../screens/screens.entity';
import { BrandsEntity } from '../brands/brands.entity';
import { UnitsEntity } from '../units/units.entity';
import { CatagoriesEntity } from '../catagories/catagories.entity';
import { PricesEntity } from '../prices/prices.entity';
import { DiscountEntity } from '../discounts/discounts.entity';
import { DiscountView } from '../discounts/discountsView.entity';
import { EnvoiceDetailsEntity } from '../envoice-details/envoice-details.entity';

@Entity('products')
export class ProductsEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    code: string;

    @Column({
        type: 'varchar',
        length: '1000',
    })
    name: string;

    @Column({
        type: 'text',
    })
    images: string;

    @Column({
        type: 'float',
    })
    quantity: number;

    @Column({
        type: 'float',
    })
    min: number;

    @Column({
        type: 'double',
    })
    rrp: number;

    @Column({
        type: 'text',
    })
    description_first: string;

    @Column({
        type: 'text',
    })
    description_second: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    status: number;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    /*************************
     * Key           *
     *************************/

    @ManyToOne(type => BrandsEntity, brand => brand.products)
    brand: BrandsEntity;

    @ManyToOne(type => UnitsEntity, unit => unit.products)
    unit: UnitsEntity;

    @ManyToOne(type => CatagoriesEntity, catagory => catagory.products)
    catagory: CatagoriesEntity;

    @OneToMany(type => PricesEntity, price => price.product)
    prices: PricesEntity;

    @OneToMany(type => EnvoiceDetailsEntity, detail => detail.product)
    envoiceDetails: EnvoiceDetailsEntity[];
}
