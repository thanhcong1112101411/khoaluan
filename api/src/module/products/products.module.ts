import { Module, forwardRef } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';
import { ProductsEntity } from './products.entity';
import { CatagoriesModule } from '../catagories/catagories.module';
import { BrandsModule } from '../brands/brands.module';
import { UnitsModule } from '../units/units.module';
import { PricesModule } from '../prices/prices.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([ProductsEntity]),
      HistoryModule,
      RoleGroupsModule,
      CatagoriesModule,
      BrandsModule,
      UnitsModule,
      PricesModule,
    ],
    exports: [
      ProductsService,
    ],
    providers: [ProductsService],
    controllers: [ProductsController]
})
export class ProductsModule {}
