import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager, getManager, OneToOne } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { RoleDefault, Screens, ProductStatus, ProductType, PriceType } from '../../utils/enum.util';
import { CreateProductDto } from './dto/createProduct.dto';
import { ProductsEntity } from './products.entity';
import { BrandsService } from '../brands/brands.service';
import { CatagoriesService } from '../catagories/catagories.service';
import { UnitsService } from '../units/units.service';
import { async } from 'rxjs/internal/scheduler/async';
import { CatagoriesEntity } from '../catagories/catagories.entity';
import { UnitsEntity } from '../units/units.entity';
import { UpdateProductDto } from './dto/updateProduct.dto';
import { BrandsEntity } from '../brands/brands.entity';
import { PricesEntity } from '../prices/prices.entity';
import { PricesService } from '../prices/prices.service';
import { DiscountView } from '../discounts/discountsView.entity';
import { GetProductCartDto } from './dto/getProductCart.dto';
import { AnyPtrRecord } from 'dns';


const _ = require('lodash');
const readline = require('readline');
const fs = require('fs');
const lineReader = require('line-reader');
const HashMap = require('hashmap');
const LineReaderSync = require("line-reader-sync")

@Injectable()
export class ProductsService {

    constructor(
        @InjectRepository(ProductsEntity)
        private readonly productsRepository: Repository<ProductsEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
        private readonly brandsService: BrandsService,
        private readonly catagoriesService : CatagoriesService,
        private readonly unitsService: UnitsService,
        private readonly pricesService: PricesService,
        
    ) { }

    /**
     * the function add product
     * @param body 
     * @param image 
     * @param token_decoded 
     */
    async insert(body: CreateProductDto, image: string, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check code
        const productCheck = await this.findProductByCode(body.code);
        if (!!productCheck)
            throw {message: 'Mã sản phẩm đã tồn tại'}

        // add
        
        let product = new ProductsEntity();
        product.code = body.code;
        product.name = body.name;
        product.images = image;
        product.quantity = body.quantity;
        product.min = body.min;
        if (body.rrp){
            product.rrp = body.rrp;
        }else{
            product.rrp = 0;
        }
        product.description_first = body.description_first;
        product.description_second = body.description_second;
        product.status = ProductStatus.ACTIVE;

        let catagory: CatagoriesEntity;
        let brand: BrandsEntity;
        let unit: UnitsEntity;

        const getCatagory = async(id) => {
            const data = await this.catagoriesService.findCatagoryById(id);
            if (!data)
                throw {message: "catagory is not exist"};
            catagory = data;
        }
        const getBrand = async(id) => {
            const data = await this.brandsService.findBrandById(id);
            if (!data)
                throw {message: "brand is not exist"};
            brand = data;
        }
        const getUnit = async(id) => {
            const data = await this.unitsService.findunitById(id);
            if (!data)
                throw {message: "unit is not exist"};
            unit = data;
        }

        await Promise.all([
            getCatagory(body.catagory_id),
            getBrand(body.brand_id),
            getUnit(body.unit_id),
        ]);

        product.catagory = catagory;
        product.brand = brand;
        product.unit = unit;


        const data = await this.productsRepository.save(product);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Sản phẩm',
            'Thêm sản phẩm "'+ data.name +'"',
        )

        return data;
        
    }

    /**
     * the function update catagory
     * @param body 
     * @param token_decoded (user_id, role, email)
     * @param image 
     */
    async update(id: number, body: UpdateProductDto, token_decoded: any, image: string){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        const product = await this.findProductAllById(id);
        if (!product)
            throw {message: 'product is not exist'}

        // check code
        if (product.code !== body.code){
            const productCheck = await this.findProductByCode(body.code);
            if (!!productCheck)
                throw {message: 'Mã sản phẩm đã tồn tại'}
        }

        // check catagory, brand, unit
        let catagory: CatagoriesEntity = null;
        let brand: BrandsEntity = null;
        let unit: UnitsEntity = null;
        const getCatagory = async(id) => {
            if (product.catagory.id == id){
                return;
            }
            const data = await this.catagoriesService.findCatagoryById(id);
            if (!data)
                throw {message: "catagory is not exist"};
            catagory = data;
        }
        const getBrand = async(id) => {
            if (product.brand.id == id){
                return;
            }
            const data = await this.brandsService.findBrandById(id);
            if (!data)
                throw {message: "brand is not exist"};
            brand = data;
        }
        const getUnit = async(id) => {
            if (product.unit.id == id){
                return;
            }
            const data = await this.unitsService.findunitById(id);
            if (!data)
                throw {message: "unit is not exist"};
            unit = data;
        }

        await Promise.all([
            getCatagory(body.catagory_id),
            getBrand(body.brand_id),
            getUnit(body.unit_id),
        ]);

        product.code = body.code;
        product.name = body.name;
        product.images = image;
        product.quantity = body.quantity;
        product.min = body.min;
        if (body.rrp){
            product.rrp = body.rrp;
        }else{
            product.rrp = 0;
        }
        product.description_first = body.description_first;
        product.description_second = body.description_second;
        
        if (catagory){
            product.catagory = catagory;
        }
        if (brand){
            product.brand = brand;
        }
        if (unit){
            product.unit = unit;
        }

        const data = await this.productsRepository.save(product);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Sản phẩm',
            'cập nhật sản phẩm "' + product.name + '('+ product.id +')"',
        )

        return data;
    }

    /**
     * This function enable, disable product by body and role
     * @param  id, token_decoded (user_id, role, email)
     * 
     */
    async setStatus(token_decoded: any, id: number) {
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check user exist
        let product = await  this.findProductById(id);
        if (!product) throw {message: 'Product is not exist', status: 400};

        // set status user
        product.status = product.status === ProductStatus.ACTIVE ? ProductStatus.INACTIVE : ProductStatus.ACTIVE;

        // save history
        await this.historyService.insertHistory(
            token_decoded.uuid,
            'Sản phẩm',
            'Cập nhật trạng thái sản phẩm "' + product.code + '"',
        )
        

        return this.productsRepository.save(product);
    }

    /**
     * This function get all products by options
     * @param options (current_page, limit, name, type)
     * @param token_decoded (user_id, role, email)
     * @returns object {pages, count, list_user, load_more}
     */
    async adminFindAll(options: any, token_decoded: any){

        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS || o.id == Screens.PRICES; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = 'FROM products AS products ';
        // discount
        query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount '+
                'FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) '+
                'as discount ON products.id = discount.productId ';

        // price
        // query += 'LEFT JOIN (SELECT productId, price as importPrice FROM import_price_view) as importsprice ON importsprice.productId = products.id '+
        //         'LEFT JOIN (SELECT productId, price as exportPrice FROM export_price_view) as exportsPrice ON exportsPrice.productId = products.id ';
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';

        // catagory
        query += 'lEFT JOIN (SELECT catagories.id as catagoryId, catagories.name as catagoryName FROM catagories) as catagories ON catagories.catagoryId = products.catagoryId ';

        // brand
        query += 'LEFT JOIN (SELECT id as brandId, name as brandName FROM brands) as brands ON brands.brandId = products.brandId ';

        // unit
        query += 'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';

        // search catagory
        if (options.catagory){
            const catagory = await this.catagoriesService.findCatagoryContainDeletedById(options.catagory);
            if(!catagory){
                throw {message: 'catagory is not exist in system'};
            }

            query += 'WHERE catagories.catagoryId = ' + options.catagory;
        }

        // search name
        if(options.name){
            if(options.catagory){
                query += "AND ( products.name like '%" + options.name + "%' OR brands.brandName like '%" + options.name + "%' OR catagories.catagoryName like '%" + options.name + "%' )";
            }else{
                query += "WHERE ( products.name like '%" + options.name + "%' OR brands.brandName like '%" + options.name + "%' OR catagories.catagoryName like '%" + options.name + "%' )";
            }
        }
        // search type
        if(options.type){
            // check type
            if (options.type != ProductType.ACTIVE && options.type != ProductType.INACTIVE && options.type != ProductType.WATING_PRICE){
                throw {message: 'Type is not exist'}
            }
            if (options.catagory || options.name){
                query += " AND ";
            }else{
                query += " WHERE ";
            }

            if (options.type == ProductType.ACTIVE){
                query += 'products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null';

            } else if (options.type == ProductType.INACTIVE){
                query += 'products.status = ' + ProductStatus.INACTIVE;
            }else {
                query += '(importsprice.importPrice is null OR exportsPrice.exportPrice is null) AND products.status = ' + ProductStatus.ACTIVE;
            }
        }

        const query_count = "SELECT COUNT(*) as quantity " + query;
        const query_get_data = "SELECT * " + query + " ORDER BY products.created_at DESC LIMIT " + start + ", " + query_limit;
        // console.log(query_get_data);

        let entityManger = getManager();

        let results = await Promise.all([
            entityManger.query(query_count),
            entityManger.query(query_get_data),
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        

        // pages quantity
        const pages = Math.ceil(parseInt(results[0][0].quantity) / query_limit);

        return {
            pages: pages,
            count: parseInt(results[0][0].quantity),
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * This function get all products none discount in date
     * @param options (current_page, limit, name, date_from, date_to)
     * @param token_decoded (user_id, role, email)
     * @returns object {pages, count, list_user, load_more}
     */
    async adminGetProductNoneDiscountInDate(options: any, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS || o.id == Screens.DISCOUNTS; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = 'FROM products AS products ';

        // price
        // query += 'LEFT JOIN (SELECT productId, price as importPrice FROM import_price_view) as importsprice ON importsprice.productId = products.id '+
        //         'LEFT JOIN (SELECT productId, price as exportPrice FROM export_price_view) as exportsPrice ON exportsPrice.productId = products.id ';
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        // catagory
        query += 'lEFT JOIN (SELECT catagories.id as catagoryId, catagories.name as catagoryName FROM catagories) as catagories ON catagories.catagoryId = products.catagoryId ';

        // brand
        query += 'LEFT JOIN (SELECT id as brandId, name as brandName FROM brands) as brands ON brands.brandId = products.brandId ';

        // unit
        query += 'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';

        // discount

        query += ' LEFT JOIN '+
                '( SELECT * FROM (SELECT discounts.id as discount_id, discounts.date_from, discounts.date_to FROM discounts WHERE (discounts.date_from >= "' + options.date_from +' 00:00:00 "' +
                'AND discounts.date_from <= "' + options.date_to +' 23:59:59") OR ( discounts.date_to >= "' + options.date_from +' 00:00:00 " AND discounts.date_to <= "' + options.date_to +' 23:59:59" ) OR ( "' + options.date_from +' 00:00:00 " >= discounts.date_from AND "' + options.date_from +' 23:59:59" <=  discounts.date_to ) AND deleted_at is null) AS discounts LEFT JOIN '+ 
                'discounts_products_products ON discounts.discount_id = discounts_products_products.discountsId ) as discount ON products.id = discount.productsId ' +
                'WHERE discount.discountsId is null ';

        // search catagory
        if (options.catagory){
            const catagory = await this.catagoriesService.findCatagoryContainDeletedById(options.catagory);
            if(!catagory){
                throw {message: 'catagory is not exist in system'};
            }

            query += 'AND catagories.catagoryId = ' + options.catagory;
        }

        // search name
        if(options.name){
            query += " AND ( products.name like '%" + options.name + "%' OR brands.brandName like '%" + options.name + "%' OR catagories.catagoryName like '%" + options.name + "%' )";
            
        }

        const query_count = "SELECT COUNT(*) as quantity " + query;
        const query_get_data = "SELECT * " + query + " ORDER BY products.created_at DESC LIMIT " + start + ", " + query_limit;

        let entityManger = getManager();

        let results = await Promise.all([
            entityManger.query(query_count),
            entityManger.query(query_get_data),
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(parseInt(results[0][0].quantity) / query_limit);

        return {
            pages: pages,
            count: parseInt(results[0][0].quantity),
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * This function get all products by options
     * @param options (current_page, limit, name, price(order), catagory, brand )
     * @returns object {pages, count, list_user, load_more}
     */
    async customerFindAll(options: any){

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = 'FROM products AS products ';
        // discount
        query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount '+
                'FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) '+
                'as discount ON products.id = discount.productId ';

        // price
        // query += 'LEFT JOIN (SELECT productId, price as importPrice FROM import_price_view) as importsprice ON importsprice.productId = products.id '+
        //         'LEFT JOIN (SELECT productId, price as exportPrice FROM export_price_view) as exportsPrice ON exportsPrice.productId = products.id ';
                query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        // catagory
        query += 'lEFT JOIN (SELECT catagories.id as catagoryId, catagories.name as catagoryName FROM catagories) as catagories ON catagories.catagoryId = products.catagoryId ';

        // brand
        query += 'LEFT JOIN (SELECT id as brandId, name as brandName FROM brands) as brands ON brands.brandId = products.brandId ';

        // unit
        query += 'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';

        // check product selling
        query += 'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null ';

        // search catagory
        if (options.catagory){
            const catagory = await this.catagoriesService.findCatagoryContainDeletedById(options.catagory);
            if(!catagory){
                throw {message: 'catagory is not exist in system'};
            }

            query += 'AND catagories.catagoryId = ' + options.catagory + ' ';
        }

        // search name
        if(options.name){
            query += "AND ( products.name like '%" + options.name + "%' OR brands.brandName like '%" + options.name + "%' OR catagories.catagoryName like '%" + options.name + "%' )";
        }

        // search brand [1,2,3]
        if(options.brand){
            const brand = JSON.parse(options.brand);
       
            let brandQuery = '';
            brand.forEach((element, index) => {
                brandQuery += 'brands.brandId = ' + element;
                if(index != brand.length - 1){
                    brandQuery += " OR ";
                }
            });

            query += 'AND ( ' + brandQuery + ')';
        }
        
        let query_count = "SELECT COUNT(*) as quantity " + query;
        let query_get_data = '';

        // order price
        if (options.price){
            if(options.price != 1 && options.price != 2){
                throw {message: 'type is not exist'};
            }
            if(options.price == 1){
                query_get_data = "SELECT * " + query + " ORDER BY exportsPrice.exportPrice ASC LIMIT " + start + ", " + query_limit;
            }else{
                query_get_data = "SELECT * " + query + " ORDER BY exportsPrice.exportPrice DESC LIMIT " + start + ", " + query_limit;
            }
            
        }else{
            query_get_data = "SELECT * " + query + " ORDER BY products.created_at DESC LIMIT " + start + ", " + query_limit;
        }

        let entityManger = getManager();

        let results = await Promise.all([
            entityManger.query(query_count),
            entityManger.query(query_get_data),
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(parseInt(results[0][0].quantity) / query_limit);

        // delete import price
        let data = [];
        await results[1].forEach(async element => {
            delete element.importPrice;
            data.push(element);
        });

        return {
            pages: pages,
            count: parseInt(results[0][0].quantity),
            data: data,
            load_more: is_load_more,
        };
    }

    /**
     * This function get all products  by options and catagory id
     * @param options (current_page, limit, price(order), brand )
     * @returns object {pages, count, list_user, load_more}
     */
    async customerCatagoryFindAll(catagoryId: number, options: any){

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = 'FROM products AS products ';
        // discount
        query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount '+
                'FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) '+
                'as discount ON products.id = discount.productId ';

        // price
        // query += 'LEFT JOIN (SELECT productId, price as importPrice FROM import_price_view) as importsprice ON importsprice.productId = products.id '+
        //         'LEFT JOIN (SELECT productId, price as exportPrice FROM export_price_view) as exportsPrice ON exportsPrice.productId = products.id ';
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        // catagory
        query += 'lEFT JOIN (SELECT catagories.id as catagoryId, catagories.name as catagoryName FROM catagories) as catagories ON catagories.catagoryId = products.catagoryId ';

        // brand
        query += 'LEFT JOIN (SELECT id as brandId, name as brandName FROM brands) as brands ON brands.brandId = products.brandId ';

        // unit
        query += 'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';

        // check product selling
        query += 'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null ';

        // search catagory
        const catagory = await this.catagoriesService.findCatagoryContainDeletedById(catagoryId);
        if(!catagory){
            throw {message: 'catagory is not exist in system'};
        }
        query += 'AND catagories.catagoryId = ' + catagoryId + ' ';

        // search brand [1,2,3]
        if(options.brand){
            const brand = JSON.parse(options.brand);
    
            let brandQuery = '';
            brand.forEach((element, index) => {
                brandQuery += 'brands.brandId = ' + element;
                if(index != brand.length - 1){
                    brandQuery += " OR ";
                }
            });

            query += 'AND ( ' + brandQuery + ')';
        }
        
        let query_count = "SELECT COUNT(*) as quantity " + query;
        let query_get_data = '';

        // order price
        if (options.price){
            if(options.price != 1 && options != 2){
                throw {message: 'type is not exist'};
            }
            if(options.price == 1){
                query_get_data = "SELECT * " + query + " ORDER BY exportsPrice.exportPrice ASC LIMIT " + start + ", " + query_limit;
            }else{
                query_get_data = "SELECT * " + query + " ORDER BY exportsPrice.exportPrice ASC LIMIT " + start + ", " + query_limit;
            }
            
        }else{
            query_get_data = "SELECT * " + query + " ORDER BY products.created_at DESC LIMIT " + start + ", " + query_limit;
        }

        let entityManger = getManager();

        let results = await Promise.all([
            entityManger.query(query_count),
            entityManger.query(query_get_data),
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(parseInt(results[0][0].quantity) / query_limit);

        // delete import price
        let data = [];
        await results[1].forEach(async element => {
            delete element.importPrice;
            data.push(element);
        });

        return {
            pages: pages,
            count: parseInt(results[0][0].quantity),
            data: data,
            load_more: is_load_more,
        };
    }

    /**
     * The function get product detail by id - ADMIN
     * @Param token_decoded(user_id, role, email)
     * @param id 
     */
    async adminGetDetail(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }
        
        // discount
        let query = 'SELECT * FROM (SELECT * FROM products WHERE products.id = ' + id + ') AS products LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) as discount ON products.id = discount.productId';
        
        // catagory
        query += ' LEFT JOIN (SELECT catagories.id catagoryId, catagories.name as catagoryName FROM catagories) as catagory ON products.catagoryId = catagory.catagoryId';
        // brand
        query += ' LEFT JOIN (SELECT brands.id as brandId, brands.name as brandName FROM brands) as brand ON products.brandId = brand.brandId';
        // unit
        query += ' LEFT JOIN (SELECT units.id as unitId, units.name as unitName FROM units) as unit ON products.unitId = unit.unitId ';
        //price
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        let entityManger = getManager();
        let product = await entityManger.query(query);

        // check product
        if (!product)
            throw {message: 'product is not exist'}

        // let getPrice = async(id: number, type: number) => {
        //     let result = await this.pricesService.getPirceOfProductId(id, type);
        //     return result;
        // }

        // let results = await Promise.all([
        //     getPrice(id, PriceType.IMPORT),
        //     getPrice(id, PriceType.EXPORT),
        // ]);

        // const data = Object.assign(product[0], {importPrice: results[0], exportPirce: results[1]});

        return product[0];
        
    }

    /**
     * The function get product detail by id - CUSTOMER
     * @param id 
     */
    async customerGetDetail(id: number){
        // discount
        let query = 'SELECT * FROM (SELECT * FROM products WHERE products.id = ' + id + ') AS products LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) as discount ON products.id = discount.productId';
        
        // catagory
        query += ' LEFT JOIN (SELECT catagories.id catagoryId, catagories.name as catagoryName FROM catagories) as catagory ON products.catagoryId = catagory.catagoryId';
        // brand
        query += ' LEFT JOIN (SELECT brands.id as brandId, brands.name as brandName FROM brands) as brand ON products.brandId = brand.brandId';
        // unit
        query += ' LEFT JOIN (SELECT units.id as unitId, units.name as unitName FROM units) as unit ON products.unitId = unit.unitId ';

        //price
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';

        let entityManger = getManager();
        let product = await entityManger.query(query);

        // check product
        if (!product)
            throw {message: 'product is not exist'}

        // const exportPrice = await this.pricesService.getPirceOfProductId(id, PriceType.EXPORT);

        // const data = Object.assign(product[0], {exportPirce: exportPrice});

        return product[0];
    }

    /**
     * The function find product by code
     * @param code 
     */
    async findProductByCode(code: string){
        return this.productsRepository.findOne({code: code});
    }

    /**
     * The function find product by id
     * @param code 
     */
    async findProductById(id: number){
        return this.productsRepository.findOne({id});
    }

    /**
     * The function find product by id with catagory, brand, unit
     * @param code 
     */
    async findProductAllById(id: number){
        return this.productsRepository.findOne({id: id}, {relations: ["catagory","brand", "unit"]});
    }

    /**
     * the function find product by id contain import price and export price
     * @param id 
     */
    async findProductByIdWithPrice(id: number){
        let query = 'SELECT * FROM ( SELECT * FROM products WHERE products.id = ' + id + ') AS products ';
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        // 'LEFT JOIN (SELECT productId, price as importPrice FROM import_price_view) as importsprice ON importsprice.productId = products.id '+
        // 'LEFT JOIN (SELECT productId, price as exportPrice FROM export_price_view) as exportsPrice ON exportsPrice.productId = products.id';

        let entityManger = getManager();
        let product = await entityManger.query(query);

        return product;
    }

    /**
     * the function check product by id contain discount from date_from to date_to
     * @param id 
     */
    async isProductByIdWithDiscount(id: number, date_from: string, date_to: string){
        let query = 'SELECT * FROM (SELECT * FROM products WHERE products.id = ' + id + ') AS products ';

        query += ' LEFT JOIN '+
        '( SELECT * FROM (SELECT discounts.id as discount_id, discounts.date_from, discounts.date_to FROM discounts WHERE (discounts.date_from >= "' + date_from +' 00:00:00 "' +
        'AND discounts.date_from <= "' + date_to +' 23:59:59") OR ( discounts.date_to >= "' + date_from +' 00:00:00 " AND discounts.date_to <= "' + date_to +' 23:59:59" ) OR ( "' + date_from +' 00:00:00 " >= discounts.date_from AND "' + date_from +' 23:59:59" <=  discounts.date_to ) AND deleted_at is null) AS discounts LEFT JOIN '+ 
        'discounts_products_products ON discounts.discount_id = discounts_products_products.discountsId ) as discount ON products.id = discount.productsId ';

        let entityManger = getManager();
        let product = await entityManger.query(query);
        if (!product)
            throw {message: 'product is not exist in system'}
        if (product.discountsId){
            return true;
        }

        return false;
    }

    /**
     * The function get product by id contain import price, export price, discount
     * @param id 
     */
    async findProductByIdWithPriceDiscount(id: number){
        let query = 'SELECT * FROM ( SELECT * FROM products WHERE products.id = ' + id + ') AS products ';
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        
        query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) as discount ON products.id = discount.productId ' +
        'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null';

        let entityManger = getManager();
        let product = await entityManger.query(query);

        return product;
    }

    /**
     * Get product cart
     * @param body 
     */
    async getProductCart(body: GetProductCartDto){
        let products = [];
        let entityManger = getManager();

        await Promise.all(body.products.map(async(element, i) => {

            // check duplicate product id
            const index = await _.findLastIndex(body.products, function(o) { return o.id == element.id; });
            if (index != i)
                throw {message: "product id duplicate"};

            let query = 'SELECT * FROM ( SELECT * FROM products WHERE products.id = ' + element.id + ') AS products '+
            'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';
            query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
            query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) as discount ON products.id = discount.productId ' +
            'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null';
        
            const data = await entityManger.query(query);
            
            if (!data || data.length == 0){
                throw {message: 'product is not exist or blocked or not update price'}
            }
            let product = Object.assign(data[0], {quantityProduct: element.quantity});
            // delete import price
            delete product.importPrice;

            products.push(product);

        }))

        return products;
    }

    /**
     * Get product for add discount (product picking)
     * @param body 
     */
    async getProductAddDiscount(body: GetProductCartDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRODUCTS || o.id == Screens.EXPORT_ENVOICE; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        let products = [];
        let entityManger = getManager();

        await Promise.all(body.products.map(async(element, i) => {

            let query = 'SELECT * FROM ( SELECT * FROM products WHERE products.id = ' + element + ') AS products '+
            'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';
            query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
            query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) as discount ON products.id = discount.productId ' +
            'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null';
        
            const data = await entityManger.query(query);
            
            if (!data || data.length == 0){
                throw {message: 'product is not exist or blocked or not update price'}
            }
            let product = data[0];

            products.push(product);

        }))

        return products;
    }

    async getRecomendDetail(id: number){
        let listHUI = [];
        // await lineReader.eachLine('public/output2.txt', async (line) => {
        //     if (line.includes('STOP')) {
        //        const data = await this.listProductRecomendDetail(listHUI, id);
        //        console.log("--------------------data ----------------");
        //        console.log(data);
        //        return data;
        //     }
        //         let split = await line.split(":");
        //         let items = await split[0].split(" ");
        //         let twu = parseInt(split[1]);
                
        //         let index = await items.indexOf(id);
        //         if (index != -1 && items.length > 1){
        //             listHUI.push({items: items, twu: twu});
        //         }
        // });

        const lrs = new LineReaderSync("public/output2.txt")
        while(true){
            const line = lrs.readline()
            if(line.includes('STOP')){
                const data = await this.listProductRecomendDetail(listHUI, id);
                // console.log("--------------------data ----------------");
                // console.log(data);
                return data;
            }else{
                let split = await line.split(":");
                let items = await split[0].split(" ");
                let twu = parseInt(split[1]);
                
                let index = await items.indexOf(id);
                if (index != -1 && items.length > 1){
                    listHUI.push({items: items, twu: twu});
                }
            }
        }
        
    }

    async listProductRecomendDetail(listHUI: Array<any>, id: number){
        // sort twu desc
        let huiList = await listHUI.sort(function (a,b) {
            return b.twu - a.twu;
        })

        // console.log("----------------HUI----------------");
        // console.log(huiList);

        let productList = [];
        huiList.forEach(element => {
            let items = element.items;
            items = _.remove(items, function(n) {
                return n != id;
            });

            items.forEach(item => {
                let index = productList.indexOf(item);
                if (index == -1 && productList.length < 4){
                    productList.push(item);
                }else if (productList.length == 4){
                    return;
                }
            });
            
            return productList;
        });

        // console.log("--------------- product list -------------------");
        // console.log(productList);

        let products = [];
        let entityManger = getManager();

        await Promise.all(productList.map(async(element, i) => {
            const id = parseInt(element);

            let query = 'SELECT * FROM ( SELECT * FROM products WHERE products.id = ' + id + ') AS products '+
            'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';
            query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
            query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) as discount ON products.id = discount.productId ' +
            'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null';
        
            const data = await entityManger.query(query);
            
            let product = data[0];
            // delete import price
            delete product.importPrice;

            products.push(product);

        }))

        return products;
    }
    /**
     * The function get top 3 product newest
     */
    async getTopNew(){
        let query = 'FROM products AS products ';
        // discount
        query += 'LEFT JOIN (SELECT discounts_products_products.productsId as productId, discounts_view.amount as discountAmount '+
                'FROM discounts_view LEFT JOIN discounts_products_products ON discounts_view.id = discounts_products_products.discountsId) '+
                'as discount ON products.id = discount.productId ';

        // price
        // query += 'LEFT JOIN (SELECT productId, price as importPrice FROM import_price_view) as importsprice ON importsprice.productId = products.id '+
        //         'LEFT JOIN (SELECT productId, price as exportPrice FROM export_price_view) as exportsPrice ON exportsPrice.productId = products.id ';
        query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id '+
                'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        // catagory
        query += 'lEFT JOIN (SELECT catagories.id as catagoryId, catagories.name as catagoryName FROM catagories) as catagories ON catagories.catagoryId = products.catagoryId ';

        // brand
        query += 'LEFT JOIN (SELECT id as brandId, name as brandName FROM brands) as brands ON brands.brandId = products.brandId ';

        // unit
        query += 'LEFT JOIN (SELECT id as unitId, name as unitName FROM units ) as units ON units.unitId = products.unitId ';

        // check product selling
        query += 'WHERE products.status = ' + ProductStatus.ACTIVE + ' AND importsprice.importPrice is not null AND exportsPrice.exportPrice is not null ';
    
        query = "SELECT * " + query + " ORDER BY products.created_at DESC LIMIT 0,3";

        let entityManger = getManager();

        const results = await entityManger.query(query);

        // delete import price
        let data = [];
        await results.forEach(async element => {
            delete element.importPrice;
            data.push(element);
        });

        return data;
    }
    
}
