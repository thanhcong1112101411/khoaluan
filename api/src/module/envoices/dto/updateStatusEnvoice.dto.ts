import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class UpdateStatusEnvoiceDto {

    @IsNotEmpty({ message: 'status is require' })
    @ApiModelProperty({ required: true, example: '1 - WATING; 2 - COMPLETE; 3 - CANCEL' })
    status: number;
}