import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class UpdateNoteEnvoiceDto {

    @IsNotEmpty({ message: 'note is require' })
    @IsNotBlank('note', { message: 'note is not white space' })
    @ApiModelProperty({ required: true, example: '' })
    note: string;
}