import { Controller, Get, UseGuards, Req, Res, HttpStatus, Post, Body, Param, Put } from '@nestjs/common';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiImplicitQuery, ApiUseTags } from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken';
import { dataError, getPaginationData, dataSuccess } from '../../utils/jsonFormat';
import { Response } from 'express';
import { UserUtil } from '../../utils/user.util';
import { EnvoicesService } from './envoices.service';
import { CreateEnvoiceDto } from './dto/createEnvoice.dto';
import { UpdateNoteEnvoiceDto } from './dto/updateNoteEnvoice.dto';
import { UpdateStatusEnvoiceDto } from './dto/updateStatusEnvoice.dto';


@ApiUseTags('envoices')
@Controller('api/envoices')
export class EnvoicesController {
    constructor(
        private readonly envoiceService: EnvoicesService,
    ) { }

    @Roles(RolesType.All)
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add envoice - USER | ROOT | EXPORT ENVOICE MANAGEMENT' })
    async add(@Body() body: CreateEnvoiceDto, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.envoiceService.insert(body, token_decoded);
            delete data.user;
            return res.status(HttpStatus.CREATED).send(dataSuccess('Đặt hàng thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('admin-get-all')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List envoice - ROOT | EXPORT ENVOICE MANAGEMENT'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'code envoice, user name, user phone', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'status', description: 'type of envoice (1 - WATING || 2 - COMPLETED || 3 - CANCELED)', required: true, type: Number})
    @ApiImplicitQuery({name: 'type', description: 'type of envoice (1 - USER ORDER || 2 - ADMIN ADD)', required: false, type: Number})
    @ApiImplicitQuery({name: 'date_from', description: 'date from', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'date_to', description: 'date to', required: false, type: 'string'})

    async adminListEnvoices(@Req() request, @Res() res: Response) {
        let limit, current_page, status, type;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        if (request.query.status){
            status = Number(request.query.status);
            if (isNaN(status)) {
                return res.status(HttpStatus.OK).send(dataError('status should be a number', null));
            }
        }

        if (request.query.type){
            type = Number(request.query.type);
            if (isNaN(type)) {
                return res.status(HttpStatus.OK).send(dataError('type should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
            type,
            status,
            date_from: request.query.date_from,
            date_to: request.query.date_to,
        };

        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            let data_results = await this.envoiceService.adminFindAll(options, token_decoded);
            let data = [];
            await Promise.all(data_results.data.map(async(element) => {
                let item = element;
                let user = UserUtil.serialize(item.user);
                item.user = user;
                data.push(item);
            }));

            return res.status(HttpStatus.OK).send(getPaginationData('OK', data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-detail/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get envoice detail - ROOT | EXPORT_ENVOICE MANAGEMENT | OWNER ' })
    async getDetail(@Param('id') id: number, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
       
        try {
            const data = await this.envoiceService.getDetail(id, token_decoded);
            data.user = UserUtil.serialize(data.user);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-amount')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get envoice amount - ROOT | EXPORT_ENVOICE MANAGEMENT | RECOMEND ' })
    @ApiImplicitQuery({name: 'date_from', description: 'date from', required: true, type: 'string'})
    @ApiImplicitQuery({name: 'date_to', description: 'date to', required: true, type: 'string'})
    async getAmount(@Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        
        let options = {
            date_from: request.query.date_from,
            date_to: request.query.date_to,
        };

        try {
            const data = await this.envoiceService.getAmount(options, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-list-envoice/:uuid')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get envoice list of user by userID - ROOT | EXPORT_ENVOICE MANAGEMENT | OWNER ' })
    async getListEnvoiceOfUser(@Param('uuid') uuid: string, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.envoiceService.getListEnvoiceOfUser(uuid, token_decoded);
            let result = [];
            await Promise.all(data.map(async(element) => {
                let item = element;
                delete item.user;
                result.push(item);
            }));
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('update-note/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get envoice list of user by userID - ROOT | EXPORT_ENVOICE MANAGEMENT' })
    async updateNoteEnvoice(@Param('id') id: number, @Body() body: UpdateNoteEnvoiceDto, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            await this.envoiceService.updateNoteEnvoice(id, body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Cập nhật ghi chú thành công', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('set-status/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get envoice list of user by userID - ROOT | EXPORT_ENVOICE MANAGEMENT' })
    async setStatus(@Param('id') id: number, @Body() body: UpdateStatusEnvoiceDto, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            await this.envoiceService.setStatus(id, body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Cập nhật trạng thái thành công', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-envoice-for-hui')
    @ApiOperation({ title: 'get envoice for hui' })
    async getEnvoiceForHui(@Req() request, @Res() res: Response) {
        
        try {
            const data = await this.envoiceService.getEnvoiceForHui();
            return res.status(HttpStatus.OK).send(dataSuccess('OK', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

}


