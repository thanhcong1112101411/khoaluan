import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';
import { UsersEntity } from '../users/users.entity';
import { ScreensEntity } from '../screens/screens.entity';
import { BrandsEntity } from '../brands/brands.entity';
import { UnitsEntity } from '../units/units.entity';
import { CatagoriesEntity } from '../catagories/catagories.entity';
import { PricesEntity } from '../prices/prices.entity';
import { DiscountEntity } from '../discounts/discounts.entity';
import { DiscountView } from '../discounts/discountsView.entity';
import { ProductsEntity } from '../products/products.entity';
import { EnvoiceDetailsEntity } from '../envoice-details/envoice-details.entity';

@Entity('envoices')
export class EnvoicesEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    code: string;

    @Column({
        type: 'float',
    })
    money: number;

    @Column({
        type: 'float',
    })
    utility: number;

    @Column({
        type: 'text',
    })
    note: string;

    @Column({
        type: 'boolean',
    })
    isDiscount: boolean;

    @Column({
        type: 'smallint',
        default: 1,
    })
    status: number;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    /*************************
     * Key           *
     *************************/

    @ManyToOne(type => UsersEntity, user => user.envoices)
    user: UsersEntity;

    @OneToMany(type => EnvoiceDetailsEntity, detail => detail.envoice)
    envoiceDetails: EnvoiceDetailsEntity[];
}
