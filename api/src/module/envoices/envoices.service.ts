import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager, getManager, Connection, ConnectionManager } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { RoleDefault, Screens, DiscountType, EnvoiceStatus, EnvoiceType } from '../../utils/enum.util';
import _ from 'lodash';
import { View } from 'typeorm/schema-builder/view/View';
import { ProductsService } from '../products/products.service';
import { CreateEnvoiceDto } from './dto/createEnvoice.dto';
import { EnvoicesEntity } from './envoices.entity';
import { EnvoiceDetailsEntity } from '../envoice-details/envoice-details.entity';
import { UsersService } from '../users/users.service';
import { UpdateNoteEnvoiceDto } from './dto/updateNoteEnvoice.dto';
import { UpdateStatusEnvoiceDto } from './dto/updateStatusEnvoice.dto';
const moment = require('moment');

@Injectable()
export class EnvoicesService {

    constructor(
        @InjectRepository(EnvoicesEntity)
        private readonly envoiceRepository: Repository<EnvoicesEntity>,

        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
        private readonly productsService: ProductsService,
        private readonly usersService: UsersService,
    ) { }

    /**
     * this function add brand 
     * @param body 
     * @param token_decoded (user_id, role, email)
     */
    async insert(body: CreateEnvoiceDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT && token_decoded.role != RoleDefault.USER){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }
        // get user
        const user = await this.usersService.findOneUserActiveById(token_decoded.user_id);

        // get envoice latest in date
        let now = moment();

        let year = now.get('year');
        let month = now.get('month') + 1;  // 0 to 11
        let date = now.get('date');
        const currentString = year + "/" + month + "/" + date;

        let currentEnvoiceCode = '';
        const envoiceLatest = await this.GetEnvoiceLatestInDate(currentString);
        // if there was a envoice for the date, increase envoice number by 1 (HD2020/06/03/1 => HD2020/06/03/2)
        if (envoiceLatest){
            let orderNumber = envoiceLatest.code.replace("HD" + currentString + "/", "");
            let orderNext = parseInt(orderNumber) + 1;
            currentEnvoiceCode = "HD" + currentString + "/" + orderNext;
        }else{
            currentEnvoiceCode = "HD" + currentString + "/1";
        }

        let envoiceDetails = [];
        let totalUtility = 0;
        let totalMoney = 0;
        let isDiscount = false;
        const entityManager = getManager();
        await Promise.all(body.products.map(async(element) => {
            // get product
            const productInf = await this.productsService.findProductByIdWithPriceDiscount(element.id);
            if (productInf.length == 0 || !productInf){
                throw {message: 'data error'}
            }
            const product = productInf[0];
            console.log(product);

            const productAdd = await this.productsService.findProductById(element.id);

            // add detail
            let EnvoiceDetail = new EnvoiceDetailsEntity();
            EnvoiceDetail.amount = element.quantity;
            if (product.discountAmount){
                EnvoiceDetail.discount = product.discountAmount;
                isDiscount = true;
            }else {
                EnvoiceDetail.discount = 0;
            }
            EnvoiceDetail.exportPrice = product.exportPrice;
            EnvoiceDetail.money = product.exportPrice * ( 1 - product.discountAmount / 100) * element.quantity;
            EnvoiceDetail.utility = (product.exportPrice * ( 1 - product.discountAmount / 100) - product.importPrice) * element.quantity;
            EnvoiceDetail.product = productAdd;

            await entityManager.save(EnvoiceDetail);

            totalUtility += EnvoiceDetail.utility;
            totalMoney += EnvoiceDetail.money;
            envoiceDetails.push(EnvoiceDetail);

            // push envoice detail
        }))

        // add
        let envoice = new EnvoicesEntity();
        envoice.code = currentEnvoiceCode;
        envoice.isDiscount = isDiscount;
        envoice.money = totalMoney;
        envoice.utility = totalUtility;
        envoice.envoiceDetails = envoiceDetails;
        envoice.note = body.note;
        envoice.status = EnvoiceStatus.WATING;
        envoice.user = user;


        const data = await this.envoiceRepository.save(envoice);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Hóa đơn',
            'Thêm hóa đơn "' + data.code + '('+ data.id +')"',
        )

        return data;
        
    }

    /**
     * This function get all news (transaction) by options
     * @param options (current_page, limit, name)
     * @returns object {pages, count, list_user, load_more}
     */
    async adminFindAll(options: any, token_decoded: any){

        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.envoiceRepository.createQueryBuilder('envoice')
            .leftJoinAndSelect("envoice.user", "user")
            .leftJoinAndSelect("user.role", 'role');

        // status
        if (options.status != EnvoiceStatus.WATING && options.status != EnvoiceStatus.COMPLETE && options.status != EnvoiceStatus.CANCEL)
            throw {message: 'status is not exits'}
        query.where('envoice.status = :status', {status: options.status});

        // type (1 - USER ORDER , 2 - ADMIN ADD)
        if (options.type){
            if (options.type != EnvoiceType.ADMIN_ADD && options.type != EnvoiceType.USER_ORDER)
                throw {message: 'type is not exist'}
            if (options.type == EnvoiceType.ADMIN_ADD){
                query.where('role.id != :userRole', {userRole: RoleDefault.USER});
            }else{
                query.where('role.id = :userRole', {userRole: RoleDefault.USER});
            }
        }

        // name
        if (options.name) {
            // using sub query || nested query
            query.andWhere('envoice.code like :name OR user.phone like :name OR user.first_name like :name', {name: '%' + options.name + '%'});
            
        }

        // date
        if (options.date_from && options.date_to) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(EnvoicesEntity, 'envoice')
                    .where('envoice.created_at >= :dateFrom AND envoice.created_at <= :dateTo', {dateFrom: options.date_from + " 00:00:00", dateTo: options.date_to + " 23:59:59"})
                    .getQuery();
                return 'envoice.id IN ' + subQuery;
            });
        } else if (options.date_from){
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(EnvoicesEntity, 'envoice')
                    .where('envoice.created_at >= :dateFrom', {dateFrom: options.date_from + " 00:00:00"})
                    .getQuery();
                return 'envoice.id IN ' + subQuery;
            });
        } else if (options.date_to){
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(EnvoicesEntity, 'envoice')
                    .where('envoice.created_at <= :dateTo', {dateTo: options.date_to + " 23:59:59"})
                    .getQuery();
                return 'envoice.id IN ' + subQuery;
            });
        }

        let count_query = query.getCount();
        let data_query = query.orderBy('envoice.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * Get discount detail by id
     * @param id 
     * @param token_decoded 
     */
    async getDetail(id: number, token_decoded: any){
        
        const data = await this.envoiceRepository.createQueryBuilder('envoice')
                            .where('envoice.id = :id', {id: id})
                            .leftJoinAndSelect('envoice.user', 'user')
                            .leftJoinAndSelect('envoice.envoiceDetails', 'envoiceDetails')
                            .leftJoinAndSelect('envoiceDetails.product','product')
                            .getOne();
        
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            if (token_decoded.role == RoleDefault.USER){
                if (data.user.uuid.toString() != token_decoded.user_id){
                    throw {message: 'Forbidden', status: 403};
                }
            }else{
                const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
                const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE});
                if (index == -1){
                    throw {message: 'Forbidden', status: 403};
                }
            }
        }

        return data;
    }

    /**
     * Get discount detail by id
     * @param id 
     * @param token_decoded 
     */
    async getAmount(options: any, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE || o.id == Screens.RECOMEND_SYSTEM});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check date
        if(moment(options.date_to) < moment(options.date_from)){
            throw {message: 'Định dạng ngày không đúng'}
        }
    
        const data = await this.envoiceRepository.createQueryBuilder('envoice')
            .where('envoice.created_at >= :dateFrom AND envoice.created_at <= :dateTo', {dateFrom: options.date_from + " 00:00:00", dateTo: options.date_to + " 23:59:59"})
            .andWhere('envoice.status = :status', { status: EnvoiceStatus.COMPLETE})
            .getCount();
        
        return data;
    }

    /**
     * Get list envoice of user by userid
     * @param userId 
     * @param token_decoded 
     */
    async getListEnvoiceOfUser(uuid: string, token_decoded: any){
        const user = await this.usersService.findOneUserActiveById(uuid);
        
        if( !user)
            throw {message: 'user is not exist or block'}
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            if (token_decoded.role == RoleDefault.USER){
                if (user.uuid.toString() != token_decoded.user_id){
                    throw {message: 'Forbidden', status: 403};
                }
            }else{
                const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
                const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE});
                if (index == -1){
                    throw {message: 'Forbidden', status: 403};
                }
            }
        }

        return this.envoiceRepository.createQueryBuilder('envoice')
                .leftJoinAndSelect('envoice.user', 'user')
                .where('user.id = :id', {id: user.id})
                .orderBy('envoice.created_at', "DESC")
                .getMany();

    }

    /**
     * update note by id
     * @param id 
     * @param body 
     * @param token_decoded 
     */
    async updateNoteEnvoice(id: number, body: UpdateNoteEnvoiceDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        let data = await this.envoiceRepository.createQueryBuilder('envoice')
                .where('envoice.id = :id',{id: id})
                .getOne();
        
        if (!data){
            throw {message: 'envoice is not exist'}
        }

        data.note = body.note;
        await this.envoiceRepository.save(data);
    }

    /**
     * set status envoice by id
     * @param id 
     * @param body 
     * @param token_decoded 
     */
    async setStatus(id: number, body: UpdateStatusEnvoiceDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.EXPORT_ENVOICE});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        let data = await this.envoiceRepository.createQueryBuilder('envoice')
                .where('envoice.id = :id',{id: id})
                .getOne();
        
        if (!data){
            throw {message: 'envoice is not exist'}
        }

        if (body.status != EnvoiceStatus.CANCEL && body.status != EnvoiceStatus.WATING && body.status != EnvoiceStatus.COMPLETE)
            throw {message: 'status is not exist'}
            
        data.status = body.status;
        await this.envoiceRepository.save(data);
    }

    /**
     * The function get envoice latest in date
     * @param currentString 
     */
    async GetEnvoiceLatestInDate(currentString: string){
        return this.envoiceRepository.createQueryBuilder('envoice')
            .where('envoice.created_at <= :dateTo and envoice.created_at >= :dateFrom', {dateTo: currentString + " 23:59:59", dateFrom: currentString + " 00:00:00"})
            .orderBy('envoice.created_at', 'DESC')
            .skip(0)
            .take(1)
            .getOne();
    }

    async getEnvoiceForHui(){
        const data = await this.envoiceRepository.createQueryBuilder('envoice')
            .where("envoice.status = :status", {status: EnvoiceStatus.COMPLETE})
            .leftJoinAndSelect("envoice.envoiceDetails", "detail")
            .leftJoinAndSelect("detail.product", "product")
            .getMany();

        let result = [];
        data.forEach(element => {
            let item: any = {};
            item.twu = element.utility;
            let product = [];
            let u = [];
            element.envoiceDetails.forEach(detail => {
                product.push(detail.product.id);
                u.push(detail.utility);
            })
            item.product = product;
            item.u = u;
            result.push(item);
        });

        return result;
        
    }
}
