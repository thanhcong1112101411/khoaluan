import { Module } from '@nestjs/common';
import { EnvoicesService } from './envoices.service';
import { EnvoicesController } from './envoices.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnvoicesEntity } from './envoices.entity';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';
import { UsersModule } from '../users/users.module';
import { ProductsModule } from '../products/products.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([EnvoicesEntity]),
    RoleGroupsModule,
    HistoryModule,
    UsersModule,
    ProductsModule,
  ],
  providers: [EnvoicesService],
  controllers: [EnvoicesController],
  exports: [
    EnvoicesService,
  ]
})
export class EnvoicesModule {}
