import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity('content-pages')
export class ContentPageEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    heading: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    title: string;

    @Column({
        type: 'text',
    })
    content: string;

}
