import { Module } from '@nestjs/common';
import { ContentPagesService } from './content-pages.service';
import { ContentPagesController } from './content-pages.controller';
import { ContentPageEntity } from './content-pages.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([ContentPageEntity]),
  ] ,
  providers: [ContentPagesService],
  controllers: [ContentPagesController]
})
export class ContentPagesModule {}
