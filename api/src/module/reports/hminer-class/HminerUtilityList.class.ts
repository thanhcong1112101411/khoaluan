import { HminerElement } from './HminerElement.class';

export class HminerUtilityList {
    item: number;  // the item
    sumNutils: number = 0;  // the sum of non-closed utilities
    sumNRutils: number = 0;  // the sum of remaining utilities
    cutils: number = 0; // closed utility
    crutils: number = 0; // closed remaining utility
    cputils: number = 0; // closed prefix utility

    elements: Array<HminerElement> = []; // the elements

    /**
     * contructor
     * @param item 
     */
    constructor(item: number){
        this.item = item;
    }

    /**
	 * Method to add an element to this utility list and update the sums at the same time.
	 */
    addElement(element: HminerElement){
        this.sumNutils += element.nutils;
        this.sumNRutils += element.nrutils;
        this.elements.push(element);
    }

    updateElement(oldElement: HminerElement, nutils: number, nrutils: number, index: number){
        this.sumNutils = this.sumNutils + nutils;
        this.sumNRutils = this.sumNRutils + nrutils;
        let newElement = oldElement;
        newElement.nutils += nutils;
        newElement.nrutils += nrutils;

        this.elements[index] = newElement;
    }

    /**
	 * Get the support of the itemset represented by this utility-list
	 * @return the support as a number of trnsactions
	 */
    getSupport(): any{
        return this.elements.length;
    }


}