export class HminerElement{
    tid: number;
    nutils: number;
    nrutils: number;
    putils: number;
    ppos: number;

    constructor(tid: number, nutils: number, nrutils: number, putils: number, ppos: number){
        this.tid = tid;
        this.nutils = nutils;
        this.nrutils = nrutils;
        this.putils = putils;
        this.ppos = ppos;
    }
}