export class Element{
    tid: number;
    iutils: number;
    rutils: number;

    constructor(tid: number, iutils: number, rutils: number){
        this.tid = tid;
        this.iutils = iutils;
        this.rutils = rutils;
    }
}