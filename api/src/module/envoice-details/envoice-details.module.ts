import { Module } from '@nestjs/common';
import { EnvoiceDetailsController } from './envoice-details.controller';
import { EnvoiceDetailsService } from './envoice-details.service';
import { EnvoiceDetailsEntity } from './envoice-details.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([EnvoiceDetailsEntity]),
  ],
  controllers: [EnvoiceDetailsController],
  providers: [EnvoiceDetailsService]
})
export class EnvoiceDetailsModule {}
