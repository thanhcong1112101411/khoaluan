import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreateEnvoiceDetailDto {

    @IsNotEmpty({ message: 'userId is required' })
    @ApiModelProperty({ required: true, example: 1 })
    userId: number;

    @ApiModelProperty({ required: true, example: '' })
    note: string;

    @IsNotEmpty({ message: 'products is require' })
    @IsNotBlank('products', { message: 'products is not white space' })
    @ApiModelProperty({ example: [{id: 1, quantity: 2}, {id: 2, quantity: 1}] })
    products: any;
}