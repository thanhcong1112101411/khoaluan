import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager, getManager } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { RoleDefault, Screens, DiscountType } from '../../utils/enum.util';
import _ from 'lodash';
import { View } from 'typeorm/schema-builder/view/View';
import { ProductsService } from '../products/products.service';
import { EnvoiceDetailsEntity } from './envoice-details.entity';
const moment = require('moment');

@Injectable()
export class EnvoiceDetailsService {
    constructor(
        @InjectRepository(EnvoiceDetailsEntity)
        private readonly envoiceDetailRepository: Repository<EnvoiceDetailsEntity>,
    ) { }

    
}
