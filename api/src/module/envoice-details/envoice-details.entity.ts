import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';
import { UsersEntity } from '../users/users.entity';
import { ScreensEntity } from '../screens/screens.entity';
import { BrandsEntity } from '../brands/brands.entity';
import { UnitsEntity } from '../units/units.entity';
import { CatagoriesEntity } from '../catagories/catagories.entity';
import { PricesEntity } from '../prices/prices.entity';
import { DiscountEntity } from '../discounts/discounts.entity';
import { DiscountView } from '../discounts/discountsView.entity';
import { ProductsEntity } from '../products/products.entity';
import { EnvoicesEntity } from '../envoices/envoices.entity';

@Entity('envoice_details')
export class EnvoiceDetailsEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'float',
    })
    amount: number;

    @Column({
        type: 'float',
    })
    exportPrice: number;

    @Column({
        type: 'int',
    })
    discount: number;

    @Column({
        type: 'float',
    })
    money: number;

    @Column({
        type: 'float',
    })
    utility: number;

    /*************************
     * Key           *
     *************************/

    @ManyToOne(type => ProductsEntity, product => product.envoiceDetails)
    product: ProductsEntity;

    @ManyToOne(type => EnvoicesEntity, envoices => envoices.envoiceDetails)
    envoice: EnvoicesEntity;
}
