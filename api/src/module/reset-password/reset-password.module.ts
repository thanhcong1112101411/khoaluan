import { Module } from '@nestjs/common';
import { ResetPasswordService } from './reset-password.service';
import { ResetPasswordController } from './reset-password.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResetPasswordEntity } from './reset-password.entity';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ResetPasswordEntity]),
    UsersModule,
  ],
  exports: [
      ResetPasswordService,
  ],
  providers: [ResetPasswordService],
  controllers: [ResetPasswordController]
})
export class ResetPasswordModule {}
