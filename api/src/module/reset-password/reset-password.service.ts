import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResetPasswordEntity } from './reset-password.entity';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { MailStatus } from '../../utils/enum.util';
import { StringHelper } from '../../utils/string.helper';
import { EmailUtil } from '../../utils/email.util';
import config from '../../../config/config';
import { PasswordDto } from './dto/password.dto';

@Injectable()
export class ResetPasswordService {
    constructor(
        @InjectRepository(ResetPasswordEntity)
        private resetPasswordRepository: Repository<ResetPasswordEntity>,
        private userService: UsersService,
    ) { }

    /**
     * Check token and email
     * @param token
     * @param email
     * @param type
     * @returns {Promise<any>}
     */
    async validateToken(token: string, email: string, type: MailStatus): Promise<any> {
        const check = await this.resetPasswordRepository.findOne({ token, email, type });
        if (!check) return false;
        return true;
    }

    /**
     * Delete row reset password when complete change password
     * @param email
     * @param type
     * @returns {Promise<any>}
     */
    async deleteResetPassword(email: string, type: MailStatus): Promise<any> {
        return await this.resetPasswordRepository.delete({ email, type });
    }

    /**
     * Send mail and token
     * @param email
     * @param type
     * @returns {Promise<any>}
     */
    async sendTokenMail(email: string, type: MailStatus): Promise<any> {

        // Check exists email only normal account (except social account)
        const check = await this.userService.findOneByEmailNormal(email);
        if (!check) throw { message: 'Email is not exsits' };

        // delete exists email in reset-password
        const checkEmail = await this.checkEmail(email, MailStatus.FORGOT_PASSWORD);
        if (checkEmail){
            await this .deleteResetPassword(email, MailStatus.FORGOT_PASSWORD);
        }

        // Generate token
        const generateToken = StringHelper.generateOtpToken();
        const resetPassword = {
            email,
            token: generateToken,
            type,
        };

        // Save to database token and email
        await this.resetPasswordRepository.save(resetPassword);

        // Encode URL and sendmail
        const encodeUrl = StringHelper.encodeBase64(JSON.stringify(resetPassword));
        let emailUtil = EmailUtil.getInstance();
        let url, message;

        if (type === MailStatus.FORGOT_PASSWORD) {
            url = config.urlRestPassword;
            message = 'Reset your new password';
        }

        if (type === MailStatus.RESEND_EMAIL) {
            url = config.urlVerify;
            message = 'Verify email';
        }
        console.log(url);
        await emailUtil.sendEmail([email], message, message, `Click link to URL: ${url}${encodeUrl}`);
    }

    /**
     * Check token and update new password
     * @param encode_token
     * @param passwordDto
     * @returns {Promise<any>}
     */
    async checkTokenAndUpdatePassword(encode_token: string, passwordDto: PasswordDto): Promise<any> {

        const { password, password_confirmation } = passwordDto;

        // Compare password confirmation and password
        if (password_confirmation !== password) throw { message: 'Password confirmation is not same' };

        // Decode and check token
        const decode = StringHelper.decodeBase64(encode_token);
        if (!decode) throw { message: 'Token is invalid' };
        const jsonParse = await JSON.parse(decode);
        const { email, token } = jsonParse;
        const check = await this.validateToken(token, email, MailStatus.FORGOT_PASSWORD);
        if (!check) throw { message: 'Token is invalid' };

        // Update password
        const user = await this.userService.findOneByEmail(email);
        if (!user) throw { message: 'Token is invalid user' };
        user.password = password;
        user.hashPassword();
        await this.userService.save(user);
        await this.deleteResetPassword(email, MailStatus.FORGOT_PASSWORD);
    }

    /**
     * Check token
     * @param encode_token
     * @param mail_type
     * @returns {Promise<any>}
     */
    async checkToken(encode_token: string, mail_type: MailStatus): Promise<any> {
        // Decode and check token
        const decode = StringHelper.decodeBase64(encode_token);
        if (!decode) throw { message: 'Token is invalid' };
        const jsonParse = await JSON.parse(decode);
        const { email, token, type } = jsonParse;
        if (mail_type !== type) throw { message: 'Token is invalid type' };
        const check = await this.validateToken(token, email, type);
        if (!check) throw { message: 'Token is not exists' };
        return email;
    }

    /**
     * Check Email
     * @param email
     * @param mail_type
     * @returns {Promise<any>}
     */
    async checkEmail(email: string, mail_type: MailStatus): Promise<boolean> {
        return !!await this.resetPasswordRepository.findOne({ email, type: mail_type });
    }
}
