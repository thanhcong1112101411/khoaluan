import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity('password_resets')
export class ResetPasswordEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '225',
    })
    token: string;

    @Index('email_reset')
    @Column({ length: 255 })
    email: string;

    @Column({
        type: 'int',
    })
    type: number;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
