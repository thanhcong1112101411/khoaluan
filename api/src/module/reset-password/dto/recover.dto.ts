import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class RecoverDto {

    @ApiModelProperty({ required: true, example: 'admin@gmail.com' })
    @IsNotEmpty({ message: 'Email is required' })
    @IsNotBlank('email', { message: 'Is not white space' })
    email: string;
}
