import { Controller, Post, Body, Res, HttpStatus, Put, Param, Get } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiImplicitParam } from '@nestjs/swagger';
import { ResetPasswordService } from './reset-password.service';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { RecoverDto } from './dto/recover.dto';
import { CustomValidationPipe } from '../../common/pipe/customValidation.pipe';
import { MailStatus } from '../../utils/enum.util';
import { dataError, dataSuccess } from '../../utils/jsonFormat';
import { Response } from 'express';
import { PasswordDto } from './dto/password.dto';

@ApiUseTags('Reset Password')
@Controller('api/reset-password')
export class ResetPasswordController {
    constructor(
        private resetPasswordService: ResetPasswordService,
    ) { }

    @Roles(RolesType.All)
    @Post()
    @ApiOperation({ title: 'Reset password' })
    @ApiResponse({ status: 200, description: 'Mail sent' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async resetPassword(@Body(CustomValidationPipe) recoverDto: RecoverDto, @Res() res: Response) {
        try {
            await this.resetPasswordService.sendTokenMail(recoverDto.email, MailStatus.FORGOT_PASSWORD);
        } catch (error) {
            return res.status(error.status || HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('Mail send complete', null));
    }

    @Roles(RolesType.All)
    @Put('change_password/:token')
    @ApiOperation({ title: 'Update new password' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    @ApiImplicitParam({ name: 'token', description: 'token email', required: true, type: 'string' })
    async changePassword(@Param('token') token: string, @Body(CustomValidationPipe) passwordDto: PasswordDto, @Res() res: Response) {
        try {
            await this.resetPasswordService.checkTokenAndUpdatePassword(token, passwordDto);
        } catch (error) {
            return res.status(error.status || HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('Change password is completed', null));
    }

    @Roles(RolesType.All)
    @Get('create-new-password/:token')
    @ApiOperation({ title: 'Check token' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    @ApiImplicitParam({ name: 'token', description: 'token email', required: true, type: 'string' })
    async checkToken(@Param('token') token: string, @Res() res: Response) {
        try {
            await this.resetPasswordService.checkToken(token, MailStatus.FORGOT_PASSWORD);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError('Token is invalid', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('oK', null));
    }
}
