import { Module } from '@nestjs/common';
import { UploadImagesController } from './upload-images.controller';

@Module({
  controllers: [UploadImagesController],
})
export class UploadImagesModule {}
