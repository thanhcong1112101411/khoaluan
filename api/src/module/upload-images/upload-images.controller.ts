import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get, UseInterceptors, UploadedFiles, Req, HttpException, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam, ApiImplicitFile, ApiBearerAuth } from '@nestjs/swagger';
import {FilesInterceptor} from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { dataSuccess, dataError } from '../../utils/jsonFormat';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import {extname, join} from "path";
import * as fs from 'fs';
import {diskStorage} from 'multer';
import config from '../../../config/config';
import * as jwt from 'jsonwebtoken';
import { AuthGuard } from '@nestjs/passport';
import { RoleDefault } from '../../utils/enum.util';

@ApiUseTags('upload-images')
@Controller('api/upload-images')
export class UploadImagesController {

    @Roles(RolesType.All)
    @Post('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'upload image ( ADMIN )' })
    @ApiImplicitFile({name: 'images', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images', 20,
        {
            limits: {
                fileSize: 3000000,
            },
            fileFilter: (req, images, cb) => {
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'upload');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'upload');
                    let imageName = images.originalname.toLowerCase();
                    if (fs.existsSync(parentPath + "/" + imageName)) {
                        imageName = imageName.replace(extname(images.originalname).toLowerCase(),"") + "-1" + extname(images.originalname).toLowerCase();
                    }
                    imageName = imageName.replace("(", "-");
                    imageName = imageName.replace(")", "");
                    return cb(null, imageName);
                },
            }),
        },
    ))
    async upload(@Req() request: Request, @UploadedFiles() images, @Res() res: Response) {
        // check role
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded:any = jwt.decode(token);

        // check role
        if (token_decoded.role == RoleDefault.USER){
            throw {message: 'Forbidden', status: 403};
        }

        // validate
        if (typeof images === 'undefined' || images.length == 0)
            return res.status(HttpStatus.OK).send(dataError('File is require', null));

        const image_link = config.uploadConfig.FILE_UPLOAD_PATH.replace('public/', '') + '/upload/' + images[0].filename;
        
        try {
            return res.status(HttpStatus.CREATED).send(dataSuccess('upload image', image_link));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

}
