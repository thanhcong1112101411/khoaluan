import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { HistoryEntity } from './history.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from '../users/users.service';
import { RoleDefault, Screens } from '../../utils/enum.util';
import { RoleGroupsService } from '../role-groups/role-groups.service';

import _ from 'lodash';

@Injectable()
export class HistoryService {
    constructor(
        @Inject(forwardRef(() => UsersService))
        private usersService: UsersService,
        @InjectRepository(HistoryEntity)
        private readonly historyRepository: Repository<HistoryEntity>,
        @Inject(forwardRef(()=> RoleGroupsService))
        private roleGroupsService: RoleGroupsService
    ){}

    /**
     * insert history
     * @param uuid 
     * @param table 
     * @param action 
     */
    async insertHistory(uuid: string, table: string, action: string){
        const user = await this.usersService.findOneUserActiveById(uuid);
        if (user.role.id != RoleDefault.USER){
            let history = new HistoryEntity();

            history.user = user;
            history.action = action;
            history.table = table;

            await this.historyRepository.save(history);
        }
    }

    /**
     * This function get all items (transaction) by options and role
     * @param options (last_id, limit, name)
     * @param token_decoded (user_id, role, email)
     * @returns object {count, list_user, load_more}
     */
    async findAll(options: any, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupsService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.HISTORY; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.historyRepository.createQueryBuilder('history')
            .leftJoinAndSelect('history.user', 'user');

        // name
        if (options.name) {
            // using sub query || nested query
            query.where('history.table like :name or user.first_name like :name or user.last_name like :name', {name: '%' + options.name + '%'});
        }

        // count list users
        let count_query = query.getCount();

        // list users
        let data_query = query.orderBy('history.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();


        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }
}
