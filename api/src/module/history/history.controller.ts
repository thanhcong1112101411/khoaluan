import { Controller, Get, UseGuards, Req, Res, HttpStatus } from '@nestjs/common';
import { HistoryService } from './history.service';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiImplicitQuery, ApiUseTags } from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken';
import { dataError, getPaginationData } from '../../utils/jsonFormat';
import { Response } from 'express';
import { UserUtil } from '../../utils/user.util';

@ApiUseTags('History')
@Controller('api/history')
export class HistoryController {
    constructor(
        private readonly historyService: HistoryService,
    ) { }

    @Roles(RolesType.All)
    @Get('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List history - ROOT | History management'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name', required: false, type: 'string'})
    async listHistory(@Req() request, @Res() resSwagger: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        let limit, current_page;

        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return resSwagger.status(HttpStatus.BAD_REQUEST).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
        };

        try {
            let data_results = await this.historyService.findAll(options, token_decoded);
            let data = [];
            for (let item of data_results.data){
                let user = UserUtil.serialize(item.user);
                item.user = user;
                data.push(item);
            }
            return resSwagger.status(HttpStatus.OK).send(getPaginationData('OK', data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return resSwagger.status(error.status || HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
