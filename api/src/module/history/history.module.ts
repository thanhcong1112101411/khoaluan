import { Module, forwardRef } from '@nestjs/common';
import { HistoryService } from './history.service';
import { HistoryController } from './history.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistoryEntity } from './history.entity';
import { UsersModule } from '../users/users.module';
import { RoleGroupsModule } from '../role-groups/role-groups.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([HistoryEntity]),
      forwardRef(() => UsersModule),
      forwardRef(() => RoleGroupsModule),
    ] ,
    providers: [HistoryService],
    controllers: [HistoryController],
    exports: [
      HistoryService,
    ],
})
export class HistoryModule {}
