import { Module, HttpModule } from '@nestjs/common';
import { GeneralApiController } from './general-api.controller';
import { GeneralApiService } from './general-api.service';

@Module({
  imports: [HttpModule],
  controllers: [GeneralApiController],
  providers: [GeneralApiService]
})
export class GeneralApiModule {}
