import { Controller, Get, Res, HttpStatus, Param } from '@nestjs/common';
import { ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { GeneralApiService } from './general-api.service';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { dataSuccess, dataError } from '../../utils/jsonFormat';
import { Response } from 'express';

@ApiUseTags('General API')
@Controller('api/general-api')
export class GeneralApiController {
    constructor(
        private readonly generalApiService : GeneralApiService,
    ) { }

    @Roles(RolesType.All)
    @Get('city')
    @ApiOperation({ title: 'get city' })
    async getCity(@Res() res: Response) {
        try {
            const result = await this.generalApiService.getCity();
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result.LtsItem));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('district/:id')
    @ApiOperation({ title: 'get district' })
    async getDistrict(@Param('id') id: number, @Res() res: Response) {
        try {
            const result = await this.generalApiService.getDistrict(id);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('ward/:id')
    @ApiOperation({ title: 'get ward' })
    async getWard(@Param('id') id: number, @Res() res: Response) {
        try {
            const result = await this.generalApiService.getWard(id);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('country')
    @ApiOperation({ title: 'get country' })
    async getCountry(@Res() res: Response) {
        try {
            const result = await this.generalApiService.getCountry();
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
