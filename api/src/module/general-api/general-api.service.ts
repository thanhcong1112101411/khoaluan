import { Injectable, HttpService } from '@nestjs/common';
import { Response } from 'express';

@Injectable()
export class GeneralApiService {
    constructor(private httpService: HttpService) {}

    async getCity(): Promise<any> {
        const data = await this.httpService.get("https://thongtindoanhnghiep.co/api/city").toPromise();
        return data.data;
    }

    async getDistrict(id: number): Promise<any> {
        const data = await this.httpService.get("https://thongtindoanhnghiep.co/api/city/"+id+"/district").toPromise();
        return data.data;
    }

    async getWard(id: number): Promise<any> {
        const data = await this.httpService.get("https://thongtindoanhnghiep.co/api/district/"+id+"/ward").toPromise();
        return data.data;
    }

    async getCountry(): Promise<any> {
        const data = await this.httpService.get("https://restcountries.eu/rest/v2/all").toPromise();
        return data.data;
    }
}
