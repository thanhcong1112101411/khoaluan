import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards } from '@nestjs/common';
import {
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { UsersService } from '../../module/users/users.service';
import { CustomValidationPipe } from '../../common/pipe/customValidation.pipe';
import * as jwt from 'jsonwebtoken';
import { classToClass } from 'class-transformer';
import { UserUtil } from '../../utils/user.util';
import { CreateUserDto } from './dto/createUser.dto';
import { UpdateUserDto } from './dto/updateUser.dto';
import { PasswordDto } from '../auth/dto/password.dto';
import { UserType } from 'src/utils/enum.util';
import { UserStatus } from '../../utils/enum.util';
const _ = require('lodash');

@ApiUseTags('Users')
@Controller('api/users')
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
    ) { }

    @Roles(RolesType.All)
    @Get('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List Users - Root & User Management & account management'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'email', description: 'Email', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'user_type', description: '1-ADMIN  2-USER', required: false, type: Number})
    @ApiImplicitQuery({name: 'status', description: '1-ACTIVE , 2-INACTIVE', required: false, type: Number})
    async getUsers(@Req() request, @Res() resSwagger: Response) {
        let list_users = [];
        let limit, current_page, user_type, status;

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.user_type) {
            user_type = Number(request.query.user_type);
            if (isNaN(user_type)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('user type should be a number', null));
            }
        }

        if (request.query.status) {
            status = Number(request.query.status);
            if (isNaN(status)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('status should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
            email: request.query.email,
            user_type,
            status,
        };

        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            let data_results = await this.usersService.findAll(options, token_decoded);
            for (let user of data_results.data) {
                const role = user.role;
                let userItem = UserUtil.serialize(user);
                userItem.role = role;
                list_users.push(userItem);
            }
            return resSwagger.status(HttpStatus.OK).send(getPaginationData('OK', list_users,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (e) {
            return resSwagger.status(HttpStatus.OK).send(dataError(e.message || 'Error', null));
        }
    }

    @Roles(RolesType.All)
    @Post('user-register')
    @ApiOperation({ title: 'User register - All' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async userRegister(@Body(CustomValidationPipe) body: CreateUserDto, @Res() res: Response) {
        try {
            const user = await this.usersService.register(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Đăng ký thành công', UserUtil.serialize(user)));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Post('admin-register')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Admin register - root | account management ' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async adminRegister(@Req() request, @Body(CustomValidationPipe) body: CreateUserDto, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const user = await this.usersService.register(body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Đăng ký thành công', UserUtil.serialize(user)));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put(':uuid')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Update user - owner | root | account management | user management'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async userUpdate(@Param('uuid') uuid: string, @Body(CustomValidationPipe) body: UpdateUserDto, @Res() res: Response, @Req() request){
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const user = await  this.usersService.updateUser(uuid, body, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('Câp nhật tài khoản thành công', UserUtil.serialize(user)));
        } catch  (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('change-password/:uuid')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update new password - owner | root | account management | user management' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async changePassword(@Param('uuid') uuid: string, @Body(CustomValidationPipe) passwordDto: PasswordDto, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            await this.usersService.updatePassword(uuid, passwordDto, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('thay đổi mật khẩu thành công', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get(':uuid')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get user detail - Owner | root | account management | user management' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async getDetail(@Param('uuid') uuid: string, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            let user = await this.usersService.getDetail(uuid, token_decoded);
            const role = user.role;
            user = UserUtil.serialize(user);
            user.role = role;

            return res.status(HttpStatus.OK).send(dataSuccess('Ok', user));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('set-status-user/:uuid')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'enable/disable user - Root | user management | account management'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async setStatusUser(@Param('uuid') uuid: string, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            let user;
            user = await this.usersService.setStatus(token_decoded, uuid);
            console.log(user);
            if (user.status == UserStatus.ACTIVE){
                return res.status(HttpStatus.OK).send(dataSuccess('Đã mở tài khoản', null));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Đã khóa tài khoản', null));
            
        } catch (error){
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
    
    @Roles(RolesType.All)
    @Put('change-role/:uuid')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'change role - Root | DECENTRALIZATION MANAGEMENT'})
    @ApiImplicitQuery({name: 'role', description: '', required: true, type: Number})
    async changeRole(@Param('uuid') uuid: string, @Res() res: Response, @Req() request){
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try{
            const data = await this.usersService.updateRole(uuid, request.query.role, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess("Thay đổi nhóm quyền thành công",data));
        }catch(e){
            return res.status(HttpStatus.OK).send(dataError(e.message || 'Error', null))
        }
    }
}
