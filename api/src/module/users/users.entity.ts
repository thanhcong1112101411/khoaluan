import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';
import { RoleGroupEntity } from '../role-groups/role-groups.entity';
import { HistoryEntity } from '../history/history.entity';
import { FeedbacksEntity } from '../feedbacks/feedbacks.entity';
import { NewsEntity } from '../news/news.entity';
import { EnvoicesEntity } from '../envoices/envoices.entity';

@Entity('users')
@Unique('user_phone_country_code_unique', ['phone', 'country_code'])
export class UsersEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'binary',
        length: 36,
    })
    @Generated('uuid')
    uuid: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    avatar: string;

    @Index('user-first-name')
    @Column({
        type: 'varchar',
        length: '255',
    })
    first_name: string;

    @Index('user-last-name')
    @Column({
        type: 'varchar',
        length: '255',
    })
    last_name: string;

    @Index('user_email_unique', { unique: true })
    @Column({ length: 255 })
    email: string;

    @Exclude()
    @Column({ length: 255 })
    password: string;

    @Index('user_phone')
    @Column()
    phone: string;

    @Column()
    country_code: number;

    @Column({
        nullable: true,
    })
    bod: Date;

    @Column({
        type: 'smallint',
        default: 1,
    })
    gender: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    fb_id: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    gg_id: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    address: string;

    @Column({
        type: 'int',
    })
    ward_id: number;

    @Column({
        type: 'int',
    })
    district_id: number;

    @Column({
        type: 'int',
    })
    city_id: number;

    @Column({
        type: 'smallint',
        default: 1,
    })
    status: number;

    /*************************
     * Key           *
     *************************/

    @ManyToOne(type => RoleGroupEntity, role => role.users)
    role: RoleGroupEntity;

    @OneToMany(type => HistoryEntity, history => history.user)
    history: HistoryEntity;

    @OneToMany(type => FeedbacksEntity, feedback => feedback.user)
    feedback: FeedbacksEntity;

    @OneToMany(type => NewsEntity, news => news.user)
    news: NewsEntity;

    @OneToMany(type => EnvoicesEntity, envoice => envoice.user)
    envoices: EnvoicesEntity[];

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    /*
    * This property is used to get full phone number of user
    * */
    get full_phone_number() {
        return '+' + this.country_code + this.phone;
    }

    /*
    * This property is used to get user_id with string type
    * */
    get user_id_str() {
        return this.uuid.toString();
    }

    /*
    * This property is used to get full name of user
    * */
    get full_name() {
        return (this.first_name + ' ' + this.last_name).trim();
    }

    /*
     * This function check valid user
     */
    get is_valid() {
        return !!this.status;
    }

    /*
     * This function has password
     */
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8);
    }

    /*
     * This function compare password
     */
    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }
}
