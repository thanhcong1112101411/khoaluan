import { Injectable, Inject, forwardRef, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersEntity } from './users.entity';
import { Repository, getManager } from 'typeorm';
import { UserStatus, UserType, Screens, RoleDefault, SocialType, AccountDefault } from '../../utils/enum.util';
import { PhoneNumberUtil } from '../../utils/phone-number.util';
import { UpdateUserDto } from './dto/updateUser.dto';
import { PasswordDto } from '../auth/dto/password.dto';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { CreateUserDto } from './dto/createUser.dto';
import { LoginDto } from '../auth/dto/login.dto';
import config from '../../../config/config';
import { HistoryService } from '../history/history.service';
import { LoginSocialDto } from '../auth/dto/loginSocial.dto';

const moment = require('moment');
const _ = require('lodash');

@Injectable()
export class UsersService {
    constructor(
        @Inject(forwardRef(() => HistoryService))
        private historyService: HistoryService,
        @InjectRepository(UsersEntity)
        private readonly usersRepository: Repository<UsersEntity>,
        private readonly roleGroupService: RoleGroupsService,
    ) { }

    /**
     * This is function using search user id by uuid or id
     * @param user_id 
     */
    async findOneUserById(uuid: number | string): Promise<UsersEntity> {
        let query = this.usersRepository.createQueryBuilder('user').leftJoinAndSelect('user.role','role');
        if (typeof uuid === 'string') {
            query.where('user.uuid = :uuid', { uuid });
        }
        if (typeof uuid === 'number') {
            query.where('user.id = :uuid', { uuid });
        }
        return await query.getOne();
    }

    /**
     * This is function using search user id by uuid or id ACTIVE
     * @param user_id 
     */
    async findOneUserActiveById(uuid: number | string): Promise<UsersEntity> {
        
        let query = this.usersRepository.createQueryBuilder('user')
            .leftJoinAndSelect('user.role','role')
            .where('user.status = :status', {status: UserStatus.ACTIVE});
        if (typeof uuid === 'string') {
            query.where('user.uuid = :uuid', { uuid });
        }
        if (typeof uuid === 'number') {
            query.where('user.id = :uuid', { uuid });
        }
        return await query.getOne();
    }


    /**
     * the function find admin account active by id or uuid
     * @param uuid 
     */
    async findOneAdminAccountActiveById(uuid: number | string): Promise<UsersEntity> {
        let query = this.usersRepository.createQueryBuilder('user')
            .leftJoinAndSelect('user.role','role')
            .where('user.status = :status', {status: UserStatus.ACTIVE})
            .andWhere('rold id != :userRole', {userRole: RoleDefault.USER});
        if (typeof uuid === 'string') {
            query.where('user.uuid = :uuid', { uuid });
        }
        if (typeof uuid === 'number') {
            query.where('user.id = :uuid', { uuid });
        }
        return await query.getOne();
    }


    /**
     * This function check exist user by email or phone number
     * @param email
     * @param phone
     * @param country_code
     */
    async isExistUser(email: string, phone: string, country_code: number): Promise<boolean> {
        let user = await this.usersRepository.createQueryBuilder('user')
            .where('email = :email', {email})
            .orWhere('phone = :phone AND country_code = :country_code', {phone, country_code})
            .getOne();

        return !!user;
    }

    /**
     * This function check exist user by phone number
     * @param phone
     * @param country_code
     */
    async isExistPhone(phone: string, country_code: number): Promise<boolean> {
        let user = await this.usersRepository.createQueryBuilder('user')
            .where('phone = :phone AND country_code = :country_code', {phone, country_code})
            .getOne();

        return !!user;
    }

    /**
     * This function get user by email
     * @param email
     */
    async findOneByEmail(email: string): Promise<UsersEntity> {
        return this.usersRepository.findOne({email});
    }

    /**
     * This function get user by email except email social
     * @param email
     */
    async findOneByEmailNormal(email: string): Promise<UsersEntity> {
        return this.usersRepository.createQueryBuilder('user')
            .where('email = :email', {email: email})
            .andWhere('status = :status', {status: UserStatus.ACTIVE})
            .andWhere('fb_id is null and gg_id is null')
            .getOne();
    }

    /**
     * This function login with email
     * @param body
     */
    async login(body: LoginDto): Promise<UsersEntity> {
        let query = this.usersRepository.createQueryBuilder('user')
        .leftJoinAndSelect('user.role','role')
        .where('email = :email', {email: body.username})
        .andWhere('status = :status', {status: UserStatus.ACTIVE})
        .andWhere('fb_id is null and gg_id is null');

        if (body.type == UserType.ADMIN){
            query.andWhere('role.id != :roleType', {roleType: RoleDefault.USER});
        }else if (body.type == UserType.USER) {
            query.andWhere('role.id = :roleType', {roleType: RoleDefault.USER});
        }else {
            throw {message: 'type is not exist', status: 400};
        }

        const user = await query.getOne();
        if (!user) throw {message: 'Email không tồn tại', status: 400};

        // check password
        if (!user.checkIfUnencryptedPasswordIsValid(body.password)){
            throw {message: 'Mật khẩu không chính xác', status: 400};
        }

        // save history
        if (body.type == UserType.ADMIN){
            await this.historyService.insertHistory(
                user.uuid.toString(),
                'Tài khoản quản trị',
                'Đăng nhâp tài khoản "' + user.email + '"',
            );
        }
        
        return user;
    }

    /**
     * 
     * @param options
     *  @param token_decoded (user_id, email,role)
     */

    async findAll(options: any, token_decoded: any) {
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.USER || o.id == Screens.ACCOUNTS || o.id == Screens.DECENTRALIZATION ; });
            if(index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        // return this.usersRepository.find();
        let query = this.usersRepository.createQueryBuilder('user')
            .leftJoinAndSelect('user.role', 'role');

        // name
        if (options.name) {
            // using sub query || nested query
            query.where(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(UsersEntity, 'user')
                    .where('first_name like :name OR last_name like :name', {name: '%' + options.name + '%'})
                    .getQuery();
                return 'user.id IN ' + subQuery;
            });
        }

        // email and name
        if (options.name && options.email){
            // using sub query || nested query
            query.where(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(UsersEntity, 'user')
                    .where('first_name like :name OR last_name like :name OR email like :email', {name: '%' + options.name + '%', email: '%' + options.email + '%'})
                    .getQuery();
                return 'user.id IN ' + subQuery;
            });
        }

        // email
        else if (options.email){
            // using sub query || nested query
            query.where(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(UsersEntity, 'user')
                    .where('email like :email', {email: '%' + options.email + '%'})
                    .getQuery();
                return 'user.id IN ' + subQuery;
            });
        }

        // user type
        if (options.user_type){
            if (options.user_type == UserType.ADMIN){
                query.andWhere('role.id != :role', {role: UserType.USER});
            }else if (options.user_type == UserType.USER){
                query.andWhere('role.id = :role', {role: UserType.USER});
            }else{
                throw {message: 'user type is not exists', status: 400};
            }
            
        }

        // user status
        if (options.status){
            if (options.status == UserStatus.ACTIVE){
                query.andWhere('user.status = :status', {status: UserStatus.ACTIVE});
            }else if(options.status == UserStatus.INACTIVE){
                query.andWhere('user.status = :status', {status: UserStatus.INACTIVE});
            }else{
                throw {message: 'user status is not exists', status: 400};
            }
        }

        // count list users
        let count_query = query.getCount();

        // list users
        let data_query = query.orderBy('user.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * This function register user
     * @param body
     */
    async register(body: CreateUserDto, token_decoded?: any): Promise<UsersEntity> {
        //check role
        if (token_decoded){
            if (token_decoded.role != RoleDefault.ROOT){
                const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
                const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ACCOUNTS; });
                if (index == -1){
                    throw {message: 'Forbidden', status: 403};
                }
            }
        }else{
            if (body.role != RoleDefault.USER){
                throw {message: 'role is not exists', status: 403};
            }
        }
        
        // validate phone number
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, body.country_code);
        if (!data_phone_number) throw {message: 'Số điện thoại không chính xác', status: 400};
        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();

        // Check user exists
        let isExistUser = await this.isExistUser(body.email, body.phone, body.country_code);
        if (isExistUser) throw {message: 'Tài khoản này đã tồn tại', status: 409};

        let user = new UsersEntity();
        user.email = body.email;
        user.country_code = body.country_code;
        user.phone = body.phone;
        user.password = body.password;
        user.gender = 1;
        user.first_name = body.name;

        const roleInf = await this.roleGroupService.getRoleGroupInf(body.role);
        if (!roleInf) throw {message: 'role group is not exist', status: 400};

        user.role = roleInf;

        user.hashPassword();
        user.status = UserStatus.ACTIVE;
        user = await this.usersRepository.save(user);

        // save history
        if (token_decoded){
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'users',
                'register account "' + user.email + '"',
            )
        }

        return user;
    }

    /**
     * This function update user
     * @param token_decoded (role, email, user_id)
     * @param body
     * @param uuid
     * Return updateUser
     */
    async updateUser(uuid: string, body: UpdateUserDto, token_decoded: any){

        // check role
        if (token_decoded.role != RoleDefault.ROOT && token_decoded.user_id !== uuid){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ACCOUNTS || o.id == Screens.USER; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // get user by uuid
        let user = await this.findOneUserById(uuid);
        if (!user) throw {message: 'User does not exist in system', status: 400};

        // validate phone number
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone.trim(), body.country_code.toString().trim());
        if (!data_phone_number) throw {message: 'Số điện thoại không chính xác', status: 400};
      
        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();
        if (user.phone && user.country_code){
            if (user.country_code.toString().trim() !== body.country_code.toString().trim() || user.phone.trim() !== body.phone.trim()){
                if (await this.isExistPhone(body.phone.trim(), body.country_code))
                    throw {message: 'Số điện thoại đã tồn tại', status: 400};
            }
        }
        
        // validate email
        if (user.email !== body.email.trim()) {
            if (await this.findOneByEmail(body.email.trim()))
                throw {message: 'Email đã tồn tại', status: 400};
        }

        user = await this.usersRepository.merge(user, body);
        user.first_name = body.name;

        // save history
        if (user.role.id == RoleDefault.USER){
            await this.historyService.insertHistory(
                token_decoded.uuid,
                'tài khoản người dùng',
                'cập nhật tài khoản "' + user.email + '"',
            )
        }else {
            await this.historyService.insertHistory(
                token_decoded.uuid,
                'tài khoản quản trị viên',
                'cập nhật tài khoản "' + user.email + '"',
            )
        }
        
        return this.usersRepository.save(user);
    }

    /**
     * update new password
     * @param uuid
     * @param passwordDto
     * @returns {Promise<any>}
     */
    async updatePassword(uuid: string, passwordDto: PasswordDto, token_decoded: any): Promise<any> {

        // check role
        if (token_decoded.role != RoleDefault.ROOT && token_decoded.user_id !== uuid){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ACCOUNTS || o.id == Screens.USER; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // get user by uuid
        let user = await this.findOneUserById(uuid);
        if (!user) throw {message: 'User does not exist in system', status: 400};

        const { password, password_confirmation } = passwordDto;

        // Compare password confirmation and password
        if (password_confirmation !== password) throw { message: 'Mật khẩu xác nhận không đúng' };

        // Update password
        user.password = password;
        user.hashPassword();
        await this.usersRepository.save(user);

        // save history
        if (user.role.id == RoleDefault.USER){
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'Tài khoản khách hàng',
                'Thay đổi mật khẩu tài khoản "' + user.email + '"',
            )
        }else {
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'Tài khoản quản trị viên',
                'Thay đổi mật khẩu tài khoản "' + user.email + '"',
            )
        }
    }

    /**
     * get user detail
     * @param uuid
     * @param token_decoded (user_id, email,role)
     * @returns {Promise<any>}
     */
    async getDetail(uuid: string, token_decoded: any): Promise<any> {

        // check role
        if (token_decoded.role != RoleDefault.ROOT && token_decoded.user_id !== uuid){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ACCOUNTS || o.id == Screens.USER; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }
        // get user by uuid
        let user;
        user = await this.findOneUserById(uuid);
        if (!user) throw {message: 'User does not exist in system', status: 400};

        // get address
        if (!isNaN(user.ward_id)){
            let address = user.address + ", " + await this.getAddressByWardId(user.ward_id);
            user.fullAddress = address;
        }
        return user;
    }

    /**
     * This function enable, disable user by body and role
     * @param  uuid, token_decoded (user_id, role, email)
     * 
     */
    async setStatus(token_decoded: any, uuid: string) {
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ACCOUNTS || o.id == Screens.USER; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check user exist
        let user = await  this.findOneUserById(uuid);
        if (!user) throw {message: 'Tài khoản này không tồn tại', status: 400};

        // check account default
        if (user.id == AccountDefault)
            throw {message: 'Đây là tài khoản mặc định, không thể khóa !'}

        // set status user
        user.status = user.status === UserStatus.ACTIVE ? UserStatus.INACTIVE : UserStatus.ACTIVE;

        // save history
        if (user.role.id == RoleDefault.USER){
            await this.historyService.insertHistory(
                token_decoded.uuid,
                'Tài khoản khách hàng',
                'Cập nhật trạng thái tài khoản "' + user.email + '"',
            )
        } else{
            await this.historyService.insertHistory(
                token_decoded.uuid,
                'Tài khoản quản trị viên',
                'Cập nhật trạng thái tài khoản "' + user.email + '"',
            )
        }
        

        return this.usersRepository.save(user);
    }

    /**
     * This function enable, disable user by body an
     * @param  uuid, 
     * @param token_decoded (user_id, role, email)
     * @param role
     */
    async updateRole(uuid: string, role: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.DECENTRALIZATION; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        if (uuid == token_decoded.user_id && token_decoded.role != RoleDefault.ROOT){
            throw {message: 'Bạn không thể cập nhật quyền cho tài khoản của mình'};
        }

        let user = await  this.findOneAdminAccountActiveById(uuid);
        if (!user)
            throw {message: 'account is not exists in system'}
            
        if (user.id == AccountDefault)
            throw {message: 'Đây là tài khoản mặc định, không thể thay đổi nhóm quyền'}

        const roleInf = await this.roleGroupService.getRoleGroupInf(role);
        if (!roleInf) 
            throw {message: 'role group is not exits', status: 400};

        user.role = roleInf;
        await this.usersRepository.save(user);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Tài khoản quản trị viên',
            'Cập nhật nhóm quyền tài khoản "' + user.email + '"',
        )
    }

    /**
     * This is function get user social by access token
     * @param access_token
     * @param social_type
     */
    async findUserByAccessToken(access_token: string, social_type: number): Promise<any> {
        const httpService = new HttpService();
        if (![SocialType.FB, SocialType.GG].includes(social_type)) throw { message: 'Social type incorrect', status: 400 };

        try {
            return social_type === SocialType.FB ? await httpService.get(config.apiCheckTokenFacebook + access_token).toPromise()
                : await httpService.get(config.apiCheckTokenGoogle + access_token).toPromise();
        } catch (e) {
            throw { message: e.message || 'Access token invalid', status: e.status || 400 };
        }
    }

    /**
     * This function get user by GG ID or FB ID
     * @param app_id
     * @param type
     */
    async getProfileByAppid(app_id: string, type: number): Promise<UsersEntity> {
        return type === SocialType.FB ? await this.usersRepository.findOne({fb_id: app_id}, {relations: ['role']})
            : await this.usersRepository.findOne({gg_id: app_id});
    }

    /**
     * This function update user
     * @param user
     */
    async save(user: UsersEntity): Promise<UsersEntity> {
        return this.usersRepository.save(user);
    }

    async checkUser(token_decoded: any, uuid: string): Promise<UsersEntity>{
        if(token_decoded.user_id != uuid){
            throw {message: "data error"};
        }
        const user = await this.findOneUserById(uuid);
        return user;
    }

    /**
     * This function register user
     * @param body
     */
    async registerSocial(body: LoginSocialDto): Promise<UsersEntity> {

        let user = new UsersEntity();
        user.email = body.email;
        user.password = '';
        user.first_name = body.name;
        if (body.type == SocialType.FB){
            user.fb_id = body.id;
        }else if (body.type == SocialType.GG){
            user.gg_id = body.id
        } else{
            throw {message: "type error"}
        }

        const roleInf = await this.roleGroupService.getRoleGroupInf(RoleDefault.USER);
        if (!roleInf) throw {message: 'role group is not exist', status: 400};

        user.role = roleInf;

        user.status = UserStatus.ACTIVE;
        user = await this.usersRepository.save(user);

        return user;
    }

    /**
     * get address by wardID
     * @param wardId 
     */
    async getAddressByWardId(wardId: number){
        const httpService = new HttpService();
        const data = await httpService.get("https://thongtindoanhnghiep.co/api/ward/" + wardId).toPromise();
        return data.data.Title + ", " + data.data.QuanHuyenTitle + ", " + data.data.TinhThanhTitle;
    }
}
