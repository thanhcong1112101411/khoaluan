import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class UpdateUserDto {

    @IsNotEmpty({ message: 'name is require' })
    @IsNotBlank('name', { message: 'first name is not white space' })
    @ApiModelProperty({ example: 'Alex' })
    name: string;

    @IsNotEmpty({ message: 'Email is require' })
    @IsNotBlank('email', { message: 'Email Is not white space' })
    @ApiModelProperty({ example: 'admin@gmail.com' })
    email: string;

    @IsNotEmpty({ message: 'phone number is require' })
    @IsNotBlank('phone number', { message: 'Phone number is not white space' })
    @ApiModelProperty({ example: '01229125354' })
    phone: string;

    @IsNotEmpty({ message: 'country code is require' })
    @ApiModelProperty({ example: 84 })
    country_code: number;
    
    @IsNotEmpty({ message: 'address is require' })
    @IsNotBlank('address', { message: 'address is not white space' })
    @ApiModelProperty({ example: 'John' })
    address: string;

    @IsNotEmpty({ message: 'birthdate is require' })
    @IsNotBlank('birthdate', { message: 'birthdate is not white space' })
    @ApiModelProperty({ example: '1991-01-01' })
    bod: string;

    @IsNotEmpty({ message: 'ward is require' })
    @IsInt()
    @ApiModelProperty({ example: 328 })
    ward: number;

    @IsNotEmpty({ message: 'district is require' })
    @IsInt()
    @ApiModelProperty({ example: 79 })
    district: number;

    @IsNotEmpty({ message: 'city is require' })
    @IsInt()
    @ApiModelProperty({ example: 4 })
    city: number;

    @IsNotEmpty({ message: 'gender is require' })
    @IsInt()
    @ApiModelProperty({ example: '0: other, 1: male, 2: female' })
    gender: number;
}
