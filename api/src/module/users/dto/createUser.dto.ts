import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreateUserDto {

    @IsNotEmpty({ message: 'Email is require' })
    @IsNotBlank('email', { message: 'email is not white space' })
    @ApiModelProperty({ example: 'admin@gmail.com' })
    email: string;

    @IsNotEmpty({ message: 'phone number is require' })
    @IsNotBlank('phone number', { message: 'phone number is not white space' })
    @ApiModelProperty({ example: '01229125354' })
    phone: string;

    @IsNotEmpty({ message: 'country code is require' })
    @ApiModelProperty({ example: 84 })
    country_code: number;

    @IsNotEmpty({ message: 'First name is require' })
    @IsNotBlank('first_name', { message: 'first name is not white space' })
    @ApiModelProperty({ example: 'Alex' })
    name: string;

    @IsNotEmpty({ message: 'Password is require' })
    @IsNotBlank('password', { message: 'password is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @IsNotEmpty({ message: 'role is require' })
    @ApiModelProperty({
        example: '1: Root, 2: user, ...',
    })
    role: number;
}
