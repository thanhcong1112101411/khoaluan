import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersEntity } from './users.entity';
import { UsersController } from './users.controller';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([UsersEntity]),
        RoleGroupsModule,
        HistoryModule,
    ],
    exports: [
        UsersService,
    ],
    providers: [UsersService],
    controllers: [UsersController],
})
export class UsersModule { }
