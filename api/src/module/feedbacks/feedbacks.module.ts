import { Module, forwardRef } from '@nestjs/common';
import { FeedbacksController } from './feedbacks.controller';
import { FeedbacksService } from './feedbacks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeedbacksEntity } from './feedbacks.entity';
import { UsersModule } from '../users/users.module';
import { RoleGroupsModule } from '../role-groups/role-groups.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([FeedbacksEntity]),
    forwardRef(() => UsersModule),
    forwardRef(() => RoleGroupsModule),
  ] ,
  controllers: [FeedbacksController],
  providers: [FeedbacksService]
})
export class FeedbacksModule {}
