import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreateFeedbackDto {
    @IsNotEmpty({ message: 'title is required' })
    @IsNotBlank('title', { message: 'title is not white space' })
    @ApiModelProperty({ required: true, example: 'giao hàng' })
    title: string;

    @IsNotEmpty({ message: 'content is required' })
    @IsNotBlank('content', { message: 'content is not white space' })
    @ApiModelProperty({ required: true, example: 'giao hàng chậm hơn dự kiến' })
    content: string;
}
