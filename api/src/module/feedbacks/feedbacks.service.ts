import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FeedbacksEntity } from './feedbacks.entity';
import { Repository } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { RoleDefault, Screens } from '../../utils/enum.util';
import { UsersService } from '../users/users.service';
import { CreateFeedbackDto } from './dto/createFeedback.dto';

import _ from 'lodash';

@Injectable()
export class FeedbacksService {

    constructor(
        @InjectRepository(FeedbacksEntity)
        private readonly feedbackRepository: Repository<FeedbacksEntity>,
        @Inject(forwardRef(()=> RoleGroupsService))
        private roleGroupsService: RoleGroupsService,
        @Inject(forwardRef(() => UsersService))
        private usersService: UsersService,
    ){}

    /**
     * This function get all feedbacks (transaction) by options and role
     * @param options (last_id, limit, name)
     * @param token_decoded (user_id, role, email)
     * @returns object {count, list_user, load_more}
     */
    async findAll(options: any, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupsService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.FEEDBACKS; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.feedbackRepository.createQueryBuilder('feedback')
            .leftJoinAndSelect('feedback.user', 'user');

        // name
        if (options.name) {
            // using sub query || nested query
            query.where('feedback.title like :name', {name: '%' + options.name + '%'});
        }

        // count list users
        let count_query = query.getCount();

        // list users
        let data_query = query.orderBy('feedback.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();


        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * insert feedback
     * @param body
     * @param token_decoded(user_id, role, email)
     */
    async insert(body: CreateFeedbackDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.USER){
            throw {message: 'Forbidden', status: 403};
        }
        const user = await this.usersService.findOneUserActiveById(token_decoded.user_id);
        
        let feedback = new FeedbacksEntity();
        feedback.title = body.title;
        feedback.content = body.content;
        feedback.user = user;

        return this.feedbackRepository.save(feedback);
    }
}
