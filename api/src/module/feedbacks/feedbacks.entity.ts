import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';
import { UsersEntity } from '../users/users.entity';

@Entity('feedbacks')
export class FeedbacksEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    title: string;

    @Column({
        type: 'text',
    })
    content: string;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    /*************************
     * Key           *
     *************************/

    @ManyToOne(type => UsersEntity, user => user.history)
    user: UsersEntity;
}
