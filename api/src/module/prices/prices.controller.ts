import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get, UseInterceptors, UploadedFiles, Req, HttpException, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam, ApiImplicitFile, ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import {FilesInterceptor} from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import {extname, join} from "path";
import * as fs from 'fs';
import {diskStorage} from 'multer';
import config from '../../../config/config';
import * as jwt from 'jsonwebtoken';
import { AuthGuard } from '@nestjs/passport';
import { CustomValidationPipe } from '../../common/pipe/customValidation.pipe';
import { CreatePriceDto } from './dto/createPrice.dto';
import { PricesService } from './prices.service';
import { PriceType } from '../../utils/enum.util';


@ApiUseTags('prices')
@Controller('api/prices')
export class PricesController {
    constructor(
        private readonly pricesService: PricesService,
    ) { }

    @Roles(RolesType.All)
    @Post('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add price - root | price management ' })
    async addPrice(@Req() request, @Body() body: CreatePriceDto, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.pricesService.insert(body, token_decoded);
            if(data.type == PriceType.IMPORT){
                return res.status(HttpStatus.CREATED).send(dataSuccess('Thêm giá nhập thành công ', data));
            }else{
                return res.status(HttpStatus.CREATED).send(dataSuccess('Thêm giá xuất thành công', data));
            }
            
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get(':productId')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List Price of product - ROOT | PRICE MANAGEMENT'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async getListPriceProduct(@Param('productId') productId: number, @Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            let data_results = await this.pricesService.getListPriceProduct(productId, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', data_results));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
