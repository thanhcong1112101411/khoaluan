import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable, PrimaryColumn, Timestamp } from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';
import { UsersEntity } from '../users/users.entity';
import { ScreensEntity } from '../screens/screens.entity';
import { BrandsEntity } from '../brands/brands.entity';
import { UnitsEntity } from '../units/units.entity';
import { CatagoriesEntity } from '../catagories/catagories.entity';
import { ProductsEntity } from '../products/products.entity';

@Entity('prices')
@Unique('price_primary_key',['product', 'date_from', 'type'])
export class PricesEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @PrimaryColumn()
    @Column({
        type: 'timestamp',
        precision: null,
    })
    date_from: Date;

    @PrimaryColumn()
    @Column({
        type: 'smallint',
        default: 1,
    })
    type: number;

    @PrimaryColumn()
    @Column({
        type: 'double',
        default: 1,
    })
    price: number;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    /*************************
     * Key           *
     *************************/

    @ManyToOne(type => ProductsEntity, product => product.prices, {primary: true})
    @JoinColumn()
    product: ProductsEntity;
}
