import { Module, forwardRef } from '@nestjs/common';
import { PricesController } from './prices.controller';
import { PricesService } from './prices.service';
import { PricesEntity } from './prices.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistoryModule } from '../history/history.module';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { ProductsModule } from '../products/products.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([PricesEntity]),
        forwardRef(() => ProductsModule),
        HistoryModule,
        RoleGroupsModule,
    ],
    exports: [
        PricesService,
    ],
    controllers: [PricesController],
    providers: [PricesService]
})
export class PricesModule {}
