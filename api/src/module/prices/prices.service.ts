import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { RoleDefault, Screens, PriceType } from '../../utils/enum.util';
import _ from 'lodash';
import { CreatePriceDto } from './dto/createPrice.dto';
import { PricesEntity } from './prices.entity';
import { ProductsService } from '../products/products.service';
let moment = require('moment');

@Injectable()
export class PricesService {
    constructor(
        @InjectRepository(PricesEntity)
        private readonly pricesRepository: Repository<PricesEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
        @Inject(forwardRef(() => ProductsService))
        private productsService: ProductsService,
    ) { }

    /**
     * this function add price
     * @param body (productId, date_from, type, price)
     * @param token_decoded (user_id, role, email)
     */
    async insert(body: CreatePriceDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRICES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check product
        const productCheck = await this.productsService.findProductByIdWithPrice(body.productId);
     
        if (!productCheck)
            throw {message: 'product is not exist in system'}

        // check price type
        if(body.type != PriceType.IMPORT && body.type != PriceType.EXPORT){
            throw {message: 'type is not exist'}
        }
        if(body.type == PriceType.IMPORT){
            if(productCheck[0].exportPrice != null && productCheck[0].exportPrice < body.price){
                throw {message: 'Giá bán hiện tại của sản phẩm là ' + productCheck[0].exportPrice + ' đ , bạn cần nhập giá bán cao hơn giá nhập'}
            }
        }
        if(body.type == PriceType.EXPORT){
            if(productCheck[0].importPrice != null && productCheck[0].importPrice > body.price){
                throw {message: 'Giá nhập hiện tại của sản phẩm là ' + productCheck[0].importPrice + ' đ, bạn cần nhập giá nhập nhỏ hơn giá bán'}
            }
        }

        // add
        const product = await this.productsService.findProductById(body.productId);
        let price = new PricesEntity();
        price.date_from = new Date();
        price.type = body.type;
        price.price = body.price;
        price.product = product;

        const data = await this.pricesRepository.save(price);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Giá sản phẩm',
            'Thêm giá sản phẩm "' + product.name + '('+ product.id +')"',
        )

        return data;
    }

    /**
     * The function get price list of product by productID
     * @param productId 
     * @param token_decoded (user_id, role, email)
     */
    async getListPriceProduct(productId: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.PRICES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const importPriceList = await this.pricesRepository.createQueryBuilder('price')
            .where('productId = :id', {id: productId})
            .andWhere('type = :type', {type: PriceType.IMPORT})
            .getMany();
        
        const exportPriceList = await this.pricesRepository.createQueryBuilder('price')
            .where('productId = :id', {id: productId})
            .andWhere('type = :type', {type: PriceType.EXPORT})
            .getMany();

        const data = {
            importPriceList: importPriceList,
            exportPriceList: exportPriceList,
        }

        return data;
    }

    // the function find price apply now of product by product id and price type

    async getPirceOfProductId(id: number, type: number){
        let query = 'SELECT * FROM ( SELECT * FROM products WHERE products.id = ' + id + ') AS products ';
        if (type == PriceType.IMPORT){
            query += 'LEFT JOIN (SELECT * FROM (SELECT productId as aID, MAX(date_from) as maxDate FROM `prices` WHERE type = 1 GROUP BY productId ) as a LEFT JOIN ( SELECT productId as bID, date_from, price as importPrice FROM prices) as b on b.bID = a.aID AND b.date_from = a.maxDate) as importsprice ON importsprice.aID = products.id ';
        }else if (type == PriceType.EXPORT){
            query += 'LEFT JOIN (SELECT * FROM (SELECT productId as a2ID, MAX(date_from) as maxDate FROM `prices` WHERE type = 2 GROUP BY productId ) as a2 LEFT JOIN ( SELECT productId as b2ID, date_from, price as exportPrice FROM prices) as b2 on b2.b2ID = a2.a2ID AND  b2.date_from = a2.maxDate) as exportsPrice ON exportsPrice.a2ID = products.id ';
        }

        let entityManger = getManager();
        
        return await entityManger.query(query);;
    }
}
