import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreatePriceDto {

    @IsNotEmpty({ message: 'product id is require' })
    @ApiModelProperty({ example: 1 })
    productId: number;

    @IsNotEmpty({ message: 'type is require' })
    @ApiModelProperty({ example: "1 - giá nhập; 2 - giá xuất" })
    type: number;

    @IsNotEmpty({ message: 'price is require' })
    @ApiModelProperty({ example: 200000 })
    price: number;

}
