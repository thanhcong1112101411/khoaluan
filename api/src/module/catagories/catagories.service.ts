import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CatagoriesEntity } from './catagories.entity';
import { Repository } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { CreateCatagoryDto } from './dto/createCatagory.dto';
import { RoleDefault, Screens } from '../../utils/enum.util';
import { UpdateCatagoryDto } from './dto/updateCatagory.dto';


const _ = require('lodash');

@Injectable()
export class CatagoriesService {
    constructor(
        @InjectRepository(CatagoriesEntity)
        private readonly catagoriesRepository: Repository<CatagoriesEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
    ) { }

    /**
     * the function add catagory
     * @param body 
     * @param image 
     * @param token_decoded 
     */
    async insertCatagory(body: CreateCatagoryDto, image: string, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.CATAGORIES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check name
        const catagoryCheck = await this.catagoriesRepository.createQueryBuilder('catagory')
        .where('catagory.deleted_at is null')
        .andWhere('name = :name', {name: body.name})
        .getOne();

        if (!!catagoryCheck)
            throw {message: 'Tên danh mục đã tồn tại'}

        // add
        let catagory = new CatagoriesEntity();
        catagory.name = body.name;
        catagory.image = image;
        const data = await this.catagoriesRepository.save(catagory);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Danh mục sản phẩm',
            'Thêm danh mục sản phẩm',
        )

        return data;
        
    }

    /**
     * This function get all catagories (transaction) by options
     * @param options (current_page, limit, name)
     * @returns object {pages, count, list_user, load_more}
     */
    async findAll(options: any){

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.catagoriesRepository.createQueryBuilder('catagory')
            .where('catagory.deleted_at is null');

        // name
        if (options.name) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(CatagoriesEntity, 'catagory')
                    .where('name like :name', {name: '%' + options.name + '%'})
                    .getQuery();
                return 'catagory.id IN ' + subQuery;
            });
        }

        let count_query = query.getCount();
        let data_query = query.orderBy('catagory.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }



    /**
     * the function update catagory
     * @param body 
     * @param token_decoded (user_id, role, email)
     * @param image 
     */
    async update(id: number, body: UpdateCatagoryDto, token_decoded: any, image?: string){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.CATAGORIES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        const catagory = await this.findCatagoryById(id);
        if (!catagory)
            throw {message: 'catagory is not exist'}

        // check name
        if (catagory.name !== body.name){
            const checkName = await this.findCatagoryByName(body.name);
            if (!!checkName)
                throw {message: 'Tên danh mục đã tồn tại'}
        }

        catagory.name = body.name;
        if (image){
            catagory.image = image;
        }

        const data = await this.catagoriesRepository.save(catagory);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Danh mục sản phẩm',
            'cập nhật danh mục sản phẩm "' + catagory.name + '('+ catagory.id +')"',
        )

        return data;
    }

    /**
     * this function delete catagory
     * @param id 
     * @param token_decoded (user_id, role, email)
     */
    async delete(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.CATAGORIES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        const catagory = await this.findCatagoryById(id);
        if (!catagory)
            throw {message: 'catagory is not exist'}

        // delete role group
        catagory.deleted_at = new Date();
        await this.catagoriesRepository.save(catagory);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Danh mục sản phẩm',
            'Xóa danh mục sản phẩm "' + catagory.name + '('+ catagory.id +')"',
        )
    }

    /**
     * the function get catagory detail
     * @param id 
     */
    async getDetail(id: number){
        const catagory = await this.findCatagoryById(id);

        if (!catagory)
            throw {message: 'catagory is not exist'}
        return catagory;
    }

    async getAllCatagory(){
        return this.catagoriesRepository.createQueryBuilder("catagory")
            .where("catagory.deleted_at is null")
            .getMany();
    }


    /**
     * find catagory by id
     * @param id 
     */
    async findCatagoryById(id: number){
        return this.catagoriesRepository.createQueryBuilder('catagory')
                .where('id = :id', {id: id})
                .andWhere('deleted_at is null')
                .getOne();
    }

    /**
     * find catagory by name
     * @param name 
     */
    async findCatagoryByName(name: string){
        return this.catagoriesRepository.createQueryBuilder('catagory')
                .where('name = :name', {name: name.trim()})
                .andWhere('deleted_at is null')
                .getOne();
    }

    /**
     * find catagory by id CONTAIN DELETED
     * @param id 
     */
    async findCatagoryContainDeletedById(id: number){
        return this.catagoriesRepository.createQueryBuilder('catagory')
                .where('id = :id', {id: id})
                .getOne();
    }
}
