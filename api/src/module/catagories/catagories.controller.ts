import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get, UseInterceptors, UploadedFiles, Req, HttpException, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam, ApiImplicitFile, ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import {FilesInterceptor} from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import {extname, join} from "path";
import * as fs from 'fs';
import {diskStorage} from 'multer';
import config from '../../../config/config';
import * as jwt from 'jsonwebtoken';
import { AuthGuard } from '@nestjs/passport';
import { CatagoriesService } from './catagories.service';
import { CreateCatagoryDto } from './dto/createCatagory.dto';

@ApiUseTags('catagories')
@Controller('api/catagories')
export class CatagoriesController {

    constructor(
        private readonly catagoriesService: CatagoriesService,
    ) { }

    @Roles(RolesType.All)
    @Post('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add catagory (name: ?, images: ?) - Root | catagory Management' })
    @ApiImplicitFile({name: 'images', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images', 20,
        {
            limits: {
                fileSize: 3000000,
            },
            fileFilter: (req, images, cb) => {
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'catagories');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const randomName = Date.now();
                    return cb(null, `${randomName}${extname(images.originalname).toLowerCase()}`);
                },
            }),
        },
    ))
    async addCatagory(@Req() request: Request, @UploadedFiles() images, @Res() res: Response) {
        
        // validate
        if (typeof images === 'undefined' || images.length == 0)
            return res.status(HttpStatus.OK).send(dataError('File is require', null));

        const image = config.uploadConfig.FILE_UPLOAD_PATH.replace('public/', '') + '/catagories/' + images[0].filename;
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        
        let body: CreateCatagoryDto;
        body = request.body;
        try {
            let data = await this.catagoriesService.insertCatagory(body, image, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Thêm danh mục thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-all')
    @ApiOperation({title: 'Get List Catagories - ALL'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name', required: false, type: 'string'})
    async listRole(@Req() request, @Res() res: Response) {
        let limit, current_page;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
        };

        try {
            let data_results = await this.catagoriesService.findAll(options);
            return res.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-detail/:id')
    @ApiOperation({ title: 'get catagory detail - All ' })
    async getDetail(@Param('id') id: number, @Req() request, @Res() res: Response) {
        try {
            const data = await this.catagoriesService.getDetail(id);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('update/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'update catagory (name: ?, images: ?) - Root | catagory Management' })
    @ApiImplicitFile({name: 'images', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images', 20,
        {
            limits: {
                fileSize: 3000000,
            },
            fileFilter: (req, images, cb) => {
                if (typeof(images) === 'undefined'){
                    return cb(null, true);
                }
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'catagories');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const randomName = Date.now();
                    return cb(null, `${randomName}${extname(images.originalname).toLowerCase()}`);
                },
            }),
        },
    ))
    async updateCatagory(@Param('id') id: number, @Req() request: Request, @UploadedFiles() images, @Res() res: Response) {
        
        // validate
        if (typeof images === 'undefined' || images.length == 0){
            const token = request.headers.authorization.split(' ')[1];
            const token_decoded = jwt.decode(token);
            try{
                const data = await this.catagoriesService.update(id, request.body, token_decoded);
                return res.status(HttpStatus.OK).send(dataSuccess('Cập nhật danh mục thành công', data));
            }catch (error) {
                return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
            }
        }

        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        const image = config.uploadConfig.FILE_UPLOAD_PATH.replace('public/', '') + '/catagories/' + images[0].filename;
        try {
            const data = await this.catagoriesService.update(id, request.body, token_decoded, image);
            return res.status(HttpStatus.OK).send(dataSuccess('Cập nhật danh mục thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('delete/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'delete catagory- Root | catagory Management' })
    async deleteCatagory(@Param('id') id: number, @Req() request: Request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            await this.catagoriesService.delete(id, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('Xóa danh mục thành công', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-all-catagory')
    @ApiOperation({ title: 'get catagory all - All ' })
    async getAllCatagory(@Req() request, @Res() res: Response) {
        try {
            const data = await this.catagoriesService.getAllCatagory();
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
