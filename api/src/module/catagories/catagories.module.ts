import { Module } from '@nestjs/common';
import { CatagoriesController } from './catagories.controller';
import { CatagoriesService } from './catagories.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CatagoriesEntity } from './catagories.entity';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([CatagoriesEntity]),
      RoleGroupsModule,
      HistoryModule,
    ] ,
    exports: [
      CatagoriesService,
    ],
    controllers: [CatagoriesController],
    providers: [CatagoriesService]
})
export class CatagoriesModule {}
