import { Module } from '@nestjs/common';
import { HuiController } from './hui.controller';
import { HuiService } from './hui.service';

@Module({
  controllers: [HuiController],
  providers: [HuiService]
})
export class HuiModule {}
