import { Injectable } from '@nestjs/common';
import { UtilityList } from './class/UtilityList.class';
import { Pair } from './class/Pair.class';
import { Element } from './class/Element.class';

const lineReader = require('line-reader');
const HashMap = require('hashmap');

@Injectable()
export class HuiService {

    mapItemToTWU = new HashMap();

    delay(time) {
        return new Promise((resolve) => { 
            setTimeout(resolve, time)
        });
     }

    async getItemTwu(){
        let mapItemToTWU = new HashMap();
        
        await lineReader.eachLine('public/input.txt', async (line) => {
            if (line.includes('STOP')) {
                return mapItemToTWU;
            }
                let split = await line.split(":");
                let items = await split[0].split(" ");
                let transactionUtility = parseInt(split[1]);
                // for each item, we add the transaction utility to its TWU
				for(let i=0; i <items.length; i++){
                    let item = parseInt(items[i]);
                    // get the current TWU of that item
                    let twu = await mapItemToTWU.get(item);
                    // add the utility of the item in the current transaction to its twu
                    twu =  (twu == null)? 
                        transactionUtility : twu + transactionUtility;
                    await mapItemToTWU.set(item, twu);
                }
        });
        return mapItemToTWU;
    }

    async runAlgorithm(){
        const minUtility = 42;
        //  We create a  map to store the TWU of each item
        this.mapItemToTWU = await this.getItemTwu();
        await this.delay(500);
        console.log('mapItemToTWU')
        console.log(this.mapItemToTWU);

        // CREATE A LIST TO STORE THE UTILITY LIST OF ITEMS WITH TWU  >= MIN_UTILITY.
		let listOfUtilityLists = new Array<UtilityList>();
		// CREATE A MAP TO STORE THE ITEM OF TRANSACTION WITH SIZE AT THAT TIME
		// Key : item list string    Value :  size at that time
        let mapTransactionListPosition = new HashMap();
        
        // For each item
        await this.mapItemToTWU.forEach(async function(value, key) {
            if(this.mapItemToTWU.get(key) >= minUtility){
                // create an empty Utility List that we will fill later.
                let uList = new UtilityList(key);
                // add the item to the list of high TWU items
				await listOfUtilityLists.push(uList);
            }
        });
        // sort 
        await listOfUtilityLists.sort(function (a,b) {
            let compare = this.mapItemToTWU.get(a.item) - this.mapItemToTWU.get(b.item);
            // if the same, use the lexical order otherwise use the TWU
            return (compare == 0)? a.item - b.item :  compare;
        })

        console.log('listOfUtilityLists');
        console.log(listOfUtilityLists);

        // variable to count the number of transaction
        let tid = 1;
        
       
        // for each line (transaction) until the end of file
        await lineReader.eachLine('public/input.txt', async (line) => {
            if (line.includes('STOP')) {
        
                await this.printListOfUtilityLists(listOfUtilityLists);

                // await this.hMiner([], 0, listOfUtilityLists, minUtility, mapItemToTWU, mapTransactionListPosition);
                return;
            }
            console.log("-------------------------- TID: " + tid.toString() + "----------------");
            // split the line according to the separator
			let split = line.split(":");
			// get the list of items
			let items = split[0].split(" ");
			// get the list of utility values corresponding to each item
			// for that transaction
			let utilityValues = split[2].split(" ");
				
			// Copy the transaction into lists but 
			// without items with TWU < minutility
				
            let remainingUtility =0;
            // Create a list to store items
            let revisedTransaction = new Array<Pair>();
            // create hash map key
            let hashKey = "";

            // for each item
            for(let i=0; i <items.length; i++){
                /// convert values to integers
                let pair = new Pair();
                pair.item = parseInt(items[i]);
                pair.utility = parseInt(utilityValues[i]);
                // if the item has enough utility
                if(this.mapItemToTWU.get(pair.item) >= minUtility){
                    revisedTransaction.push(pair);
                    // remainingUtility += pair.utility;
                }
            }

            // sort 
            await revisedTransaction.sort(function (a,b) {
                let compare = this.mapItemToTWU.get(a.item) - this.mapItemToTWU.get(b.item);
                // if the same, use the lexical order otherwise use the TWU
                return (compare == 0)? a.item - b.item :  compare;
            })
            console.log("------------revisedTransaction---------");
            console.log(revisedTransaction);

            // check if duplicate transaction exists
            let keyCheck = "";
            for(let pair of revisedTransaction){
                keyCheck += pair.item.toString();
            }
            console.log("------------- HASH MAP ----------------------");
            console.log(mapTransactionListPosition);
            const valueHashMap = await mapTransactionListPosition.get(keyCheck.trim());
            if(valueHashMap){
                console.log("exist");
                // for each item left in the transaction
                for(let i = revisedTransaction.length - 1; i >= 0; i--){
                    const pair = revisedTransaction[i];
                        
                    // // Add a new Element to the utility list of this item corresponding to this transaction
                    // let element = await new HminerElement(tid, pair.utility, remainingUtility, 0, ppos);
                    
                    const index = await listOfUtilityLists.findIndex((value) =>{
                        return value.item == pair.item;
                    })
                    let oldElement = await this.findHminerElementWithTID(listOfUtilityLists[index], valueHashMap);

                    // update element
                    await listOfUtilityLists[index].updateElement(oldElement.element, pair.utility, remainingUtility, oldElement.index);

                    // subtract the utility of this item from the remaining utility
                    remainingUtility = remainingUtility + pair.utility;
                }
            }else{
                console.log("non-exist");
                // for each item left in the transaction
                for(let i = revisedTransaction.length - 1; i >= 0; i--){
                    const pair = revisedTransaction[i];
                    hashKey = await pair.item.toString() + hashKey + " ";

                    // compute PPOS
                    let ppos = 0;
                    if (i == 0){
                        ppos = -1;
                    }else{
                        // previously item
                        const item = revisedTransaction[i-1].item;
                        // position of previously item in listOfUtilityLists
                        const indexItem = await listOfUtilityLists.findIndex(value => {
                            return value.item == item;
                        })
                        // ppos is size of CULs[previously item].tidlist
                        ppos = listOfUtilityLists[indexItem].elements.length;
                    }
                        
                    // Add a new Element to the utility list of this item corresponding to this transaction
                    let element = await new Element(tid, pair.utility, remainingUtility, 0, ppos);
                    
                    const index = await listOfUtilityLists.findIndex((value) =>{
                        return value.item == pair.item;
                    })
                    listOfUtilityLists[index].addElement(element);

                    // subtract the utility of this item from the remaining utility
                    remainingUtility = remainingUtility + pair.utility;
                }
            }

            
            // save hash table
            if(hashKey != ""){
                mapTransactionListPosition.set(hashKey.trim(), tid);
            }
            
            tid++; // increase tid number for next transaction
        });

    }

    async printListOfUtilityLists(list: Array<UtilityList>){
        let printStr1 = "";
        let printStr2 = "";
        for(let utility of list){
            printStr1 = utility.sumNutils.toString() + " " + utility.sumNRutils.toString() + " " + utility.sumCutils.toString() + "/" + utility.sumCrutils.toString() + "/" + utility.sumCputils.toString();
            console.log(printStr1);

            for(let element of utility.elements){
                printStr2 = element.tid.toString() + " " + element.nutils.toString() + " " + element.nrutils.toString() + " " + element.putils.toString() + " " + element.ppos.toString();
                console.log(printStr2);
            }
        }
    }

    async findHminerElementWithTID(ulist: UtilityList, tid: number){
		let list = ulist.elements;
		
		// perform a binary search to check if  the subset appears in  level k-1.
        let first = 0;
        let last = list.length - 1;
       
        // the binary search
        while( first <= last )
        {
        	let middle = ( first + last ) >>> 1; // divide by 2

            if(list[middle].tid < tid){
            	first = middle + 1;  //  the itemset compared is larger than the subset according to the lexical order
            }
            else if(list[middle].tid > tid){
            	last = middle - 1; //  the itemset compared is smaller than the subset  is smaller according to the lexical order
            }
            else{
                return {
                    element: list[middle],
                    index: middle,
                }
            }
        }
        return null;
        
    }
}
