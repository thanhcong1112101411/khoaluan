import { Controller, Get, Res, HttpStatus, Req } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { Response } from 'express';
import { HuiService } from './hui.service';

@ApiUseTags('hui')
@Controller('api/hui')
export class HuiController {
    constructor(
        private readonly huiService: HuiService,
    ) { }

    @Roles(RolesType.All)
    @Get('')
    @ApiOperation({title: 'HUI-MINER'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async huminer(@Req() request, @Res() res: Response){
        try {
            const data = await this.huiService.runAlgorithm();
            return res.status(HttpStatus.OK).send(dataSuccess('OK', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    
}
