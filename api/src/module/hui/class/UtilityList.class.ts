import { Element } from "./Element.class";

export class UtilityList {
    item: number;  // the item
    sumNutils: number = 0;  // the sum of non-closed utilities
    sumNRutils: number = 0;  // the sum of remaining utilities
    sumCutils: number = 0; // closed utility
    sumCrutils: number = 0; // closed remaining utility
    sumCputils: number = 0; // closed prefix utility

    elements: Array<Element> = []; // the elements

    /**
     * contructor
     * @param item 
     */
    constructor(item: number){
        this.item = item;
    }

    /**
	 * Method to add an element to this utility list and update the sums at the same time.
	 */
    addElement(element: Element){
        this.sumNutils += element.nutils;
        this.sumNRutils += element.nrutils;
        this.elements.push(element);
    }

    updateElement(oldElement: Element, nutils: number, nrutils: number, index: number){
        this.sumNutils = this.sumNutils + nutils;
        this.sumNRutils = this.sumNRutils + nrutils;
        let newElement = oldElement;
        newElement.nutils += nutils;
        newElement.nrutils += nrutils;

        this.elements[index] = newElement;
    }

    /**
	 * Get the support of the itemset represented by this utility-list
	 * @return the support as a number of trnsactions
	 */
    getSupport(): any{
        return this.elements.length;
    }


}