export interface JwtPayload {
    email: string;
    user_id: string;
    role?: number;
}
