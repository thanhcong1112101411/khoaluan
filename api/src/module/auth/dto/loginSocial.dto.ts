import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class LoginSocialDto {
    @IsNotEmpty({ message: 'name is required' })
    @IsNotBlank('name', { message: 'name is not white space' })
    @ApiModelProperty({ required: true, example: 'Thanh Cong' })
    name: string;

    @IsNotEmpty({ message: 'email is required' })
    @IsNotBlank('email', { message: 'email is not white space' })
    @ApiModelProperty({ required: true, example: 'thanhcong111210141@gmail.com' })
    email: string;

    @IsNotEmpty({ message: 'id is required' })
    @IsNotBlank('id', { message: 'id is not white space' })
    @ApiModelProperty({ required: true, example: '997389298237883' })
    id: string;

    @IsNotEmpty({ message: 'type is required' })
    @ApiModelProperty({ required: true, example: 1 })
    type: number;
}
