import { ApiModelProperty } from '@nestjs/swagger';

export class PasswordDto {
    @ApiModelProperty({ required: true, example: '123456' })
    password_confirmation: string;

    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;
}
