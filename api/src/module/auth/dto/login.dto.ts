import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class LoginDto {
    @IsNotEmpty({ message: 'Username is required' })
    @IsNotBlank('username', { message: 'Is not white space' })
    @ApiModelProperty({ required: true, example: 'admin@gmail.com' })
    username: string;

    @IsNotEmpty({ message: 'Password is required' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @IsNotEmpty({ message: 'Type Login is required' })
    @ApiModelProperty({ required: true, example: '1 - Admin ; 2 - User' })
    type: number;
}
