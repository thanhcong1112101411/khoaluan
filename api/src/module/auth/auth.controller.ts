import { Controller, Get, UseGuards, Body, Post, Req, HttpStatus, Res } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { ApiUseTags, ApiBearerAuth, ApiResponse, ApiOperation, ApiImplicitQuery } from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '../../utils/jsonFormat';
import { LoginDto } from './dto/login.dto';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserUtil } from '../../utils/user.util';
import { CustomValidationPipe } from '../../common/pipe/customValidation.pipe';
import { passport, authenticate } from 'passport';
import { LoginSocialDto } from './dto/loginSocial.dto';
import * as jwt from 'jsonwebtoken';

@ApiUseTags('Auth')
@Controller('api/auth')
export class AuthController {
    constructor(
        private readonly userService: UsersService,
        private readonly authService: AuthService,
    ) { }

    @Roles(RolesType.All)
    @Post('login')
    @ApiOperation({ title: 'Login' })
    @ApiResponse({ status: 200, description: 'Login success' })
    @ApiResponse({ status: 400, description: 'Bad request' })
    @ApiResponse({ status: 401, description: 'UNAUTHORIZED' })
    async login(@Body(CustomValidationPipe) body: LoginDto, @Res() res: Response) {
        let user, token;
        try {
            user = await this.userService.login(body);
            if (!user)
                return res.status(HttpStatus.OK).send(dataError('Password or email incorrect', null));
        } catch (e) {
            return res.status(HttpStatus.OK).send(dataError(e.message || 'Error', null));
        }

        let data: JwtPayload = {
            email: body.username,
            user_id: user.user_id_str,
            role: user.role.id,
        };

        try {
            token = await this.authService.createToken(data);
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        const role = user.role;
        user = UserUtil.serialize(user);
        user._token = token;
        user.role = role;

        return res.status(HttpStatus.OK).send(dataSuccess('Login success', user));
    }

    @Roles(RolesType.All)
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Get('user')
    @ApiOperation({ title: 'User Profile' })
    @ApiResponse({ status: 200, description: 'Users data.' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async profile(@Req() request, @Res() res: Response) {
        try {
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', UserUtil.serialize(request.user)));
        } catch (e) {
            return res.status(e.statusCode || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }

    @Roles(RolesType.All)
    @ApiOperation({title: 'Social login | type: 1-facebook, 2-google'})
    @ApiResponse({status: 200, description: 'Login success'})
    @ApiResponse({status: 400, description: 'Bad request'})
    @Post('social-login')
    async loginSocial( @Body(CustomValidationPipe) body: LoginSocialDto, @Res() res: Response ) {
        let token, data, profile;
        
        try {
            profile = await this.userService.getProfileByAppid(body.id, body.type);
            if (profile){
                let dataToken: JwtPayload = {
                    email: profile.email,
                    user_id: profile.user_id_str,
                    role: profile.role.id,
                };
                try {
                    token = await this.authService.createToken(dataToken);
                } catch (e) {
                    return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
                }
                profile = UserUtil.serialize(profile);
                profile._token = token;

                return res.status(HttpStatus.OK).send(dataSuccess('success', profile));
            }
        } catch (e) {
            return res.status(HttpStatus.OK).send(dataError(e.message || 'Error', null));
        }

        try {
            profile = await this.userService.registerSocial(body);
        } catch (e) {
            return res.status(HttpStatus.OK).send(dataError(e.message || 'Error', null));
        }

        let dataToken: JwtPayload = {
            email: profile.email,
            user_id: profile.user_id_str,
            role: profile.role.id,
        };
        try {
            token = await this.authService.createToken(dataToken);
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        profile = UserUtil.serialize(profile);
        profile._token = token;
        // profile = Object.assign(profile,{_token: token});
        return res.status(HttpStatus.OK).send(dataSuccess('Ok', profile));
    }

    @Roles(RolesType.All)
    @ApiOperation({title: 'Check user by token, uuid'})
    @Get('check-user')
    @ApiImplicitQuery({name: 'token', description: 'token', required: true, type: 'string'})
    @ApiImplicitQuery({name: 'uuid', description: 'uuid', required: true, type: 'string'})
    async checkUser(@Res() res: Response, @Req() request ) {
        const token_decoded = jwt.decode(request.query.token);
        try{
            let user;
            user = await this.userService.checkUser(token_decoded, request.query.uuid);
            const role = user.role;
            user = UserUtil.serialize(user);
            user._token = request.query.token;
            user.role = role;

            return res.status(HttpStatus.OK).send(dataSuccess('Ok', user));
        }catch(e){
            return res.status(HttpStatus.OK).send(dataError(e.message || 'Error', null));
        }
    }
}
