import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { authenticate } from 'passport';

@Module({
    imports: [UsersModule],
    providers: [AuthService, JwtStrategy],
    exports: [AuthService, JwtStrategy],
    controllers: [AuthController],
})
export class AuthModule{};
