import * as jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import config from '../../../config/config';

@Injectable()
export class AuthService {

    constructor(
        private readonly usersService: UsersService,
    ) { }

    /**
     * Create token
     * @param dataLogin
     * @returns token
     * otherwise return false
     */
    async createToken(dataLogin) {
        const user: JwtPayload = {
            email: dataLogin.email,
            user_id: dataLogin.user_id,
            role: dataLogin.role,
        };
        return await jwt.sign(user, config.jwtSecret, { expiresIn: config.jwtExpiresIn /* 2 days */ });
    }

    /**
     * validate user by email
     * @param payload
     * @returns {Promise<any>}
     */
    async validateUser(payload: JwtPayload): Promise<any> {
        try {
            return await this.usersService.findOneUserById(payload.user_id);
        } catch (e) {
            throw {
                status: 400,
                message: 'Validate user error',
            };
        }
    }
}
