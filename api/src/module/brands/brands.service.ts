import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BrandsEntity } from './brands.entity';
import { Repository } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { CreateBrandDto } from './dto/createBrand.dto';
import { RoleDefault, Screens } from '../../utils/enum.util';
import _ from 'lodash';
import { UpdateBrandDto } from './dto/updateBrand.dto';

@Injectable()
export class BrandsService {
    constructor(
        @InjectRepository(BrandsEntity)
        private readonly brandsRepository: Repository<BrandsEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
    ) { }

    /**
     * this function add brand 
     * @param body 
     * @param token_decoded (user_id, role, email)
     */
    async insert(body: CreateBrandDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.BRANDS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check name
        const brandCheck = await this.findBrandByName(body.name);
        if (!!brandCheck)
            throw {message: 'Tên thương hiệu đã tồn tại'}
        
        let brand = new BrandsEntity();
        brand.name = body.name;

        const data = await this.brandsRepository.save(brand);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Thương hiệu sản phẩm',
            'Thêm thương hiệu sản phẩm "' + brand.name + '('+ brand.id +')"',
        )

        return data;
    }

    /**
     * This function get all brands (transaction) by options
     * @param options (current_page, limit, name)
     * @returns object {pages, count, list_user, load_more}
     */
    async findAll(options: any){

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.brandsRepository.createQueryBuilder('brand')
            .where('brand.deleted_at is null');

        // name
        if (options.name) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(BrandsEntity, 'brand')
                    .where('name like :name', {name: '%' + options.name + '%'})
                    .getQuery();
                return 'brand.id IN ' + subQuery;
            });
        }

        let count_query = query.getCount();
        let data_query = query.orderBy('brand.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * the function update brand by body
     * @param id 
     * @param body 
     * @param token_decoded (user_id, role, email)
     */
    async update(id: number, body: UpdateBrandDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.BRANDS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check name if name changed
        let brand = await this.findBrandById(id);
        if (!brand)
            throw {message: "brand is not exitst"}

        if (brand.name !== body.name){
            const brandCheck = await this.findBrandByName(body.name);
            if (!!brandCheck)
                throw {message: "Tên thương hiệu đã tồn tại!"}
        }

        brand.name = body.name;

        const data = await this.brandsRepository.save(brand);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Thương hiệu sản phẩm',
            'Cập nhật thương hiệu sản phẩm "' + brand.name + '('+ brand.id +')"',
        )

        return data;
    }

    /**
     * this function delete brand
     * @param id 
     * @param token_decoded (user_id, role, email)
     */
    async delete(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.BRANDS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        const brand = await this.findBrandById(id);
        if (!brand)
            throw {message: 'brand is not exist'}

        // delete role group
        brand.deleted_at = new Date();
        await this.brandsRepository.save(brand);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Thương hiệu sản phẩm',
            'Xóa thương hiệu sản phẩm "' + brand.name + '('+ brand.id +')"',
        )
    }

    /**
     * the function get brand detail
     * @param id 
     */
    async getDetail(id: number){
        const brand = await this.findBrandById(id);

        if (!brand)
            throw {message: 'brand is not exist'}
        return brand;
    }


    async getAllBrand(){
        return this.brandsRepository.createQueryBuilder("brand")
            .where("brand.deleted_at is null")
            .getMany();
    }

    /**
     * the function find brand by name
     * @param name 
     */
    async findBrandByName(name: string){
        return this.brandsRepository.createQueryBuilder('brand')
            .where('deleted_at is null')
            .andWhere('brand.name = :name', {name: name})
            .getOne();
    }

    /**
     * the function find brand by id
     * @param name 
     */
    async findBrandById(id: number){
        return this.brandsRepository.createQueryBuilder('brand')
            .where('deleted_at is null')
            .andWhere('brand.id = :id', {id: id})
            .getOne();
    }

}
