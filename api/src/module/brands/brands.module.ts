import { Module } from '@nestjs/common';
import { BrandsController } from './brands.controller';
import { BrandsService } from './brands.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';
import { BrandsEntity } from './brands.entity';

@Module({
    imports: [
      TypeOrmModule.forFeature([BrandsEntity]),
      RoleGroupsModule,
      HistoryModule,
    ] ,
    exports: [
      BrandsService
    ],
    controllers: [BrandsController],
    providers: [BrandsService],
    
})
export class BrandsModule {}
