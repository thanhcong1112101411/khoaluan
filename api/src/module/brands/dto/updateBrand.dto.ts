import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class UpdateBrandDto {

    @IsNotEmpty({ message: 'name is require' })
    @IsNotBlank('name', { message: 'name is not white space' })
    @ApiModelProperty({ example: 'Hải sản' })
    name: string;
}
