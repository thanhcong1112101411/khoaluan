import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get, UseInterceptors, UploadedFiles, Req, HttpException, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam, ApiImplicitFile, ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import {FilesInterceptor} from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import {extname, join} from "path";
import * as fs from 'fs';
import {diskStorage} from 'multer';
import config from '../../../config/config';
import * as jwt from 'jsonwebtoken';
import { AuthGuard } from '@nestjs/passport';
import { BrandsService } from './brands.service';
import { CustomValidationPipe } from '../../common/pipe/customValidation.pipe';
import { CreateBrandDto } from './dto/createBrand.dto';
import { UpdateBrandDto } from './dto/updateBrand.dto';

@ApiUseTags('brands')
@Controller('api/brands')
export class BrandsController {
    constructor(
        private readonly brandsService: BrandsService,
    ) { }

    @Roles(RolesType.All)
    @Post('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add brand - root | brand management ' })
    async addBrand(@Req() request, @Body() body: CreateBrandDto, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.brandsService.insert(body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Thêm thương hiệu thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-all')
    @ApiOperation({title: 'Get List Brands - ALL'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name', required: false, type: 'string'})
    async listBrand(@Req() request, @Res() res: Response) {
        let limit, current_page;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return res.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return res.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
        };

        try {
            let data_results = await this.brandsService.findAll(options);
            return res.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-detail/:id')
    @ApiOperation({ title: 'get brand detail - All ' })
    async getDetail(@Param('id') id: number, @Req() request, @Res() res: Response) {
        try {
            const data = await this.brandsService.getDetail(id);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('get-all-brand')
    @ApiOperation({ title: 'get brand all - All ' })
    async getAllCatagory(@Req() request, @Res() res: Response) {
        try {
            const data = await this.brandsService.getAllBrand();
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('update/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'update brand - root | brand management ' })
    async updateBrand(@Param('id') id: number, @Req() request, @Body() body: UpdateBrandDto, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const data = await this.brandsService.update(id, body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Cập nhật thương hiệu thành công', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('delete/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'delete brand- Root | brand Management' })
    async deleteCatagory(@Param('id') id: number, @Req() request: Request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);

        try {
            await this.brandsService.delete(id, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('Xóa thương hiệu thành công', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
