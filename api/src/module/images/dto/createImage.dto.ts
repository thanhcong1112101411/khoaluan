import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreateImageDto {

    @IsNotEmpty({ message: 'title is require' })
    @IsNotBlank('title', { message: 'title is not white space' })
    @ApiModelProperty({ example: 'admin@gmail.com' })
    title: string;

    @IsNotEmpty({ message: 'type is require' })
    @ApiModelProperty({ example: '1 - BANNER, 2 - CONTENT TOP' })
    type: number;
}
