import { Module } from '@nestjs/common';
import { ImagesService } from './images.service';
import { ImagesController } from './images.controller';
import { ImagesEntity } from './images.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ImagesEntity]),
    RoleGroupsModule,
    HistoryModule,
  ] ,
  exports: [
    ImagesService,
  ],
  providers: [ImagesService],
  controllers: [ImagesController]
})
export class ImagesModule {}
