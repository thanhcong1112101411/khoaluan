import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity('images')
export class ImagesEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    title: string;

    @Column({
        type: 'text',
    })
    link: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    type: number;
}
