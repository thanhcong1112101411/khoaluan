import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { ImagesEntity } from './images.entity';
import { HistoryService } from '../history/history.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImageDto } from './dto/createImage.dto';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { Imagetype, RoleDefault, Screens } from '../../utils/enum.util';

const _ = require('lodash');

@Injectable()
export class ImagesService {
    constructor(
        @InjectRepository(ImagesEntity)
        private readonly imagesRepository: Repository<ImagesEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
    ) { }
    
    /**
     * this function add image
     * @param body 
     * @param image 
     * @param token_decoded 
     */
    async insertImage(body: CreateImageDto, image: string, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.IMAGES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check type
        if (body.type != Imagetype.BANNER && body.type != Imagetype.CONTENT_TOP && body.type != Imagetype.LOGO){
            throw {message: 'type is not exists'};
        }

        let imageAdd = new ImagesEntity();
        imageAdd.link = image;
        imageAdd.title = body.title;
        imageAdd.type = body.type;

        await this.imagesRepository.save(imageAdd);

        // save history
        if (body.type == Imagetype.BANNER){
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'Hình ảnh',
                'Thêm hình ảnh banner',
            )
        } else{
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'Hình ảnh',
                'Thêm hình ảnh content top',
            )
        }
        
    }

    /**
     * this function get list image with type
     * @param type 
     */
    async getList(type: number){
        // check type
        if (type != Imagetype.BANNER && type != Imagetype.CONTENT_TOP && type != Imagetype.LOGO)
            throw {message: 'type is not exist'};
        return this.imagesRepository.find({type: type});
    }

    async delete(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.IMAGES});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check image exits
        const image = await this.imagesRepository.findOne(id);
        if (!image) throw {message: 'Hình ảnh đang chọn không tồn tại', status: 400}

        await this.imagesRepository.delete(id);

        // save history
        if (image.type == Imagetype.BANNER){
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'Hình ảnh',
                'Xóa hình ảnh banner',
            )
        } else{
            await this.historyService.insertHistory(
                token_decoded.user_id,
                'Hình ảnh',
                'Xóa hình ảnh content top',
            )
        }
    }

}
