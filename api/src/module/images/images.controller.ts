import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get, UseInterceptors, UploadedFiles, Req, HttpException, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam, ApiImplicitFile, ApiBearerAuth } from '@nestjs/swagger';
import {FilesInterceptor} from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { dataSuccess, dataError } from '../../utils/jsonFormat';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import {extname, join} from "path";
import * as fs from 'fs';
import {diskStorage} from 'multer';
import config from '../../../config/config';
import { ImagesService } from './images.service';
import * as jwt from 'jsonwebtoken';
import { CreateImageDto } from './dto/createImage.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('images')
@Controller('api/images')
export class ImagesController {
    constructor(
        private readonly imagesService: ImagesService,
    ) { }

    @Roles(RolesType.All)
    @Post('add-image')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add image - Root | Image Management' })
    @ApiImplicitFile({name: 'images', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images', 20,
        {
            limits: {
                fileSize: 3000000,
            },
            fileFilter: (req, images, cb) => {
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join(config.uploadConfig.FILE_UPLOAD_PATH, 'images');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath);
                    }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const randomName = Date.now();
                    return cb(null, `${randomName}${extname(images.originalname).toLowerCase()}`);
                },
            }),
        },
    ))
    async addImage(@Req() request: Request, @UploadedFiles() images, @Res() res: Response) {

        // validate
        if (typeof images === 'undefined')
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('File is require', null));

        const image = config.uploadConfig.FILE_UPLOAD_PATH.replace('public/', '') + '/images/' + images[0].filename;
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            await this.imagesService.insertImage(request.body, image, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Thêm thành công', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get(':type')
    @ApiOperation({ title: 'Get List Image - All' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async getListImage(@Param('type') type: number, @Res() res: Response) {
        try {
            let data = await this.imagesService.getList(type);

            return res.status(HttpStatus.OK).send(dataSuccess('Ok', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Delete('delete-image/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'delete image - Root | Image Management' })
    async deleteImage(@Param('id') id: number,@Req() request: Request, @Res() res: Response){
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            await this.imagesService.delete(id, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('Đã xóa hình ảnh', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
