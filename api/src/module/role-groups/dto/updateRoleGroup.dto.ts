import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class UpdateRoleGroupDto {
    @IsNotEmpty({ message: 'name is required' })
    @IsNotBlank('name', { message: 'name is not white space' })
    @ApiModelProperty({ required: true, example: 'role group' })
    name: string;

    @IsNotEmpty({ message: 'screens is required' })
    @ApiModelProperty({ required: true, example: [1,2,3] })
    screens: any;
}
