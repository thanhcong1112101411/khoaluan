import { Module } from '@nestjs/common';
import { RoleGroupsController } from './role-groups.controller';
import { RoleGroupsService } from './role-groups.service';
import { RoleGroupEntity } from './role-groups.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScreensModule } from '../screens/screens.module';
import { HistoryModule } from '../history/history.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([RoleGroupEntity]),
      ScreensModule,
      HistoryModule,
    ],
    controllers: [RoleGroupsController],
    providers: [RoleGroupsService],
    exports: [
       RoleGroupsService
    ],

})
export class RoleGroupsModule {}
