import { Controller, Body, Post, Res, Put, Get, HttpStatus, Param, Req, UseGuards } from '@nestjs/common';
import { RoleGroupsService } from './role-groups.service';
import { ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery, ApiBearerAuth } from '@nestjs/swagger';
import { CreateRoleGroupDto } from './dto/createRoleGroup.dto';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { Response } from 'express';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { UserUtil } from '../../utils/user.util';
import { UpdateRoleGroupDto } from './dto/updateRoleGroup.dto';
import { getRepository } from 'typeorm';
import { serialize } from 'v8';
import { AuthGuard } from '@nestjs/passport';
import * as jwt from 'jsonwebtoken';

@ApiUseTags('Role Groups')
@Controller('api/role-groups')
export class RoleGroupsController {
    constructor(
        private readonly roleGroupsService: RoleGroupsService,
    ) { }

    @Roles(RolesType.All)
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'add role group - root | role group management' })
    async addRole(@Body() body: CreateRoleGroupDto, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const roleGroup = await this.roleGroupsService.insertRoleGroup(body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', roleGroup));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('delete-role/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'delete role group' })
    async deleteRole(@Param('id') id: number, @Res() res: Response, @Req() request){
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            await this.roleGroupsService.deleteRoleGroup(id, token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('Xóa nhóm quyền thành công', null));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Put('update-role/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'update role group - Root | Role group management' })
    async updateRole(@Param('id') id: number, @Body() body: UpdateRoleGroupDto, @Res() res: Response, @Req() request){
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const roleGroup = await this.roleGroupsService.updateRoleGroup(id, body, token_decoded);
            return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', roleGroup));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('/:id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get role group detail - except USER' })
    async detailRole(@Param('id') id: number, @Res() res: Response, @Req() request){
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const roleGroup = await this.roleGroupsService.getRole(id, token_decoded);
            let users = [];
            for(let user of roleGroup.users){
                users.push(UserUtil.serialize(user));
            }
            roleGroup.users = users;
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', roleGroup));
        } catch (error) {
            return res.status( HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles(RolesType.All)
    @Get('')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Get List role groups - ROOT | ROLE MANAGEMENT | ACCOUNT MANAGEMENT'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'current_page', description: 'current page', required: false, type: Number})
    @ApiImplicitQuery({name: 'limit', description: 'Limit of pagination', required: false, type: Number})
    @ApiImplicitQuery({name: 'name', description: 'Name', required: false, type: 'string'})
    async listRole(@Req() request, @Res() resSwagger: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        let limit, current_page;

        // check limit
        if (request.query.limit) {
            limit = Number(request.query.limit);
            if (isNaN(limit)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('limit should be a number', null));
            }
        }

        if (request.query.current_page){
            current_page = Number(request.query.current_page);
            if (isNaN(current_page)) {
                return resSwagger.status(HttpStatus.OK).send(dataError('current page should be a number', null));
            }
        }

        let options = {
            current_page,
            limit,
            name: request.query.name,
        };

        try {
            let data_results = await this.roleGroupsService.findAll(options, token_decoded);
            return resSwagger.status(HttpStatus.OK).send(getPaginationData('OK', data_results.data,
                data_results.count, data_results.load_more, data_results.pages));
        } catch (error) {
            return resSwagger.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
