import { Injectable, MiddlewareConsumer, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RoleGroupEntity } from './role-groups.entity';
import { Repository } from 'typeorm';
import { CreateRoleGroupDto } from './dto/createRoleGroup.dto';
import { ScreensService } from '../screens/screens.service';
import { UpdateRoleGroupDto } from './dto/updateRoleGroup.dto';
import { RoleDefault, Screens } from '../../utils/enum.util';
import { HistoryService } from '../history/history.service';

const _ = require('lodash');

@Injectable()
export class RoleGroupsService {
    constructor(
        @Inject(forwardRef(() => HistoryService))
        private historyService: HistoryService,
        @InjectRepository(RoleGroupEntity)
        private readonly roleGroupsRepository: Repository<RoleGroupEntity>,
        @Inject(forwardRef(() => ScreensService))
        private screensService: ScreensService,
    ){}

    /*
    * get role group detail
    * @param RoleId
    */
    async getRoleGroupDetail(roleId: number){
        return this.roleGroupsRepository.findOne({id: roleId, deleted_at: null}, {relations: ['screens']});
    }

    

    /**
     * get role group info
     * @param RoleId
     */
    async getRoleGroupInf(roleId: number){
        return this.roleGroupsRepository.findOne({id: roleId, deleted_at: null});
    }

    async getRoleGroupByName(roleName: string){
        return this.roleGroupsRepository.findOne({name: roleName, deleted_at: null});
    }

    /*
    * add role group by body and token_decoded
    * @param body
    * @param token_decoded (user_id, role, email) 
    */
    async insertRoleGroup(body: CreateRoleGroupDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ROLES; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check name
        const roleGroupName = await this.getRoleGroupByName(body.name);
        if (roleGroupName) throw {message: 'Tên nhóm quyền đã tồn tại', status: 400}

        let screensArray = [];
        for(let screenId of body.screens){
            const screenInf = await this.screensService.getScreenInf(screenId);
            screensArray.push(screenInf);
        }

        // check empty
        if (screensArray.length == 0){
            throw {message: 'Không có quyền nào được chọn', status: 400}
        }

        let roleGroup = new RoleGroupEntity();
        roleGroup.name = body.name;
        roleGroup.screens = screensArray;

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'nhóm quyền',
            'thêm nhóm quyền " ' + roleGroup.name + '"',
        )

        return this.roleGroupsRepository.save(roleGroup);
    }

    /**
     * this function update role group
     * @param id 
     * @param body 
     * @param token_decoded (user_id, role, email) 
     */
    async updateRoleGroup(id: number, body: UpdateRoleGroupDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ROLES; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }
        // check role id update (except id ROOT, NONE, USER)
        if (id == RoleDefault.NONE || id == RoleDefault.ROOT || id == RoleDefault.USER)
            throw {message: "Đây là nhóm quyền mặc định, không thể chỉnh sửa"}

        // check role exist
        let roleGroup = await this.getRoleGroupDetail(id);
        if( !roleGroup) throw {message: 'Nhóm quyền không tồn tại', status: 400};

        // delete all screen in role group
        roleGroup.screens = roleGroup.screens.filter(screen => {
            screen.id !== 0
        })
        await this.roleGroupsRepository.save(roleGroup);

        // update role
        let screensArray = [];
        for(let screenId of body.screens){
            const screenInf = await this.screensService.getScreenInf(screenId);
            screensArray.push(screenInf);
        }
        roleGroup.name = body.name;
        roleGroup.screens = screensArray;

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'nhóm quyền',
            'cập nhật nhóm quyền "' + roleGroup.name + '"',
        );

        return this.roleGroupsRepository.save(roleGroup);
    }

    /**
     * delete role group by id, role
     * @param id 
     * @param token_decoded 
     */
    async deleteRoleGroup(id: number, token_decoded){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ROLES; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check role group exist
        let roleGroup = await this.getRoleGroupDetail(id);
        if( !roleGroup) throw {message: 'nhóm quyền không tồn tại', status: 400};

        // check role user and root
        if(id == RoleDefault.ROOT || id == RoleDefault.USER || id == RoleDefault.NONE)
            throw {message: 'Đây là nhóm quyền mặc định, không thể xóa'};

        // check role group have user
        const roleUser = await this.roleGroupsRepository.findOne(id, {relations: ['users']});
        if(roleUser.users.length > 0)
            throw {message: 'Tồn tại tài khoản trong nhóm quyền, không thể xóa', status: 400};
        
        // delete role group
        roleGroup.deleted_at = new Date();
        await this.roleGroupsRepository.save(roleGroup);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'role group',
            'delete role group " ' + roleGroup.name + '"',
        )
    }

    /**
     * get role group info contain Screens, user
     * @param id 
     * @param token_decoded (user_id, role, email)
     */
    async getRole(roleId: number, token_decoded: any){
        // check role
        if (token_decoded.role == RoleDefault.USER){
            throw {message: 'Forbidden', status: 403};
        }
        if (roleId == RoleDefault.ROOT){
            const screens = await this.screensService.getAllScreens();
            let roleGroup = await this.roleGroupsRepository.findOne({id: roleId, deleted_at: null}, {relations: ['users']});
            roleGroup.screens = screens;
            return roleGroup;
        }

        return this.roleGroupsRepository.findOne({id: roleId, deleted_at: null}, {relations: ['screens','users']});
    }

    /**
     * This function get all items (transaction) by options and role
     * @param options (last_id, limit, name)
     * @param token_decoded (user_id, role, email)
     * @returns object {count, list_user, load_more}
     */
    async findAll(options: any, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ROLES || o.id == Screens.ACCOUNTS; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.roleGroupsRepository.createQueryBuilder('roleGroup')
            .leftJoinAndSelect('roleGroup.screens', 'screen')
            .where('roleGroup.deleted_at is null')
            .andWhere('roleGroup.id != :roleGroupId', {roleGroupId: RoleDefault.USER});

        // name
        if (options.name) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(RoleGroupEntity, 'RoleGroup')
                    .where('name like :name', {name: '%' + options.name + '%'})
                    .getQuery();
                return 'RoleGroup.id IN ' + subQuery;
            });
        }

        let count_query = query.getCount();
        let data_query = query.orderBy('roleGroup.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }
}
