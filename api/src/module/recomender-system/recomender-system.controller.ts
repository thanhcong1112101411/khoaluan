import { Controller, Get, Res, HttpStatus, Req } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { Response } from 'express';
import { RecomenderSystemService } from './recomender-system.service';

@ApiUseTags('recomender-system')
@Controller('api/recomender-system')
export class RecomenderSystemController {
    constructor(
        private readonly recomenderSystemService: RecomenderSystemService,
    ) { }

    @Roles(RolesType.All)
    @Get('hui')
    @ApiOperation({title: 'H-MINER'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    @ApiImplicitQuery({name: 'minUtility', description: 'min utility', required: true, type: Number})
    @ApiImplicitQuery({name: 'date_from', description: 'date from search. ex: 2020-05-07', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'date_to', description: 'date to search. ex: 2020-05-07', required: false, type: 'string'})
    async hui(@Req() request, @Res() res: Response){
        let minUtility;

        // check limit
        if (request.query.minUtility) {
            minUtility = Number(request.query.minUtility);
            if (isNaN(minUtility)) {
                return res.status(HttpStatus.OK).send(dataError('minUtility should be a number', null));
            }
        }

        let options = {
            minUtility,
            date_from: request.query.date_from,
            date_to: request.query.date_to,
        };

        try {
            const data = await this.recomenderSystemService.run(options);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', data));
        } catch (error) {
            return res.status(HttpStatus.OK).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
