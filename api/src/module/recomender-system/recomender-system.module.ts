import { Module } from '@nestjs/common';
import { RecomenderSystemController } from './recomender-system.controller';
import { RecomenderSystemService } from './recomender-system.service';
import { EnvoicesModule } from '../envoices/envoices.module';

@Module({
  imports: [
    EnvoicesModule,
  ],
  controllers: [RecomenderSystemController],
  providers: [RecomenderSystemService]
})
export class RecomenderSystemModule {}
