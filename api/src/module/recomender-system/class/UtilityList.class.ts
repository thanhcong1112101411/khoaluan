import { Element } from './Element.class';

export class UtilityList {
    item: number;  // the item
    sumIutils: number = 0;  // the sum of item utitlities
    sumRutils: number = 0;  // the sum of remaining utitlities
    elements: Array<Element> = []; // the elements

    /**
     * contructor
     * @param item 
     */
    constructor(item: number){
        this.item = item;
    }

    /**
	 * Method to add an element to this utility list and update the sums at the same time.
	 */
    addElement(element: Element){
        this.sumIutils += element.iutils;
        this.sumRutils += element.rutils;
        this.elements.push(element);
    }

    /**
	 * Get the support of the itemset represented by this utility-list
	 * @return the support as a number of trnsactions
	 */
    getSupport(): any{
        return this.elements.length;
    }


}