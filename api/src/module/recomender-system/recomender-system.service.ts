import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UtilityList } from './class/UtilityList.class';
import { Pair } from './class/Pair.class';
import { Element } from './class/Element.class';
import { HminerElement } from './hminer-class/HminerElement.class';
import { HminerPair } from './hminer-class/HminerPair.class';
import { HminerUtilityList } from './hminer-class/HminerUtilityList.class';
import { Any } from 'typeorm';
import { EnvoicesService } from '../envoices/envoices.service';

const readline = require('readline');
const fs = require('fs');
const lineReader = require('line-reader');
const HashMap = require('hashmap');
const _ = require('lodash');

@Injectable()
export class RecomenderSystemService {
    constructor(
        private readonly envoicesService: EnvoicesService,
        
    ) { }

    mapFMAP = new HashMap();
    result = new Array();
    envoice = [];

    delay(time) {
        return new Promise((resolve) => { 
            setTimeout(resolve, time)
        });
     }

    getItemTwu(){
        let mapItemToTWU = new HashMap();
        
        lineReader.eachLine('public/input.txt', async (line) => {
            if (line.includes('STOP')) {
                return mapItemToTWU;
            }
                let split = await line.split(":");
                let items = await split[0].split(" ");
                let transactionUtility = parseInt(split[1]);
                // for each item, we add the transaction utility to its TWU
				for(let i=0; i <items.length; i++){
                    let item = parseInt(items[i]);
                    // get the current TWU of that item
                    let twu = await mapItemToTWU.get(item);
                    // add the utility of the item in the current transaction to its twu
                    twu =  (twu == null)? 
                        transactionUtility : twu + transactionUtility;
                    await mapItemToTWU.set(item, twu);
                }
        });
        return mapItemToTWU;
    }
    async run(options: any){
        const minUtility = options.minUtility;
        const date_from = options.date_from;
        const dtate_to = options.date_to;
        console.log("1 - ", new Date()); 

        let mapItemToTWU = new HashMap();
        this.envoice = await this.envoicesService.getEnvoiceForHui();
        console.log(this.envoice);

        this.envoice.forEach(element => {
            let items = element.product;
            let transactionUtility = parseInt(element.twu);

            // for each item, we add the transaction utility to its TWU
				for(let i=0; i <items.length; i++){
                    let item = parseInt(items[i]);
                    // get the current TWU of that item
                    let twu = mapItemToTWU.get(item);
                    // add the utility of the item in the current transaction to its twu
                    twu =  (twu == null)? 
                        transactionUtility : twu + transactionUtility;
                    mapItemToTWU.set(item, twu);

                }
        });
        console.log("----------------mapItemToTWU --------------------")
        console.log(mapItemToTWU);

        await this.runHminerAlgorithm(mapItemToTWU, minUtility);

        
        // const minUtility = 6000;
        // let mapItemToTWU = new HashMap();
        
        // await lineReader.eachLine('public/input.txt', async (line) => {
            
        //     if (line.includes('STOP')) {
        //         console.log(mapItemToTWU);
        //         await this.runHminerAlgorithm(mapItemToTWU, minUtility);
        //     }
        //         let split = await line.split(":");
        //         let items = await split[0].split(" ");
        //         let transactionUtility = parseInt(split[1]);
        //         // for each item, we add the transaction utility to its TWU
		// 		for(let i=0; i <items.length; i++){
        //             let item = parseInt(items[i]);
        //             // get the current TWU of that item
        //             let twu = await mapItemToTWU.get(item);
        //             // add the utility of the item in the current transaction to its twu
        //             twu =  (twu == null)? 
        //                 transactionUtility : twu + transactionUtility;
        //             await mapItemToTWU.set(item, twu);

        //         }
        // });
    }
    
    async runHminerAlgorithm(mapItemToTWU: any, minUtility: number){
        //  We create a  map to store the TWU of each item
        
        // let mapItemToTWU = this.getItemTwu();
        // await this.delay(1000);

        // CREATE A LIST TO STORE THE UTILITY LIST OF ITEMS WITH TWU  >= MIN_UTILITY.
		let listOfUtilityLists = new Array<HminerUtilityList>();
		// CREATE A MAP TO STORE THE ITEM OF TRANSACTION WITH SIZE AT THAT TIME
		// Key : item list string    Value :  size at that time
        let mapTransactionListPosition = new HashMap();
        
        // For each item
        await mapItemToTWU.forEach(async function(value, key) {
            if(mapItemToTWU.get(key) >= minUtility){
                // create an empty Utility List that we will fill later.
                let uList = new HminerUtilityList(key);
                // add the item to the list of high TWU items
				await listOfUtilityLists.push(uList);
            }
        });
        // sort 
        await listOfUtilityLists.sort(function (a,b) {
            let compare = mapItemToTWU.get(a.item) - mapItemToTWU.get(b.item);
            // if the same, use the lexical order otherwise use the TWU
            return (compare == 0)? a.item - b.item :  compare;
        })

        // variable to count the number of transaction
        let tid = 1;
        
       
        // for each line (transaction) until the end of file
        this.envoice.forEach(e => {
            // if (line.includes('STOP')) {
        
            //     // await this.printListOfUtilityLists(listOfUtilityLists);
            //     console.log("2 - ", new Date());

            //     await this.hMiner([], listOfUtilityLists, minUtility, mapItemToTWU, mapTransactionListPosition);
            //     console.log("-------------------------------------------");
                
            //     // write file
            //     var file = fs.createWriteStream('public/output2.txt');
            //     file.on('error', (err) => { /* error handling */ });
            //     this.result.forEach(v => { 
            //         const data = v.sor.trim() + ":" + v.twu + '\n';
            //         file.write(data);
            //     });
            //     file.write("STOP");
            //     file.end();
                
            //     // console.log(this.result);
            //     console.log(this.result.length);
            //     console.log("3 - ", new Date());
            //     this.result = [];
            //     return;
            // }

            // console.log("-------------------------- TID: " + tid.toString() + "----------------");
            // split the line according to the separator
			//let split = line.split(":");
			// get the list of items
			let items = e.product;
			// get the list of utility values corresponding to each item
			// for that transaction
			let utilityValues = e.u;
				
			// Copy the transaction into lists but 
			// without items with TWU < minutility
				
            let remainingUtility =0;
            // Create a list to store items
            let revisedTransaction = new Array<HminerPair>();
            // create hash map key
            let hashKey = "";
            let newTWU = 0;
            // for each item
            for(let i=0; i <items.length; i++){
                /// convert values to integers
                let pair = new HminerPair();
                pair.item = parseInt(items[i]);
                pair.utility = parseInt(utilityValues[i]);
                // if the item has enough utility
                if(mapItemToTWU.get(pair.item) >= minUtility){
                    revisedTransaction.push(pair);
                    // remainingUtility += pair.utility;
                    newTWU += pair.utility; // NEW OPTIMIZATION
                }
            }

            // sort 
            revisedTransaction.sort(function (a,b) {
                let compare = mapItemToTWU.get(a.item) - mapItemToTWU.get(b.item);
                // if the same, use the lexical order otherwise use the TWU
                return (compare == 0)? a.item - b.item :  compare;
            })
            // console.log("------------revisedTransaction---------");
            // console.log(revisedTransaction);

            // check if duplicate transaction exists
            let keyCheck = "";
            for(let pair of revisedTransaction){
                keyCheck += pair.item.toString();
            }
            // console.log("------------- HASH MAP ----------------------");
            // console.log(mapTransactionListPosition);
            const valueHashMap = mapTransactionListPosition.get(keyCheck.trim());
            if(valueHashMap){
                // console.log("exist");
                // for each item left in the transaction
                for(let i = revisedTransaction.length - 1; i >= 0; i--){
                    const pair = revisedTransaction[i];
                        
                    // // Add a new Element to the utility list of this item corresponding to this transaction
                    // let element = await new HminerElement(tid, pair.utility, remainingUtility, 0, ppos);
                    
                    const index = listOfUtilityLists.findIndex((value) =>{
                        return value.item == pair.item;
                    })
                    let oldElement = this.findHminerElementWithTID(listOfUtilityLists[index], valueHashMap);

                    // update element
                    listOfUtilityLists[index].updateElement(oldElement.element, pair.utility, remainingUtility, oldElement.index);

                    // subtract the utility of this item from the remaining utility
                    remainingUtility = remainingUtility + pair.utility;
                }
            }else{
                // console.log("non-exist");
                // for each item left in the transaction
                for(let i = revisedTransaction.length - 1; i >= 0; i--){
                    const pair = revisedTransaction[i];
                    hashKey = pair.item.toString() + hashKey + " ";

                    // compute PPOS
                    let ppos = 0;
                    if (i == 0){
                        ppos = -1;
                    }else{
                        // previously item
                        const item = revisedTransaction[i-1].item;
                        // position of previously item in listOfUtilityLists
                        const indexItem = listOfUtilityLists.findIndex(value => {
                            return value.item == item;
                        })
                        // ppos is size of CULs[previously item].tidlist
                        ppos = listOfUtilityLists[indexItem].elements.length;
                    }
                        
                    // Add a new Element to the utility list of this item corresponding to this transaction
                    let element = new HminerElement(tid, pair.utility, remainingUtility, 0, ppos);
                    
                    const index = listOfUtilityLists.findIndex((value) =>{
                        return value.item == pair.item;
                    })
                    listOfUtilityLists[index].addElement(element);

                    // subtract the utility of this item from the remaining utility
                    remainingUtility = remainingUtility + pair.utility;
                }
            }

            
            // save hash table
            if(hashKey != ""){
                mapTransactionListPosition.set(hashKey.trim(), tid);
            }

            // Build EUCS
            for(let i=revisedTransaction.length - 1; i>=0; i--){
                let pair = revisedTransaction[i];
                let mapFMAPItem = this.mapFMAP.get(pair.item);
                if (mapFMAPItem == null){
                    mapFMAPItem = new HashMap();
                    this.mapFMAP.set(pair.item, mapFMAPItem);
                }

                for(let j=i+1; j < revisedTransaction.length; j++){
                    let pairAfter = revisedTransaction[j];
                    let twuSum = mapFMAPItem.get(pairAfter.item);
                    if(twuSum == null){
                        mapFMAPItem.set(pairAfter.item, newTWU);
                        this.mapFMAP.set(pair.item, mapFMAPItem);
                    }else{
                        mapFMAPItem.set(pairAfter.item, twuSum + newTWU);
                        this.mapFMAP.set(pair.item, mapFMAPItem);
                    }
                }
            }

            // console.log("-------------------- EUCS -------------------------")
            // console.log(this.mapFMAP);
            
            tid++; // increase tid number for next transaction

        });
        // await this.printListOfUtilityLists(listOfUtilityLists);
        console.log("2 - ", new Date());

        await this.hMiner([], listOfUtilityLists, minUtility, mapItemToTWU, mapTransactionListPosition);
        console.log("-------------------------------------------");
        
        // write file
        var file = fs.createWriteStream('public/output2.txt');
        file.on('error', (err) => { /* error handling */ });
        this.result.forEach(v => { 
            const data = v.sor.trim() + ":" + v.twu + '\n';
            file.write(data);
        });
        file.write("STOP");
        file.end();
        
        // console.log(this.result);
        console.log(this.result.length);
        console.log("3 - ", new Date());
        this.result = [];
        
    }

    /**
	 * This is the recursive method to find all high utility itemsets. It writes
	 * the itemsets to the output file.
	 * @param prefix  This is the current prefix. Initially, it is empty.
	 * @param pUL This is the Utility List of the prefix. Initially, it is empty.
	 * @param ULs The utility lists corresponding to each extension of the prefix.
	 * @param minUtility The minUtility threshold.
	 * @param prefixLength The current prefix length
	 * @throws IOException
	 */
	async hMiner(prefix: Array<number>, ULs: Array<HminerUtilityList>, minUtility: number, mapItemToTWU: any, mapTransactionListPosition: any){
        
        
        // const promises = await ULs.map(async (ig, index) => {
        //     const test = await this.test(prefix, ig, index, ULs, minUtility, mapItemToTWU, mapTransactionListPosition);
        //     return test;
            
        // })
        // await Promise.all(promises);
        
        // For each extension X of prefix P
        for(let i=0; i< ULs.length; i++){
            let X = ULs[i];

            // let sorted_prefix = new Array<number>(prefix.length + 1);
            // sorted_prefix = prefix.slice();
            let sorted_prefix = prefix.slice();
            sorted_prefix.push(X.item);

            // If pX is a high utility itemset.
            // we save the itemset:  pX 
            const sumUtils = X.sumNutils + X.cutils;
            const sumRutils = X.sumNRutils + X.crutils;

            if( sumUtils >= minUtility){
                
                let prefix = "";
                await sorted_prefix.forEach(async element => {
                    prefix = prefix + element + " ";
                });
                let item = {sor: prefix, item: X.item, twu: sumUtils};
                
                await this.result.push(item);
            }
            
            // If the sum of the remaining utilities for pX
            // is higher than minUtility, we explore extensions of pX.
            // (this is the pruning condition)
            if(await (sumUtils + sumRutils) >= minUtility){
                // This list will contain the utility lists of pX extensions.
                let exULs = new Array<HminerUtilityList>();

                exULs = await this.HminerConstruct(X, ULs, i, minUtility, sorted_prefix.length, mapItemToTWU, mapTransactionListPosition);
                
                // // We create new prefix pX
                // prefix[prefixLength] = X.item;
                
                // We make a recursive call to discover all itemsets with the prefix pXY
                await this.hMiner(sorted_prefix, exULs, minUtility, mapItemToTWU, mapTransactionListPosition); 
            }
        }
    
    }

    async test(prefix: Array<number>, item: HminerUtilityList, index: number, ULs: Array<HminerUtilityList>, minUtility: number, mapItemToTWU: any, mapTransactionListPosition: any ){
    
        let X = item;

        // let sorted_prefix = new Array<number>(prefix.length + 1);
        // sorted_prefix = prefix.slice();
        let sorted_prefix = prefix.slice();
        sorted_prefix.push(X.item);

        // If pX is a high utility itemset.
        // we save the itemset:  pX 
        const sumUtils = X.sumNutils + X.cutils;
        const sumRutils = X.sumNRutils + X.crutils;

        if( sumUtils >= minUtility){
                
            let prefix = "";
            await sorted_prefix.forEach(async element => {
                prefix = prefix + element + " ";
            });
            let item = {sor: prefix, item: X.item, twu: sumUtils};
                
            await this.result.push(item);
        }
            
        // If the sum of the remaining utilities for pX
        // is higher than minUtility, we explore extensions of pX.
        // (this is the pruning condition)
        if(await (sumUtils + sumRutils) >= minUtility){
            // This list will contain the utility lists of pX extensions.
            let exULs = new Array<HminerUtilityList>();

            exULs = await this.HminerConstruct(X, ULs, index, minUtility, sorted_prefix.length, mapItemToTWU, mapTransactionListPosition);
                
            // // We create new prefix pX
            // prefix[prefixLength] = X.item;
                
            // We make a recursive call to discover all itemsets with the prefix pXY
            await this.hMiner(sorted_prefix, exULs, minUtility, mapItemToTWU, mapTransactionListPosition); 
        }
    }

    /**
     * This method constructs the utility list of pXY
     * @param X 
     * @param Uls 
     * @param position 
     * @param minUtility 
     */
	async HminerConstruct(X: HminerUtilityList, CULs: Array<HminerUtilityList>, st: number, minUtility: number, length: number, mapItemToTWU: any, mapTransactionListPosition: any) {
        
        let exCULs = new Array<HminerUtilityList>();
        let LAU = [];
        let CUTIL = [];
        let ey_tid = [];

        for(let i=0; i< CULs.length - 1; i++){
            let uList = new HminerUtilityList(CULs[i].item);
            exCULs.push(uList);
            LAU.push(0);
            CUTIL.push(0);
            ey_tid.push(0);
        }

        let sz = CULs.length - (st + 1);
        let extSz = sz;
        
        for(let j = st + 1; j <= CULs.length - 1; j++){
            let mapFAMPItem = await this.mapFMAP.get(X.item);
            if (mapFAMPItem != null){
                let twu = await mapFAMPItem.get(CULs[j].item);

                if (twu != null && twu < minUtility){
                    exCULs[j] = null;
                    extSz = sz - 1;
                }else{
                    let uList = new HminerUtilityList(CULs[j].item);
                    exCULs[j] = uList;
                    ey_tid[j] = 0;
                    LAU[j] = X.cutils + X.crutils + X.sumNutils + X.sumNRutils; // La-Prune
                    CUTIL[j] = X.cutils + X.crutils; // C-prune
                }
            }
            
        }

        // console.log("------------------------ exCULs------------------------");
        // console.log(exCULs);

        // HT = HashMap<Array<Integer>, Integer>
        let HT = new HashMap();
        let newT = [];

        for (let ex of X.elements){
            newT = [];

            for(let j= st + 1; j <= CULs.length -1 ; j++){
                if(exCULs[j] == null){
                    continue;
                }

                let eylist = CULs[j].elements;

                while(ey_tid[j] < eylist.length && eylist[ey_tid[j]].tid < ex.tid){
                    ey_tid[j] = ey_tid[j] + 1;
                }

                if (ey_tid[j] < eylist.length && eylist[ey_tid[j]].tid == ex.tid){
                    const index= newT.indexOf(j);
                    if(index == -1){
                        newT.push(j);
                    }
                    
                }else // apply LA prune
                {
                    LAU[j] = LAU[j] - ex.nutils - ex.nrutils;
                    if(LAU[j] < minUtility){
                        exCULs[j] = null;
                        extSz = extSz - 1;
                    }
                }
            } // end for line 24 of 1C

            // console.log("--------------- ey_tid update--------------------");
            // console.log(ey_tid);
            // console.log("---------------- newT ----------------------");
            // console.log(newT);
            // console.log("------------------ LAU --------------------------");
            // console.log(LAU);

            if(newT.length == extSz){
                this.updateClosed(X, CULs, st, exCULs, newT, ex, ey_tid);
            }else{
                if (newT.length == 0){
                    continue;
                }
                let remainingUtility = 0;
                if(!HT.has(newT)){
                    // console.log("non exist");
                    HT.set(newT,exCULs[newT[newT.length - 1]].elements.length);

                    // Insert new entries in exCULs for each newT
                    for (let i = newT.length - 1; i >= 0; i--) {
                        let CULListOfItem = exCULs[newT[i]];
                        let Y = CULs[newT[i]].elements[ey_tid[newT[i]]];

                        // Add a new Element to the utility list of this
                        // item corresponding to this transaction
                        let element = new HminerElement(ex.tid, ex.nutils + Y.nutils - ex.putils,
                                remainingUtility, ex.nutils, 0);

                        if (i > 0)// PPOS
                            element.ppos = exCULs[newT[i - 1]].elements.length;
                        else
                            element.ppos = -1;

                        CULListOfItem.addElement(element);
                        exCULs[newT[i]] = CULListOfItem;
                        remainingUtility += Y.nutils - ex.putils; // changed check

                    }
                }else // duplicate transaction
                {
                    // console.log("exits");
                    let dupPos = HT.get(newT);
                    this.updateElement(X, CULs, st, exCULs, newT, ex, dupPos, ey_tid);
                }
            }

            for (let j = st + 1; j <= CULs.length - 1; j++){
                CUTIL[j] = CUTIL[j] + ex.nutils + ex.nrutils;
            }

            // console.log("-------------------- exCULs next------------------");
            // await this.printListOfUtilityLists(exCULs);
                
    
        }; 

        // console.log("------------------ CUTIL -------------------");
        // console.log(CUTIL);

        // console.log("------------------ HT -------------------");
        // console.log(HT);

        // console.log("------------exCULs-----------");
        // await this.printListOfUtilityLists(exCULs);

        // filter
		let filter_CULs = new Array<HminerUtilityList>();
		for (let j = st + 1; j <= CULs.length; j++) {
			if (CUTIL[j] < minUtility || exCULs[j] == null) {
				continue;

			} else {
				if (length > 1) {
					exCULs[j].cutils += CULs[j].cutils + X.cutils - X.cputils;
					exCULs[j].crutils += CULs[j].crutils;
					exCULs[j].cputils += X.cutils;
				}
				filter_CULs.push(exCULs[j]);

			}
        }
        // console.log("------------filter_CULs-----------");
        // await this.printListOfUtilityLists(filter_CULs);
		return filter_CULs;
	}

    /**
	 * Do a binary search to find the element with a given tid in a utility list
	 * @param ulist the utility list
	 * @param tid  the tid
	 * @return  the element or null if none has the tid.
	 */
	findHminerElementWithTID(ulist: HminerUtilityList, tid: number){
        let list = ulist.elements;
        if (!list){
            return null;
        }
		// perform a binary search to check if  the subset appears in  level k-1.
        let first = 0;
        let last = list.length - 1;
       
        // the binary search
        while( first <= last )
        {
        	let middle = ( first + last ) >>> 1; // divide by 2

            if(list[middle].tid < tid){
            	first = middle + 1;  //  the itemset compared is larger than the subset according to the lexical order
            }
            else if(list[middle].tid > tid){
            	last = middle - 1; //  the itemset compared is smaller than the subset  is smaller according to the lexical order
            }
            else{
                return {
                    element: list[middle],
                    index: middle,
                }
            }
        }
        return null;
        
    }

    async printListOfUtilityLists(list: Array<HminerUtilityList>){
        let printStr1 = "";
        let printStr2 = "";
        for(let utility of list){
            if(utility == null){
                // console.log(null);
            }else{
                printStr1 = utility.item.toString() + " - " + utility.sumNutils.toString() + " " + utility.sumNRutils.toString() + " " + utility.cutils.toString() + "/" + utility.crutils.toString() + "/" + utility.cputils.toString();
                console.log(printStr1);

                for(let element of utility.elements){
                    printStr2 = element.tid.toString() + " " + element.nutils.toString() + " " + element.nrutils.toString() + " " + element.putils.toString() + " " + element.ppos.toString();
                    console.log(printStr2);
                }
            }
        }
    }

    async printListOfUtilityListContruct(list: Array<HminerUtilityList>, ey: any, LAU: any, CUTIL: any){
        if (list.length == 0){
            console.log("null");
            return;
        }
        let printStr1 = "";
        let printStr2 = "";
        let printStr3 = "";
        for( let i=0; i < list.length; i++){
            let utility = list[i];
            //printStr1 = utility.sumNutils.toString() + " " + utility.sumNRutils.toString() + " " + utility.cutils.toString() + "/" + utility.crutils.toString() + "/" + utility.cputils.toString();
            printStr1 = utility.item.toString();
            console.log(printStr1);
            printStr3 = "ey: " + ey[i].toString() + "; LAU: " + LAU[i].toString() + "; CUTIL: " + CUTIL[i].toString();
            console.log(printStr3);

            if(utility.elements){
                for(let element of utility.elements){
                    printStr2 = element.tid.toString() + " " + element.nutils.toString() + " " + element.nrutils.toString() + " " + element.putils.toString() + " " + element.ppos.toString();
                    console.log(printStr2);
                }
            }
        }
    }

    /** This method check if the combination pXY of two itemsets
	 * pX and pY and extensions of pXY 
	 * should be pruned according to the PU-Prune property.
	 * @param X  the utility-list of item pX
	 * @param Y  the utility-list of itemset pY
	 * @param minUtil the minUtility threshold
	 * @return  true if pXY and its extensionsshould be pruned
	 */
	// async EUCSPrune(X: HminerUtilityList, Y: HminerUtilityList, minUtil: number, mapItemToTWU: any) {
    //     let sum = 0;
    //     let listElementX = X.elements;
	// 	// for each partition 
	// 	for(let i = 0; i < listElementX.length; i++) {
    //         const tid = listElementX[i].tid;
    //         // if Y appear in that partition
    //         if(await this.findHminerElementWithTID(Y, tid)){
    //             sum += mapItemToTWU.get(tid);
    //         };
	// 	}
	// 	// if the sum is less than minutil, we can prune pXY and its supersets
	// 	return sum < minUtil;
    // }
    
    async CSCheck(x: string, t: number, mapItemToTWU: any, mapTransactionListPosition: any){
        if(x.length < 1){
            return false;
        }
        const Cfirst = x.substr(x.length - 1,1);
        const Sfirst = x.substr(x.length - 2,1);
        let Cquantity = 0;
        let Squantity = 0;

        // compute quantity of C
        
        const minTWU = await mapItemToTWU.get(parseInt(Cfirst));
        await mapItemToTWU.forEach( async(value, key) => {
            if(value >= minTWU){
                Cquantity++;
            }
        })

        // compute quantity of S
        const transactionStr: string = await mapTransactionListPosition.search(t);
        for(let i=0; i< transactionStr.length -1 ; i++){
            if (transactionStr.charAt(i) === Sfirst){
                Squantity = transactionStr.length - 1 - i;
                break;
            }
        }

        if(Cquantity == Squantity){
            return true;
        }
        return false;
    }

    async updateClosed(X: HminerUtilityList, CULs: Array<HminerUtilityList>, st: number,
        exCULs: Array<HminerUtilityList>, newT: Array<number>, ex: HminerElement, 
        ey_tid: Array<number>){


        let nru = 0;
		for (let j = newT.length - 1; j >= 0; j--) {
			// matched extension at location st+j, j in newT.
			// System.out.println("st: "+st+" newT j: "+newT.get(j));
			let ey = CULs[newT[j]];
			let eyy = ey.elements[ey_tid[newT[j]]];
			// System.out.println(ex.Nu+ey.elements.get(ey_tid.get(newT.get(j))).Nu
			// -ex.Pu);
			exCULs[newT[j]].cutils += ex.nutils + eyy.nutils - ex.putils;
			exCULs[newT[j]].crutils += nru;
			exCULs[newT[j]].cputils += ex.nutils; // ex.Pu or ex.Nu?
			nru = nru + eyy.nutils - ex.putils;
		}

    }

    async updateElement(X: HminerUtilityList, CULs: Array<HminerUtilityList>, st: number,
        exCULs: Array<HminerUtilityList>, newT: Array<number>, ex: HminerElement, dupPos: number,
        ey_tid: Array<number>){
        
        let nru = 0;
		let pos = dupPos;
		for (let j = newT.length - 1; j >= 0; j--) {
			let ey = CULs[newT[j]];
			let eyy = ey.elements[ey_tid[newT[j]]];
			// System.out.println(exCULs.get(newT.get(j)).elements.size()+" "+pos);
			exCULs[newT[j]].elements[pos].nutils += ex.nutils + eyy.nutils - ex.putils;
			exCULs[newT[j]].sumNutils += ex.nutils + eyy.nutils - ex.putils;
			exCULs[newT[j]].elements[pos].nrutils += nru;
			exCULs[newT[j]].sumNRutils += nru;
			exCULs[newT[j]].elements[pos].putils += ex.nutils;
			nru = nru + eyy.nutils - ex.putils;
			// pos should come from exCULs only.
			// pos=ey.elements.get(ey_tid.get(newT.get(j))).Ppos;
			pos = exCULs[newT[j]].elements[pos].ppos;

		}
    }
}
