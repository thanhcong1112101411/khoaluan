import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { RoleDefault, Screens } from '../../utils/enum.util';
import _ from 'lodash';
import { UnitsEntity } from './units.entity';
import { CreateUnitDto } from './dto/createUnit.dto';
import { UpdateUnitDto } from './dto/updateUnit.dto';

@Injectable()
export class UnitsService {
    constructor(
        @InjectRepository(UnitsEntity)
        private readonly unitsRepository: Repository<UnitsEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
    ) { }

    /**
     * this function add unit 
     * @param body 
     * @param token_decoded (user_id, role, email)
     */
    async insert(body: CreateUnitDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.UNITS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check name
        const unitCheck = await this.findUnitByName(body.name);
        if (!!unitCheck)
            throw {message: 'Tên đơn vị tính đã tồn tại'}
        
        let unit = new UnitsEntity();
        unit.name = body.name;

        const data = await this.unitsRepository.save(unit);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Đơn vị tính',
            'Thêm đơn vị tính "' + unit.name + '('+ unit.id +')"',
        )

        return data;
    }

    /**
     * This function get all units (transaction) by options
     * @param options (current_page, limit, name)
     * @returns object {pages, count, list_user, load_more}
     */
    async findAll(options: any){

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.unitsRepository.createQueryBuilder('unit')
            .where('unit.deleted_at is null');

        // name
        if (options.name) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(UnitsEntity, 'unit')
                    .where('name like :name', {name: '%' + options.name + '%'})
                    .getQuery();
                return 'unit.id IN ' + subQuery;
            });
        }

        let count_query = query.getCount();
        let data_query = query.orderBy('unit.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * the function update unit by body
     * @param id 
     * @param body 
     * @param token_decoded (user_id, role, email)
     */
    async update(id: number, body: UpdateUnitDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.UNITS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check name if name changed
        let unit = await this.findunitById(id);
        if (!unit)
            throw {message: "unit is not exitst"}

        if (unit.name !== body.name){
            const unitCheck = await this.findUnitByName(body.name);
            if (!!unitCheck)
                throw {message: "Tên đơn vị tính đã tồn tại!"}
        }

        unit.name = body.name;

        const data = await this.unitsRepository.save(unit);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Đơn vị tính',
            'Cập nhật đơn vị tính "' + unit.name + '('+ unit.id +')"',
        )

        return data;
    }

    /**
     * this function delete unit
     * @param id 
     * @param token_decoded (user_id, role, email)
     */
    async delete(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.UNITS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        const unit = await this.findunitById(id);
        if (!unit)
            throw {message: 'unit is not exist'}

        // delete role group
        unit.deleted_at = new Date();
        await this.unitsRepository.save(unit);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Đơn vị tính',
            'Xóa đơn vị tính "' + unit.name + '('+ unit.id +')"',
        )
    }

    /**
     * the function get unit detail
     * @param id 
     */
    async getDetail(id: number){
        const unit = await this.findunitById(id);

        if (!unit)
            throw {message: 'brand is not exist'}
        return unit;
    }

    async getAllUnit(){
        return this.unitsRepository.createQueryBuilder("unit")
            .where("unit.deleted_at is null")
            .getMany();
    }

    /**
     * the function find unit by name
     * @param name 
     */
    async findUnitByName(name: string){
        return this.unitsRepository.createQueryBuilder('unit')
            .where('deleted_at is null')
            .andWhere('unit.name = :name', {name: name})
            .getOne();
    }

    /**
     * the function find unit by id
     * @param name 
     */
    async findunitById(id: number){
        return this.unitsRepository.createQueryBuilder('unit')
            .where('deleted_at is null')
            .andWhere('unit.id = :id', {id: id})
            .getOne();
    }
}
