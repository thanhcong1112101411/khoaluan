import { Module } from '@nestjs/common';
import { UnitsController } from './units.controller';
import { UnitsService } from './units.service';
import { UnitsEntity } from './units.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([UnitsEntity]),
      RoleGroupsModule,
      HistoryModule,
    ] ,
    exports: [
      UnitsService,
    ],
    controllers: [UnitsController],
    providers: [UnitsService]
})
export class UnitsModule {}
