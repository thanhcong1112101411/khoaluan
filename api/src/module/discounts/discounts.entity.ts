import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable, ViewEntity, Connection, ViewColumn, OneToOne } from 'typeorm';
import { Exclude } from 'class-transformer';
import { ProductsEntity } from '../products/products.entity';
import { DiscountView } from './discountsView.entity';

@Entity('discounts')
export class DiscountEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    name: string;

    @Column({
        type: 'int',
    })
    amount: number;

    @Column({
        type: 'timestamp',
        precision: null,
    })
    date_from: Date;

    @Column({
        type: 'timestamp',
        precision: null,
    })
    date_to: Date;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    /*************************
     * Key           *
     *************************/

    // @OneToOne(type => DiscountView, discountView => discountView.discount)
    // discountView: DiscountView;

    @ManyToMany(type => ProductsEntity)
    @JoinTable()
    products: ProductsEntity[];
}
