import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class CreateDiscountDto {

    @IsNotEmpty({ message: 'name is require' })
    @IsNotBlank('name', { message: 'name is not white space' })
    @ApiModelProperty({ example: 'Hải sản' })
    name: string;

    @IsNotEmpty({ message: 'products is require' })
    @IsNotBlank('products', { message: 'products is not white space' })
    @ApiModelProperty({ example: [1,2,3] })
    products: any;

    @IsNotEmpty({ message: 'amout is require' })
    @ApiModelProperty({ example: 20 })
    amout: number;

    @IsNotEmpty({ message: 'date_from is require' })
    @IsNotBlank('date_from', { message: 'date_from is not white space' })
    @ApiModelProperty({ example: '2020-05-06' })
    date_from: string;

    @IsNotEmpty({ message: 'date_to is require' })
    @IsNotBlank('date_to', { message: 'date_to is not white space' })
    @ApiModelProperty({ example: '2020-05-06' })
    date_to: string;
}
