import { Module } from '@nestjs/common';
import { DiscountsController } from './discounts.controller';
import { DiscountsService } from './discounts.service';
import { DiscountEntity} from './discounts.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistoryModule } from '../history/history.module';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { DiscountView } from './discountsView.entity';
import { ProductsModule } from '../products/products.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([DiscountEntity]),
      TypeOrmModule.forFeature([DiscountView]),
      RoleGroupsModule,
      HistoryModule,
      ProductsModule,
    ] ,
    controllers: [DiscountsController],
    providers: [DiscountsService]
})
export class DiscountsModule {}
