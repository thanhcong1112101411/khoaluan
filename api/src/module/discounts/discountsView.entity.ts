import {ViewEntity, ViewColumn, Connection, OneToOne, JoinColumn} from "typeorm";
import { DiscountEntity } from "./discounts.entity";


@ViewEntity('discounts_view')
export class DiscountView{

    @ViewColumn()
    id: number;

    @ViewColumn()
    name: string;

    @ViewColumn()
    amount: number;

    @ViewColumn()
    date_from: Date;

    @ViewColumn()
    date_to: Date;

    @ViewColumn()
    created_at: Date;

    @ViewColumn()
    updated_at: Date;

    @ViewColumn()
    deleted_at: Date;

    /*************************
     * Key           *
     *************************/

    // @OneToOne(type => DiscountEntity, discount => discount.discountView)
    // discount: DiscountEntity;
}