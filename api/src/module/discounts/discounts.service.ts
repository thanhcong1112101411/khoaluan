import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager, getManager } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { RoleDefault, Screens, DiscountType } from '../../utils/enum.util';
import { DiscountEntity} from './discounts.entity';
import { CreateDiscountDto } from './dto/createDiscount.dto';
import _ from 'lodash';
import { View } from 'typeorm/schema-builder/view/View';
import { DiscountView } from './discountsView.entity';
import { ProductsService } from '../products/products.service';
const moment = require('moment');

@Injectable()
export class DiscountsService {
    constructor(
        @InjectRepository(DiscountEntity)
        private readonly discountRepository: Repository<DiscountEntity>,

        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
        private readonly productsService: ProductsService,
    ) { }

    /**
     * this function add brand 
     * @param body 
     * @param token_decoded (user_id, role, email)
     */
    async insert(body: CreateDiscountDto, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.DISCOUNTS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }
        // check name
        const checkName = await this.findDiscountByName(body.name);
        if (checkName)
            throw {message: 'Tên giảm giá đã tồn tại'}

        // check date
        if(moment(body.date_from) <= moment()){
            throw {message: 'date from must be greater current date'}
        }

        if(moment(body.date_to) < moment(body.date_from)){
            throw {message: 'date to must be greater or equal with date from'}
        }

        let products = [];
        await Promise.all(body.products.map(async(element) => {
            // check in date_from to date_to have apply any discount?
            let isExist = await this.productsService.isProductByIdWithDiscount(element, body.date_from, body.date_to);
      
            if (!isExist){
                const productAdd = await this.productsService.findProductById(element);
                products.push(productAdd);
            }else{
                throw{message: "product is not exist or product had discount"};
            }
        }))

        // add
        let discount = new DiscountEntity();
        discount.name = body.name;
        discount.amount = body.amout;
        discount.date_from = new Date(body.date_from + " 00:00:00");
        discount.date_to = new Date(body.date_to + " 23:59:59");
        discount.products = products;

        const data = await this.discountRepository.save(discount);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Giảm giá',
            'Thêm giảm giá "' + data.name + '('+ data.id +')"',
        )

        return data;
        
    }

    /**
     * Get discount detail by id
     * @param id 
     * @param token_decoded 
     */
    async getDetail(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.DISCOUNTS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        return this.discountRepository.createQueryBuilder('discount')
            .where('discount.deleted_at is null')
            .andWhere('discount.id = :id', {id: id})
            .leftJoinAndSelect('discount.products','product')
            .getMany();
    }

    async delete(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.DISCOUNTS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }
        
        const discount = await this.findDiscountById(id);
        if (!discount){
            throw {message: 'discount is not exist'}
        }
         
        discount.deleted_at = new Date();
        
        await this.discountRepository.save(discount);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Giảm giá',
            'Xóa giảm giá "' + discount.name ,
        )

    }

    /**
     * This function get all news (transaction) by options
     * @param options (current_page, limit, name)
     * @returns object {pages, count, list_user, load_more}
     */
    async findAll(options: any, token_decoded: any){

        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.DISCOUNTS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = '';

        // type
        if (options.type){
            if (options.type != DiscountType.APPLING && options.type != DiscountType.APLLIED && options.type != DiscountType.WATING && options.type != DiscountType.DELETED)
                throw {message: 'type is not exist'}
            
            if (options.type == DiscountType.APPLING){
                query = ' FROM discounts_view ';
            }
            if (options.type == DiscountType.APLLIED){
                query = ' FROM discounts WHERE discounts.date_to < CURRENT_DATE() AND  discounts.deleted_at is null ';
            }
            if (options.type == DiscountType.WATING){
                query = ' FROM discounts WHERE discounts.date_from > CURRENT_DATE() AND discounts.deleted_at is null ';
            }
            if (options.type == DiscountType.DELETED){
                query = ' FROM discounts WHERE discounts.deleted_at is not null ';
            }
        }

        // search name
        if(options.name){
            if (options.type == DiscountType.APPLING){
                query += ' WHERE discount.name like "%' + options.name + '%"';
            }else{
                query += ' AND discount.name like "%' + options.name + '%"';
            }
        }

        console.log(query);

        const query_count = "SELECT COUNT(*) as quantity " + query;
        const query_get_data = "SELECT * " + query + " ORDER BY created_at DESC LIMIT " + start + ", " + query_limit;

        let entityManger = getManager();

        let results = await Promise.all([
            entityManger.query(query_count),
            entityManger.query(query_get_data),
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(parseInt(results[0][0].quantity) / query_limit);

        return {
            pages: pages,
            count: parseInt(results[0][0].quantity),
            data: results[1],
            load_more: is_load_more,
        };
    }

    /**
     * find discount by id
     * @param id 
     */
    async findDiscountById(id: number){
        return this.discountRepository.createQueryBuilder('discount')
            .where('discount.id =: id', {id: id})
            .andWhere('discount.deleted_at is null')
            .getOne();
    }

    /**
     * find discount by name
     * @param name 
     */
    async findDiscountByName(name: string){
        return this.discountRepository.createQueryBuilder('discount')
            .where('discount.name = :name', {name: name})
            .getOne();
    }

}
