import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '../../../common/decorators/blank.decorator';

export class UpdateNewsDto {

    @IsNotEmpty({ message: 'title is require' })
    @IsNotBlank('title', { message: 'title is not white space' })
    @ApiModelProperty({ example: 'Những loại thực phẩm được ưa thích' })
    title: string;

    @IsNotEmpty({ message: 'content is require' })
    @IsNotBlank('content', { message: 'content is not white space' })
    @ApiModelProperty({ example: '<p>Những loại thực phẩm được ưa thích</p>' })
    content: string;
}
