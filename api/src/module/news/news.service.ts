import { Injectable } from '@nestjs/common';
import { NewsEntity } from './news.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoleGroupsService } from '../role-groups/role-groups.service';
import { HistoryService } from '../history/history.service';
import { CreateNewsDto } from './dto/createNews.dto';
import { Screens, RoleDefault } from '../../utils/enum.util';
import _ from 'lodash';
import { UsersService } from '../users/users.service';
import { UpdateNewsDto } from './dto/updateNews.dto';

@Injectable()
export class NewsService {
    constructor(
        @InjectRepository(NewsEntity)
        private readonly newsRepository: Repository<NewsEntity>,
        private readonly roleGroupService: RoleGroupsService,
        private readonly historyService: HistoryService,
        private readonly userService: UsersService,
    ) { }

    /**
     * the function add news
     * @param body 
     * @param image 
     * @param token_decoded 
     */
    async insert(body: CreateNewsDto, image: string, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.NEWS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check user
        const user = await this.userService.findOneUserById(token_decoded.user_id);
        if (!user)
            throw {message: 'account is not exist or blocked'};

        // add
        let news = new NewsEntity();
        news.title = body.title;
        news.content = body.content;
        news.image = image;
        news.click = 0;
        news.user = user;

        const data = await this.newsRepository.save(news);
        delete data.user;

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Tin tức',
            'Thêm tin tức "' + data.title.substr(0,50) + '... ('+ data.id +')"',
        )

        return data;
        
    }

    /**
     * This function get all news (transaction) by options
     * @param options (current_page, limit, name)
     * @returns object {pages, count, list_user, load_more}
     */
    async findAll(options: any){

        const current_page = options.current_page || 1;
        const query_limit = Number(options.limit).valueOf() || 10;
        let start = (current_page - 1) * query_limit;
        let is_load_more = false;

        let query = this.newsRepository.createQueryBuilder('news')
            .leftJoinAndSelect("news.user", "user")
            .where('news.deleted_at is null');

        // name
        if (options.name) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(NewsEntity, 'news')
                    .where('title like :name', {name: '%' + options.name + '%'})
                    .getQuery();
                return 'news.id IN ' + subQuery;
            });
        }

        // date search
        if (options.date) {
            // using sub query || nested query
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('id')
                    .from(NewsEntity, 'news')
                    .where('created_at BETWEEN :dateFrom AND :dateTo', {dateFrom: options.date_from + " 00:00:00", dateTo: options.date_to + " 23:59:59"})
                    .getQuery();
                return 'news.id IN ' + subQuery;
            });
        }

        let count_query = query.getCount();
        let data_query = query.orderBy('news.created_at', 'DESC')
                                .skip(start)
                                .take(query_limit)
                                .getMany();

        let results = await Promise.all([
            count_query,
            data_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        // pages quantity
        const pages = Math.ceil(results[0] / query_limit);

        return {
            pages: pages,
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }


    /**
     * the function get news detail
     * @param id 
     */
    async getDetail(id: number){
        const news = await this.newsRepository.createQueryBuilder('news')
            .leftJoinAndSelect('news.user', 'user')
            .where('news.id = :id', {id: id})
            .andWhere('news.deleted_at is null')
            .getOne();

        if (!news)
            throw {message: 'news is not exist'}
        return news;
    }

    // **
    //  * the function update news
    //  * @param body 
    //  * @param token_decoded (user_id, role, email)
    //  * @param image 
    //  */
    async update(id: number, body: UpdateNewsDto, token_decoded: any, image?: string){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.NEWS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        let news = await this.findNewsById(id);
        if (!news)
            throw {message: 'news is not exist'};

        news.title = body.title.trim();
        news.content = body.content.trim();

        if (image){
            news.image = image;
        }

        const data = await this.newsRepository.save(news);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Tin tức',
            'Cập nhật tin tức "' + news.title.substr(0,100) + '... ('+ news.id +')"',
        )

        return data;
    }

    // **
    //  * this function delete news
    //  * @param id 
    //  * @param token_decoded (user_id, role, email)
    //  */
    async delete(id: number, token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.NEWS});
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        // check exist
        let news = await this.findNewsById(id);
        if (!news)
            throw {message: 'news is not exist'}

        // delete role group
        news.deleted_at = new Date();
        await this.newsRepository.save(news);

        // save history
        await this.historyService.insertHistory(
            token_decoded.user_id,
            'Tin tức',
            'Xóa tin tức "' + news.title.substr(0,100) + '..."',
        )
    }

    /**
     * the function increase quantum click
     * @param id 
     */
    async increaseClick(id: number){
        let news = await this.findNewsById(id);
        if (!news)
            throw {message: 'news is not exist'}

        news.click = news.click + 1;

        await this.newsRepository.save(news);
    }

    /**
     * the function find news by id
     * @param id
     */
    async findNewsById(id: number){
        return this.newsRepository.createQueryBuilder('news')
            .where('deleted_at is null')
            .andWhere('news.id = :id', {id: id})
            .getOne();
    }

    /**
     * The function get top 3 newest
     */
    async getTopNew(){
        const data = this.newsRepository.createQueryBuilder('news')
        .where('news.deleted_at is null')
        .orderBy("news.created_at", "DESC")
        .skip(0)
        .take(3)
        .getMany();

        return data;
    }

}
