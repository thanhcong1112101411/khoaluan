import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { NewsEntity } from './news.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleGroupsModule } from '../role-groups/role-groups.module';
import { HistoryModule } from '../history/history.module';
import { UsersModule } from '../users/users.module';

@Module({
    imports: [
      TypeOrmModule.forFeature([NewsEntity]),
      RoleGroupsModule,
      HistoryModule,
      UsersModule,
    ] ,
    controllers: [NewsController],
    providers: [NewsService]
})
export class NewsModule {}
