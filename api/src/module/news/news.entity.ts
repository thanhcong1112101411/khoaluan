import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Exclude } from 'class-transformer';
import { UsersEntity } from '../users/users.entity';

@Entity('news')
export class NewsEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '1000',
    })
    title: string;

    @Column({
        type: 'text',
    })
    content: string;

    @Column({
        type: 'text',
    })
    image: string;

    @Column({
        type: 'int',
    })
    click: number;

    /*************************
    * Date time              *
    *************************/

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    // *************************
    //  * Key           *
    //  *************************/

    @ManyToOne(type => UsersEntity, user => user.news)
    user: UsersEntity;
}
