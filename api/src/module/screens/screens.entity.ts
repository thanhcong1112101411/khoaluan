import { Entity, Column, PrimaryGeneratedColumn, Index, Generated, Unique, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';

@Entity('screens')
export class ScreensEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    name: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    url: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    icon: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    type: number;

}
