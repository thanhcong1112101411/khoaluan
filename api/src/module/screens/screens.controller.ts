
import { Controller, Body, Post, Res, Put, Get, HttpStatus, Param, Req, UseGuards } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery, ApiBearerAuth } from '@nestjs/swagger';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesType } from '../../common/guards/roles.guards';
import { Response } from 'express';
import { dataSuccess, dataError, getPaginationData } from '../../utils/jsonFormat';
import { UserUtil } from '../../utils/user.util';
import { getRepository } from 'typeorm';
import { serialize } from 'v8';
import { AuthGuard } from '@nestjs/passport';
import * as jwt from 'jsonwebtoken';
import { ScreensService } from './screens.service';

@ApiUseTags('Screens')
@Controller('api/screens')
export class ScreensController {
    constructor(
        private readonly screensService: ScreensService,
    ) { }

    @Roles(RolesType.All)
    @Get()
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'get list screens - root | role group management' })
    async getList(@Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const token_decoded = jwt.decode(token);
        try {
            const result = await this.screensService.getAll(token_decoded);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
