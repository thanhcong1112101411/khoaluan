import { Module, forwardRef } from '@nestjs/common';
import { ScreensService } from './screens.service';
import { ScreensController } from './screens.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScreensEntity } from './screens.entity';
import { RoleGroupsModule } from '../role-groups/role-groups.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ScreensEntity]),
    forwardRef(() => RoleGroupsModule),
  ],
  providers: [ScreensService],
  controllers: [ScreensController],
  exports: [
      ScreensService,
  ],
})
export class ScreensModule {}
