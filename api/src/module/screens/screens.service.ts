import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ScreensEntity } from './screens.entity';
import { Repository } from 'typeorm';
import { RoleDefault, Screens } from '../../utils/enum.util';
import { RoleGroupsService } from '../role-groups/role-groups.service';

const _ = require('lodash');

@Injectable()
export class ScreensService {
    constructor(
        @Inject(forwardRef(() => RoleGroupsService))
        private roleGroupsService: RoleGroupsService,
        @InjectRepository(ScreensEntity)
        private readonly screensRepository: Repository<ScreensEntity>,
    ){}

    /**
     * function get screen information
     * @param screenId 
     */
    async getScreenInf(screenId: number){
        return this.screensRepository.findOne(screenId);
    }

    async getAllScreens(){
        return this.screensRepository.find();
    }

    /*
    * get all screens by token_decoded
    * @param token_decoded (user_id, role, email) 
    */
   async getAll(token_decoded: any){
        // check role
        if (token_decoded.role != RoleDefault.ROOT){
            const roleGroup = await this.roleGroupsService.getRoleGroupDetail(token_decoded.role);
            const index = _.findIndex(roleGroup.screens, function(o) { return o.id == Screens.ROLES; });
            if (index == -1){
                throw {message: 'Forbidden', status: 403};
            }
        }

        return this.getAllScreens();
    }
}
