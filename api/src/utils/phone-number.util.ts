const GLPN = require('google-libphonenumber');
import config from '../../config/config';

export class PhoneNumberUtil {

    // validate phone number
    static getCountryCodeAndNationalNumber(phone, country_code) {
        let phoneUtil = GLPN.PhoneNumberUtil.getInstance();
        let phone_number = '+' + country_code + '' + phone;

        // validate phone number
        if (!this.isValidPhoneNumber(phone_number)) {
            return false;
        }

        // get full phone number
        let full_phone_number = this.getPhoneNumber(phone_number);
        if (!full_phone_number) {
            return false;
        }

        try {
            let number = phoneUtil.parseAndKeepRawInput(phone_number);
            return {
                country_code: number.getCountryCode(),
                national_number: number.getNationalNumber(),
                full_phone_number,
            };
        } catch (e) {
            return false;
        }
    }

    // validate
    static isValidPhoneNumber(phone_number) {
        let phoneUtil = GLPN.PhoneNumberUtil.getInstance();
        try {
            let number = phoneUtil.parseAndKeepRawInput(phone_number);
            if (phoneUtil.isValidNumber(number)) {
                return true;
            }
            return false;
        } catch (ex) {
            return false;
        }
    }

    // convert format E164
    static getPhoneNumber(phone_number) {
        const PNF = GLPN.PhoneNumberFormat;
        let phoneUtil = GLPN.PhoneNumberUtil.getInstance();
        try {
            let number = phoneUtil.parseAndKeepRawInput(phone_number);
            return phoneUtil.format(number, PNF.E164);
        } catch (ex) {
            return false;
        }
    }

    // parse phone number
    static parsePhone(phone_number: string) {
        let phoneUtil = GLPN.PhoneNumberUtil.getInstance();
        try {
            return phoneUtil.parseAndKeepRawInput(phone_number);
        } catch (ex) {
            return false;
        }
    }
}
