import Redis from 'ioredis';

export class RedisService {
    private redis;
    private static instance: RedisService;

    constructor(options) {
        this.redis = new Redis({
            host: options.host,
            port: options.port,
            password: options.password,
        });
    }

    static getInstance(options) {
        if (!RedisService.instance) {
            RedisService.instance = new RedisService(options);
        }
        return RedisService.instance;
    }

    public async del(key) {
        return this.redis.del(key);
    }

    public async hgetall(key) {
        return this.redis.hgetall(key);
    }

    public async hmget(...args) {
        return this.redis.hmget(args);
    }

    public async hget(...args) {
        return this.redis.hget(args);
    }

    public async hmset(...args) {
        return this.redis.hmset(args);
    }

    public async setKeyToRedis(key, field, value, expTime) {
        await this.del(key);
        await this.redis.hmset(key, field, value);
        if (expTime) {
            await this.redis.expire(key, expTime);
        }
    }

    public async lpush(key, expTime, ...args) {
        await this.redis.lpush(key, args);
        if (expTime) {
            await this.redis.expire(key, expTime);
        }
    }

    public async lrange(key: string, start: number, stop: number) {
        return this.redis.lrange(key, start, stop);
    }
}
