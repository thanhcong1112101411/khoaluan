import { Injectable } from '@nestjs/common';
import { RedisService } from './services/redis.service';
import config from '../../config/config';

@Injectable()
export class RedisUtil {
    private static instance: RedisUtil;

    constructor() {

    }

    static getInstance() {
        if (!RedisUtil.instance) {
            RedisUtil.instance = new RedisUtil();
        }
        return RedisUtil.instance;
    }

    /*
    * @param {string} key - EX: user_cache
    * @param {string} field - EX: user_cache || <random token> 123456
    * @param {Object} value - EX: data {name: 'Admin', id: 1}
    * @param {number} expire_time
     */
    public async setInfoRedis(key, field, value, expire_time = null) {
        let result = {
            status: false,
            message: '',
        };
        const expTime = expire_time || config.redisConfig.redisExpireOtp;

        const redis = RedisService.getInstance(config.redisConfig.connect);
        try {
            await redis.setKeyToRedis(key, field, value, expTime);
            return {
                status: true,
                message: 'Success',
            };
        } catch (ex) {
            result.message = ex.message ? ex.message : ex;
            return result;
        }
    }

    /**
     * This function get info from redis cache
     * @param key
     * @param field
     * @param del
     */
    public async getInfoRedis(key, field, del = false) {
        let result = { isValidate: false, value: undefined };
        const redis = RedisService.getInstance(config.redisConfig.connect);
        let info = await redis.hgetall(key);

        if (del) {
            await redis.del(key);
        }

        if (info[field])
            result = { isValidate: true, value: info[field] };

        return result;
    }

    /*============Code Example==============*
     let redis = RedisUtil.getInstance();
        // get cache
        let getCache = await redis.getInfoRedis('key', 'field2');
        if (getCache.isValidate)
            return res.status(HttpStatus.OK).send(dataSuccess('oK', getCache.value));

        // {string} data
        await redis.setInfoRedis('key', 'field2', 'hehe haha');
        return res.status(HttpStatus.OK).send(dataSuccess('Save', null));
     ===========================================================================================*/
}
