import { classToClass, plainToClass, classToPlain } from 'class-transformer';
import * as jwt from 'jsonwebtoken';

export class UserUtil {

    /**
     * This is function custome response user
     * @param user 
     */
    static serialize(user) {
        user.uuid = user.uuid.toString();
        // user.created_at = new Date(user.created_at).getTime();
        return Object.assign({}, classToClass(user));
    }

    /**
     * This is function check user by role
     * @param roles 
     * @param token 
     */
    static checkUserByRole(roles: number[], token: string) {
        if (!token) return false;
        let token_decoded = jwt.decode(token);
        if (roles.indexOf(token_decoded['role']) > -1) return true;
        return false;
    }
}