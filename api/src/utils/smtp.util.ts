import config from '../../config/config';

const nodemailer = require('nodemailer');

export class SmtpUtil {
    private transporter;
    private static instance: SmtpUtil;

    constructor() {
        this.transporter = nodemailer.createTransport(config.emailSetting.mailGun);

    }

    static getInstance() {
        if (!SmtpUtil.instance)
            SmtpUtil.instance = new SmtpUtil();

        return SmtpUtil.instance;
    }

    /**
     * This function send mail for email's
     * @param receiver
     * @param subject
     * @param text
     * @param html
     */
    public async sendEmail(receiver: string[], subject: string, text: string, html?: string) {
        const msg = {
            to: receiver,
            from: config.emailSetting.name + ' <' + config.emailSetting.from + '>',
            subject,
            text,
            html,
        };
        return this.transporter.sendMail(msg);
    }

    /**
     * This function get payment email template
     * @param data
     */
    static paymentTemplate2(data: any) {
        return `<p>Your invoice,<br/>
               <p>Transaction: ${data.transaction_id}</p>
               <p>Customer: ${data.customer}</p>
               <p>Email: ${data.email}</p>
               <p>Amount: ${data.amount}</p>
               <p>Charge in month: ${data.time}</p>
               <p>The next time: ${data.next_time}</p>
               <p>Thank you,</p>`;
    }

    /**
     * This function get payment email template
     * @param data
     */
    static paymentTemplate(data: any) {
        return `<p>Your invoice,<br/>
               <p>Transaction: ${data.transaction_id}</p>
               <p>Customer: ${data.customer}</p>
               <p>Email: ${data.email}</p>
               <p>Amount: ${data.amount}</p>
               <p>Charge in month: ${data.time}</p>
               <p>The next time: ${data.next_time}</p>
               <p>Thank you,</p>`;
    }
}
