const md5 = require('md5');
export class StringHelper {
    public static generateOtpToken() {
        return md5(Math.random().toString(36).substring(2, 30));
    }

    public static generateOtpCode() {
        return Math.floor(1000 + (9999 - 1000) * Math.random()); // '1234';// Math.random().toString(36).substring(2, 30);
    }

    public static isEmail(email) {
        const emailRegex = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/g;
        return emailRegex.test(email);
    }

    public static encodeBase64(string) {
        return Buffer.from(string).toString('base64');
    }

    public static decodeBase64(string) {
        return Buffer.from(string, 'base64').toString('ascii');
    }

    public static getNumberInString(string) {
        const numberRegex = /[+-]?\d+(\.\d+)?/g;
        return string.match(numberRegex);
    }

    public static isEmpty(value) {
        value === undefined ||
            value === null ||
            (typeof value === 'object' && Object.keys(value).length === 0) ||
            (typeof value === 'string' && value.trim().length === 0);
    }

    public static randomImageName() {
        return Math.random().toString(36).substring(2, 30) + Math.floor(1000 + (9999 - 1000) * Math.random());
    }
}
