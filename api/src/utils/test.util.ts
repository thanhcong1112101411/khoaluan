import { Injectable } from '@nestjs/common';
import { DatabaseService } from './services/database.service';

@Injectable()
export class TestUtils {
    databaseService: DatabaseService;

	/**
	 * Creates an instance of TestUtils
	 */
    constructor(databaseService: DatabaseService) {
        this.databaseService = databaseService;
    }

	/**
	 * Returns the entites of the database
	 */
    private async getEntities() {
        const entities = [];
        (await (await this.databaseService.connection).entityMetadatas).forEach(x =>
            entities.push({ name: x.name, tableName: x.tableName }),
        );
        return entities;
    }

    async addEntities(entity, data) {
        const repository = await this.databaseService.getRepository(
            entity,
        );
        return await repository.save(data);
    }

	/**
	 * Cleans the database and reloads the entries
	 */
    async resetDb() {
        const entities = await this.getEntities();
        await this.cleanAll(entities);
    }

	/**
	 * Cleans all the entities
	 */
    private async cleanAll(entities) {
        try {
            for (const entity of entities) {
                const repository = await this.databaseService.getRepository(
                    entity.name,
                );
                await repository.query(`SET FOREIGN_KEY_CHECKS=0;`);
            }
            for (const entity of entities) {
                const repository = await this.databaseService.getRepository(
                    entity.name,
                );
                await repository.query(`DELETE FROM ${entity.tableName};`);
                await repository.query(`ALTER TABLE ${entity.tableName} AUTO_INCREMENT = 1`);
            }
        } catch (error) {
            throw new Error(`ERROR: Cleaning test db: ${error}`);
        }
    }
}