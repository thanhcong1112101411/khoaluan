import config from '../../config/config';

export class EmailUtil {
    private transporter;
    private static instance: EmailUtil;

    constructor() {
        this.transporter = require('@sendgrid/mail');
        this.transporter.setApiKey(config.emailSetting.SENDGRID_API_KEY);
    }

    static getInstance() {
        if (!EmailUtil.instance)
            EmailUtil.instance = new EmailUtil();

        return EmailUtil.instance;
    }

    /**
     * This function send mail for email's
     * @param receiver
     * @param subject
     * @param text
     * @param html
     */
    public async sendEmail(receiver: string[], subject: string, text: string, html: string) {
        const msg = {
            to: receiver,
            from: config.emailSetting.name + ' <' + config.emailSetting.from + '>',
            subject,
            text,
            html,
        };
        return this.transporter.send(msg);
    }
}
