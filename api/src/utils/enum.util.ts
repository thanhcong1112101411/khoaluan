export enum UserStatus {
    ACTIVE = 1,
    INACTIVE = 2,
}
/*----------UserType-----------*/
export enum UserType {
    ADMIN = 1,
    USER = 2,
}

/*---------- Screen type ---------*/
export enum ScreenType {
    OTHER = 1,
    NEWSMANAGEMENT = 2,
    PRODUCTMANAGEMENT = 3,
    ENVOICEMANAGEMENT = 4,
    ACCOUNTMANAGEMENT = 5,
    ROLES = 6,
    REPORTS = 7,
    INTERFACES = 8
}

/*---------- screens ---------*/
export enum Screens{
    HISTORY = 1,
    FEEDBACKS = 2,
    NEWS = 3,
    PRODUCTS = 4,
    PRICES = 5,
    DISCOUNTS = 6,
    UNITS = 7,
    BRANDS = 8,
    CATAGORIES = 9,
    EXPORT_ENVOICE = 10,
    USER = 11,
    ACCOUNTS = 12,
    DECENTRALIZATION = 13,
    ROLES = 14,
    RECOMEND_SYSTEM = 15,
    REVENUE = 16,
    PUBLIC = 17,
    CONTENTS = 18,
    IMAGES = 19,
}
/*---------- RoleDefault ----------*/
export enum RoleDefault{
    ROOT = 1,
    USER = 2,
    NONE = 3,
}
/*---------- AccountDefault -------*/
export const AccountDefault = 1;
/*----------- social type -------*/
export enum SocialType {
    FB = 1,
    GG = 2,
}
/*------------Reset passowrd--------------*/
export enum MailStatus {
    FORGOT_PASSWORD = 1,
    RESEND_EMAIL = 2,
}
/*------------ Images Type -----------*/
export enum Imagetype {
    BANNER = 1,
    CONTENT_TOP = 2,
    LOGO = 3,
}
/*----------- GenderType -------------*/
export enum GenderType {
    MALE = 1,
    FEMALE = 2,
    OTHER = 3,
}

/*----------- PriceType -------------*/
export enum PriceType {
    IMPORT = 1, // gia nhap
    EXPORT = 2, // gia ban
}

/*----------- ProductStatus -------------*/
export enum ProductStatus {
    ACTIVE = 1,
    INACTIVE = 2,
}

/*----------- ProductType -------------*/
export enum ProductType {
    ACTIVE = 1,
    INACTIVE = 2,
    WATING_PRICE = 3,
}

/*----------- DiscountType -------------*/
export enum DiscountType {
    APPLING = 1,
    APLLIED = 2,
    WATING = 3,
    DELETED = 4,
}

/*----------- EnvoiceStatus -------------*/
export enum EnvoiceStatus{
    WATING = 1,
    COMPLETE = 2,
    CANCEL = 3,
}
/*----------- EnvoiceType -------------*/
export enum EnvoiceType{
    USER_ORDER = 1,
    ADMIN_ADD = 2,
}