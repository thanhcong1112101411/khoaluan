import { validateOrReject } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { getValidationError } from './jsonFormat';

export class ValidatorUtil {

    static async validateDTO(dto, body) {
        let errors;

        let loginDto = plainToClass(dto, body);
        try {
            await validateOrReject(loginDto);
        } catch (e) {
            errors = getValidationError(e);
        }
        // Convernt access properties 
        const data = Object['values'](errors[0]);
        const value = Object['values'](data[0]);
        return value[0];
    }
}
