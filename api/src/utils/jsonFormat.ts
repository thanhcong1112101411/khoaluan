/**
 * This function format data json
 * @param message
 * @param data
 */
export function dataSuccess(message, data) {
    return {
        status: true,
        message,
        data,
    };
}

/**
 * This function format data json
 * @param message
 * @param data
 * @param result_code
 */
export function dataError(message, data, result_code?: number) {
    return {
        status: false,
        result_code,
        message,
        data,
    };
}

/**
 * This function is used to return result with pagination
 * @param {String} message
 * @param {Array} data
 * @param {Number} count
 * @param {Boolean} load_more
 * @returns Object
 */
export function getPaginationData(message, data, count, load_more, pages?) {
    let result = dataSuccess(message, null);
    // Format for pagination
    result.data = {
        count,
        results: data,
        load_more,
        pages,
    };

    return result;
}

/**
 * This function is used to return pagination results list with current_page, next_page and total_page
 * @param {String} message
 * @param {Array} data
 * @param {Number} count
 * @param {Object} options
 * Example options: {
 *     current_page: 1
 *     next_page: 2
 *     total_page: 5
 * }
 * @returns Object
 */
export function getPaginationDataWithLimitAndOffset(message, data, count, options) {
    let result = dataSuccess(message, null);
    result.data = {
        count,
        results: data,
        current_page: options.current_page,
        next_page: options.next_page,
        total_page: options.total_page,
    };

    return result;
}

/**
 * This function is used to parse ValidationError with {property: constraints}
 * @param {Array} errors
 * @returns Array
 */
export function getValidationError(errors) {
    if (!errors) {
        return [];
    }

    if (errors && !errors.length) {
        return [];
    }

    let result = [];
    for (let err of errors) {
        if (err.property && err.constraints) {
            let item = {};
            item[err.property] = err.constraints;
            result.push(item);
        } else if (err.property && err.children) {
            let item = {};
            item[err.property] = getValidationError(err.children);
            result.push(item);
        }
    }

    return result;
}
