import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import {NestExpressApplication} from '@nestjs/platform-express';
import config from '../config/config';
import { join } from 'path';
const bodyParser = require('body-parser');
import * as express from 'express';

async function bootstrap() {
    
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.enableCors();

    const options = new DocumentBuilder()
        .setTitle('API Document')
        .setDescription('API description')
        .setVersion('1.0')
        .addTag('cats')
        .addBearerAuth()
        .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('document', app, document);

    // /* Accept files static*/
    // app.useStaticAssets(join(__dirname, '..', 'public'));

    /* Use bodyParser */
    app.use(bodyParser.urlencoded({
        extended: true,
    }));
    //app.use(express.static('public'));

    app.use('/public', express.static(join(__dirname, '..', 'public')));
    
    await app.listen(config.mainPort);
}
bootstrap();
