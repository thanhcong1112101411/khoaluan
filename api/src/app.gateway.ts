import { SubscribeMessage, WebSocketGateway, OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect, WebSocketServer, WsResponse } from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';

@WebSocketGateway()
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server: Server;

    private logger: Logger = new Logger('AppGateway');

    afterInit(server: Server) {
        this.logger.log('Initialized!');
    }

    handleDisconnect(client: Socket) {
        this.logger.log(`Client disconnected: ${client.id}`);
    }
    handleConnection(client: Socket, ...args: any[]) {
        this.logger.log(`Client connected: ${client.id}`);
    }

    @SubscribeMessage('client-send-fetch-account')
    fetchAccount(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-account', text);
    }

    @SubscribeMessage('client-send-fetch-user')
    fetchUser(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-user', text);
    }

    @SubscribeMessage('client-send-fetch-role-group')
    fetchRoleGroup(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-role-group', text);
    }

    @SubscribeMessage('client-send-change-role-group')
    changeRoleGroup(client: Socket, text: string): void{
        this.server.emit('server-send-change-role-group', text);
    }

    @SubscribeMessage('client-send-fetch-images')
    fetchImages(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-images', text);
    }

    @SubscribeMessage('client-send-fetch-feedbacks')
    fetchFeedbacks(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-feedbacks', text);
    }

    // --------------------------- Catagories ----------------------------
    @SubscribeMessage('client-send-fetch-catagories')
    fetchCatagories(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-catagories', text);
    }

    // --------------------------- Brands ----------------------------
    @SubscribeMessage('client-send-fetch-brands')
    fetchBrands(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-brands', text);
    }

    // --------------------------- Units ----------------------------
    @SubscribeMessage('client-send-fetch-units')
    fetchUnits(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-units', text);
    }

    // --------------------------- News ----------------------------
    @SubscribeMessage('client-send-fetch-news')
    fetchNews(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-news', text);
    }

    // --------------------------- News ----------------------------
    @SubscribeMessage('client-send-fetch-export-envoices')
    fetchExportEnvoice(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-export-envoices', text);
    }

    // --------------------------- News ----------------------------
    @SubscribeMessage('client-send-fetch-products')
    fetchProducts(client: Socket, text: string): void{
        this.server.emit('server-send-fetch-products', text);
    }
}
