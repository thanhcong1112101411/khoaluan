import { PipeTransform, HttpException } from '@nestjs/common';
import { validateOrReject } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { getValidationError } from '../../utils/jsonFormat';

export class CustomValidationPipe extends Error implements PipeTransform {

    async transform(dto: any, body: any): Promise<any> {
        const check = await this.validateDTO(dto, body.metatype);
        if (check) throw new HttpException({
            status: false,
            message: check,
        }, 400);
        return dto;
    }

    private async validateDTO(dto, body): Promise<string> {
        let errors;

        let loginDto = plainToClass(body, dto);
        try {
            await validateOrReject(loginDto);
        } catch (e) {
            errors = getValidationError(e);
            const data = await Object.values(errors[0]);
            const value = await Object.values(data[0]);
            return value[0];
        }
    }
}