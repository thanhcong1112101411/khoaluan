import { Injectable, CanActivate, ExecutionContext, HttpService } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import config from '../../../config/config';
import { HttpStatus } from '@nestjs/common';
import { UsersService } from '../../module/users/users.service';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        private readonly usersService: UsersService,
    ) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        // Get allowed roles from decorators
        const allowedRoles = this.reflector.get<number[]>('roles', context.getHandler());
        // If controller contains roles type All, pass
        if (!allowedRoles) return false;
        if (allowedRoles.indexOf(RolesType.All) > -1) return true;

        return true;
        // Parse context to HTTP request to get authorize token from header
        const request = context.switchToHttp().getRequest();
        if (!request.headers.authorization) return false;
        const token = request.headers.authorization.split(' ')[1];

        // decode token
        let token_decoded = jwt.decode(token);
        return true;
    }
}

export enum RolesType {
    All = 0,
    Role1 = 1,
    Role2 = 2,
}
