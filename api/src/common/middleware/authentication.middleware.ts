import { Injectable, NestMiddleware } from '@nestjs/common';
import { dataError } from '../../utils/jsonFormat';
import { Reflector } from '@nestjs/core';
import { Request, Response } from 'express';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {

    constructor(private readonly reflector: Reflector) { }

    use(req: Request, res: Response, next: () => void) {
        let str = req.headers.authorization;
        console.log(str);

        if (!str) {
            return res.status(401).send(dataError('Unauthorized', null));
        }

        return next();
    }

    // resolve() { }
}
