import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { Imagetype } from '../utils/enum.util';

export class ImagesDataDefault1587446441300 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('images').save(
            [
                // logo
                {
                    title: 'ollafoods',
                    link: 'public/assets/images/1587281459866.jpg',
                    type: Imagetype.LOGO
                },
                // banner
                {
                    title: 'ollafoods',
                    link: 'public/assets/images/1587271034438.jpg',
                    type: Imagetype.BANNER
                },
                {
                    title: 'ollafoods',
                    link: 'public/assets/images/1587271135215.jpg',
                    type: Imagetype.BANNER
                },
                // content top
                {
                    title: 'ollafoods',
                    link: 'public/assets/images/1587281495795.jpg',
                    type: Imagetype.CONTENT_TOP,
                },
                {
                    title: 'ollafoods',
                    link: 'public/assets/images/1587281521505.jpg',
                    type: Imagetype.CONTENT_TOP
                },
            ],
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE * FROM `images`');
    }

}
