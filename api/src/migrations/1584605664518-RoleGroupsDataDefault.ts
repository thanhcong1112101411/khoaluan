import { MigrationInterface, QueryRunner, getRepository, TableForeignKey } from 'typeorm';

export class RoleGroupsDataDefault1584605664518 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('role_groups').save(
            [
                {
                    id: 1,
                    name: 'root',
                    create_at: '2019-11-18 03:46:04',
                },
                {
                    id: 2,
                    name: 'user',
                    create_at: '2020-03-19 03:46:04',
                },
                {
                    id: 3,
                    name: 'none',
                    create_at: '2019-11-18 03:46:04',
                }
            ],
        );
        await queryRunner.createForeignKey("users", new TableForeignKey({
            columnNames: ["roleId"],
            referencedColumnNames: ["id"],
            referencedTableName: "role_groups",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE * FROM `role_groups`');
    }

}
