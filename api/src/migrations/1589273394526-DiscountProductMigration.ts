import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class DiscountProductMigration1589273394526 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'discounts_products_products',
            columns: [
                {
                    name: 'discountsId',
                    type: 'int',
                    isPrimary: true,
                },
                {
                    name: 'productsId',
                    type: 'int',
                    isPrimary: true,
                },
            ]
        }));
        
        await queryRunner.createForeignKey("discounts_products_products", new TableForeignKey({
            columnNames: ["discountsId"],
            referencedColumnNames: ["id"],
            referencedTableName: "discounts",
            onDelete: "CASCADE"
        }));
        await queryRunner.createForeignKey("discounts_products_products", new TableForeignKey({
            columnNames: ["productsId"],
            referencedColumnNames: ["id"],
            referencedTableName: "products",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
