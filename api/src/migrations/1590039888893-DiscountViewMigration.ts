import {MigrationInterface, QueryRunner, Connection} from "typeorm";
import { View } from "typeorm/schema-builder/view/View";

export class DiscountViewMigration1590039888893 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createView(new View({
            name: 'discounts_view',
            expression: (connection: Connection) => connection.createQueryBuilder()
            .select("*")
            .from('discounts', 'discount')
            .where("CURRENT_TIMESTAMP() >= discount.date_from and CURRENT_TIMESTAMP() <= discount.date_to")
            .andWhere("discount.deleted_at is null")
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
