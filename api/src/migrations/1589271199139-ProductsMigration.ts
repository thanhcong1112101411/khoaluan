import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class ProductsMigration1589271199139 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'products',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'unitId',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'brandId',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'catagoryId',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'code',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '1000',
                    isNullable: true,
                },
                {
                    name: 'images',
                    type: 'text',
                    isNullable: true,
                },
                {
                    name: 'quantity',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'min',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'rrp',
                    type: 'double',
                    isNullable: true,
                    default: 0,
                },
                {
                    name: 'description_first',
                    type: 'text',
                    isNullable: true,
                },
                {
                    name: 'description_second',
                    type: 'text',
                    isNullable: true,
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ]
        }))

        await queryRunner.createForeignKey("products", new TableForeignKey({
            columnNames: ["unitId"],
            referencedColumnNames: ["id"],
            referencedTableName: "units",
            onDelete: "CASCADE"
        }));
        await queryRunner.createForeignKey("products", new TableForeignKey({
            columnNames: ["brandId"],
            referencedColumnNames: ["id"],
            referencedTableName: "brands",
            onDelete: "CASCADE"
        }));
        await queryRunner.createForeignKey("products", new TableForeignKey({
            columnNames: ["catagoryId"],
            referencedColumnNames: ["id"],
            referencedTableName: "catagories",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
