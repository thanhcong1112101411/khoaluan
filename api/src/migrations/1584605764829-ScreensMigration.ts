import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class ScreensMigration1584605764829 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'screens',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'url',
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'icon',
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'type', // 1: account ; 2: management; 3: interface; 4: reports; 5: other
                    type: 'smallint',
                    isNullable: false,
                },
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('screens');
    }

}
