import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';
import { ScreenType, Screens } from '../utils/enum.util';

export class ScreensDataDefault1584605808162 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('screens').save(
            [
                // other
                {
                    id: Screens.HISTORY,
                    name: 'lịch sử',
                    url: 'history',
                    icon: 'faHistory',
                    type: ScreenType.OTHER,
                },
                {
                    id: Screens.FEEDBACKS,
                    name: 'phản hồi',
                    url: 'feedbacks',
                    icon: 'faComment',
                    type: ScreenType.OTHER,
                },

                // news management
                {
                    id: Screens.NEWS,
                    name: 'quản lý tin tức',
                    url: 'news',
                    icon: 'faNewspaper',
                    type: ScreenType.NEWSMANAGEMENT,
                },

                // product management
                {
                    id: Screens.PRODUCTS,
                    name: 'Quản lý sản phẩm',
                    url: 'products',
                    icon: 'faCube',
                    type: ScreenType.PRODUCTMANAGEMENT,
                },
                {
                    id: Screens.PRICES,
                    name: 'Quản lý giá',
                    url: 'prices',
                    icon: 'faDollarSign',
                    type: ScreenType.PRODUCTMANAGEMENT,
                },
                {
                    id: Screens.DISCOUNTS,
                    name: 'Quản lý giảm giá',
                    url: 'discounts',
                    icon: 'faMoneyCheckAlt',
                    type: ScreenType.PRODUCTMANAGEMENT,
                },
                {
                    id: Screens.UNITS,
                    name: 'đơn vị tính',
                    url: 'units',
                    icon: 'faBookmark',
                    type: ScreenType.PRODUCTMANAGEMENT,
                },
                {
                    id: Screens.BRANDS,
                    name: 'Thương hiệu',
                    url: 'brands',
                    icon: 'faBroom',
                    type: ScreenType.PRODUCTMANAGEMENT,
                },
                {
                    id: Screens.CATAGORIES,
                    name: 'Danh mục',
                    url: 'catagories',
                    icon: 'faClipboardList',
                    type: ScreenType.PRODUCTMANAGEMENT,
                },

                // envoice management
                {
                    id: Screens.EXPORT_ENVOICE,
                    name: 'hóa đơn bán',
                    url: 'export_envoice',
                    icon: 'faMoneyBillWave',
                    type: ScreenType.ENVOICEMANAGEMENT,
                },

                // account management
                {
                    id: Screens.USER,
                    name: 'Khách hàng',
                    url: 'users',
                    icon: 'faUsers',
                    type: ScreenType.ACCOUNTMANAGEMENT,
                },
                {
                    id: Screens.ACCOUNTS,
                    name: 'Quản trị viên',
                    url: 'accounts',
                    icon: 'faUserTie',
                    type: ScreenType.ACCOUNTMANAGEMENT,
                },
                // roles
                {
                    id: Screens.DECENTRALIZATION,
                    name: 'phân quyền',
                    url: 'decentralization',
                    icon: 'faPenAlt',
                    type: ScreenType.ROLES,
                },
                {
                    id: Screens.ROLES,
                    name: 'nhóm quyền',
                    url: 'roles',
                    icon: 'faUserClock',
                    type: ScreenType.ROLES,
                },

                // reports
                {
                    id: Screens.RECOMEND_SYSTEM,
                    name: 'Hệ thống gợi ý',
                    url: 'recomend-system',
                    icon: 'faChartLine',
                    type: ScreenType.REPORTS,
                },
                {
                    id: Screens.REVENUE,
                    name: 'Doanh thu',
                    url: 'revenue',
                    icon: 'faDollarSign',
                    type: ScreenType.REPORTS,
                },
                {
                    id: Screens.PUBLIC,
                    name: 'Thống kê chung',
                    url: 'public_reports',
                    icon: 'faChartPie',
                    type: ScreenType.REPORTS,
                },

                // interface
                {
                    id: Screens.CONTENTS,
                    name: 'Nội dung',
                    url: 'contents',
                    icon: 'faHouseDamage',
                    type: ScreenType.INTERFACES,
                },
                {
                    id: Screens.IMAGES,
                    name: 'Hình ảnh',
                    url: 'images',
                    icon: 'faImages',
                    type: ScreenType.INTERFACES,
                },

            ],
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE * FROM `screens`');
    }

}
