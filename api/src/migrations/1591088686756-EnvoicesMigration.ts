import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class EnvoicesMigration1591088686756 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'envoices',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'userId',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'code',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'money',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'utility',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'note',
                    type: 'text',
                    isNullable: true,
                },
                {
                    name: 'isDiscount',
                    type: 'boolean',
                    default: false,
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ]
        }))
        await queryRunner.createForeignKey("envoices", new TableForeignKey({
            columnNames: ["userId"],
            referencedColumnNames: ["id"],
            referencedTableName: "users",
            onDelete: "CASCADE"
        }));
    }


    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
