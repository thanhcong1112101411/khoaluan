import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class TypeormMetadataMigration1590039829612 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'typeorm_metadata',
            columns: [
                {
                    name: 'type',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'schema',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'value',
                    type: 'text',
                    isNullable: true,
                },
                
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}

