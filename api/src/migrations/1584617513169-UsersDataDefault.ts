import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';

export class UsersDataDefault1584617513169 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('users').save(
            [
                {
                    id: 1,
                    uuid: '5fdc191c-cc39-477b-b813-1d454c08f04c',
                    first_name: 'John',
                    last_name: 'Doe',
                    address: 'Tan Binh, HCM city',
                    email: 'admin@gmail.com',
                    country_code: '84',
                    phone: '975716502',
                    password: '$2a$08$G99bKZbBj0xpnnLYroccHObRE.gSxy3P7FYQMRWUHXWBsSx3QQ6.G',
                    status: 1,
                    gender: 1,
                    roleId: 1,
                    created_at: '2019-11-18 03:46:04',
                },
            ],
        );
        
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE * FROM `users`');
    }

}
