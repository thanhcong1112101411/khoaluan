import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class RoleGroupScreenMigration1584625919892 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'role_groups_screens_screens',
            columns: [
                {
                    name: 'roleGroupsId',
                    type: 'int',
                    isPrimary: true,
                },
                {
                    name: 'screensId',
                    type: 'int',
                    isPrimary: true,
                },
            ]
        }));
        await queryRunner.createForeignKey("role_groups_screens_screens", new TableForeignKey({
            columnNames: ["roleGroupsId"],
            referencedColumnNames: ["id"],
            referencedTableName: "role_groups",
            onDelete: "CASCADE"
        }));
        await queryRunner.createForeignKey("role_groups_screens_screens", new TableForeignKey({
            columnNames: ["screensId"],
            referencedColumnNames: ["id"],
            referencedTableName: "screens",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
