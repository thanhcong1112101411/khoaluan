import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class PricesMigration1589382778216 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'prices',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isGenerated: true,
                    isPrimary: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'productId',
                    type: 'int',
                    isPrimary: true,
                    isNullable: true,
                },
                {
                    name: 'date_from',
                    type: 'timestamp',
                    isPrimary: true,
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'type',
                    type: 'smallint',
                    isPrimary: true,
                    isNullable: true,
                    
                },
                {
                    name: 'price',
                    type: 'double',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ]
        }))

        await queryRunner.createForeignKey("prices", new TableForeignKey({
            columnNames: ["productId"],
            referencedColumnNames: ["id"],
            referencedTableName: "products",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('prices');
    }

}
