import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey} from 'typeorm';

export class UsersMigration1571986429582 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'users',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'roleId',
                    type: 'int',
                    isNullable: true,
                    default: 1,
                },
                {
                    name: 'uuid',
                    type: 'BINARY(36)',
                    default: null,
                },
                {
                    name: 'avatar',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'first_name',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'last_name',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'email',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'password',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'phone',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'country_code',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'bod',
                    type: 'date',
                    isNullable: true,
                },
                {
                    name: 'gender',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'fb_id',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'gg_id',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'address',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'ward_id',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'district_id',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'city_id',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: true,
                },

            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('users');
    }

}
