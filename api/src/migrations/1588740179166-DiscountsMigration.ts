import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class DiscountsMigration1588740179166 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'discounts',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'amount',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'date_from',
                    type: 'timestamp',
                    isNullable: true,
                },
                {
                    name: 'date_to',
                    type: 'timestamp',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ]
        }))

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('discounts');
    }

}
