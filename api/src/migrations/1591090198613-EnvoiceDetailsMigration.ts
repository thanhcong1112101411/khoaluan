import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class EnvoiceDetailsMigration1591090198613 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'envoice_details',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'envoiceId',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'productId',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'amount',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'exportPrice',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'discount',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'money',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'utility',
                    type: 'float',
                    isNullable: true,
                },
                
            ]
        }))
        await queryRunner.createForeignKey("envoice_details", new TableForeignKey({
            columnNames: ["envoiceId"],
            referencedColumnNames: ["id"],
            referencedTableName: "envoices",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("envoice_details", new TableForeignKey({
            columnNames: ["productId"],
            referencedColumnNames: ["id"],
            referencedTableName: "products",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
