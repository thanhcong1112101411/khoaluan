import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './common/guards/roles.guards';
import { UsersModule } from './module/users/users.module';
import { UsersController } from './module/users/users.controller';
import { AuthModule } from './module/auth/auth.module';
import { AuthenticationMiddleware } from './common/middleware/authentication.middleware';
import config from '../config/config';
import { RoleGroupsModule } from './module/role-groups/role-groups.module';
import { ScreensModule } from './module/screens/screens.module';
import { ResetPasswordModule } from './module/reset-password/reset-password.module';
import { HistoryModule } from './module/history/history.module';
import { ImagesModule } from './module/images/images.module';
import { ContentPagesModule } from './module/content-pages/content-pages.module';
import { GeneralApiModule } from './module/general-api/general-api.module';
import { AppGateway } from './app.gateway';
import { FeedbacksModule } from './module/feedbacks/feedbacks.module';
import { ReportsModule } from './module/reports/reports.module';
import { HuiModule } from './module/hui/hui.module';
import { CatagoriesModule } from './module/catagories/catagories.module';
import { BrandsModule } from './module/brands/brands.module';
import { UnitsModule } from './module/units/units.module';
import { DiscountsModule } from './module/discounts/discounts.module';
import { NewsModule } from './module/news/news.module';
import { ProductsModule } from './module/products/products.module';
import { PricesModule } from './module/prices/prices.module';
import { EnvoicesModule } from './module/envoices/envoices.module';
import { EnvoiceDetailsModule } from './module/envoice-details/envoice-details.module';
import { RecomenderSystemModule } from './module/recomender-system/recomender-system.module';
import { UploadImagesModule } from './module/upload-images/upload-images.module';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql' as 'mysql',
            host: config.env.host,
            port: config.env.port,
            username: config.env.username,
            password: config.env.password,
            database: config.env.database,
            entities: [ __dirname + '../../dist/src/module/**/*.entity{.ts,.js}',],
            subscribers: [],
            synchronize: false,
            // logging: true,
        }),
        // import module
        AuthModule,
        UsersModule,
        RoleGroupsModule,
        ScreensModule,
        ResetPasswordModule,
        HistoryModule,
        ImagesModule,
        ContentPagesModule,
        GeneralApiModule,
        FeedbacksModule,
        ReportsModule,
        HuiModule,
        CatagoriesModule,
        BrandsModule,
        UnitsModule,
        DiscountsModule,
        NewsModule,
        ProductsModule,
        PricesModule,
        EnvoicesModule,
        EnvoiceDetailsModule,
        RecomenderSystemModule,
        UploadImagesModule,
    ],
    controllers: [AppController, UsersController,
    ],
    providers: [{
        provide: APP_GUARD,
        useClass: RolesGuard,
    }, AppGateway],
})

export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuthenticationMiddleware)
            .forRoutes(
                { path: '/api/auth/user', method: RequestMethod.ALL },
            );
    }
}
